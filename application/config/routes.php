<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'error/error_404';
$route['cms'] = 'cms/cms';
$route['create-new-page'] = 'cms/newPage';
$route['all-pages'] = 'cms/allPages';
$route['update-page/:num'] = 'cms/updatePage';
$route['update-main-page/:num'] = 'cms/updateMainPage';
$route['deletePage/:num'] = 'cms/deletePage';
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['dashboard'] = 'dashboard/dashboard';
$route['home'] = 'home';
$route['homePage'] = 'home/homePage';
$route['how-it-works'] = 'home/howItWork';
$route['pricings'] = 'home/pricing';
$route['jobs'] = 'home/jobCategory';
$route['about-us'] = 'home/aboutUs';
$route['brand-assests'] = 'home/brandAssests';
$route['get-hired'] = 'home/getHire';
$route['confirmation/:num'] = 'welcome/activation';
$route['employee-confirmation/:num'] = 'welcome/employeeConfirmation';
$route['terms-of-service'] = 'welcome/termsService';
$route['pages/(:any)'] = 'welcome/loadPage';


/* End of file routes.php */
/* Location: ./application/config/routes.php */