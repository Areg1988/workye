<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Agents extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->client) {
        } elseif ($this->user) {
        } else {
            redirect('login');
        }
    }
    public function index()
    {
        $user = User::all();
        $this->view_data['users'] = $user;
        $this->content_view = 'agent/all';
    }
}