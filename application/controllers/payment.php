<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $access = FALSE;
        if ($this->user) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "payment") {
                    $access = TRUE;
                }
            }
            if (!$access) {
                redirect('login');
            }
        } elseif ($this->client) {
            redirect('login');
        } else {
            redirect('login');
        }

    }
    public function index() {
        $data2 = Setting::first();
        $employes = Card::find(array('conditions' => array('employes_id =?', $this->user->id)));
        $this->view_data['employes'] = $employes;
        $this->view_data['public_key'] = $data2->stripe_key;
        $this->content_view = 'payment/payment';
    }

    public function create() {
        $data['core_settings'] = Setting::first();
        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];

        \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);

        $employes = Card::find(array('conditions' => array('employes_id =?', $this->user->id)));

        if(!$employes) {
            $customer = \Stripe\Customer::create([
                "description" => $this->user->email,

            ]);
            $data = [
                'employes_id' => $this->user->id,
                'card_id' =>$customer->id,
            ];
            $employes = Card::create($data);
        }
        // Use your test API key (switch to the live key later)
        if (isset($_POST['stripeToken'])){
            try {
                $cu = \Stripe\Customer::retrieve($employes->card_id); // stored in your application
                $cu->source = $_POST['stripeToken']; // obtained with Checkout
                $cu->save();
                $success = "Your card details have been updated!";
                $data2 = [
                    'last'=> $cu->sources->data[0]->last4,
                    'brand' => $cu->sources->data[0]->brand
                ];
                $employes->update_attributes($data2);
            }
            catch(\Stripe\Error\Card $e) {
                // Use the variable $error to save any errors
                // To be displayed to the customer later in the page
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $error = $err['message'];
            }
            // Add additional error handling here as needed
        }
        redirect('payment');
    }
}