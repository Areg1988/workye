<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $access = true;
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));


    }

    public function index()
    {

        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
            ];

            $this->load->view('blueline/pages/home', $data);
        } else {
            $data = [
                'userAuth' => 'false'
            ];

            $this->load->view('blueline/pages/home', $data);
        }
    }

    public function homePage()
    {

        $category = Categorie::all();
        $this->view_data['category'] =   $category;
        $this->content_view = '/pages/home3';
//    die;
//        if ($this->userAuth) {
//            $data = [
//                'userData' => $this->userData,
//                'userAuth' => $this->userAuth,
//            ];
//
//            $this->content_view = '/pages/home3';
//        } else {
//            $data = [
//                'userAuth' => 'false'
//            ];
//
//            $this->content_view = '/pages/home3';
//        }
    }

    public function howItWork()
    {
        $category = Categorie::all();
        $this->view_data['category'] =   $category;
        $this->content_view = '/pages/howItWorks';
    }

    public function pricing()
    {
        $category = Categorie::all();
        $this->view_data['category'] =   $category;

        if ($this->user or $this->client ) {
//            $category = Categorie::all();
//            $SubCategories = SubCategories::all();
//            $companyId = $this->userData->company->id;
//            $subscription = Subscription::find(array('conditions' => array('company_id =?', $companyId)));
//            $subCategory = SubCategories::find_by_id($subscription->sub_category);
//            $categoryClient = Categorie::find_by_id($subscription->category);
//            $pricingList = PricingList::find_by_id($subscription->subscription_id);
//            $userData2 = [
//                'subscription' => $subCategory->name,
//                'category' => $categoryClient->name,
//                'pricingList' => $pricingList->name,
//                'pricingType' => $subscription->paymant_type,
//            ];
//            if ($subscription->paymant_type == 'monthly') {
//                $userData2['sum'] = $subscription->sum;
//            } else {
//                $userData2['sum'] = $subscription->sum / 12;
//            }
//            $data = [
//                'userData' => $this->userData,
//                'userAuth' => $this->userAuth,
//                'pricingPlans' => PricingList::all(),
//                'firstPlans' => PricingList::find(1),
//                'categories' => $category,
//                'SubCategories' => $SubCategories,
//                'pricing' => true,
//                'pricingPLan' => $userData2
//            ];
//
//
//
//            $this->view_data['pricingPLan'] =   $userData2;
//            $this->view_data['userData'] =  $data['userData'];
//            $this->view_data['userAuth'] =  $data['userAuth'];
//            $this->view_data['pricingPlans'] =  $data['pricingPlans'];
//            $this->view_data['firstPlans'] =  $data['firstPlans'];
//            $this->view_data['categories'] =  $data['categories'];
//            $this->view_data['SubCategories'] =  $data['SubCategories'];
//            $this->view_data['pricing'] =  true;
            $data = [
                'userAuth' => 'false',

                'pricingPlans' => PricingList::all(),
                'firstPlans' => PricingList::find(1),
                'pricing' => true
            ];

            $this->view_data['firstPlans'] =  $data['firstPlans'];
            $this->view_data['pricingPlans'] =  $data['pricingPlans'];
            $this->content_view = '/pages/pricing';


        } else {
            $data = [
                'userAuth' => 'false',

                'pricingPlans' => PricingList::all(),
                'firstPlans' => PricingList::find(1),
                'pricing' => true
            ];

            $this->view_data['firstPlans'] =  $data['firstPlans'];
            $this->view_data['pricingPlans'] =  $data['pricingPlans'];
            $this->view_data['pricing'] =  true;
            $this->content_view = '/pages/pricing';
        }

    }

    public function getPricePlan()
    {

        $id = $_POST['id'];
        $data = PricingList::find($id);
        $res = [
            'id' => $data->id,
            'name' => $data->name,
            'starter' => $data->starter,
            'standard' => $data->standard,
            'professional' => $data->professional,
            'enterprise' => $data->enterprise,
        ];
        print_r(json_encode($res));
    }

    public function jobCategory()
    {
//         $dataCount = Project::find_by_sql('SELECT
//         sub_categories.category_id,
// sub_categories.name
//     category,
//     COUNT(*) AS `num`
// FROM
//     projects
// INNER JOIN sub_categories ON projects.category = sub_categories.name
// GROUP BY
//     category');
        $dataCount = Project::find_by_sql('SELECT
     categories.name
         category_id,
         COUNT(category_id) AS SUM
     FROM
         sub_categories
     LEFT JOIN categories ON categories.id = sub_categories.category_id
     GROUP BY
         category_id');
        // $i = 0;
        // foreach ($dataCount as $value) {
        //     $data2[$i]['name'] = $value->category;
        //     $data2[$i]['count'] = $value->num;
        //     $data2[$i]['categoryId'] = $value->category_id;
        //     $i++;
        // }

//    $i = 0;
//         foreach ($dataCount as $value) {
//             $data2[$i]['name'] = $value->category_id;
//             $data2[$i]['count'] = $value->sum;

//             $i++;
//         }


        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
                'categories' => Categorie::find_by_sql('SELECT * FROM `categories` ORDER BY name ASC'),
                'subCategory' => SubCategories::find_by_sql('SELECT * FROM `sub_categories` ORDER BY name ASC'),
                'count' => $dataCount,
                'jobs' => true
            ];

            $this->load->view('blueline/pages/jobCategory', $data);
        } else {
            $data = [
                'userAuth' => 'false',
                'categories' => Categorie::find_by_sql('SELECT * FROM `categories` ORDER BY name ASC'),
                'subCategory' => SubCategories::find_by_sql('SELECT * FROM `sub_categories` ORDER BY name ASC'),
                'count' => $dataCount,
                'jobs' => true
            ];

            $this->load->view('blueline/pages/jobCategory', $data);
        }


    }
    public function getCategoryId() {
        $category = $_POST['category'];

        $SubCategories = SubCategories::find($category);
        $data = [
            'name' => $SubCategories->name,
            'category_id' => $SubCategories->category_id
        ];

        echo json_encode(array(

            'data' => $data
        ));

    }

    public function getHire()
    {
        $category = Categorie::all();
        $this->view_data['category'] =   $category;
        $this->content_view = '/pages/getHire';




    }

    public function aboutUs()
    {
        $this->load->view('blueline/pages/aboutUs');

    }

    public function brandAssests()
    {
        $this->load->view('blueline/pages/brandAssests');

    }

    public function termsService()
    {
        $this->load->view('blueline/pages/termsService');
    }

    public function loadPage()
    {
        $name = $this->uri->segment('2');

        $data = Pages::findByName($name);

        $this->load->view('blueline/pages/newPage', ['data' => $data]);
    }



}