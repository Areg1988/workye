<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $access = true;
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));


    }

    public function index()
    {

        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
            ];

            $this->load->view('blueline/pages/home', $data);
        } else {
            $data = [
                'userAuth' => 'false'
            ];

            $this->load->view('blueline/pages/home', $data);
        }
    }

    public function homePage()
    {

//        if ($this->userAuth) {
//            $data = [
//                'userData' => $this->userData,
//                'userAuth' => $this->userAuth,
//            ];
//
//            $this->load->view('blueline/pages/home3', $data);
//        } else {
//            $data = [
//                'userAuth' => 'false'
//            ];

        $this->load->view('blueline/pages/home3');
        // }
    }
    public function howItWork()
    {

        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
                'howItWork' => true
            ];

            $this->load->view('blueline/pages/howItWorks', $data);
        } else {
            $data = [
                'userAuth' => 'false',
                'howItWork' => true
            ];

            $this->load->view('blueline/pages/howItWorks', $data);
        }
    }
    public function jobCategory()
    {
//         $dataCount = Project::find_by_sql('SELECT
//         sub_categories.category_id,
// sub_categories.name
//     category,
//     COUNT(*) AS `num`
// FROM
//     projects
// INNER JOIN sub_categories ON projects.category = sub_categories.name
// GROUP BY
//     category');
        $dataCount = Project::find_by_sql('SELECT
     categories.name
         category_id,
         COUNT(category_id) AS SUM
     FROM
         sub_categories
     LEFT JOIN categories ON categories.id = sub_categories.category_id
     GROUP BY
         category_id');
        // $i = 0;
        // foreach ($dataCount as $value) {
        //     $data2[$i]['name'] = $value->category;
        //     $data2[$i]['count'] = $value->num;
        //     $data2[$i]['categoryId'] = $value->category_id;
        //     $i++;
        // }

//    $i = 0;
//         foreach ($dataCount as $value) {
//             $data2[$i]['name'] = $value->category_id;
//             $data2[$i]['count'] = $value->sum;

//             $i++;
//         }


        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
                'categories' => Categorie::find_by_sql('SELECT * FROM `categories` ORDER BY name ASC'),
                'subCategory' => SubCategories::find_by_sql('SELECT * FROM `sub_categories` ORDER BY name ASC'),
                'count' => $dataCount,
                'jobs' => true
            ];

            $this->load->view('blueline/pages/jobCategory', $data);
        } else {
            $data = [
                'userAuth' => 'false',
                'categories' => Categorie::find_by_sql('SELECT * FROM `categories` ORDER BY name ASC'),
                'subCategory' => SubCategories::find_by_sql('SELECT * FROM `sub_categories` ORDER BY name ASC'),
                'count' => $dataCount,
                'jobs' => true
            ];

            $this->load->view('blueline/pages/jobCategory', $data);
        }


    }

    public function getCategoryId() {
        $category = $_POST['category'];

        $SubCategories = SubCategories::find($category);
        $data = [
            'name' => $SubCategories->name,
            'category_id' => $SubCategories->category_id
        ];

        echo json_encode(array(

            'data' => $data
        ));

    }

    public function getHire()
    {
        if ($this->userAuth) {
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
                'getHired' => true
            ];

            $this->load->view('blueline/pages/getHire', $data);
        } else {
            $data = [
                'userAuth' => 'false',
                'getHired' => true
            ];

            $this->load->view('blueline/pages/getHire', $data);
        }

    }

    public function aboutUs()
    {
        $this->load->view('blueline/pages/aboutUs');

    }

    public function brandAssests()
    {
        $this->load->view('blueline/pages/brandAssests');

    }

    public function termsService()
    {
        $this->load->view('blueline/pages/termsService');
    }

    public function loadPage()
    {
        $name = $this->uri->segment('2');

        $data = Pages::findByName($name);

        $this->load->view('blueline/pages/newPage', ['data' => $data]);
    }



    public function pricing()
    {


        if ($this->user or $this->client ) {
            $category = Categorie::all();
            $SubCategories = SubCategories::all();
            $companyId = $this->userData->company->id;
            $subscription = Subscription::find(array('conditions' => array('company_id =?', $companyId)));
            $subCategory = SubCategories::find_by_id($subscription->sub_category);
            $categoryClient = Categorie::find_by_id($subscription->category);
            $pricingList = PricingList::find_by_id($subscription->subscription_id);
            $userData2 = [
                'subscription' => $subCategory->name,
                'category' => $categoryClient->name,
                'pricingList' => $pricingList->name,
                'pricingType' => $subscription->paymant_type,
            ];
            if ($subscription->paymant_type == 'monthly') {
                $userData2['sum'] = $subscription->sum;
            } else {
                $userData2['sum'] = $subscription->sum / 12;
            }
            $data = [
                'userData' => $this->userData,
                'userAuth' => $this->userAuth,
                'pricingPlans' => PricingList::all(),
                'firstPlans' => PricingList::find(1),
                'categories' => $category,
                'SubCategories' => $SubCategories,
                'pricing' => true,
                'pricingPLan' => $userData2
            ];



            $this->load->view('blueline/pages/pricing', $data);

        } else {
            $data = [
                'userAuth' => 'false',

                'pricingPlans' => PricingList::all(),
                'firstPlans' => PricingList::find(1),
                'pricing' => true
            ];
            $this->load->view('blueline/pages/pricing', $data);
        }

    }


    public function activation()
    {
        $data['core_settings'] = Setting::first();
        $id = $this->uri->segment('2');
        $client = Client::find($id);

        $type = 'client';
        $data = [
            'categories' => Categorie::all(),
            'client' => $client,
            'type' => $type,
            'subCategories' => SubCategories::all(),
            'public_key' => $data['core_settings']->stripe_key,
        ];

        $this->load->view('blueline/auth/confitmation', $data);
    }
    public function getCategory()
    {
        $category = $_POST['category'];
        $SubCategories = SubCategories::all(array('conditions' => array('category_id =?', $category)));
        $count = 0;
        foreach ($SubCategories as $value) {
            $data[$count]['name'] = $value->name;
            $data[$count]['category_id'] = $value->category_id;
            $data[$count]['id'] = $value->id;
            $count++;
        }
        echo json_encode(array(

            'data' => $data
        ));

    }

    public function checkActivaton()
    {
        $id = $_POST['id'];

        $client = Client::find($id);

        echo json_encode(array(

            'data' => $client->conf_email
        ));

    }

    public function checkActivatonEnp()
    {
        $id = $_POST['id'];

        $client = User::find($id);

        echo json_encode(array(

            'data' => $client->status
        ));

    }

    public function employes()
    {
        $core_settings = Setting::first();
        $this->load->library('parser');
        $this->load->helper('file');
        $this->load->helper('notification');
        $this->load->library('email');
        $client_attr['firstname'] = $_POST['firstname'];
        if($_POST['invite']) {
            $client_attr['local'] = 1;
        } else {
            $client_attr['local'] = 0;
        }

        $client_attr['company_id'] = $_POST['companyId'];
        $client_attr['lastname'] = $_POST['lastname'];
        $client_attr['email'] = $_POST['email'];
        $client_attr['username'] = $_POST['username'];
        $client_attr['status'] = 'inactive';
        $client_attr['admin'] = 0;
        $client_attr['password'] = $_POST['password'];
        $client_attr['access'] = '1,2,3,108,15,105,20,18,21';
        $client_attr['queue'] = 1;
        $category = Categorie::find($_POST['category']);
        $client_attr['title'] = $category->name;
        $SubCategory_attr['subCategory'] = $_POST['subCategory'];
        $client_attr2['cvInput'] = $_POST['cvInput'];
        $client_attr2['address'] = $_POST['address'];
        $client_attr2['zipcode'] = $_POST['zipcode'];
        $client_attr2['city'] = $_POST['city'];
        $client_attr2['country'] = $_POST['country'];
        $config['upload_path'] = './files/media/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|doc|docx';
        $config['max_width'] = '1800';
        $config['max_height'] = '1800';
        $config['max_size'] = '0';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {
            $data = ['upload_data' => $this->upload->data()];
        }

        $client_attr['userpic'] = $data['upload_data']['file_name'];
        if ($this->upload->do_upload('cvInput')) {
            $data = ['upload_data' => $this->upload->data()];
        }
        $client_attr2['cvInput'] = $data['upload_data']['file_name'];
        $user = User::create($client_attr);
        $client_attr2['user_id'] = $user->id;
        Employe::create($client_attr2);
        $this->email->from($core_settings->email, $core_settings->company);
        $this->email->to($_POST["email"]);
        $parse_data = [
            'link' => base_url() . 'login/',
            'company' => $core_settings->company,
            'first_name' => $client_attr['firstname'],
            'last_name' => $client_attr['lastname'],
            'logo' => '<img src="' . base_url() . '' . $core_settings->logo . '" alt="' . $core_settings->company . '"/>',
            'activation_link' => 'https://workye.com/welcome/activate?email=' . $client_attr['email'] . '&id=' . $user->id,
        ];

        $this->email->subject('Activation Email');
        $email = read_file('./application/views/' . $core_settings->template . '/templates/email_create_account.html');
        $message = $this->parser->parse_string($email, $parse_data);
        $this->email->message($message);
        $this->email->send();
        Redirect('/employee-confirmation/' . $user->id);

    }


    public function employeeConfirmation()
    {

        $id = $this->uri->segment('2');
        $client = User::find($id);
        $this->load->view('blueline/auth/confirmationEmp', ['client' => $client]);
    }

    public function activate()
    {
        $core_settings = Setting::first();
        $email = $_GET['email'];
        $id = $_GET['id'];
        $user = User::find($id);
        $data['status'] = 'active';
        $projects = Project::find('all', array('conditions' => array('company_id = ?', $user->company_id)));

        if($projects) {
            foreach ($projects as $project) {
                ProjectHasWorker::create(['project_id' => $project->id, 'user_id' => $user->id]);
            }

        }
        $user->update_attributes($data);
        $this->load->view('blueline/auth/activationEnp', ['core_settings' => $core_settings]);
    }

    public function activateClient()
    {
        $core_settings = Setting::first();
        $email = $_GET['email'];
        $id = $_GET['id'];

        $user = Client::find($id);

        $data['conf_email'] = false;
        $user->update_attributes($data);
        $this->load->view('blueline/auth/activation', ['core_settings' => $core_settings]);
    }

    public function SendEmail()
    {
        $this->load->library('parser');
        $this->load->helper('file');
        $this->load->helper('notification');
        $this->load->library('email');
        $core_settings = Setting::first();
        $id = $_GET['id'];
        $type = $_GET['type'];
        $this->email->from($core_settings->email, $core_settings->company);

        if ($type == 'client') {
            $client = Client::find($id);
            $this->email->to($client->email);
            $parse_data = [
                'link' => base_url() . 'login/',
                'company' => $core_settings->company,
                'first_name' => $client->firstname,
                'last_name' => $client->lastname,
                'logo' => '<img src="' . base_url() . '' . $core_settings->logo . '" alt="' . $core_settings->company . '"/>',
                'activation_link' => 'https://workye.com/welcome/activateClient?email=' . $client->email . '&id=' . $client->id,
            ];
            $this->email->subject('Activation Email');
            $email = read_file('./application/views/' . $core_settings->template . '/templates/email_create_account.html');
            $message = $this->parser->parse_string($email, $parse_data);
            $this->email->message($message);
            $this->email->send();
            redirect('confirmation/' . $id, ['type' => $type]);
        } else {
            $client = User::find($id);
            $this->email->to($client->email);
            $parse_data = [
                'link' => base_url() . 'login/',
                'first_name' => $client->firstname,
                'last_name' => $client->lastname,
                'logo' => '<img src="' . base_url() . '' . $core_settings->logo . '" alt="' . $core_settings->company . '"/>',
                'activation_link' => 'https://workye.com/welcome/activate?email=' . $client->email . '&id=' . $client->id,
            ];
            $this->email->subject('Activation Email');
            $email = read_file('./application/views/' . $core_settings->template . '/templates/email_create_account.html');
            $message = $this->parser->parse_string($email, $parse_data);
            $this->email->message($message);
            $this->email->send();
            redirect('employee-confirmation/' . $id, ['type' => $type]);
        }


    }

    public  function activationFree() {
        $id = $_GET['id'];
        $user = Client::find($id);
        $user->update_attributes(['inactive' => false]);
        redirect('login');
    }

    public function stripepay($id = false, $sum = false, $type = 'card')
    {
        $data['core_settings'] = Setting::first();

        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];
        $paymantType = $_POST['paymantType'];
        $id = $_POST['id'];
        $client = Client::find($id);
        $subCategory = $_POST['subCategory'];
        $category = $_POST['category'];
        $sum = $_POST['sum'];

        if ($paymantType == 'monthly') {
            $data2 = [
                'sub_category' => $subCategory,
                'category' => $category,
                'sum' => $sum,
                'status' => 'active',
                'next_paymant' => date('Y-m-d', strtotime('+1 month')),
                'paymant_type' => 'monthly',
                'date' => date('Y-m-d'),
            ];
        } else if ($paymantType == 'annually') {
            $data2 = [
                'sub_category' => $subCategory,
                'category' => $category,
                'sum' => $sum,
                'status' => 'active',
                'next_paymant' => date('Y-m-d', strtotime('+1 year')),
                'paymant_type' => 'annually',
                'date' => date('Y-m-d'),
            ];
        }


        $companyId = $client->company->id;

        $subscription = Subscription::find(array('conditions' => array('company_id =?', $companyId)));

        $subscription->update_attributes($data2);
        $invoiceRef = Invoice::last();
        if (!$invoiceRef) {
            $reference = 40000;
        } else {
            $reference = $invoiceRef->reference + 1;
        }
        $data = [
            'status' => 'open',
            'company_id' => $client->company_id,
            'currency' => 'EUR',
            'discount' => '0',
            'sum' => $sum,
            'reference' => $reference,
            'subscription_id' => $subscription->subscription_id,
            'issue_date' => date("Y-m-d"),
            'due_date' => date("Y-m-d"),
        ];

        $pAymantinvoice = Invoice::create($data);

        $new_invoice_reference = $data['reference'];

        $invoice_reference = Setting::first();

        $invoice_reference->update_attributes(['invoice_reference' => $new_invoice_reference]);

        $invoice = $pAymantinvoice;
        // Stores errors:
        $errors = [];

        // Need a payment token:
        if (isset($_POST['stripeToken'])) {
            $token = $_POST['stripeToken'];

            // Check for a duplicate submission, just in case:
            // Uses sessions, you could use a cookie instead.
            if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
                $errors['token'] = 'You have apparently resubmitted the form. Please do not do that.';
                $this->session->set_flashdata('message', 'error: You have apparently resubmitted the form. Please do not do that.');
            } else { // New submission.
                $_SESSION['token'] = $token;
            }
        } else {
            $this->session->set_flashdata('message', 'error: The order cannot be processed. Please make sure you have JavaScript enabled and try again.');
            $errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
            log_message('error', 'Stripe: ERROR - Payment canceled for invoice #' . $data['core_settings']->invoice_prefix . $invoice->reference . '.');
        }

        // Set the order amount somehow:
        //$sum_exp = explode('.', intval($sum));
        $amount = $sum * 100; // in cents

        //Get currency

        $currency = $invoice->currency;

        // If no errors, process the order:
        if (empty($errors)) {
            // create the charge on Stripe's servers - this will charge the user's card
            try {
                // Set API key for stripe:
                \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);

                // Charge the order:
                $charge = \Stripe\Charge::create(
                    [
                        'amount' => $amount, // amount in cents, again
                        'currency' => $currency,
                        'card' => $token,
                        'receipt_email' => $invoice->company->client->email,
                        'description' =>  $invoice->reference,
                    ]
                );

                // Check that it was paid:
                if ($charge->paid == true) {
                    $attr = [];
                    $paid_date = date('Y-m-d', time());
                    $payment_reference = $invoice->reference . '00' . InvoiceHasPayment::count(['conditions' => 'invoice_id = ' . $invoice->id]) + 1;
                    $attributes = [
                        'invoice_id' => $invoice->id,
                        'name' => $payment_reference,
                        'amount' => '1',
                        'value' => $_POST['sum'],
                        'type' => 'credit_card',
                        'description' => $data['subscription_id']];
                    InvoiceHasItem::create($attributes);

                    if ($_POST['sum'] >= $invoice->outstanding) {
                        $invoice->update_attributes(['paid_date' => $paid_date, 'status' => 'Paid']);
                    } else {
                        $invoice->update_attributes(['status' => 'PartiallyPaid']);
                    }
                    $user = Client::find($id);
                    $user->update_attributes(['inactive' => false]);
                    $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_payment_complete'));
                    log_message('error', 'Stripe: Payment for Invoice #' . $invoice->reference . ' successfully made');
                } else { // Charge was not paid!
                    $this->session->set_flashdata('message', 'error: Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction.');
                    log_message('error', 'Stripe: ERROR - Payment for Invoice #' . $invoice->reference . ' was not successful!');
                }
            } catch (\Stripe\Error\Card $e) {
                // Card was declined.
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $errors['stripe'] = $err['message'];
                $this->session->set_flashdata('message', 'error: Card was declined!');
                log_message('error', 'Stripe: ERROR - Credit Card was declined by Stripe! Payment process canceled for invoice #' . $invoice->reference . '.');
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Too many requests made to the API too quickly!');
                log_message('error', 'Too many stripe requests: ' . $err['message']);
            } catch (\Stripe\Error\Authentication $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe authentication error: ' . $err['message']);
            } catch (\Stripe\Error\InvalidRequest $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe invalid request error: ' . $err['message']);
            } catch (\Stripe\Error\ApiConnection $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe API connection error: ' . $err['message']);
            } catch (\Stripe\Error\Base $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe error: ' . $err['message']);
            } catch (Exception $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Error during stripe process: ' . $err['message']);
            }
        } else {
            $this->session->set_flashdata('message', 'error: ' . $errors['token']);
            log_message('error', 'Stripe: ' . $errors['token']);
        }

        redirect('login');
    }

    public function getPricePlan()
    {

        $id = $_POST['id'];
        $data = PricingList::find($id);
        $res = [
            'id' => $data->id,
            'name' => $data->name,
            'starter' => $data->starter,
            'standard' => $data->standard,
            'professional' => $data->professional,
            'enterprise' => $data->enterprise,
        ];
        print_r(json_encode($res));
    }
}
