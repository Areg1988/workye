<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $access = FALSE;
        if ($this->client) {
            redirect('login');
        } elseif ($this->user) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "email") {
                    $access = TRUE;
                }
            }
            if (!$access) {
                redirect('login');
            }
        } else {
            redirect('login');
        }

    }
    public function index() {
        $user = User::all();
        $clients = Client::all();

        $this->view_data['form_action'] = 'email/sendEmail';
        $this->view_data['users'] = $user;
        $this->view_data['clients'] = $clients;
        $this->content_view = 'email/all';
    }
    function sendEmail() {
        if($_POST){
            $this->load->library('parser');
            $this->load->helper('file');
            $this->load->helper('notification');
            $this->load->library('email');
            $clientEmail = $_POST['clientEmail'];
            $core_settings = Setting::first();
            $this->email->from($core_settings->email, $core_settings->company);
            $clientEmail = explode(",", $clientEmail);
            array_pop($clientEmail);
            $email = read_file('./application/views/' . $core_settings->template . '/templates/email.html');
            $parse_data = [
                'link' => base_url() . 'login/',
                'company' => $core_settings->company,
                'logo' => '<img src="' . base_url() . '' . $core_settings->logo . '" alt="' . $core_settings->company . '"/>',
                'message' => $_POST['message'],
                'subject' => $_POST['subject'],
            ];

            for($i =0; $i< count($clientEmail); $i++ ) {
                $this->email->subject($_POST['subject']);
                $message = $this->parser->parse_string($email, $parse_data);
                $this->email->message($message);
                $this->email->to($clientEmail[$i]);
                $this->email->send();
            }

            redirect('email');
        }else {
            $this->theme_view = 'modal';
            $this->content_view = 'email/_email';
        }
    }
}