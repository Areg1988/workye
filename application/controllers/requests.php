<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Requests extends MY_Controller
{

    function __construct()
    {

        parent::__construct();
        $access = FALSE;
        if ($this->client) {

            redirect('crequests');
        } elseif ($this->user) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "requests") {
                    $access = TRUE;
                }
            }
            if (!$access) {
                redirect('login');
            }
        } else {
            redirect('login');
        }

        $this->view_data['submenu'] = array(
            $this->lang->line('application_all') => 'requests',
            $this->lang->line('application_Active') => 'requests/filter/active',
            $this->lang->line('application_Inactive') => 'requests/filter/inactive',
            $this->lang->line('application_ended') => 'requests/filter/ended',

        );
    }

    public function index()
    {
        $data = [];
        if ($this->user->admin) {
            $requests = Request::all();
        } else {
            $requests = Request::all(array('conditions' => array('employes_id = ?', $this->user->id)));
        }
        $count = 0;
        foreach ($requests as $request) {
            $user = User::find($request->employes_id);
            $company = Company::find($request->company_id);
            $project = Project::find($request->project_id);
            $data[$count] = [
                'id' => $request->id,
                'fullName' => $user->firstname . ' ' . $user->lastname,
                'company' => $company->name,
                'project' => $project->name,
                'sum' => $request->sum,
                'description' => $request->description,
                'status' => $request->status,
                'type' => $request->type
            ];
            $count++;
        }

        $this->view_data['requests'] = $data;
        $this->content_view = 'requests/requests';
    }

    public function create()
    {

        $data = [
            'employes_id' => $this->user->id,
            'project_id' => $_POST['id'],
            'status' => 'pending',
            'description' => $_POST['description'],
            'company_id' => $_POST['compnay'],
            'type' => 'client'
        ];

        Request::create($data);
        redirect('requests');

    }

    public function createEmp()
    {

        if ($_POST) {
            $project = Project::find($_POST['id']);
            $data = [
                'employes_id' => $this->user->id,
                'sum' => $_POST['phases'],
                'project_id' => $project->id,
                'status' => 'pending',
                'description' => $_POST['description'],
                'company_id' => $project->company_id,
                'type' => 'client'
            ];
            Request::create($data);
            redirect('/requests');
        } else {
            $id = $_GET['id'];
            $project = Project::find($id);
            $this->theme_view = 'modal';
            $this->view_data['project'] = $project;
            $this->view_data['id'] = $id;
            $this->view_data['form_action'] = 'requests/createEmp';
            $this->content_view = 'projects/_requests';
        }

    }

    public function delete($id)
    {

        $request = Request::find($id);
        $request->delete();
        redirect('requests');
    }

    public function update($id = false)
    {
        $data['core_settings'] = Setting::first();
        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];

        \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);
        $request = Request::find($id);
        $employes = Card::find(array('conditions' => array('employes_id =?', $request->employes_id)));
        $user = User::find($request->employes_id);
        $company = Company::find($request->company_id);
        $project = Project::find($request->project_id);
        if($_POST) {

            $value = $_POST['value'] * 100;

            $cu = \Stripe\Customer::retrieve($employes->card_id);
            $refund = \Stripe\Charge::create([
                'source' =>  $cu->default_source,
                'customer' =>  $employes->card_id,
                'currency' => 'EUR',
                'amount' => $value,
            ]);
            $request->update_attributes(['status' => 'Paid']);
            $invoiceRef = Invoice::last();
            if (!$invoiceRef) {
                $reference = 40000;
            } else {
                $reference = $invoiceRef->reference + 1;
            }
            $data = [
                'status' => 'open',
                'company_id' => $request->company_id,
                'currency' => 'EUR',
                'discount' => '0',
                'sum' => $_POST['value'],
                'reference' => $reference,
                'issue_date' => date("Y-m-d"),
                'due_date' => date("Y-m-d"),
            ];

            $invoice = Invoice::create($data);

            $invoices = [
                'invoice_id' => $invoice->id,
                'amount' => '1',
                'description' => 'Payment to Employee '.$user->firstname.' '. $user->lastname .' for project '.$project->name,
                'value' =>  $_POST['value'],
                'name' =>  'Payment to Employee',
                'type' => 'done'
            ];
            $data2 = [
                'description' => 'Payment to Employee '.$user->firstname.' '. $user->lastname .' for project '.$project->name,
                'type' => 'payment',
                'date' => date("Y-m-d"),
                'currency' => 'EUR',
                'value' => $_POST['value'],
                'reference' => $reference,
                'project_id' => $project->id,
                'status' => 'Paid',
                'invoice_id' => $invoice->id,
                'user_id' => $user->id
            ];
            Expense::create($data2);
            InvoiceHasItem::create($invoices);

            $new_invoice_reference = $data['reference'];

            $invoice_reference = Setting::first();

            $invoice_reference->update_attributes(['invoice_reference' => $new_invoice_reference]);
            redirect('requests');
        }else {

            $data = [
                'id' => $request->id,
                'fullName' => $user->firstname . ' ' . $user->lastname,
                'company' => $company->name,
                'project' => $project->name,
                'reference' => $project->reference,
                'description' => $request->description,
                'status' => $request->status,
                'type' => $request->type,

            ];

            $this->view_data['request'] = $data;
            $this->view_data['employes'] = $employes;
            $this->content_view = 'requests/_accept';
        }
    }
}