<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class EmpPay extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->client) {
            redirect('login');
        } elseif ($this->user) {
            if($this->user->admin) {

            }else {
                redirect('login');
            }
        } else {
            redirect('login');
        }
        $this->view_data['submenu'] = array(
            $this->lang->line('application_all') => 'empPay',
            $this->lang->line('application_Active') => 'empPay/filter/active',
            $this->lang->line('application_Inactive') => 'empPay/filter/inactive',
        );

    }
    public function index() {

    }
}