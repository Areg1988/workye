<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cRequests extends MY_Controller
{

    function __construct()
    {  
        parent::__construct();
        $access = FALSE;
        if ($this->client) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "crequests") {
                    $access = TRUE;
                }
            }
            if (!$access) {
                redirect('login');
            }
        } elseif ($this->user) {
            redirect('requests');
        } else {
            redirect('login');
        }

        $this->view_data['submenu'] = array(
            $this->lang->line('application_all') => 'requests',
            $this->lang->line('application_Active') => 'requests/filter/active',
            $this->lang->line('application_Inactive') => 'requests/filter/inactive',
            $this->lang->line('application_ended') => 'requests/filter/ended',

        );
    }
    
    public function index() {
        $requests = Request::all(array('conditions' => array('company_id = ?', $this->client->company->id)));
        $count = 0;
       foreach($requests as $request) {
          $user = User::find($request->employes_id); 
          $project = Project::find($request->project_id);
          $data[$count] = [
              'fullName' => $user->firstname.' '.$user->lastname,
              'reference' => $project->reference,
              'project' => $project->name,
              'sum' => $request->sum,
              'description'=> $request->description,
              'status' => $request->status,
              'type' => $request->type,
              'id' => $request->id
              ];
              $count++;
       }
        $this->view_data['requests'] = $data;
        $this->content_view = 'requests/client/requests';
    }
    
    public function view($id = false) {
          $request = Request::find($id);
          $user = User::find($request->employes_id); 
          $project = Project::find($request->project_id);
          $data = [
              'fullName' => $user->firstname.' '.$user->lastname,
              'reference' => $project->reference,
              'project' => $project->name,
              'sum' => $request->sum,
              'description'=> $request->description,
              'status' => $request->status,
              'type' => $request->type,
              'id' => $request->id
              ];
            $this->theme_view = 'modal';
            $this->view_data['request'] = $data;
            $this->view_data['form_action'] = 'crequests/update';
            $this->content_view = 'requests/client/_requests';
    }
    
    public function update() {
         $request = Request::find($_POST['id']);
         if($_POST['status'] == 'Approved') {
        $data = [
            'status' =>  'New',
            'sender' => 'c'.$this->client->id,
            'recipient' => 'u'.$request->employes_id,
            'subject' => 'Your payment status changes',
            'message' => 'Your payment status changes to '.$_POST['status'],
            'time' => date("Y-m-d H:i:s"),
            'conversation' => random_string('sha1'),
        ];
         } else if($_POST['status'] == 'Refuse') {
            $data = [
                'status' =>  'New',
                'sender' => 'c'.$this->client->id,
                'recipient' => 'u'.$request->employes_id,
                'subject' => 'Your request status changes',
                'message' => 'Your request status changes to '.$_POST['status'].'<br>'.$_POST['message'],
                'time' => date("Y-m-d H:i:s"),
                'conversation' => random_string('sha1'),
            ];
         }
         Privatemessage::create($data);
         $request->update_attributes(['status' => $_POST['status']]);
        redirect('crequests');
        
    }
     public function delete($id) {
         
        $request = Request::find($id);
        $request->delete();
        redirect('crequests');
     }

}