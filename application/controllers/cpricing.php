<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cPricing extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $access = FALSE;
        if ($this->client) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "cpricing") {
                    $access = TRUE;
                }
            }
        } elseif ($this->user) {
            redirect('pricing');
            if (!$access) {
                redirect('login');
            }
        } else {
            redirect('login');
        }
    }

    public function index()
    {
        $data2 = Setting::first();

        $id = $this->client->id;
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $id)));

            if($subscription and $subscription->status !== 'inactive' ) {
                $subCategory = SubCategories::find($subscription->sub_category);
                $category = Categorie::find($subscription->category);
                $pricing = PricingList::find($subscription->subscription_id);
                $data['subCategory'] = $subCategory->name;
                $data['category'] = $category->name;
                $data['nextPaymant'] = $subscription->next_paymant;
            $data['type'] = $subscription->paymant_type;
            $data['pricing'] = $pricing->name;
            $data['publishable_key'] = $data2->stripe_key;
            $data['count'] = $subscription->count;
            if ($subscription->paymant_type == 'annually') {
                $sum = $subscription->sum / 12;

                if ($sum == $pricing->starter) {
                    $data['title'] = 'Starter';
                } elseif ($sum == $pricing->standard) {
                    $data['title'] = 'Standard';
                } elseif ($sum == $pricing->professional) {
                    $data['title'] = 'Professional';
                } elseif ($sum == $pricing->enterprise) {
                    $data['title'] = 'Enterprise';
                }
            } else {
                $sum = $subscription->sum;
                if ($sum == $pricing->starter) {
                    $data['title'] = 'Starter';
                } elseif ($sum == $pricing->standard) {
                    $data['title'] = 'Standard';
                } elseif ($sum == $pricing->professional) {
                    $data['title'] = 'Professional';
                } elseif ($sum == $pricing->enterprise) {
                    $data['title'] = 'Enterprise';
                }
            }
        }else {

            $data = [];
        }


        $this->view_data['data'] = $data;

        $this->content_view = 'pricing/client_views/pricing';
    }

    public function update()
    {
        $id = $this->client->id;
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $this->client->company->id)));
        // $pricing = PricingList::find($subscription->subscription_id);
        $subCategory = SubCategories::all();
        $category = Categorie::all();

        $data2 = Setting::first();
        $data['subCategory'] = $subCategory;
        $data['category'] = $category;
        $data['subscriptionId'] = $subscription->id;
        $data['type'] = $subscription->paymant_type;
        $data['count'] = $subscription->count;
        $data['public_key'] = $data2->stripe_key;
        //$data['pricing'] = $pricing->name;
        $this->view_data['data'] = $data;
        $this->view_data['form_action'] = 'pricing/create';
        $this->content_view = 'pricing/client_views/update';
    }

    public function cancel()
    {
        $id = $this->client->company->id;
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $id)));
        $date = date('Y-m-d', strtotime($subscription->date . '+15 day'));
        $date2 = date('Y-m-d');
        $project = Project::all(array('conditions' => array('company_id =?', $id)));
       // $request = Request::all(array('conditions' => array('company_id =?', $id)));
        if ($date2 < $date and $project) {
              echo json_encode(array(
                  'data' => 'true'
            ));
              die;

        } elseif($date2 < $date and !$project) {
            echo json_encode(array(
                'data' => 'false'
            ));
            die;
        } elseif($date2 > $date) {
            echo json_encode(array(
                'data' => 'true'
            ));
            die;

        }
    }
    public function cancelPlan() {
        $id = $this->client->company->id;
        $request = Request::all(array('conditions' => array('company_id =?', $id)));
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $id)));
        $project = Project::all(array('conditions' => array('company_id =?', $id)));
        $ticket = Ticket::all(array('conditions' => array('company_id =?', $id)));
        for ($i = 0; $i < count($project); $i++) {
            $project[$i]->delete();
        }
        for ($j = 0; $j < count($request); $j++) {
            $request[$j]->delete();
        }
        for ($q = 0; $q < count($ticket); $q++) {
            $ticket[$q]->delete();
        }
        $subscription->delete();
        redirect('cinvoices');
    }

    public function stripepay($id = false, $sum = false, $type = 'card')
    {
        $data['core_settings'] = Setting::first();
        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];
        $paymantType = $_POST['type'];
        $client = $this->client;
        $subCategory = $_POST['subCategory'];
        $category = $_POST['category'];
        $pricingId = $_POST['pricingId'];

        $sum = $_POST['sum'];

        preg_match_all('!\d+!', $_POST['pricing'], $matches);
        if ($paymantType == 'monthly') {
            $data2 = [
                'sub_category' => $subCategory,
                'category' => $category,
                'sum' => $sum,
                'status' => 'active',
                'next_paymant' => date('Y-m-d', strtotime('+1 month')),
                'paymant_type' => 'monthly',
                'subscription_id' => $pricingId,
                'count' => (int)$matches[0][0]
            ];
        } else if ($paymantType == 'annually') {
            $data2 = [
                'sub_category' => $subCategory,
                'category' => $category,
                'sum' => $sum ,
                'status' => 'active',
                'next_paymant' => date('Y-m-d', strtotime('+1 year')),
                'paymant_type' => 'annually',
                'subscription_id' => $pricingId,
                'count' => (int)$matches[0][0]
            ];
        }
        $companyId = $client->company->id;
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $companyId)));
        if($subscription) {
            $subscription->update_attributes($data2);
        }else {
            $data2['company_id'] = $this->client->company->id;
            $data2['date'] =  date('Y-m-d');
            $subscription = Subscription::create($data2);
        }

        $invoiceRef = Invoice::last();
        if (!$invoiceRef) {
            $reference = 40000;
        } else {
            $reference = $invoiceRef->reference + 1;
        }
        $data = [
            'status' => 'open',
            'company_id' => $client->company_id,
            'currency' => 'EUR',
            'discount' => '0',
            'sum' => $sum,
            'reference' => $reference,
            'subscription_id' => $subscription->subscription_id,
            'issue_date' => date("Y-m-d"),
            'due_date' => date("Y-m-d"),
        ];

        $pAymantinvoice = Invoice::create($data);

        $new_invoice_reference = $data['reference'];

        $invoice_reference = Setting::first();

        $invoice_reference->update_attributes(['invoice_reference' => $new_invoice_reference]);

        $invoice = $pAymantinvoice;
        // Stores errors:
        $errors = [];

        // Need a payment token:
        if (isset($_POST['stripeToken'])) {
            $token = $_POST['stripeToken'];

            // Check for a duplicate submission, just in case:
            // Uses sessions, you could use a cookie instead.
            if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
                $errors['token'] = 'You have apparently resubmitted the form. Please do not do that.';
                $this->session->set_flashdata('message', 'error: You have apparently resubmitted the form. Please do not do that.');
            } else { // New submission.
                $_SESSION['token'] = $token;
            }
        } else {
            $this->session->set_flashdata('message', 'error: The order cannot be processed. Please make sure you have JavaScript enabled and try again.');
            $errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
            log_message('error', 'Stripe: ERROR - Payment canceled for invoice #' . $data['core_settings']->invoice_prefix . $invoice->reference . '.');
        }

        // Set the order amount somehow:
        //$sum_exp = explode('.', intval($sum));
        $amount = $sum * 100; // in cents

        //Get currency

        $currency = $invoice->currency;

        // If no errors, process the order:
        if (empty($errors)) {
            // create the charge on Stripe's servers - this will charge the user's card
            try {
                // Set API key for stripe:
                \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);

                // Charge the order:
                $charge = \Stripe\Charge::create(
                    [
                        'amount' => $amount, // amount in cents, again
                        'currency' => $currency,
                        'card' => $token,
                        'receipt_email' => $invoice->company->client->email,
                        'description' => $invoice->reference,
                    ]
                );

                // Check that it was paid:
                if ($charge->paid == true) {
                    $attr = [];
                    $paid_date = date('Y-m-d', time());
                    $payment_reference = $invoice->reference . '00' . InvoiceHasPayment::count(['conditions' => 'invoice_id = ' . $invoice->id]) + 1;
                    $attributes = [
                        'invoice_id' => $invoice->id,
                        'name' => $payment_reference,
                        'amount' => '1',
                        'value' => $_POST['sum'],
                        'type' => 'credit_card',
                        'description' => $data['subscription_id']];
                    $invoiceHasPayment = InvoiceHasItem::create($attributes);

                    if ($_POST['sum'] >= $invoice->outstanding) {
                        $invoice->update_attributes(['paid_date' => $paid_date, 'status' => 'Paid']);
                    } else {
                        $invoice->update_attributes(['status' => 'PartiallyPaid']);
                    }

                    $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_payment_complete'));
                    log_message('error', 'Stripe: Payment for Invoice #' . $invoice->reference . ' successfully made');
                } else { // Charge was not paid!
                    $this->session->set_flashdata('message', 'error: Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction.');
                    log_message('error', 'Stripe: ERROR - Payment for Invoice #' . $invoice->reference . ' was not successful!');
                }
            } catch (\Stripe\Error\Card $e) {
                // Card was declined.
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $errors['stripe'] = $err['message'];
                $this->session->set_flashdata('message', 'error: Card was declined!');
                log_message('error', 'Stripe: ERROR - Credit Card was declined by Stripe! Payment process canceled for invoice #' . $invoice->reference . '.');
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Too many requests made to the API too quickly!');
                log_message('error', 'Too many stripe requests: ' . $err['message']);
            } catch (\Stripe\Error\Authentication $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe authentication error: ' . $err['message']);
            } catch (\Stripe\Error\InvalidRequest $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe invalid request error: ' . $err['message']);
            } catch (\Stripe\Error\ApiConnection $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe API connection error: ' . $err['message']);
            } catch (\Stripe\Error\Base $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Stripe error: ' . $err['message']);
            } catch (Exception $e) {
                $e_json = $e->getJsonBody();
                $err = $e_json['error'];
                $this->session->set_flashdata('message', 'error: Payment could not be processed!');
                log_message('error', 'Error during stripe process: ' . $err['message']);
            }
        } else {
            $this->session->set_flashdata('message', 'error: ' . $errors['token']);
            log_message('error', 'Stripe: ' . $errors['token']);
        }

        redirect('cpricing');
    }

    public function pay($id, $sum, $type)
    {

        $data['core_settings'] = Setting::first();
        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];

        \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);

        $client = Client::find($this->client->id);


        $customer = \Stripe\Customer::create([
            "description" => $this->cleint->email,

        ]);
        $data = [
            'card_id' => $customer->id,
        ];
        $client->update_attributes($data);

        // Use your test API key (switch to the live key later)
        if (isset($_POST['stripeToken'])) {
            try {
                $cu = \Stripe\Customer::retrieve($this->client->card_id); // stored in your application
                $cu->source = $_POST['stripeToken']; // obtained with Checkout
                $cu->save();
                $success = "Your card details have been updated!";
                $this->retrive();
            } catch (\Stripe\Error\Card $e) {
                // Use the variable $error to save any errors
                // To be displayed to the customer later in the page
                $body = $e->getJsonBody();
                $err = $body['error'];
                $error = $err['message'];
            }
            // Add additional error handling here as needed
        }


    }



    public function retrive()
    {
        $data['core_settings'] = Setting::first();
        $stripe_keys = [
            'secret_key' => $data['core_settings']->stripe_p_key,
            'publishable_key' => $data['core_settings']->stripe_key
        ];

        \Stripe\Stripe::setApiKey($stripe_keys['secret_key']);
        $subscription = Subscription::find(array('conditions' => array('company_id =?', $this->client->company->id)));
        $cu = \Stripe\Customer::retrieve($this->client->card_id);
        $refund = \Stripe\Charge::create([
            'source' => $cu->default_source,
            'customer' => $this->client->card_id,
            'currency' => 'EUR',
            'amount' => $subscription->sum * 100,
        ]);
        $subscription->delete();
        redirect('cinvoices');
    }
}