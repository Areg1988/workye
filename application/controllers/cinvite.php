<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cInvite extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $access = FALSE;
        if ($this->client) {
            foreach ($this->view_data['menu'] as $key => $value) {

                if ($value->link == "cinvite") {
                    $access = TRUE;
                }
            }
            if (!$access && !empty($this->view_data['menu'][0])) {
                redirect($this->view_data['menu'][0]->link);
            } elseif (empty($this->view_data['menu'][0])) {
                $this->view_data['error'] = "true";
                $this->session->set_flashdata('message', 'error: You have no access to any modules!');
                redirect('login');
            }
        } elseif ($this->user) {
            redirect('login');
        } else {
            redirect('login');
        }


        $this->view_data['submenu'] = array(
            $this->lang->line('application_my_projects') => 'cinvite'
        );
        function submenu($id)
        {
            return array(
                $this->lang->line('application_back') => 'cinvite',
                $this->lang->line('application_overview') => 'cinvite/view/' . $id,
                $this->lang->line('application_media') => 'cinvite/media/' . $id,
            );
        }
    }

    function index()
    {
        $localUser = User::find('all', array('conditions' => array('company_id = ?',$this->client->company->id)));
        $this->view_data['localUsers'] = $localUser;
        $this->content_view = 'invite/client/all';
    }

    function invite() {

        if($_POST){
            $this->load->library('parser');
            $this->load->helper('file');
            $this->load->helper('notification');
            $this->load->library('email');
            $core_settings = Setting::first();

            $email = str_replace(' ', '', $_POST['email']);
        //    for ($i=0; $i < count($email); $i++){
                $parse_data = [
                    'link' => base_url() . 'login/',
                    'company' => $core_settings->company,
                    'client' => $this->client->company->name,
                    'first_name' => $this->client->firstname,
                    'last_name' => $this->client->lastname,
                    'message' => $_POST['message'],
                    'invite_link' => 'https://workye.com/register?id='.$this->client->company->id.'&invite=true&',
                ];
                $this->email->from($core_settings->email, $core_settings->company);
                $this->email->to($email);
                $this->email->subject('Invite');
                $email = read_file('./application/views/' . $core_settings->template . '/templates/email_invite.html');
                $message = $this->parser->parse_string($email, $parse_data);
                $this->email->message($message);
                $this->email->send();
         //   }

            redirect('/cinvite');

        }else{
            $this->view_data['form_action'] = 'cinvite/invite';
            $this->theme_view = 'modal';
            $this->content_view = 'invite/client/invite';
        }

    }

}