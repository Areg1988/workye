<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cProjects extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $access = FALSE;
        if ($this->client) {
            $this->view_data['invoice_access'] = FALSE;
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "cinvoices") {
                    $this->view_data['invoice_access'] = TRUE;
                }
                if ($value->link == "cprojects") {
                    $access = TRUE;
                }
            }
            if (!$access && !empty($this->view_data['menu'][0])) {
                redirect($this->view_data['menu'][0]->link);
            } elseif (empty($this->view_data['menu'][0])) {
                $this->view_data['error'] = "true";
                $this->session->set_flashdata('message', 'error: You have no access to any modules!');
                redirect('login');
            }
        } elseif ($this->user) {
            redirect('projects');
        } else {
            redirect('login');
        }


        $this->view_data['submenu'] = array(
            $this->lang->line('application_my_projects') => 'cprojects'
        );
        function submenu($id)
        {
            return array(
                $this->lang->line('application_back') => 'cprojects',
                $this->lang->line('application_overview') => 'cprojects/view/' . $id,
                $this->lang->line('application_media') => 'cprojects/media/' . $id,
            );
        }
    }

    function index()
    {


        $options = array('conditions' => 'status != "closed" AND company_id = ' . $this->client->company_id);
        $this->view_data['ticket'] = Ticket::all($options);
        $projectId = Project::find('all', array('conditions' => array('company_id=?', $this->client->company->id)));
        $subscription = Subscription::find('all', array('conditions' => array('company_id=?', $this->client->company->id)));
        $this->view_data['project'] = Project::find('all', array('conditions' => array('company_id=?', $this->client->company->id)));
        $countTaks = 0;
        $countEmployes = 0;
        for ($i = 0; $i < count($projectId); $i++) {
            // var_dump($projectId[$i]->id);
            $taskId = ProjectHasTask::find('all', array('conditions' => array('project_id=?', $projectId[$i]->id)));
            $employess = ProjectHasWorker::find('all', array('conditions' => array('project_id=?', $projectId[$i]->id)));
            $countTaks += count($taskId);
            $countEmployes += count($employess);
        }


        $this->view_data['userCount'] = $countEmployes;
        $this->view_data['subscription'] = $subscription;
        $this->view_data['countTask'] = $countTaks;
        $this->content_view = 'projects/client_views/all';
    }

    function view($id = FALSE)
    {
        $this->view_data['submenu'] = array(
            $this->lang->line('application_back') => 'cprojects',
            $this->lang->line('application_overview') => 'cprojects/view/' . $id,
            $this->lang->line('application_media') => 'cprojects/media/' . $id,
        );
        $this->load->helper('file');
        $this->view_data['submenu'] = array();
        $this->view_data['project'] = Project::find($id);
        $this->view_data['go_to_taskID'] = $taskId;
        $this->view_data['first_project'] = Project::first();
        $this->view_data['last_project'] = Project::last();
        $this->view_data['project_has_invoices'] = Invoice::all(array('conditions' => array('project_id = ? AND estimate != ?', $id, 1)));
        if (!isset($this->view_data['project_has_invoices'])) {
            $this->view_data['project_has_invoices'] = array();
        }
        $tasks = ProjectHasTask::count(array('conditions' => 'project_id = ' . $id));
        $this->view_data['alltasks'] = $tasks;
        $this->view_data['opentasks'] = ProjectHasTask::count(array('conditions' => array('status != ? AND project_id = ?', 'done', $id)));
        $this->view_data['usercountall'] = User::count(array('conditions' => array('status = ?', 'active')));
        $this->view_data['usersassigned'] = ProjectHasWorker::count(array('conditions' => array('project_id = ?', $id)));

        $this->view_data['assigneduserspercent'] = round($this->view_data['usersassigned'] / $this->view_data['usercountall'] * 100);


        //Format statistic labels and values
        $this->view_data["labels"] = "";
        $this->view_data["line1"] = "";
        $this->view_data["line2"] = "";

        $daysOfWeek = getDatesOfWeek();
        $this->view_data['dueTasksStats'] = ProjectHasTask::getDueTaskStats($id, $daysOfWeek[0], $daysOfWeek[6]);
        $this->view_data['startTasksStats'] = ProjectHasTask::getStartTaskStats($id, $daysOfWeek[0], $daysOfWeek[6]);


        foreach ($daysOfWeek as $day) {
            $counter = "0";
            $counter2 = "0";
            foreach ($this->view_data['dueTasksStats'] as $value):
                if ($value->due_date == $day) {
                    $counter = $value->tasksdue;
                }
            endforeach;
            foreach ($this->view_data['startTasksStats'] as $value):
                if ($value->start_date == $day) {
                    $counter2 = $value->tasksdue;
                }
            endforeach;
            $this->view_data["labels"] .= '"' . $day . '"';
            $this->view_data["labels"] .= ',';
            $this->view_data["line1"] .= $counter . ",";
            $this->view_data["line2"] .= $counter2 . ",";
        }


        $this->view_data['time_days'] = round((human_to_unix($this->view_data['project']->end . ' 00:00') - human_to_unix($this->view_data['project']->start . ' 00:00')) / 3600 / 24);
        $this->view_data['time_left'] = $this->view_data['time_days'];
        $this->view_data['timeleftpercent'] = 100;

        if (human_to_unix($this->view_data['project']->start . ' 00:00') < time() && human_to_unix($this->view_data['project']->end . ' 00:00') > time()) {
            $this->view_data['time_left'] = round((human_to_unix($this->view_data['project']->end . ' 00:00') - time()) / 3600 / 24);
            $this->view_data['timeleftpercent'] = $this->view_data['time_left'] / $this->view_data['time_days'] * 100;
        }
        if (human_to_unix($this->view_data['project']->end . ' 00:00') < time()) {
            $this->view_data['time_left'] = 0;
            $this->view_data['timeleftpercent'] = 0;
        }
        $this->view_data['allmytasks'] = ProjectHasTask::all(array('conditions' => array('project_id = ? AND user_id = ?', $id, $this->user->id)));
        $this->view_data['mytasks'] = ProjectHasTask::count(array('conditions' => array('status != ? AND project_id = ? AND user_id = ?', 'done', $id, $this->user->id)));
        $this->view_data['tasksWithoutMilestone'] = ProjectHasTask::find('all', array('conditions' => array('milestone_id = ? AND project_id = ? ', '0', $id)));

        $tasks_done = ProjectHasTask::count(array('conditions' => array('status = ? AND project_id = ?', 'done', $id)));
        $this->view_data['progress'] = $this->view_data['project']->progress;
        if ($this->view_data['project']->progress_calc == 1) {
            if ($tasks) {
                @$this->view_data['progress'] = round($tasks_done / $tasks * 100);
            }
            $attr = array('progress' => $this->view_data['progress']);
            $this->view_data['project']->update_attributes($attr);
        }
        @$this->view_data['opentaskspercent'] = ($tasks == 0 ? 0 : $tasks_done / $tasks * 100);
        $projecthasworker = ProjectHasWorker::all(array('conditions' => array('user_id = ? AND project_id = ?', $this->user->id, $id)));
        @$this->view_data['worker_is_client_admin'] = CompanyHasAdmin::all(array('conditions' => array('user_id = ? AND
		 company_id = ?',
            $this->user->id,
            $this->view_data['project']->company_id)));
//        if (!$projecthasworker && $this->user->admin != 1 && !$this->view_data['worker_is_client_admin']) {
//            $this->session->set_flashdata('message', 'error:'.$this->lang->line('messages_no_access_error'));
//            redirect('cprojects');
//        }
        $tracking = $this->view_data['project']->time_spent;
        if (!empty($this->view_data['project']->tracking)) {
            $tracking = (time() - $this->view_data['project']->tracking) + $this->view_data['project']->time_spent;
        }
        $this->view_data['timertime'] = $tracking;
        $this->view_data['time_spent_from_today'] = time() - $this->view_data['project']->time_spent;
        $tracking = floor($tracking / 60);
        $tracking_hours = floor($tracking / 60);
        $tracking_minutes = $tracking - ($tracking_hours * 60);


        $this->view_data['time_spent'] = $tracking_hours . " " . $this->lang->line('application_hours') . " " . $tracking_minutes . " " . $this->lang->line('application_minutes');
        $this->view_data['time_spent_counter'] = sprintf("%02s", $tracking_hours) . ":" . sprintf("%02s", $tracking_minutes);

        $this->content_view = 'projects/client_views/view';


    }

    function media($id = FALSE, $condition = FALSE, $media_id = FALSE)
    {
        $this->load->helper('notification');
        $this->view_data['submenu'] = array(
            $this->lang->line('application_back') => 'cprojects',
            $this->lang->line('application_overview') => 'cprojects/view/' . $id,
            $this->lang->line('application_media') => 'cprojects/media/' . $id,
        );
        switch ($condition) {
            case 'view':

                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['_wysihtml5_mode']);
                    unset($_POST['files']);
                    //$_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['text'] = $_POST['message'];
                    unset($_POST['message']);
                    $_POST['project_id'] = $id;
                    $_POST['media_id'] = $media_id;
                    $_POST['from'] = $this->client->firstname . ' ' . $this->client->lastname;
                    $this->view_data['project'] = Project::find_by_id($id);
                    $this->view_data['media'] = ProjectHasFile::find($media_id);
                    $message = Message::create($_POST);
                    if (!$message) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_message_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_message_success'));
                        foreach ($this->view_data['project']->project_has_workers as $workers) {
                            send_notification($workers->user->email, "[" . $this->view_data['project']->name . "] New comment", 'New comment on media file: ' . $this->view_data['media']->name . '<br><strong>' . $this->view_data['project']->name . '</strong>');
                        }

                    }
                    redirect('cprojects/media/' . $id . '/view/' . $media_id);
                }
                $this->content_view = 'projects/client_views/view_media';
                $this->view_data['media'] = ProjectHasFile::find($media_id);
                $project = Project::find_by_id($id);
                if ($project->company_id != $this->client->company->id) {
                    redirect('cprojects');
                }
                $this->view_data['form_action'] = 'cprojects/media/' . $id . '/view/' . $media_id;
                $this->view_data['filetype'] = explode('.', $this->view_data['media']->filename);
                $this->view_data['filetype'] = $this->view_data['filetype'][1];
                $this->view_data['backlink'] = 'cprojects/view/' . $id;
                break;
            case 'add':
                $this->content_view = 'projects/_media';
                $this->view_data['project'] = Project::find($id);
                if ($_POST) {
                    $config['upload_path'] = './files/media/';
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = '*';

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors('', ' ');
                        $this->session->set_flashdata('message', 'error:' . $error);
                        redirect('cprojects/view/' . $id);
                    } else {
                        $data = array('upload_data' => $this->upload->data());

                        $_POST['filename'] = $data['upload_data']['orig_name'];
                        $_POST['savename'] = $data['upload_data']['file_name'];
                        $_POST['type'] = $data['upload_data']['file_type'];
                    }

                    unset($_POST['send']);
                    unset($_POST['userfile']);
                    unset($_POST['file-name']);
                    unset($_POST['files']);
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['project_id'] = $id;
                    $_POST['client_id'] = $this->client->id;
                    $media = ProjectHasFile::create($_POST);
                    if (!$media) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_media_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_media_success'));
                        $attributes = array('subject' => $this->lang->line('application_new_media_subject'), 'message' => '<b>' . $this->client->firstname . ' ' . $this->client->lastname . '</b> ' . $this->lang->line('application_uploaded') . ' ' . $_POST['name'], 'datetime' => time(), 'project_id' => $id, 'type' => 'media', 'client_id' => $this->client->id);
                        $activity = ProjectHasActivity::create($attributes);

                        foreach ($this->view_data['project']->project_has_workers as $workers) {
                            send_notification($workers->user->email, "[" . $this->view_data['project']->name . "] " . $this->lang->line('application_new_media_subject'), $this->lang->line('application_new_media_file_was_added') . ' <strong>' . $this->view_data['project']->name . '</strong>');
                        }
                        if (is_object($this->view_data['project']->company->client)) {
                            send_notification($this->view_data['project']->company->client->email, "[" . $this->view_data['project']->name . "] " . $this->lang->line('application_new_media_subject'), $this->lang->line('application_new_media_file_was_added') . ' <strong>' . $this->view_data['project']->name . '</strong>');
                        }
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['title'] = $this->lang->line('application_add_media');
                    $this->view_data['form_action'] = 'cprojects/media/' . $id . '/add';
                    $this->content_view = 'projects/_media';
                }
                break;
            case 'update':
                $this->content_view = 'projects/_media';
                $this->view_data['media'] = ProjectHasFile::find($media_id);
                $this->view_data['project'] = Project::find($id);
                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['_wysihtml5_mode']);
                    unset($_POST['files']);
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $media_id = $_POST['id'];
                    $media = ProjectHasFile::find($media_id);
                    if ($this->view_data['media']->client_id != "0") {
                        $media->update_attributes($_POST);
                    }
                    if (!$media) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_media_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_media_success'));
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['title'] = $this->lang->line('application_edit_media');
                    $this->view_data['form_action'] = 'cprojects/media/' . $id . '/update/' . $media_id;
                    $this->content_view = 'projects/_media';
                }
                break;
            case 'delete':
                $media = ProjectHasFile::find($media_id);
                if ($media->client_id != "0") {
                    $media->delete();
                    ProjectHasFile::find_by_sql("DELETE FROM messages WHERE media_id = $media_id");
                }
                if (!$media) {
                    $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_delete_media_error'));
                } else {
                    unlink('./files/media/' . $media->savename);
                    $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_delete_media_success'));
                }
                redirect('cprojects/view/' . $id);
                break;
            default:
                $this->view_data['project'] = Project::find($id);
                $this->content_view = 'projects/client_views/media';
                break;
        }

    }

    function dropzone($id = FALSE)
    {

        $attr = array();
        $config['upload_path'] = './files/media/';
        $config['encrypt_name'] = TRUE;
        $config['allowed_types'] = '*';

        $this->load->library('upload', $config);


        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $attr['name'] = $data['upload_data']['orig_name'];
            $attr['filename'] = $data['upload_data']['orig_name'];
            $attr['savename'] = $data['upload_data']['file_name'];
            $attr['type'] = $data['upload_data']['file_type'];
            $attr['date'] = date("Y-m-d H:i", time());
            $attr['phase'] = "";

            $attr['project_id'] = $id;
            $attr['client_id'] = $this->client->id;
            $media = ProjectHasFile::create($attr);
            echo $media->id;

            //check image processor extension
            if (extension_loaded('gd2')) {
                $lib = 'gd2';
            } else {
                $lib = 'gd';
            }
            $config['image_library'] = $lib;
            $config['source_image'] = './files/media/' . $attr['savename'];
            $config['new_image'] = './files/media/thumb_' . $attr['savename'];
            $config['create_thumb'] = TRUE;
            $config['thumb_marker'] = "";
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 170;
            $config['height'] = 170;
            $config['master_dim'] = "height";
            $config['quality'] = "100%";


            $this->load->library('image_lib');
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();

        } else {
            echo "Upload faild";
            $error = $this->upload->display_errors('', ' ');
            $this->session->set_flashdata('message', 'error:' . $error);
            echo $error;

        }


        $this->theme_view = 'blank';
    }

    function task_change_attribute()
    {
        if ($_POST) {
            $name = $_POST["name"];
            $taskId = $_POST["pk"];
            $value = $_POST["value"];
            $task = ProjectHasTask::find_by_id($taskId);
            $task->{$name} = $value;
            $task->save();
        }
        $this->theme_view = 'blank';
    }

    function task_start_stop_timer($taskId)
    {
        $task = ProjectHasTask::find_by_id($taskId);
        if ($task->tracking != 0) {
            $diff = time() - $task->tracking;
            $task->time_spent = $task->time_spent + $diff;
            $task->tracking = "";
        } else {
            $task->tracking = time();
        }
        $task->save();
        $this->theme_view = 'blank';
    }

    public function assign($id = false)
    {
        $this->load->helper('notification');
        if ($_POST) {
            unset($_POST['send']);
            $id = addslashes($_POST['id']);
            $project = Project::find_by_id($id);
            if (!isset($_POST["user_id"])) {
                $_POST["user_id"] = array();
            }
            $query = array();
            foreach ($project->project_has_workers as $key => $value) {
                array_push($query, $value->user_id);
            }

            $added = array_diff($_POST["user_id"], $query);
            $removed = array_diff($query, $_POST["user_id"]);

            foreach ($added as $value) {
                $atributes = array('project_id' => $id, 'user_id' => $value);
                $worker = ProjectHasWorker::create($atributes);
                send_notification($worker->user->email, $this->lang->line('application_notification_project_assign_subject'), $this->lang->line('application_notification_project_assign') . '<br><strong>' . $project->name . '</strong>');
            }

            foreach ($removed as $value) {
                $atributes = array('project_id' => $id, 'user_id' => $value);
                $worker = ProjectHasWorker::find($atributes);
                $worker->delete();
            }

            $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_project_success'));
            redirect('cprojects/view/' . $id);
        } else {
            $this->view_data['users'] = User::find('all', array('conditions' => array('status=?', 'active')));
            $this->view_data['project'] = Project::find($id);
            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_assign_to_agents');
            $this->view_data['form_action'] = 'cprojects/assign';
            $this->content_view = 'projects/_assign';
        }
    }

    public function timer_reset($id = false)
    {
        $project = Project::find($id);
        $attr = array('time_spent' => '0');
        $project->update_attributes($attr);
        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_timer_reset'));
        redirect('cprojects/view/' . $id);
    }

    public function timer_set($id = false)
    {
        if ($_POST) {
            $project = Project::find_by_id($_POST['id']);
            $hours = $_POST['hours'];
            $minutes = $_POST['minutes'];
            $timespent = ($hours * 60 * 60) + ($minutes * 60);
            $attr = array('time_spent' => $timespent);
            $project->update_attributes($attr);
            $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_timer_set'));
            redirect('cprojects/view/' . $_POST['id']);
        } else {
            $this->view_data['project'] = Project::find($id);
            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_timer_set');
            $this->view_data['form_action'] = 'cprojects/timer_set';
            $this->content_view = 'projects/_timer';
        }
    }

    public function timesheets($taskid)
    {
        $this->view_data['timesheets'] = ProjectHasTimesheet::find("all", array("conditions" => array("task_id = ?", $taskid)));
        $this->view_data['task'] = ProjectHasTask::find_by_id($taskid);

        $this->theme_view = 'modal';
        $this->view_data['title'] = $this->lang->line('application_timesheet');
        $this->view_data['form_action'] = 'cprojects/timesheet_add';
        $this->content_view = 'projects/_timesheets';
    }

    public function update($id = false)
    {
        if ($_POST) {
            unset($_POST['send']);
            $id = $_POST['id'];
            unset($_POST['files']);
            $_POST = array_map('htmlspecialchars', $_POST);


            if (!isset($_POST["enable_client_tasks"])) {
                $_POST["enable_client_tasks"] = 0;
            }

            $project = Project::find_by_id($id);
            $data['id'] = $_POST['id'];
            $data['reference'] = (isset($_POST['reference'])) ? $_POST['reference'] : NULL;;;
            $data['name'] = (isset($_POST['name'])) ? $_POST['name'] : NULL;;
            $data['description'] = (isset($_POST['description'])) ? $_POST['description'] : NULL;
            $data['start'] = (isset($_POST['start'])) ? $_POST['start'] : NULL;
            $data['end'] = (isset($_POST['end'])) ? $_POST['end'] : NULL;
            $data['progress'] = (isset($_POST['progress'])) ? $_POST['progress'] : NULL;
            $data['phases'] = (isset($_POST['phases'])) ? $_POST['phases'] : NULL;
            $data['tracking'] = $_POST['tracking'];
            $data['time_spent'] = (isset($_POST['time_spent'])) ? $_POST['time_spent'] : NULL;
            $data['datetime'] = (isset($_POST['datetime'])) ? $_POST['datetime'] : NULL;;
            $data['sticky'] = (isset($_POST['sticky'])) ? $_POST['sticky'] : 0;
            $data['category'] = (isset($_POST['category'])) ? $_POST['category'] : null;
            $data['note'] = (isset($_POST['note'])) ? $_POST['note'] : null;
            $data['progress_calc'] = (isset($_POST['progress_calc'])) ? $_POST['progress_calc'] : 0;
            $data['hide_tasks'] = (isset($_POST['hide_tasks'])) ? $_POST['hide_tasks'] : 0;
            $data['enable_client_tasks'] = (isset($_POST['enable_client_tasks'])) ? $_POST['enable_client_tasks'] : 0;
            $data['enable_client_tasks'] = (isset($_POST['user_id'])) ? $_POST['user_id'] : 0;
//$data['user_id'] =(isset($_POST['user_id'])) ? $_POST['user_id'] : 0;
//            $data2['project_id'] = $_POST['id'];
//            $data2['user_id'] = $_POST['user_id'];
            $project->update_attributes($data);
//            $ProjectHasWorker = ProjectHasWorker::find('all', ['conditions' => ['project_id=?', $_POST['id']]]);
//            $ProjectHasWorker->update_attributes($data2);
//

            if (!$project) {

                $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_project_error'));
            } else {
                $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_project_success'));
            }
            redirect('cprojects/view/' . $id);
        } else {
            if ($this->user->admin == 0) {
                $this->view_data['companies'] = $this->user->companies;
            } else {
                $this->view_data['companies'] = Company::find('all', array('conditions' => array('inactive=?', '0')));
            }
            $data = SubCategories::all();
            $user = User::all();

            $i = 0;
            foreach ($data as $value) {
                $data2[$i]['name'] = $value->name;
                $data2[$i]['category_id'] = $value->category_id;
                $i++;
            }

            $this->view_data['category_list'] = $data2;
            $this->view_data['user_list'] = $user;
            $this->view_data['project'] = Project::find($id);
            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_edit_project');
            $this->view_data['form_action'] = 'cprojects/update';
            $this->content_view = 'projects/client_views/create';
        }
    }

    public function tasks($id = false, $condition = false, $task_id = false)
    {
        $this->view_data['submenu'] = array(
            $this->lang->line('application_back') => 'cprojects',
            $this->lang->line('application_overview') => 'cprojects/view/' . $id,
        );

        switch ($condition) {
            case 'add':
                $this->content_view = 'projects/client_views/_tasks';
                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['files']);
                    $description = $_POST['description'];
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['description'] = $description;
                    $_POST['value'] = str_replace(',', '', $_POST['value']);
                    $_POST['project_id'] = $id;
                    $task = ProjectHasTask::create($_POST);
                    if (!$task) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_task_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_task_success'));
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['project'] = Project::find($id);
                    $this->view_data['title'] = $this->lang->line('application_add_task');
                    $this->view_data['form_action'] = 'cprojects/tasks/' . $id . '/add';
                    $this->content_view = 'projects/client_views/_tasks';
                }
                break;
            case 'update':
                $this->content_view = 'projects/client_views/_tasks';
                $this->view_data['task'] = ProjectHasTask::find($task_id);
                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['files']);
                    if (!isset($_POST['public'])) {
                        $_POST['public'] = 0;
                    }
                    $description = $_POST['description'];
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['description'] = $description;
                    $_POST['value'] = str_replace(',', '', $_POST['value']);
                    $task_id = $_POST['id'];
                    $task = ProjectHasTask::find($task_id);

                    if ($task->user_id != $_POST['user_id']) {
                        //stop timer and add time to timesheet
                        if ($task->tracking != 0) {
                            $now = time();
                            $diff = $now - $task->tracking;
                            $timer_start = $task->tracking;
                            $task->time_spent = $task->time_spent + $diff;
                            $task->tracking = "";
                            $attributes = array(
                                'task_id' => $task->id,
                                'user_id' => $task->user_id,
                                'project_id' => $task->project_id,
                                'client_id' => 0,
                                'time' => $diff,
                                'start' => $timer_start,
                                'end' => $now
                            );
                            $timesheet = ProjectHasTimesheet::create($attributes);
                        }
                    }


                    $task->update_attributes($_POST);
                    if (!$task) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_task_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_task_success'));
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['project'] = Project::find($id);
                    $this->view_data['title'] = $this->lang->line('application_edit_task');
                    $this->view_data['form_action'] = 'cprojects/tasks/' . $id . '/update/' . $task_id;
                    $this->content_view = 'projects/client_views/_tasks';
                }
                break;
            case 'check':
                $this->theme_view = 'blank';
                $task = ProjectHasTask::find($task_id);
                if ($task->status == 'done') {
                    $task->status = 'open';
                } else {
                    $task->status = 'done';
                }
                if ($task->tracking > 0) {
                    json_response("error", htmlspecialchars($this->lang->line('application_task_timer_must_be_stopped_first')));
                }
                $task->save();
                $project = Project::find($id);
                $tasks = ProjectHasTask::count(array('conditions' => 'project_id = ' . $id));
                $tasks_done = ProjectHasTask::count(array('conditions' => array('status = ? AND project_id = ?', 'done', $id)));
                if ($project->progress_calc == 1) {
                    if ($tasks) {
                        $progress = round($tasks_done / $tasks * 100);
                    }
                    $attr = array('progress' => $progress);
                    $project->update_attributes($attr);
                }
                if (!$task) {
                    json_response("error", "Error while task toggle!");
                }
                json_response("success", "task_checked");
                break;
            case 'unlock':
                $this->theme_view = 'blank';
                $task = ProjectHasTask::find($task_id);
                $task->invoice_id = '0';
                $task->save();
                if ($task) {
                    json_response("success", htmlspecialchars($this->lang->line('application_task_has_been_unlocked')));
                } else {
                    json_response("error", htmlspecialchars($this->lang->line('application_task_has_not_been_unlocked')));
                }
                break;
            case 'delete':
                $task = ProjectHasTask::find($task_id);
                $task->delete();
                if (!$task) {
                    $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_delete_task_error'));
                } else {
                    $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_delete_task_success'));
                }
                redirect('cprojects/view/' . $id);
                break;
            default:
                $this->view_data['project'] = Project::find($id);
                $this->content_view = 'cprojects/client_views/tasks';
                break;
        }
    }

    function notes($id = FALSE)
    {
        if ($_POST) {
            unset($_POST['send']);
            $_POST = array_map('htmlspecialchars', $_POST);
            $_POST['note'] = strip_tags($_POST['note']);
            $project = Project::find($id);
            $project->update_attributes($_POST);
        }
        $this->theme_view = 'ajax';
    }

    function deletemessage($project_id, $media_id, $id)
    {
        $from = $this->client->firstname . ' ' . $this->client->lastname;
        $message = Message::find($id);
        if ($message->from == $this->client->firstname . " " . $this->client->lastname) {
            $message->delete();
        }
        if (!$message) {
            $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_delete_message_error'));
        } else {
            $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_delete_message_success'));
        }
        redirect('cprojects/media/' . $project_id . '/view/' . $media_id);
    }

    function download($media_id = FALSE, $comment_file = FALSE)
    {

        $this->load->helper('download');
        $this->load->helper('file');
        if ($media_id && $media_id != "false") {
            $media = ProjectHasFile::find($media_id);
            $media->download_counter = $media->download_counter + 1;
            $media->save();
            $file = './files/media/' . $media->savename;
        }
        if ($comment_file && $comment_file != "false") {
            $file = './files/media/' . $comment_file;
        }

        $mime = get_mime_by_extension($file);
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $mime);
            header('Content-Disposition: attachment; filename=' . basename($media->filename));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            @ob_clean();
            @flush();
            exit;
        }
    }

    function task_comment($id, $condition)
    {
        $this->load->helper('notification');
        //$project = TaskHasComment::find_by_id($id);
        switch ($condition) {
            case 'create':
                if ($_POST) {

                    $config['upload_path'] = './files/media/';
                    $config['encrypt_name'] = true;
                    $config['allowed_types'] = '*';
                    $this->load->library('upload', $config);

                    unset($_POST['send']);
                    $_POST['message'] = htmlspecialchars(strip_tags($_POST['message'], '<br><br/><p></p><a></a><b></b><i></i><u></u><span></span>'));
                    $_POST['task_id'] = $id;
                    $_POST['client_id'] = $this->client->id;
                    $_POST['datetime'] = time();

                    $attachment = false;
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors('', ' ');
                        if ($error != 'You did not select a file to upload.') {
                            //$this->session->set_flashdata('message', 'error:'.$error);
                        }
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $_POST['attachment'] = $data['upload_data']['orig_name'];
                        $_POST['attachment_link'] = $data['upload_data']['file_name'];
                        $attachment = $data['upload_data']['file_name'];
                    }
                    unset($_POST['userfile']);

                    $comment = TaskHasComment::create($_POST);
                    if (!$comment) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_success'));
                        foreach ($project->project_has_workers as $workers) {
                            send_notification($workers->user->email, "[" . $project->name . "] " . $_POST['subject'], $_POST['message'] . '<br><strong>' . $project->name . '</strong>');
                        }
                        if (is_object($project->company->client)) {
                            $access = explode(',', $project->company->client->access);
                            if (in_array('12', $access)) {
                                send_notification($project->company->client->email, "[" . $project->name . "] " . $_POST['subject'], $_POST['message'] . '<br><strong>' . $project->name . '</strong>');
                            }
                        }
                    }
                    echo "success";
                    exit;

                }
                break;
        }
    }

    function activity($id = FALSE, $condition = FALSE, $activityID = FALSE)
    {
        $this->load->helper('notification');
        $project = Project::find_by_id($id);
        //$activity = ProjectHasAktivity::find_by_id($activityID);
        switch ($condition) {
            case 'add':
                if ($_POST) {
                    unset($_POST['send']);
                    $_POST['subject'] = htmlspecialchars($_POST['subject']);
                    $_POST['message'] = strip_tags($_POST['message'], '<iframe></iframe><img><br><br/><p></p><a></a><b></b><i></i><u></u><span></span>');
                    $_POST['project_id'] = $id;
                    $_POST['client_id'] = $this->client->id;
                    $_POST['type'] = "comment";
                    unset($_POST['files']);
                    $_POST['datetime'] = time();
                    $activity = ProjectHasActivity::create($_POST);
                    if (!$activity) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_success'));
                        foreach ($project->project_has_workers as $workers) {
                            send_notification($workers->user->email, "[" . $project->name . "] " . $_POST['subject'], $_POST['message'] . '<br><strong>' . $project->name . '</strong>');
                        }
                        // if(is_object($project->company->client)){
                        // 	send_notification($project->company->client->email, "[".$project->name."] ".$_POST['subject'], $_POST['message'].'<br><strong>'.$project->name.'</strong>');
                        // }
                    }
                    //redirect('projects/view/'.$id);

                }
                break;
            case 'update':

                break;
            case 'delete':

                break;
        }

    }

    public function create()
    {
        if ($_POST) {
            unset($_POST['send']);
            $_POST['datetime'] = time();
            $_POST = array_map('htmlspecialchars', $_POST);
            unset($_POST['files']);
            $data['reference'] = $_POST['reference'];
            $data['progress'] = $_POST['progress'];
            $data['name'] = $_POST['name'];
            $data['start'] = $_POST['start'];
            $data['company_id'] = $this->client->company->id;
            $data['end'] = $_POST['end'];
            $data['category'] = $_POST['category'];
            $data['phases'] = $_POST['phases'];
            $data['description'] = $_POST['description'];
            $data['datetime'] = $_POST['datetime'];
            $data['enable_client_tasks'] = $_POST['enable_client_tasks'];
            $data['progress_calc'] = $_POST['progress_calc'];
            $project = Project::create($data);

            $ProjectHasWorker['project_id'] = $project->id;
            $ProjectHasWorker2['project_id'] = $project->id;

            $new_project_reference = $_POST['reference'] + 1;
            $project_reference = Setting::first();
            $project_reference->update_attributes(array('project_reference' => $new_project_reference));
            if (!$project) {
                $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_create_project_error'));
            } else {
                $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_create_project_success'));
                if($_POST['user_id']) {
                    $ProjectHasWorker['user_id'] = $_POST['user_id'];
                    ProjectHasWorker::create($ProjectHasWorker);
                    $ProjectHasWorker2['user_id'] = 1;
                    ProjectHasWorker::create($ProjectHasWorker2);
                }else {
                    $ProjectHasWorker['user_id'] = 1;
                    ProjectHasWorker::create($ProjectHasWorker);
                }


            }
            redirect('cprojects');
        } else {
            if ($this->user->admin == 0) {

                $this->view_data['companies'] = $this->user->companies;
            } else {
                $this->view_data['companies'] = Company::find('all', array('conditions' => array('inactive=?', '0')));
            }
            $this->view_data['next_reference'] = Project::last();
          
            $user = User::find('all', array('conditions' => array('local=?', '0')));
            $local = User::find('all', array('conditions' => array('local=?', '1')));
            $subscription = Subscription::find(['conditions' => ['company_id=?', $this->client->company->id]]);
           
            $sub_Category = SubCategories::find($subscription->sub_category);
            
            $category = Categorie::find($subscription->category);

            $this->view_data['category'] = $category;
            $this->view_data['subCategory'] = $sub_Category;
            $this->view_data['local'] = $local;
            $this->view_data['user_list'] = $user;
            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_create_project');
            $this->view_data['form_action'] = 'cprojects/create';
            $this->content_view = 'projects/client_views/create';
        }
    }

    public function milestones($id = false, $condition = false, $milestone_id = false)
    {
        $this->view_data['submenu'] = array(
            $this->lang->line('application_back') => 'cprojects',
            $this->lang->line('application_overview') => 'cprojects/view/' . $id,
        );
        switch ($condition) {
            case 'add':
                $this->content_view = 'projects/_milestones';
                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['files']);
                    $description = $_POST['description'];
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['description'] = $description;
                    $_POST['project_id'] = $id;
                    $milestone = ProjectHasMilestone::create($_POST);
                    if (!$milestone) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_milestone_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_milestone_success'));
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['project'] = Project::find($id);
                    $this->view_data['title'] = $this->lang->line('application_add_milestone');
                    $this->view_data['form_action'] = 'cprojects/milestones/' . $id . '/add';
                    $this->content_view = 'projects/_milestones';
                }
                break;
            case 'update':
                $this->content_view = 'projects/_milestones';
                $this->view_data['milestone'] = ProjectHasMilestone::find($milestone_id);
                if ($_POST) {
                    unset($_POST['send']);
                    unset($_POST['files']);
                    $description = $_POST['description'];
                    $_POST = array_map('htmlspecialchars', $_POST);
                    $_POST['description'] = $description;
                    $milestone_id = $_POST['id'];
                    $milestone = ProjectHasMilestone::find($milestone_id);
                    $milestone->update_attributes($_POST);
                    if (!$milestone) {
                        $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_save_milestone_error'));
                    } else {
                        $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_save_milestone_success'));
                    }
                    redirect('cprojects/view/' . $id);
                } else {
                    $this->theme_view = 'modal';
                    $this->view_data['project'] = Project::find($id);
                    $this->view_data['title'] = $this->lang->line('application_edit_milestone');
                    $this->view_data['form_action'] = 'cprojects/milestones/' . $id . '/update/' . $milestone_id;
                    $this->content_view = 'projects/_milestones';
                }
                break;
            case 'delete':
                $milestone = ProjectHasMilestone::find($milestone_id);

                foreach ($milestone->project_has_tasks as $value) {
                    $value->milestone_id = "";
                    $value->save();
                }
                $milestone->delete();
                if (!$milestone) {
                    $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_delete_milestone_error'));
                } else {
                    $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_delete_milestone_success'));
                }
                redirect('cprojects/view/' . $id);
                break;
            default:
                $this->view_data['project'] = Project::find($id);
                $this->content_view = 'projects/milestones';
                break;
        }
    }

    public function invoice($id = false)
    {
        if ($_POST) {
            unset($_POST['send']);
            unset($_POST['_wysihtml5_mode']);
            unset($_POST['files']);
            $project = Project::find_by_id($id);
            $values = array("project_id" => $id,
                "company_id" => $project->company_id,
                "status" => "Open",
                "reference" => $_POST["reference"],
                "issue_date" => $_POST["issue_date"],
                "due_date" => $_POST["due_date"],
                "terms" => $_POST["terms"],
                "currency" => $_POST["currency"],
                "discount" => $_POST["discount"],
                "tax" => $_POST["tax"],
                "second_tax" => $_POST["second_tax"]
            );
            $invoice = Invoice::create($values);
            $new_invoice_reference = $_POST['reference'] + 1;
            if (is_array($_POST["tasks"])) {
                foreach ($_POST["tasks"] as $value) {
                    $task = ProjectHasTask::find_by_id($value);
                    $task->invoice_id = $invoice->id;
                    $task->save();
                    $seconds = $task->time_spent;
                    $H = floor($seconds / 3600);
                    $i = ($seconds / 60) % 60;
                    $s = $seconds % 60;
                    $hours = sprintf('%0.2f', $H + ($i / 60));
                    $item_values = array("invoice_id" => $invoice->id,
                        "item_id" => 0,
                        "amount" => $hours,
                        "value" => $task->value,
                        "name" => $task->name,
                        "description" => $task->description,
                        "type" => "task",
                        "task_id" => $task->id);
                    $newItem = InvoiceHasItem::create($item_values);
                }
            }
           
            if($_POST['request']){
            $request = Request::find($_POST['request']); 
            $data = [
                'invoice_id' => intval($invoice->id),
                'value' => intval($request->sum),
                'name' => 'paymant requests',
                'amount' => '1',
                'description' => 'For paymant request #'.$request->id
                ];
                
            InvoiceHasItem::create($data);
            }

            $invoice_reference = Setting::first();
            $invoice_reference->update_attributes(array('invoice_reference' => $new_invoice_reference));
            if (!$invoice) {
                $this->session->set_flashdata('message', 'error:' . $this->lang->line('messages_create_invoice_error'));
            } else {
                $this->session->set_flashdata('message', 'success:' . $this->lang->line('messages_create_invoice_success'));
            }
            redirect('cinvoices/view/' . $invoice->id);
        } else {
          
            $request = Request::all(array('conditions' => array('company_id = ? and project_id = ?', $this->client->company->id, $id)));
            $this->view_data['requests'] = $request;
            $this->view_data['invoices'] = Invoice::all();
            $this->view_data['next_reference'] = Invoice::last();
            $this->view_data['project'] = Project::find_by_id($id);
            $this->view_data['done_tasks'] = ProjectHasTask::getDoneTasks($id);


            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_create_invoice');
            $this->view_data['form_action'] = 'cprojects/invoice/' . $id;
            $this->content_view = 'projects/client_views/_invoice';
        }
    }

    public function tracking($id = false)
    {
        $project = Project::find($id);
        if (empty($project->tracking)) {
            $project->update_attributes(array('tracking' => time()));
        } else {
            $timeDiff=time()-$project->tracking;
            $project->update_attributes(array('tracking' => '', 'time_spent' => $project->time_spent+$timeDiff));
        }
        redirect('cprojects/view/'.$id);
    }
    public function sticky($id = false)
    {
        $project = Project::find($id);
        if ($project->sticky == 0) {
            $project->update_attributes(array('sticky' => '1'));
            $this->session->set_flashdata('message', 'success:'.$this->lang->line('messages_make_sticky_success'));
        } else {
            $project->update_attributes(array('sticky' => '0'));
            $this->session->set_flashdata('message', 'success:'.$this->lang->line('messages_remove_sticky_success'));
        }
        redirect('cprojects/view/'.$id);
    }
    public function copy($id = false)
    {
        if ($_POST) {
            unset($_POST['send']);
            $id = $_POST['id'];
            unset($_POST['id']);
            $_POST['datetime'] = time();
            $_POST = array_map('htmlspecialchars', $_POST);
            unset($_POST['files']);
            if (isset($_POST['tasks'])) {
                unset($_POST['tasks']);
                $tasks = true;
            }

            $project = Project::create($_POST);
            $new_project_reference = $_POST['reference']+1;
            $project_reference = Setting::first();
            $project_reference->update_attributes(array('project_reference' => $new_project_reference));

            if ($tasks) {
                unset($_POST['tasks']);
                $source_project    = Project::find_by_id($id);
                foreach ($source_project->project_has_tasks as $row) {
                    $attributes = array(
                        'project_id' => $project->id,
                        'name' => $row->name,
                        'user_id' => '',
                        'status' => 'open',
                        'public' => $row->public,
                        'datetime' => $project->start,
                        'due_date' => $project->end,
                        'description' => $row->description,
                        'value' => $row->value,
                        'priority' => $row->priority,

                    );
                    ProjectHasTask::create($attributes);
                }
            }

            if (!$project) {
                $this->session->set_flashdata('message', 'error:'.$this->lang->line('messages_create_project_error'));
            } else {
                $this->session->set_flashdata('message', 'success:'.$this->lang->line('messages_create_project_success'));
                $attributes = array('project_id' => $project->id, 'user_id' => $this->user->id);
                ProjectHasWorker::create($attributes);
            }
            redirect('cprojects/view/'.$id);
        } else {
            $user = User::all();

            $this->view_data['user_list'] = $user;
            $this->view_data['companies'] = Company::find('all', array('conditions' => array('inactive=?','0')));
            $this->view_data['project'] = Project::find($id);
            $this->theme_view = 'modal';
            $this->view_data['title'] = $this->lang->line('application_copy_project');
            $this->view_data['form_action'] = 'cprojects/copy';
            $this->content_view = 'projects/client_views/_copy';
        }
    }
}