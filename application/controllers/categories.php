<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Categories extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $access = FALSE;
        if ($this->client) {

            redirect('ccategories');
        } elseif ($this->user) {
            foreach ($this->view_data['menu'] as $key => $value) {
                if ($value->link == "categories") {
                    $access = TRUE;
                }
            }
            if (!$access) {
                redirect('login');
            }
        } else {
            redirect('login');
        }

        $this->view_data['submenu'] = array(
            $this->lang->line('application_all') => 'categories',
            $this->lang->line('application_Active') => 'categories/filter/active',
            $this->lang->line('application_Inactive') => 'categories/filter/inactive',
            $this->lang->line('application_ended') => 'categories/filter/ended',

        );

    }

    public function index()
    {
        $this->view_data['categories'] = Categorie::all();

        $this->content_view = 'categories/categories';
    }

    public function edite_categories()
    {
        if ($_POST) {
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['description'] = $_POST['description'];
            $data['icon'] = $_POST['icon'];
            $pages = Categorie::find_by_id($data['id']);
            $pages->update_attributes($data);
            $id = $data['id'];
            $this->view_data['categories'] = Categorie::find($data['id']);
            $this->view_data['subCategories'] = SubCategories::subCategory($id);
            $this->content_view = 'categories/editeCategories';
        } else {
            $id = $this->uri->segment('3');
            $this->view_data['subCategories'] = SubCategories::subCategory($id);
            $this->view_data['categories'] = Categorie::find($id);
            $this->content_view = 'categories/editeCategories';
        }

    }

    public function create()
    {
        if ($_POST) {
            $data['name'] = $_POST['name'];
            $data['description'] = $_POST['description'];
            $data['icon'] = $_POST['icon'];
            $pages = Categorie::create($data);
            redirect('/categories');
        } else {
            $this->theme_view = 'modal';
            $this->content_view = 'categories/editeCategories';
        }

    }

    function deleteCategory()
    {

        $id = $this->uri->segment('3');
        $data = Categorie::find($id);
        $data->delete();
        redirect('/categories');
    }

    function createSubCategory()
    {

        if ($_POST) {
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['categories'];
            SubCategories::create($data);
            redirect('/categories');
        } else {
            $this->view_data['categories'] = Categorie::all();
            $this->theme_view = 'modal';
            $this->content_view = 'categories/editeSubCategories';
        }
    }

    function editSubCategory()
    {

        $id = $this->uri->segment('3');
        $this->view_data['subCategories'] = SubCategories::subCategory($id);

        $this->content_view = 'categories/subCategory';


    }
    function deleteSubCategories()
    {

        $id = $this->uri->segment('3');
        $data = SubCategories::find($id);
        $data->delete();
        redirect('/categories');
    }
    function subCategories()
    {
        $this->view_data['categories'] = Categorie::all();
        $id = $this->uri->segment('3');
        if ($_POST) {
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['categories'];
            $pages = SubCategories::find($id);
            $pages->update_attributes($data);
            redirect('/categories');
        } else {
            $data = SubCategories::find($id);
            $data2['name'] = $data->name;
            $data2['category_id'] = $data->category_id;
            $this->view_data['subCategories'] = $data2;
            $this->content_view = 'categories/editeSubCategories';
        }

    }


}