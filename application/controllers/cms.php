<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cms extends MY_Controller
{


    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        $this->content_view = 'cms/cms';
    }

    public function newPage()
    {

        $this->content_view = 'cms/newPage';
    }

    public function allPages()
    {
       
     //   $data = Pages::getAllPages();
      
        //$this->view_data['pages'] = $data;
        
        $this->theme_view = 'application';
          
        $this->content_view = 'cms/update';
    }

    public function createNewPage()
    {
        $data['title'] = $_POST['title'];
        $data['name'] = $_POST['name'];
        $data['words'] = $_POST['keyWords'];
        $data['description'] = $_POST['description'];
        $data['body'] = str_replace('style1','style',$_POST['body']);
        Pages::create($data);
        $this->content_view = 'cms/newPage';
    }

    public function updatePage()
    {
        $id = $this->uri->segment('2');
        $data = Pages::findById($id);
        $this->view_data['page'] = $data;
        $this->content_view = 'cms/newPage';
    }
    public function updateMainPage()
    {
        $id = $this->uri->segment('2');
        $data = MainPages::findById($id);
        $this->view_data['page'] = $data;

        $this->content_view = 'cms/newPage';
    }
    public function updatePageById()
    {

        $data['title'] = $_POST['title'];
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['words'] = $_POST['keyWords'];
        $data['description'] = $_POST['description'];
        $data['body'] = str_replace('style1','style',$_POST['body']);
        $pages = Pages::find_by_id($data['id']);
        $pages->update_attributes($data);
        $this->view_data['page'] = $data;
        $this->content_view = 'cms/newPage';
    }
    public function deletePage()
    {

        $id = $this->uri->segment('2');
        $data = Pages::find($id);
        $data->delete();
        redirect('/all-pages');

    }
}