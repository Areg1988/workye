<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_requests extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`employes_id` int NULL ');
        $this->dbforge->add_field('`company_id` int NULL ');
        $this->dbforge->add_field('`sum` int NULL ');
        $this->dbforge->add_field('`project_id` int NULL ');
        $this->dbforge->add_field('`description` text NULL ');
        $this->dbforge->add_field('`status` varchar(250) NULL ');
         $this->dbforge->add_field('`type` varchar(30) NULL ');
        $this->dbforge->create_table('requests', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('requests', true);
    }
}
