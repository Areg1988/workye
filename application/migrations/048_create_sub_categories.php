<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_sub_categories extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`name` varchar(250) NULL ');

        $this->dbforge->add_field('`category_id` int NULL ');
        $this->dbforge->create_table('sub_categories', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('sub_categories', true);
    }
}
