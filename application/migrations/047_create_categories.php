<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_categories extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`name` varchar(250) NULL ');
        $this->dbforge->add_field('`description` varchar(250) NULL ');
 $this->dbforge->add_field('`icon` text NULL ');
        $this->dbforge->create_table('categories', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('categories', true);
    }
}
