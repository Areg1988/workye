<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_employes extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`user_id` int NULL ');
        $this->dbforge->add_field('`address` varchar(250) NULL ');
        $this->dbforge->add_field('`zipcode` varchar(250) NULL ');
        $this->dbforge->add_field('`city` varchar(250) NULL ');
        $this->dbforge->add_field('`country` varchar(250) NULL ');
        $this->dbforge->add_field('`cvInput` varchar(250) NULL ');
        $this->dbforge->create_table('employes', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('employes', true);
    }
}
