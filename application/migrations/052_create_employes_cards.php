<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_employes_cards extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`employes_id` int NULL ');
        $this->dbforge->add_field('`card_id` varchar(250) NULL ');
        $this->dbforge->add_field('`last` varchar(30) NULL ');
        $this->dbforge->add_field('`brand` varchar(30) NULL ');
        $this->dbforge->create_table('employes_cards', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('employes_cards', true);
    }
}
