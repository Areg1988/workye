<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_main_pages extends CI_Migration
{
    public function up()
    {
        ## Create Table pages
        $this->dbforge->add_field('`id` int(11) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`name` varchar(65) NULL ');
        $this->dbforge->add_field('`description` text NULL ');
        $this->dbforge->add_field('`title` varchar(100) NULL ');
        $this->dbforge->add_field("`keyWord` varchar(255) NULL ");
        $this->dbforge->add_field('`body` longtext NULL ');
        $this->dbforge->create_table('pages', true);
    }

    public function down()
    {
        ### Drop table pages ##
        $this->dbforge->drop_table('main_pages', true);
    }
}
