<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_subscriptions extends CI_Migration
{
    public function up()
    {
        ## Create Table tickets
        $this->dbforge->add_field('`id` bigint(20) NOT NULL auto_increment');
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('`company_id` int NULL ');
        $this->dbforge->add_field('`subCategory` varchar(250) NULL ');
        $this->dbforge->add_field('`category` varchar(250) NULL ');
        $this->dbforge->add_field('`sum` varchar(250) NULL ');
        $this->dbforge->add_field('`subscription_id` varchar(250) NULL ');
        $this->dbforge->add_field('`status` varchar(250) NULL ');
        $this->dbforge->create_table('subscriptions', true);
    }

    public function down()
    {
        ### Drop table tickets ##
        $this->dbforge->drop_table('subscriptions', true);
    }
}
