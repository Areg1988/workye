<?php

class MainPages extends ActiveRecord\Model
{
    static $table_name = 'main_pages';

    public static function findByName($name)
    {
        $query = 'SELECT * FROM `main_pages` WHERE `name` = "' . $name . '"';

        return MainPages::find_by_sql($query);

    }

    public static function getAllPages()
    {

        $query = 'SELECT * FROM `main_pages` WHERE 1';

        return MainPages::find_by_sql($query);

    }

    public static function findById($id)
    {
        $query = 'SELECT * FROM `main_pages` WHERE `id` = "' . $id . '"';

        return MainPages::find_by_sql($query);

    }

    public static function updatePage($data)
    {

        $query = "UPDATE `main_pages` SET `id`=".$data['id'].",`name`= '".$data['name']."',`description`= '".$data['description']."', `title`= '".$data['title']."', `words`='".$data['words']."',`body`= '".$data['body']."' ";

        return MainPages::find_by_sql($query);
    }
    public static function deletePageById($id)
    {

        $query = "DELETE  FROM `main_pages` WHERE `id` =  $id";
        //$this->Model->delete($id);
        // return Pages::find_by_sql($query);

    }
    public static function getMainPageById($id)
    {

        $query = 'SELECT * FROM `main_pages` WHERE `id` = "' . $id . '"';

        return MainPages::find_by_sql($query);

    }
}