<?php

class SubCategories extends ActiveRecord\Model
{
    static $table_name = 'sub_categories';

    public static function subCategory($id)
    {
        $query = 'SELECT  sub_categories.id, sub_categories.name, categories.name as category
                  FROM sub_categories
                  INNER JOIN categories
                  ON sub_categories.category_id = categories.id where sub_categories.category_id = ' .$id;
        return SubCategories::find_by_sql($query);
    }
    public static function getAll()
    {
        $query = 'SELECT * FROM `sub_categories` WHERE 1';
        return SubCategories::find_by_sql($query);
    }
    public static function getByCategory($category)
    {
        $query = 'SELECT * FROM `sub_categories` WHERE category_id ='. $category;
        return SubCategories::find_by_sql($query);
    }

}
