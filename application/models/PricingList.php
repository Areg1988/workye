<?php

class PricingList extends ActiveRecord\Model
{

    static $table_name = 'pricing';

    public static function pricing()
    {
        $query = 'SELECT * FROM `pricing` WHERE 1';

        return PricingList::find_by_sql($query);
    }

    public static function pricingById($id)
    {
        $query = 'SELECT * FROM `pricing` WHERE id = ' . $id;

        return PricingList::find_by_sql($query);
    }

}