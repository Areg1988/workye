<?php

class Company extends ActiveRecord\Model {

    public static  $has_many = array(
	array('clients', 'conditions' => 'inactive != 1'),
    array('invoices'),
    array('projects'),

    array('company_has_admins'),
    array('users', 'through' => 'company_has_admins')
    );

    static $belongs_to = array(
    array('client', 'conditions' => 'inactive != 1')
    );
}