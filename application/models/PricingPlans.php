<?php

class PricingPlans extends ActiveRecord\Model
{
    static $table_name = 'pricing_plans';

    static $belongs_to = array(
        array('company')
    );
    static $has_many = array(
        array('subscription_has_items'),
        array('invoices')
    );

    public static function newInvoiceOutstanding($comp_array, $date)
    {
        $newInvoiceOutstanding = '';
        $newInvoiceOutstanding = PricingPlans::find_by_sql("SELECT
			 	* 
			FROM 
				`pricing_plans` 
			WHERE 
				`status` != 'Inactive' 
			AND 
				`end_date` > '$date' 
			AND 
				`next_payment` <= '$date' 
			ORDER BY 
				`next_payment`
		");

        return $newInvoiceOutstanding;
    }



    public static function all()
    {
        $query = 'SELECT * FROM `pricing_plans` WHERE 1';

        return PricingPlans::find_by_sql($query);
    }

}



