<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <META Http-Equiv="Cache-Control" Content="no-cache">
    <META Http-Equiv="Pragma" Content="no-cache">
    <META Http-Equiv="Expires" Content="0">
    <meta name="robots" content="none"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="18000">
    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <script>(function (d) {
            var config = {
                    kitId: 'gnf7xqu',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement, t = setTimeout(function () {
                    h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                s = d.getElementsByTagName("script")[0], a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function () {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {
                }
            };
            s.parentNode.insertBefore(tk, s)
        })(document);</script>
    <title><?= $core_settings->company; ?></title>

    <link href="<?= base_url() ?>assets/blueline/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/blueline/css/blueline.css?ver=<?= $core_settings->version; ?>" rel="stylesheet">
    <link href="<?= base_url() ?>assets/blueline/css/user.css?ver=<?= $core_settings->version; ?>" rel="stylesheet"/>
    <?= get_theme_colors($core_settings); ?>
    <script type="text/javascript" src="<?= base_url() ?>/assets/blueline/js/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>']
            = '<?php echo $this->security->get_csrf_hash(); ?>';
    </script>
</head>
<style>
    .header-button {
        padding: 10px 10px;
        border: 1px solid black;
        text-align: center;
        cursor: pointer;
    }

    body {
        background: #e3e6ed;
    }

    body, label, div, input, label {
        font-family: "sofia-pro", "Roboto", "Helvetica", "Arial", sans-serif;

    }
    .form-signin {
        max-width: 490px;
        padding: 70px;
    }
</style>

<body class="login"
      style="background-image:url('<?= base_url() ?>assets/blueline/images/backgrounds/<?= $core_settings->login_background; ?>')">
<div class="container-fluid">
    <div class="row form-signin form-register box-shadow" style="margin: 150px auto;min-height: 50px;">
        <div class="logo">
            <div class="jsx-3980414583 homepage-logo" style="width:140px;margin:  0 auto">
                <img src="/assets/blueline/images/workye.com.svg" width="100%">
            </div>
        </div>
        <hr>
        <div class='step-five' id='stepOne'>
            <div id="resendEmail">
                <div class="col-xs-4">
                    <img src="/assets/blueline/images/activation.gif" style="margin-top: 25px;" width="145px"
                         alt=”animated”>
                </div>
                <div style='margin-bottom: 15px;text-align: center;' class="col-xs-8">

                    <div style="    text-align: left;">
                        <h2 style="margin: 0 0 10px 0;font-weight: 400;text-align: left;">Check your Inbox</h2>
                        <p style="text-align: left;font-size: 12px;color: gray;">
                            We've sent a confirmation tou you inbox to verify your email and instruction for the next
                            step.
                            <br>
                            Haven`t received anything yet?</p>
                        <a href="/welcome/sendEmail?id=<?= $client->id ?>&type=<?= $type ?>"
                           class="btn btn-danger"
                           style='    text-transform: capitalize;
    font-size: 17px!important;
    border-radius: 5px;
    padding: 10px 20px;'>Resend Email
                        </a>

                    </div>

                </div>

            </div>

            <div style='margin-bottom: 15px;text-align: center;' class="row">


                <div id="activated" style="display: none">

                    <div style='margin-bottom: 15px;text-align: center;' class="col-xs-12">
                        <div class="col-xs-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="109" height="105" viewBox="0 0 109 105">
                                <g fill="none" fill-rule="evenodd">
                                    <path fill="#E6E6E6"
                                          d="M88.307 48.924v53.893c0 .727-.59 1.32-1.314 1.32H2.46a1.32 1.32 0 0 1-1.315-1.32V48.924c0-.726.442-2.112.885-2.543L44.887 5.02c.268-.27.71-.27.99.013l41.558 41.36c.43.43.872 1.805.872 2.53"/>
                                    <path stroke="#000" stroke-width="1.2"
                                          d="M88.307 48.924v53.893c0 .727-.59 1.32-1.314 1.32H2.46a1.32 1.32 0 0 1-1.315-1.32V48.924c0-.726.442-2.112.885-2.543L44.887 5.02c.268-.27.71-.27.99.013l41.558 41.36c.43.43.872 1.805.872 2.53z"/>
                                    <path fill="#FFF"
                                          d="M79.86.6H9.184c-.57 0-1.06.485-1.06 1.05v98.686c0 .565.49 1.05 1.06 1.05H79.86c.573 0 1.062-.485 1.062-1.05V1.65c.082-.565-.407-1.05-1.06-1.05"/>
                                    <path stroke="#1B1B1B" stroke-width="1.2"
                                          d="M79.86.6H9.184c-.57 0-1.06.485-1.06 1.05v98.686c0 .565.49 1.05 1.06 1.05H79.86c.573 0 1.062-.485 1.062-1.05V1.65c.082-.565-.407-1.05-1.06-1.05z"/>
                                    <path stroke="#3BD0D6" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="1.2"
                                          d="M46.644 83.62h28.402"/>
                                    <path stroke="#1B1B1B" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width=".6"
                                          d="M17.834 84.185c-.163.08-.327.16-.408.16-.08 0-.163.08-.163.163 0 .08.163.08.244.08 1.143-.403 2.123-1.21 2.94-2.1.652-.726 1.14-1.534 1.305-2.503 0-.162 0-.323-.082-.484a.63.63 0 0 0-.408-.16c-.327 0-.57.16-.816.322-.245.242-.327.485-.49.727-.897 1.614-1.306 3.47-1.387 5.41 0 .484 0 .888.244 1.292.245.404.652.727 1.06.727"/>
                                    <path stroke="#1B1B1B" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width=".6"
                                          d="M19.467 83.86c.326-.16.652-.24.98-.32v.08m.488.08c-.244.08-.408.162-.57.242-.082 0-.082.08-.164.162-.08.16.164.242.246.323.49.16 1.06.16 1.632.16 2.612 0 5.223-.08 7.916-.24-.57 0-.978.645-1.55.565"/>
                                    <path d="M16.447 39.444H43.46M21.915 28.462h47.5M47.95 39.444h27.096m-58.6 3.473H43.46m4.49 0h27.096m-58.6 3.553H43.46m4.49 0h27.096m-58.6 3.554H43.46m4.49 0h27.096m-58.6 3.553H43.46m4.49 0h27.096m-58.6 3.553H43.46m4.49 0h27.096m-58.6 3.554H43.46m4.49 0h27.096m-58.6 3.553H43.46m4.49 0h27.096m-58.6 3.553H43.46m-27.013 3.554h14.527m-14.527 18.01h14.527m28.076 0h15.996m-58.6-2.827H33.26m-2.776-54.512h30.443M32.443 11.018h26.443"
                                          stroke="#3BD0D6" stroke-width="1.2"/>
                                    <path fill="#FFF"
                                          d="M88.307 75.87v26.947c0 .727-.59 1.32-1.314 1.32H2.46a1.318 1.318 0 0 1-1.315-1.32V75.87h87.162z"/>
                                    <path stroke="#1A1A1A" stroke-width="1.2"
                                          d="M88.307 75.87v26.947c0 .727-.59 1.32-1.314 1.32H2.46a1.318 1.318 0 0 1-1.315-1.32V75.87h87.162z"/>
                                    <path fill="#FFF"
                                          d="M1.145 48.55v54.64c0 .414.436.667.773.45l42.567-27.32a.54.54 0 0 0 0-.898L1.918 48.102c-.337-.217-.773.036-.773.448"/>
                                    <path stroke="#1A1A1A" stroke-width="1.2"
                                          d="M1.145 48.55v54.64c0 .414.436.667.773.45l42.567-27.32a.54.54 0 0 0 0-.898L1.918 48.102c-.337-.217-.773.036-.773.448z"/>
                                    <path fill="#FFF"
                                          d="M88.307 48.55v54.64a.522.522 0 0 1-.798.45L43.632 76.318a.53.53 0 0 1 0-.897L87.51 48.1a.522.522 0 0 1 .797.45"/>
                                    <path stroke="#1A1A1A" stroke-width="1.2"
                                          d="M88.307 48.55v54.64a.522.522 0 0 1-.798.45L43.632 76.318a.53.53 0 0 1 0-.897L87.51 48.1a.522.522 0 0 1 .797.45z"/>
                                    <g>
                                        <path fill="#39D0D6"
                                              d="M81.566 32.2c.635-2.824 3.15-4.932 6.154-4.932 3.005 0 5.52 2.108 6.155 4.93a6.295 6.295 0 0 1 7.825.882 6.354 6.354 0 0 1 .88 7.855 6.33 6.33 0 0 1 4.91 6.177 6.33 6.33 0 0 1-4.91 6.178 6.353 6.353 0 0 1-.88 7.854 6.294 6.294 0 0 1-7.825.882c-.636 2.823-3.15 4.93-6.155 4.93-3.004 0-5.52-2.107-6.154-4.93a6.294 6.294 0 0 1-7.825-.882 6.353 6.353 0 0 1-.88-7.854 6.33 6.33 0 0 1-4.91-6.178 6.33 6.33 0 0 1 4.912-6.177 6.354 6.354 0 0 1 .88-7.855 6.295 6.295 0 0 1 7.824-.88"/>
                                        <path stroke="#1A1A1A" stroke-width="1.2"
                                              d="M81.566 32.2c.635-2.824 3.15-4.932 6.154-4.932 3.005 0 5.52 2.108 6.155 4.93a6.295 6.295 0 0 1 7.825.882 6.354 6.354 0 0 1 .88 7.855 6.33 6.33 0 0 1 4.91 6.177 6.33 6.33 0 0 1-4.91 6.178 6.353 6.353 0 0 1-.88 7.854 6.294 6.294 0 0 1-7.825.882c-.636 2.823-3.15 4.93-6.155 4.93-3.004 0-5.52-2.107-6.154-4.93a6.294 6.294 0 0 1-7.825-.882 6.353 6.353 0 0 1-.88-7.854 6.33 6.33 0 0 1-4.91-6.178 6.33 6.33 0 0 1 4.912-6.177 6.354 6.354 0 0 1 .88-7.855 6.295 6.295 0 0 1 7.824-.88z"/>
                                        <path stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2.4"
                                              d="M81.827 47.79l3.845 3.86 8.57-8.603"/>
                                    </g>
                                </g>
                            </svg>

                        </div>
                        <div style="    text-align: left;">

                            <div class='col-xs-4 '>
                                <h2 style="margin: 0 0 10px 0;font-weight: 400;text-align: left;">Email verified!</h2>
                                <p style="text-align: left;font-size: 12px;color: gray;">
                                    Login to your workye account with details to complete signup
                                </p>


                                <a class="btn btn-danger" id="continue" type="button" style="text-transform: capitalize;
    font-size: 17px!important;
    border-radius: 5px;
    padding: 10px 20px;" href='/login'>
                                    Login
                                </a>


                            </div>
                            </a>

                        </div>

                    </div>

                </div>

            </div>


        </div>

    </div>
</div>
</div>


<script>
    $(document).ready(function () {
        var url = window.location.href

        var token = csfrData['fcs_csrf_token'];
        var pathname = new URL(url).pathname;
        var urlPathname = pathname.split('/');
        var clientId = urlPathname[2];
        $("#tokenDiv").val(token)
        var myVar = setInterval(function () {
            check()
        }, 3000);

        function check() {
            $.ajax({
                url: "/welcome/checkActivatonEnp",
                type: "POST",
                data: {id: clientId, fcs_csrf_token: token},
                success: function (data) {
                    var activite = JSON.parse(data)
                    if (activite.data == 'inactive') {
                    } else {
                        clearInterval(myVar);
                        $('#continue').removeAttr('disabled')
                        $('#resendEmail').css('display', 'none')
                        $('#activated').css('display', 'block')
                    }
                }
            });
        }
    })

</script>

<script type="text/javascript" src="/assets/blueline/js/typekit.js"></script>

</body>
</html>




