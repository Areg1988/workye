<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <META Http-Equiv="Cache-Control" Content="no-cache">
    <META Http-Equiv="Pragma" Content="no-cache">
    <META Http-Equiv="Expires" Content="0">
    <meta name="robots" content="none"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="18000">

    <title><?= $core_settings->company; ?></title>

    <link href="<?= base_url() ?>assets/blueline/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?= base_url() ?>assets/blueline/css/blueline.css?ver=<?= $core_settings->version; ?>" rel="stylesheet">
    <link href="<?= base_url() ?>assets/blueline/css/user.css?ver=<?= $core_settings->version; ?>" rel="stylesheet"/>
    <?= get_theme_colors($core_settings); ?>

    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<style>
    .header-button {
        padding: 10px 10px;
        border: 1px solid black;
        text-align: center;
        cursor: pointer;
    }
</style>

<body class="login"
      style="background-image:url('<?= base_url() ?>assets/blueline/images/backgrounds/<?= $core_settings->login_background; ?>')">
<div class="container-fluid">

    <div class="row form-signin form-register box-shadow">
        <div class="logo">
            <div class="jsx-3980414583 homepage-logo" style="width:140px;margin:  0 auto">
                <img src="/assets/blueline/images/workye.com.svg" width="100%">
            </div>
        </div>

        <div class='step-five' id='stepFiveDivEnp'>
            <div class="header" style="margin-bottom: 30px">

                <hr>
            </div>

            <div style='margin-bottom: 15px;text-align: center;' class="row">

                <div class='col-xs-12 '>
                    Congratulations. your email address was confirm. Now continue to your account. login button.

                </div>
            </div>
        </div>

    </div>
</div>

</div>


</body>
</html>




