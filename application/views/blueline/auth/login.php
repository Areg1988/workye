<?php $attributes = ['class' => 'form-signin box-shadow', 'role' => 'form', 'id' => 'login']; ?>
<?= form_open('login', $attributes) ?>
<div class="logo">
    <div class="jsx-3980414583 homepage-logo" style="width:140px;margin:  0 auto">
        <img src="/assets/blueline/images/workye.com.svg" width="100%">
    </div>
</div>
<?php if ($error == 'true') {
    $message = explode(':', $message) ?>
    <div id="error">
        <?= $message[1] ?>
    </div>
    <?php
} ?>



<div class="form-group" style="border: none;">

    <input type="username" class="form-control" style="min-height: 55px;background-color: #f8f8f8; border-radius: 5px" id="username" name="username"
           placeholder="<?= $this->lang->line('application_enter_your_username'); ?>"/>
</div>
<div class="form-group" style="border: none;">

    <input type="password"  style="min-height: 55px;background-color: #F8F8F8;border-radius: 5px" class="form-control" id="password" name="password"
           placeholder="<?= $this->lang->line('application_enter_your_password'); ?>" autocomplete="off" />
</div>

<?php if ($this->config->item('recaptcha_web_key') != '') {
    ?>
    <div class="g-recaptcha" data-sitekey="<?= $this->config->item('recaptcha_web_key'); ?>"
         data-bind="recaptcha-submit" data-callback="submitForm"></div>
    <?php
} ?>

<input
        type="submit"
        style="display: block;width: 100%; height: 45px;line-height: 1;margin-bottom: 20px;border-radius: 5px"
        id="recaptcha-submit"
        value="<?= $this->lang->line('application_login'); ?>"
        class="btn btn-primary fadeoutOnClick"
/>
<div class="forgotpassword" style=" margin-bottom: 20px;   text-align: center;
    width: 100%;">
    <a href="<?= site_url('forgotpass'); ?>"><?= $this->lang->line('application_forgot_password'); ?></a></div>

<div class="sub">
    <?php if ($core_settings->registration == 1) {
        ?>
        <div class="small">
        <small><?= $this->lang->line('application_you_dont_have_an_account'); ?></small></div>
        <hr/><a href="<?= site_url('register'); ?>"
                class="btn btn-danger" style="display: block; height: 45px;  border-radius: 5px;  line-height: 3;"><?= $this->lang->line('application_create_account'); ?></a> <?php
    } ?>
</div>
<?php if ($this->config->item('recaptcha_web_key') != '') {
    ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php
} ?>

<?= form_close() ?>

