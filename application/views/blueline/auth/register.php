<?php
$validationError = $_GET['validationError'];
if ($validationError) {
    $error = 'The Email already taken';
}
?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<style>
    .header-button {
        padding: 20px 30px;
        cursor: pointer;
    }

    .button-continue {
        width: 100%;
        height: 45px;
        font-weight: bolder;
        font-size: 14px !important;
    }

    .button-back {
        height: 45px;
        background-color: #00d647;
        font-weight: bolder;
        font-size: 14px !important;
    }

    .input-class {
        min-height: 55px;
        background-color: #f8f8f8 !important;
        border-radius: 5px;
        border: none !important;
        padding: 0
    }

    .form-group-class {
        border: none !important;
        padding: 0
    }

    .form-group {
        border: none !important;
        padding: 0 !important;
    }

    .button-header {
        text-transform: uppercase;
        background-color: #f8f8f8;
        border: none;
        text-align: left;
        color: #858585;
    }
</style>
<div class="row form-signin form-register box-shadow">
    <div class="logo">
        <div class="jsx-3980414583 homepage-logo" style="width:140px;margin:  0 auto">
            <img src="/assets/blueline/images/workye.com.svg" width="100%">
        </div>
    </div>
    <div class="row">
        <div class="row" style="color: #858585; text-align: center">
            <small id="selecyHow">Select how you will use workye</small>
        </div>
        <div class="col-xs-3"></div>
        <div id="client" class="col-xs-3 header-button button-header" style="margin-right: 10px;">I WANT TO <br>
            <sctrong style="color: rgb(0, 214, 71);font-weight: bolder">HIRE SOME ONE</sctrong>
        </div>
        <div id="employee" class="col-xs-3 header-button button-header">I`M LOOKING <br>
            <strong style="color: #d43f3a; font-weight: bolder">FOR WORK</strong>
        </div>
        <div id="clientHEader" class="row header-button " style="display: none; margin-bottom: 0; text-align: center">I
            WANT TO
            <sctrong style="color: rgb(0, 214, 71);font-weight: bolder">HIRE SOME ONE</sctrong>
        </div>
        <div id="empHEader" class="row header-button " style="display: none; margin-bottom: 0; text-align: center">I`M
            LOOKING
            <sctrong style="color: #d43f3a;font-weight: bolder">FOR WORK</sctrong>
        </div>
    </div>

    <div id="clientDiv" style="display: none">
        <?php $attributes = ['class' => '', 'role' => 'form', 'id' => 'register', 'enctype' => 'multipart/form-data'] ?>
        <?= form_open($form_action, $attributes) ?>

        <?php if ($error != 'false') {
            ?>
            <div id="error" style="display:block">
                <?= $error ?>
            </div>
            <?php
        } ?>
        <div class="row">
            <input type="hidden" name="pricingPlan" id="pricingPlan" value="<?= $_GET['plan']; ?>">
            <div class="step-four" id='stepOneDiv' style=''>

                <div class=" row" style="">
                    <div class="col-xs-offset-3 col-xs-6 form-group <?php if (isset($registerdata)) {
                        echo 'has-error';
                    } ?>" style="border: none;padding: 0">

                        <input id="email" type="email" name="email" class="required email form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['email'];
                               } ?>" placeholder="EMAIL ADDRESS" required/>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-3 form-group col-xs-6 input-class"
                         style="border: none;  padding: 0 10px;">
                        <label for="imgIcon">Logo input</label>
                        <input name="imgIcon" type="file">

                    </div>
                </div>

                <div class='row'>
                    <!-- Indicates a successful or positive action -->
                    <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;"><a href="/register" style="color: white">GO BACK</a></button>
                    </div>
                    <div class="col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" id='stepFirst'>CONTINUE
                        </button>
                    </div>
                </div>


            </div>
            <div class='step-one' id='stepTwoDiv' style="display: none">

                <div class="header" style="margin-bottom: 30px">
                    <hr>
                </div>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 form-group input-class" style="border: none;padding: 0">

                        <input id="firstname" type="text" name="firstname" class=" form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['firstname'];
                               } ?>" placeholder="FIRST NAME" required/>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 form-group input-class" style="border: none;padding: 0">

                        <input id="lastname" type="text" name="lastname" class="required form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['lastname'];
                               } ?>" placeholder="LAST NAME" required/>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 form-group input-class" style="border: none;padding: 0">

                        <input id="name" type="text" name="name" class="required form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['name'];
                               } ?>" placeholder="COMPANY NAME" required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 form-group input-class" style="border: none;padding: 0">

                        <input id="website" type="text" name="website" class="required form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['website'];
                               } ?>" placeholder="COMPANY WEBSITE"/>
                    </div>
                </div>


                <div class='row'>
                    <!-- Indicates a successful or positive action -->
                    <div class="col-xs-offset-3 col-xs-3" style="padding: 0">

                        <button type="button" class="btn btn-success button-back" id='backStepOne'>GO BACK</button>
                    </div>
                    <div class="col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-danger button-continue " id='stepTwo' style='width:100%'>
                            CONTINUE
                        </button>

                    </div>
                </div>
            </div>
            <div class='step-two' style="display:none" id='stepThreeDiv'>

                <div class="header" style="margin-bottom: 30px">
                    <hr>
                </div>
                <div class="row">

                    <div class="col-xs-offset-3 col-xs-6 form-group">

                        <input id="address" type="text" name="address" class="form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['address'];
                               } ?>" placeholder="ADDRESS" required/>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-xs-offset-3 col-xs-6 form-group">

                        <input id="zipcode" type="text" name="zipcode" class="form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['zipcode'];
                               } ?>" placeholder="ZIP CODE" required/>
                    </div>

                </div>
                <div class='row'>

                    <div class="col-xs-offset-3 col-xs-6 form-group">

                        <input id="city" type="text" name="city" class="form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['city'];
                               } ?>" placeholder="CITY/TOWN" required/>
                    </div>
                </div>
                <div class='row'>

                    <div class="col-xs-offset-3 col-xs-6 form-group">

                        <input id="country" type="text" name="country" class="form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['country'];
                               } ?>" placeholder="COUNTRY" required/>
                    </div>
                </div>

                <div class='row'>
                    <!-- Indicates a successful or positive action -->
                    <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-success button-back" id='backTwoDiv'>GO BACK</button>

                    </div>
                    <div class="col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-danger button-continue " style='width:100%' id='blocTree'>
                            CONTINUE
                        </button>

                    </div>
                </div>


            </div>
            <div class="step-three" id='stepFourDiv' style='display:none'>
                <div class="header" style="margin-bottom: 30px">
                    <hr>
                </div>
                <div class='row'>
                    <div class="col-xs-offset-3 col-xs-6 form-group">
                        <input id="password" type="password" name="password" class="form-control input-class" value=""
                               placeholder="PASSWORD" required/>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-xs-offset-3 col-xs-6 form-group">
                        <input id="confirm_password" type="password" class="form-control input-class"
                               placeholder="RE PASSWORD" data-match="#password"
                               required/>
                    </div>
                </div>
                <div class='row'>
                    <!-- Indicates a successful or positive action -->
                    <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-success button-back" id='backToTreeDiv'>GO BACK</button>

                    </div>
                    <div class="col-xs-3" style="padding: 0">

                        <input type="submit" id='blocFour' class="btn btn-danger button-continue " style='width:100%'
                               value="<?= $this->lang->line('application_send'); ?>"/>
                    </div>
                </div>


            </div>


        </div>

        <div class="row">

            <div class="col-md-6" style='display:none'>

                <input type="submit" class="btn btn-success" value="<?= $this->lang->line('application_send'); ?>"/>
            </div>
        </div>
        <?= form_close() ?>
        <script>
            $('#stepFirst').click(function () {
                var emailInput = $('#email').val();
                if (emailInput.length && $('#email').parents().hasClass('has-error has-danger') !== true) {
                    $('#stepOneDiv').css('display', 'none')
                    $('#stepTwoDiv').css('display', 'block')
                }

            })
            $("#backStepOne").click(function () {

                $('#stepOneDiv').css('display', 'block')
                $('#stepTwoDiv').css('display', 'none')

            })
            $("#stepTwo").click(function () {
                var firstname = $('#firstname').val();
                var lastname = $('#lastname').val();
                var name = $('#name').val();
                var website = $('#website').val();
                if (firstname.length && lastname.length && name.length && website.length) {
                    $('#stepThreeDiv').css('display', 'block')
                    $('#stepTwoDiv').css('display', 'none')
                }
            })
            $("#backTwoDiv").click(function () {
                $('#stepTwoDiv').css('display', 'block')
                $('#stepThreeDiv').css('display', 'none')
            })
            $("#blocTree").click(function () {
                var address = $('#address').val();
                var zipcode = $('#zipcode').val();
                var city = $('#name').val();
                var country = $('#website').val();
                if (address.length && zipcode.length && city.length && country.length) {
                    $('#stepFourDiv').css('display', 'block')
                    $('#stepThreeDiv').css('display', 'none')
                }
            })
            $("#backToTreeDiv").click(function () {
                $('#stepThreeDiv').css('display', 'block')
                $('#stepFourDiv').css('display', 'none')
            })
            $("#blocFour").click(function () {
                $('#stepFourDiv').css('display', 'none')
                $('#stepFiveDiv').css('display', 'block')
            })
            $("#backToFour").click(function () {
                $('#stepThreeDiv').css('display', 'block')
                $('#stepFourDiv').css('display', 'none')
            })

            $("#blocSix").click(function () {
                $('#stepFiveDiv').css('display', 'none')
                $('#stepSixDiv').css('display', 'block')
            })
            $("#backToFourDiv").click(function () {
                $('#stepFourDiv').css('display', 'block')
                $('#stepFiveDiv').css('display', 'none')
            })


        </script>
    </div>
    <div id="employeeDiv" style="display: none">
        <?php $attributes = ['class' => '', 'role' => 'form', 'id' => 'registerEnp', 'enctype' => 'multipart/form-data']; ?>
        <?= form_open('/welcome/employes', $attributes) ?>
        <div class="stepOneEmp">
            <input type="hidden" name="local" id="local">
            <input type="hidden" name="companyId" id="companyId">
            <div class="stepOneEmp" id='stepOneEmp' style=''>


                <div class="row" style="">
                    <div class="col-xs-offset-3 col-xs-6 form-group">
                        <input id="firstnameEnp" type="text" name="firstname" class=" form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['firstname'];
                               } ?>" placeholder="<?= $this->lang->line('application_firstname'); ?>" required/>
                    </div>
                </div>


                <div class="row" style="">
                    <div class="col-xs-offset-3 col-xs-6 form-group">
                        <input id="lastnameEnp" type="text" name="lastname" class="required form-control input-class"
                               value="<?php if (isset($registerdata)) {
                                   echo $registerdata['lastname'];
                               } ?>" placeholder="<?= $this->lang->line('application_lastname'); ?>" required/>
                    </div>
                </div>

                <div class='row'>
                    <!-- Indicates a successful or positive action -->
                    <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;"><a href="/register" style="color: white">GO BACK</a></button>
                    </div>
                    <div class="col-xs-3" style="padding: 0">
                        <button type="button" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" id='stepFirstEnp'>CONTINUE
                        </button>
                    </div>
                </div>
            </div>


        </div>
        <div class='step-one' id='stepTwoDivEmp' style="display: none">

            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group <?php if (isset($registerdata)) {
                    echo 'has-error';
                } ?> ">

                    <input id="emailEnp" type="email" name="email" class="required email form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['email'];
                           } ?>" placeholder="<?= $this->lang->line('application_email'); ?>" required/>

                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group <?php if (isset($registerdata)) {
                    echo 'has-error';
                } ?>">
                    <input id="usernameEnp" type="text" name="username" class="required email form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['username'];
                           } ?>" placeholder="Username" required/>

                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <input id="passwordEnp" type="password" name="password" class="form-control input-class" value=""
                           placeholder="<?= $this->lang->line('application_password'); ?>" required/>
                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <input id="confirm_passwordEnp" type="password" class="form-control input-class"
                           data-match="#passwordEnp"
                           placeholder="<?= $this->lang->line('application_confirm_password'); ?>"
                           required/>
                </div>
            </div>


            <div class='row'>
                <!-- Indicates a successful or positive action -->
                <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;" id='backStepOneEnp'>GO BACK
                    </button>
                </div>
                <div class="col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" id='stepTwoEnp'>CONTINUE
                    </button>
                </div>
            </div>

        </div>
        <div class='step-two' style="display:none" id='stepThreeDivEnp'>


            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">

                    <select name="category" id="category" class="form-control input-class" style="height: 50px;border: none;">
                        <?php foreach ($categories as $value) { ?>
                            <option value="<?= $value->id ?>"><?= $value->name ?></option>
                        <?php } ?>
                    </select>

                </div>
            </div>

            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">

                    <select name="subCategory" id="subCategory" class="form-control input-class"
                            style="height: 50px;border: none;">

                    </select>
                </div>
            </div>


            <div class='row'>
                <!-- Indicates a successful or positive action -->
                <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;" id='backTwoDivEnp'>GO BACK
                    </button>
                </div>
                <div class="col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" id='blocTreeEnp'>CONTINUE
                    </button>
                </div>
            </div>


        </div>
        <div class="step-three" id='stepFourDivEnp' style='display:none'>

            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <label for="cvInput">CV input</label>
                    <input name="cvInput" type="file">

                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <label for="image">Image</label>
                    <input name="image" type="file">

                </div>
            </div>

            <div class='row'>
                <!-- Indicates a successful or positive action -->
                <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;" id='backToTreeDivEnp'>GO BACK
                    </button>
                </div>
                <div class="col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" id='blocFourEnp'>CONTINUE
                    </button>
                </div>
            </div>

        </div>
        <div class="step-three" id='stepFiveDivEnp' style='display:none'>

            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <input id="addressEn" type="text" name="address" class="form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['address'];
                           } ?>" placeholder="<?= $this->lang->line('application_address'); ?>" required/>
                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <input id="zipcodeEnp" type="text" name="zipcode" class="form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['zipcode'];
                           } ?>" placeholder="<?= $this->lang->line('application_zip_code'); ?>" required/>
                </div>

            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">

                    <input id="cityEnp" type="text" name="city" class="form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['city'];
                           } ?>" placeholder="<?= $this->lang->line('application_city'); ?>" required/>
                </div>
            </div>
            <div class="row" style="">
                <div class="col-xs-offset-3 col-xs-6 form-group">
                    <input id="countryEnp" type="text" name="country" class="form-control input-class"
                           value="<?php if (isset($registerdata)) {
                               echo $registerdata['country'];
                           } ?>" placeholder="<?= $this->lang->line('application_country'); ?>" required/>
                </div>
            </div>

            <div class='row'>
                <!-- Indicates a successful or positive action -->
                <div class="col-xs-offset-3 col-xs-3" style="padding: 0">
                    <button type="button" class="btn btn-success button-back" style="border-top-right-radius: 0;
    border-bottom-right-radius: 0;" id='backToFiveEnp'>GO BACK
                    </button>
                </div>
                <div class="col-xs-3" style="padding: 0">
                    <button type="submit" class="btn btn-danger button-continue " style="    border-top-left-radius: 0;
    border-bottom-left-radius: 0;" ><?= $this->lang->line('application_send'); ?>
                    </button>
                </div>
            </div>

        </div>
        <div class='step-five' id='stepFiveDivEnp' style='display:none'>
            <div class="header" style="margin-bottom: 30px"> Congratulations Please check your inbox. Click on the
                link
                in the email to confirm your email address. After you confirm click Continue
                <hr>
            </div>

            <div style='margin-bottom: 15px;text-align: center;' class="row">

                <div class='col-xs-2 col-xs-offset-4'>

                    <button type="button" class="btn btn-default"
                            style='border: 1px solid red;color: black;background-color: white;'>RE-SEND CONFIRMATION
                        EMAIL
                    </button>

                </div>
            </div>


        </div>

    </div>

    <div class="row">
        <br>
        <hr>
        <div class="col-md-12" style="text-align: center">
            Already create an account? click here to <a href="<?= site_url('login'); ?>"> login</a>
        </div>
        <div class="col-md-6" style='display:none'>

            <input type="submit" class="btn btn-success" value="<?= $this->lang->line('application_send'); ?>"/>
        </div>
    </div>
    <?= form_close() ?>
</div>
</div>


<script>


    $('#stepFirstEnp').click(function () {
        var firstnameEnp = $('#firstnameEnp').val();
        var lastnameEnp = $('#lastnameEnp').val();

        if (firstnameEnp.length && lastnameEnp.length) {
            $('#stepOneEmp').css('display', 'none')
            $('#stepTwoDivEmp').css('display', 'block')
        }
    })
    $("#backStepOneEnp").click(function () {
        $('#stepOneEmp').css('display', 'block')
        $('#stepTwoDivEmp').css('display', 'none')
    })
    $("#stepTwoEnp").click(function () {
        var emailEnp = $('#emailEnp').parents().hasClass('has-error has-danger')
        var emailEnp2 = $('#emailEnp').val()
        var usernameEnp = $('#usernameEnp').parents().hasClass('has-error has-danger')
        var passwordEnp2 = $('#passwordEnp').val()
        var confirm_passwordEnp = $('#confirm_passwordEnp').parents().hasClass('has-error has-danger')
        var confirm_passwordEnp2 = $('#confirm_passwordEnp').val()
        var usernameEnp2 = $('#usernameEnp').val()
        if (emailEnp !== true && usernameEnp !== true && passwordEnp !== true && confirm_passwordEnp !== true && emailEnp2.length && usernameEnp2.length && passwordEnp2.length && confirm_passwordEnp2.length) {
            $('#stepThreeDivEnp').css('display', 'block')
            $('#stepTwoDivEmp').css('display', 'none')
        }
    })
    $("#backTwoDivEnp").click(function () {
        $('#stepTwoDivEmp').css('display', 'block')
        $('#stepThreeDivEnp').css('display', 'none')
    })
    $("#blocTreeEnp").click(function () {
        $('#stepFourDivEnp').css('display', 'block')
        $('#stepThreeDivEnp').css('display', 'none')
    })
    $("#backToTreeDivEnp").click(function () {
        $('#stepThreeDivEnp').css('display', 'block')
        $('#stepFourDivEnp').css('display', 'none')
    })
    $("#blocFourEnp").click(function () {
        $('#stepFourDivEnp').css('display', 'none')
        $('#stepFiveDivEnp').css('display', 'block')
    })
    $("#backToFourEnp").click(function () {
        $('#stepThreeDivEnp').css('display', 'block')
        $('#stepFourDivEnp').css('display', 'none')
    })

    $("#backToFiveEnp").click(function () {
        $('#stepFourDivEnp').css('display', 'block')
        $('#stepFiveDivEnp').css('display', 'none')
    })


    $(document).ready(function () {
        $("#client").on('click', function () {
            $('#client').css('display', 'none');
            $('#employee').css('display', 'none');
            $('#selecyHow').css('display', 'none');
            $('#clientDiv').css('display', 'block');
            $('#clientHEader').css('display', 'block');
            $('#employeeDiv').css('display', 'none');
        });
        $("#employee").on('click', function () {
            $('#client').css('display', 'none');
            $('#employee').css('display', 'none');
            $('#selecyHow').css('display', 'none');
            $('#clientDiv').css('display', 'none');
            $('#employeeDiv').css('display', 'block');
            $('#empHEader').css('display', 'block');
        });


        var url_string = window.location.href
        var url = new URL(url_string);
        var email = url.searchParams.get("email");
        var type = url.searchParams.get("type");
        var validationError = url.searchParams.get("validationError");
        var invite = url.searchParams.get("invite");
        var companyId = url.searchParams.get("id");
        console.log(email)
        if (type === 'client' && email) {
            document.getElementById("client").click();
            $('#email').val(email)
        } else if (type === 'employee' && email) {
            document.getElementById("employee").click();
            $('#emailEnp').val(email)
        } else if (validationError) {
            $('#email').parent().attr('class', 'form-group  has-error has-danger')

            document.getElementById("client").click();
        } else if(invite && email) {
            document.getElementById("employee").click();
            $('#emailEnp').val(email)
            $('#local').val('true')
            $('#companyId').val(companyId)
        }

    });
    $('#category').on('change', function () {
        var category = $('#category').val()
        var token = $('input[name="fcs_csrf_token"]').val();
        $.ajax({
            url: "/welcome/getCategory/",
            type: "POST",
            data: {fcs_csrf_token: token, category: category},
            success: function (data) {
                $('#subCategory').empty()
                var subCategory = JSON.parse(data)
                var data2 = subCategory.data
                for (var i = 0; i < data2.length; i++) {
                    $('#subCategory').append('<option value="' + data2[i]['category_id'] + '">' + data2[i]['name'] + '</option>')
                }

            }
        });

    })


</script>