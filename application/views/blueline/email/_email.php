<?php
$attributes = array('class' => '', 'id' => '_item');
echo form_open($form_action, $attributes);
?>
    <div class="form-group">
        <label for="Subject">Subject</label>
        <input id="subject" name="subject" type="text" class="form-control"  value=""  required/>
    </div>
    <div class="form-group">
        <label for="message"><?= $this->lang->line('application_message'); ?></label>
        <textarea class="input-block-level summernote-modal" id="textfield" name="message"></textarea>
    </div>
<input type="hidden" id="emailInput" name="clientEmail">
<input type="submit" class="btn btn-primary"  value="Submit">
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {
        console.log(data)
        $('#emailInput').val(data)
    })
</script>
