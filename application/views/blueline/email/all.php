
<div class="col-xs-12 col-sm-12" style="padding: 0">

    <div class="box-shadow">
        <a href="email/sendEmail" id="employess" class="btn btn-primary" data-toggle="mainmodal">Send Email</a>
        <button  id="checkAll" class="btn btn-primary" >Check All </button>
        <button  id="uncheckAll" class="btn btn-danger" >Uncheck All </button>
        <div class="table-head" style="margin-top: 20px">Employes</div>
        <table class="table noclick" id="items" rel="<?=base_url()?>" cellspacing="0" cellpadding="0" style="background-color: white">
            <thead>
            <th>#</th>
            <th>Employee email</th>
            <th class="hidden-xs ">Employee last name</th>
            <th class="hidden-xs ">Employee first name</th>
            </thead>
                <?php foreach ($users as $user):?>
            <tr>
                <td  align="left"><input class="employess employessCheck" type="checkbox" value="<?= $user->email ?>"></td>
                <td   align="left"><?= $user->email ?></td>
                <td class="hidden-xs " align="left"><?= $user->lastname ?></td>
                <td class="hidden-xs "  align="left"><?= $user->firstname ?></td>
            </tr>
                <?php endforeach; ?>
        </table>

    </div>
    <div class="box-shadow">
        <a href="email/sendEmail" id="employess2" class="btn btn-primary" data-toggle="mainmodal">Send Email</a>
        <button  id="checkAllClient" class="btn btn-primary" >Check All </button>
        <button  id="uncheckAll2" class="btn btn-danger" >Uncheck All </button>
        <div class="table-head" style="margin-top: 20px">Client</div>
        <table class="table noclick" id="items" rel="<?=base_url()?>" cellspacing="0" cellpadding="0" style="background-color: white">
            <thead>
            <th>#</th>
            <th>Client email</th>
            <th class="hidden-xs ">Client last name</th>
            <th class="hidden-xs ">Client first name</th>
            <th class="hidden-xs ">Client company name</th>
            </thead>
            <tr>

                <?php foreach ($clients as $client):?>
            <tr>
                <td  align="left"><input class="employess clientCheck" type="checkbox" value="<?= $client->email ?>"></td>
                <td  align="left"><?= $client->email ?></td>
                <td class="hidden-xs " align="left"><?= $client->lastname ?></td>
                <td class="hidden-xs "  align="left"><?= $client->firstname ?></td>
                <td  class="hidden-xs " align="left"><?= $client->company->name ?></td>
            </tr>
            <?php endforeach; ?>

            </tr>
        </table>



    </div>
</div>
<script>
    $("#checkAll").click(function(){
        $('.employessCheck').prop('checked', 'checked');
    });
    $("#uncheckAll").click(function(){
        $('.employessCheck').attr('checked', false);
    });
    $("#uncheckAll2").click(function(){
        $('.clientCheck').attr('checked', false);
    });
    $("#checkAllClient").click(function(){
        $('.clientCheck').prop('checked', 'checked');
    });
    $('#employess').on('click', function () {
        data = '';
        var checkedValue = $('.employess:checked');
        console.log(checkedValue)
        for(var i = 0; i< checkedValue.length; i++) {
            if (data) {
                data += checkedValue[i].value + ','
            } else {
                data = checkedValue[i].value + ','
            }
        }
    })
    $('#employess2').on('click', function () {
        data = '';
        var checkedValue = $('.employess:checked');
        console.log(checkedValue)
        for(var i = 0; i< checkedValue.length; i++) {
            if (data) {
                data += checkedValue[i].value + ','
            } else {
                data = checkedValue[i].value + ','
            }
        }
    })



</script>