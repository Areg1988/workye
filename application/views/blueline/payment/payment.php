<style>
    .stripe-button-el,.stripe-button-el span,.stripe-button-el:hover,.Button-animationWrapper-child--primary {
        background-color: #00d647;;
        background: #00d647;;
    }
    .stripe-button-el span, .stripe-button-el,.Button-animationWrapper-child--primary {
        background-image:none ;

    }

</style>

<div class="col-xs-12" style="background-color: white;padding-top: 20px">
    <?php
    $attributes = array('class' => '', 'id' => 'payment-form');
    echo form_open('/payment/create', $attributes);
    ?>
    <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="<?= $data['publishable_key'] ?>"
            data-image="/assets/blueline/img/workye-logo-w-black.svg"
            data-name="Workey.com"
            data-panel-label="Add a new credit card"
            data-label="Add a new credit card"
            data-allow-remember-me=false
            data-email="<?php echo $this->user->email; ?>">
            data-locale="auto">
    </script>

    <?php echo form_close(); ?>
</div>
<div class="col-xs-12" style="background-color: white;padding-top: 20px">
    <div class="table-div">
<?php if($employes) { ?>
        <div class="table-head">Adding credit cards</div>
        <table class="data table" id="projects" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th>Email</th>
                <th>Last 4 digit</th>
                <th>Card type</th>

            </tr>
            </thead>

            <tbody>

                <tr>
                    <th>
                      <?= $this->user->email ?>
                    </th> <th>
                        <?= $employes->last ?>
                    </th><th>
                        <?= $employes->brand ?>
                    </th>
                </tr>


            </tbody>
        </table>
        <?php } ?>
    </div>
</div>


