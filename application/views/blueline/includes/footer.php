
<div class="clearfix"></div>


<footer class="footer7">
    <div class="container">

        <div class="one_third">

            <h4 class="roboto caps"><img style="margin-left: 10px" src="/assets/blueline/images/workye.com.png" width="108px"></h4>

            <p>workye connects you to a network
                of the world’s virtual employees —
                available on demand to
                help companies like yours accelerate,
                adapt, and scale.
                </p>

            <br>

            <ul>
                <li><i class="fa fa-map-marker fa-lg"></i> 2901 Marmora Road, Glassgow,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seattle,
                    WA 98122-1090
                </li>
                <li><i class="fa fa-phone"></i> 1 -234 -456 -7890</li>
                <li><i class="fa fa-envelope"></i> <a href="mailto:info@yourdomain.com">workye@workye.com</a></li>
            </ul>

        </div><!-- end section -->

        <div class="one_third">

            <h4 class="roboto caps">About us</h4>

            <div class="one_half">
                <ul>
                    <li><a href="#"><i class="fa fa-caret-right"></i>About Us</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Get Hire</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>How It Works</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Pricing Plans</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Brand Assets</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Blog</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Contact us</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Diversity</a></li>
                </ul>
            </div>

            <div class="one_half last">
                <ul>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Terms & Conditions</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Privacy Policy</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Acceptable Use of Policy</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Copyright Policy</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>GDPR Policy</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Cookie Policy</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Image Terms</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Data Processing Addendum</a></li>
                </ul>
            </div>

        </div><!-- end section -->

        <div class="one_third last">

            <h4 class="roboto caps">Join our Newsletter</h4>


            <br>

            <div class="newsletter3">
                <form method="get" action="index.html">
                    <input class="enter_email_input" name="samplees" id="samplees" value="Enter your email..."
                           onfocus="if(this.value == 'Enter your email...') {this.value = '';}"
                           onblur="if (this.value == '') {this.value = 'Enter your email...';}" type="text">
                    <input name="" value="Sign up" class="input_submit" type="submit">
                </form>
            </div>

            <div class="clearfix margin_top2"></div>

            <ul class="footer_social_links two">
                <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="https://www.facebook.com/workyehub/" target="_blank"><i
                                class="fa fa-facebook"></i></a></li>
                <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="https://twitter.com/workyehub" target="_blank"><i
                                class="fa fa-twitter"></i></a></li>

            </ul>


        </div><!-- end section -->

    </div><!-- end footer -->
</footer>

<div class="copyright_info5">
    <div class="container">

        <p>Copyright © 2015 LinStar.com. All rights reserved.</p>  <span><a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a></span>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('.carousel').carousel({
            interval: 20000,
            pause: true
        })
    });
</script>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

