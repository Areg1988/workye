
<style>
    * {
        font-family: "sofia-pro", "Roboto", "Helvetica", "Arial", sans-serif;
    }

    body, div, span, table, a, h1, h2, h3, h4, h5, h6 {
        font-family: "sofia-pro", "Roboto", "Helvetica", "Arial", sans-serif;
    }

    .navbar-nav > li > a {
        color: black;
        font-weight: 500;
    }

     .try-free {
        background-color: rgb(0, 214, 71);
        border-radius: 20px;
        padding: 8px 10px !important;
        color: white !important;
    }


    .navbar-nav>li>a:hover {
    color: rgb(0, 214, 71)
}
   .navbar-toggle span {
        color: black;

    }
    .try-free:hover {
        background-color: #fc5c27!important;
        color: white!important;
    }
</style>

<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Logo -->
                <!-- Navigation Menu -->
                <div class="menu_main">
                    <div class="navbar yamm navbar-default">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="https://workye.com/homePage">
                                <img src="assets/blueline/images/workye.com.svg" alt="workye.com.svg">
                            </a>
                            <div class="navbar-toggle" style=" background-color: white!important;" data-toggle="collapse"
                                 data-target="#navbar-collapse-1">
                                <a class="navbar-brand" href="https://workye.com/homePage">
                                    <img src="assets/blueline/images/workye.com.svg" alt="workye.com.svg">
                                </a>
                                <span>Menu</span>
                                <button type="button"><i class="fa fa-bars"></i></button>
                            </div>
                        </div>

                        <div id="navbar-collapse-1" class="navbar-collapse collapse ">
                            <nav>
                                <ul class="nav navbar-nav one">
                                    <li class="dropdown yamm-fw categories-li" id="product"><a  class="product dropdown-toggle">Categories
                                            <svg id="vategoryArrow" style="margin-left: 5px" width="10" height="10"
                                                 viewBox="0 0 256 256"
                                                 xmlns="http://www.w3.org/2000/svg"><title>change copy</title>
                                                <path
                                                        d="M0 66.73c0 2.66.981 5.33 2.955 7.41l117.332 123.323a10.64 10.64 0 0 0 7.712 3.314c2.912 0 5.696-1.19 7.712-3.314L253.043 74.14c4.074-4.278 3.914-11.077-.331-15.163a10.627 10.627 0 0 0-15.083.333L128 174.536 18.379 59.32c-4.075-4.279-10.827-4.429-15.083-.343A10.76 10.76 0 0 0 0 66.73z"
                                                        fill="#000" fill-rule="nonzero"></path>
                                            </svg>
                                        </a>
                                        <div class="productDiv" id="productDiv" style="display: none">
                                            <div class="fancy-menu-wrap">
                                                <div class="container">
                                                    <div class="row">

                                                       <?php  foreach ($category as $value): ?>
                                                        <div class="product-dropdown col-lg-3">
                                                            <a href="#">

                                                                <span><?= $value->name?></span>
                                                                <small><?= $value->description ?></small>
                                                            </a>
                                                        </div>
                                                       <?php endforeach;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dropdown circle-button"><a href="/pricings" class="dropdown-toggle"
                                                                          style="padding-left: 0">
                                    <span style="
    border: solid 0.8px rgba(158, 159, 162, 0.45);padding: 10px 20px;border-radius: 20px">Pricing Plans<span class="icon-new">NEW</span></span></a>

                                    </li>
                                </ul>
                                <ul class="nav navbar-nav two">
                                    <li>
                                        <a href="/get-hired">Get hired</a>
                                    </li>
                                    <li>
                                        <a href="/how-it-works">How it works</a>
                                    </li>
                                    <li class="dropdown yamm-fw" id="support"><a  class="product dropdown-toggle ">Support
                                            <svg id="vategoryArrow2" style="margin-left: 5px" width="10" height="10"
                                                 viewBox="0 0 256 256"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M0 66.73c0 2.66.981 5.33 2.955 7.41l117.332 123.323a10.64 10.64 0 0 0 7.712 3.314c2.912 0 5.696-1.19 7.712-3.314L253.043 74.14c4.074-4.278 3.914-11.077-.331-15.163a10.627 10.627 0 0 0-15.083.333L128 174.536 18.379 59.32c-4.075-4.279-10.827-4.429-15.083-.343A10.76 10.76 0 0 0 0 66.73z"
                                                        fill="#000" fill-rule="nonzero"></path>
                                            </svg>
                                        </a>
                                        <div class="productDiv" id="productDiv2" style="display: none">
                                            <div class="fancy-menu-wrap">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="product-dropdown col-lg-3">
                                                            <a href="#">
                                                                <!--<img src="/assets/blueline/pages/images/asdf.png" alt="asdf">-->
                                                                <span>Lorem ipsum support</span>
                                                                <small>Lorem ipsum dolor sup</small>
                                                            </a>
                                                        </div>
                                                        <div class="product-dropdown col-lg-3">
                                                            <a href="#">
                                                                <!--<img src="/assets/blueline/pages/images/asdf.png" alt="asdf">-->
                                                                <span>Lorem ipsum support</span>
                                                                <small>Lorem ipsum dolor sup</small>
                                                            </a>
                                                        </div>
                                                        <div class="product-dropdown col-lg-3">
                                                            <a href="#">
                                                                <!--<img src="/assets/blueline/pages/images/asdf.png" alt="asdf">-->
                                                                <span>Lorem ipsum support</span>
                                                                <small>Lorem ipsum dolor sup</small>
                                                            </a>
                                                        </div>
                                                        <div class="product-dropdown col-lg-3">
                                                            <a href="#">
                                                                <!--<img src="/assets/blueline/pages/images/asdf.png" alt="asdf">-->
                                                                <span>Lorem ipsum support</span>
                                                                <small>Lorem ipsum dolor sup</small>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/login">Login <!--<i class="fas fa-sign-in-alt"></i>--></a>
                                    </li>
                                    <li class="dropdown try-free-li" style="padding-top: 10px">
                                        <a href="/register" class="dropdown-toggle try-free">Get started</a>

                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- end Navigation Menu -->
            </div>
        </div>
    </div>
</header>


