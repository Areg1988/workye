<style>
    .box-shadow {
        -webkit-box-shadow: none;
        box-shadow: none;
    }
</style>

<div class="col-sm-13  col-md-12 main" style="padding: 0;">
    <div class="grid__col-md-6 dashboard-header" style="float: left">
        <h1 style="text-transform:capitalize;"><?=sprintf($this->lang->line('application_welcome_back'), $this->client->firstname);?></h1>
        <small><?=sprintf($this->lang->line('application_welcome_subline'), $messages_new[0]->amount, $event_count_for_today);?></small>
    </div>
    <div class="grid__col-md-6 dashboard-header hidden-xs">
        <div class="grid grid--bleed grid--justify-end">

            <div class="grid__col-3 grid__col-lg-2 grid--align-self-center">
                <h6><?=$this->lang->line('application_projects');?></h6>
                <h2><?=count($project);?></h2>
            </div>
            <div class="grid__col-3 grid__col-lg-2 grid--align-self-center">
                <h6><?=$this->lang->line('application_tasks');?></h6>
                <h2><?= $countTask;?></h2>
            </div>
            <?php if($tickets_access){ ?>
                <div class="grid__col-3 grid__col-lg-2 grid--align-self-center">
                    <h6><?=$this->lang->line('application_tickets');?></h6>
                    <h2><?=$ticketcounter;?></h2>
                </div>
            <?php } ?>

            <div class="grid__col-3 grid__col-lg-2 grid--align-self-center">
                <h6><?=$this->lang->line('application_tickets');?></h6>
                <h2><?=count($ticket);?></h2>
            </div>
            <div class="grid__col-3 grid__col-lg-2 grid--align-self-center">
                <h6>Employees</h6>
                <h2 id="employees"><?=$userCount;?></h2>
            </div>
        </div>
    </div>

    <div class="row" style="    display: inline;">

        <div class="box-shadow">
            <?php if($subscription) { ?>
            <a href="<?=base_url()?>cprojects/create" class="btn btn-primary" data-toggle="mainmodal"><?=$this->lang->line('application_create_new_project');?></a>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                    <?php $last_uri = $this->uri->segment($this->uri->total_segments());
                    if ($last_uri != 'cprojects') {
                        echo $this->lang->line('application_' . $last_uri);
                    } else {
                        echo $this->lang->line('application_all');
                    } ?> <span class="caret"></span>
                </button>
            <?php } ?>
            <div class="btn-group pull-right margin-right-3">


                <ul class="dropdown-menu pull-right" role="menu">
                    <?php foreach ($submenu as $name => $value): ?>
                        <li><a id="<?php $val_id = explode('/', $value);
                            if (!is_numeric(end($val_id))) {
                                echo end($val_id);
                            } else {
                                $num = count($val_id) - 2;
                                echo $val_id[$num];
                            } ?>" href="<?= site_url($value); ?>"><?= $name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="table-head" style="margin-top: 20px"><?= $this->lang->line('application_projects'); ?></div>
            <div class="table-div">
                <table class="data table" id="cprojects" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th width="20px" class="hidden-xs"><?= $this->lang->line('application_project_id'); ?></th>
                        <th class="hidden-xs" width="19px" class="no-sort sorting"></th>
                        <th><?= $this->lang->line('application_name'); ?></th>
                        <th class="hidden-xs"><?= $this->lang->line('application_client'); ?></th>
                        <th class="hidden-xs"><?= $this->lang->line('application_deadline'); ?></th>
                        <th class="hidden-xs"><?= $this->lang->line('application_category'); ?></th>
                        <th class="hidden-xs"><?= $this->lang->line('application_assign_to'); ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($project as $value):

                        ?>
                        <tr id="<?= $value->id; ?>">
                            <td class="hidden-xs"><?= $core_settings->project_prefix; ?><?= $value->reference; ?></td>
                            <td class="hidden-xs">

                                <div class="c100 p<?= $value->progress; ?> <?= ($value->progress == '100') ? 'green' : '' ?> small tt"
                                     title="<?= $value->progress; ?>%">
                                    <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                    </div>
                                </div>
                            </td>
                            <td onclick=""><?= $value->name; ?></td>
                            <td class="hidden-xs"><a class="label label-info"><?php if (!is_object($value->company)) {
                                        echo $this->lang->line('application_no_client_assigned');
                                    } else {
                                        echo $value->company->name;
                                    } ?></a></td>
                            <td class="hidden-xs"><span
                                        class="hidden-xs label label-success <?php if ($value->end <= date('Y-m-d') && $value->progress != 100) {
                                            echo 'label-important tt" title="' . $this->lang->line('application_overdue');
                                        } ?>"><?php $unix = human_to_unix($value->end . ' 00:00');
                                    echo '<span class="hidden">' . $unix . '</span> ';
                                    echo date($core_settings->date_format, $unix); ?></span></td>
                            <td class="hidden-xs">
                                <?= $value->category; ?>
                            </td>
                            <td class="hidden-xs">
                                <?php foreach ($value->project_has_workers as $workers): ?>
                                    <script>
                                        var employeesCount = <?= count($value->project_has_workers); ?>;

                                    </script>
                                    <img class="img-circle tt" src="<?= $workers->user->userpic; ?>"
                                         title="<?php echo $workers->user->firstname . ' ' . $workers->user->lastname; ?>"
                                         height="19px"><span
                                            class="hidden"><?php echo $workers->user->firstname . ' ' . $workers->user->lastname; ?></span>
                                <?php endforeach; ?>
                            </td>

                        </tr>

                    <?php endforeach; ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $('#employees').text(employeesCount)
    </script>
  