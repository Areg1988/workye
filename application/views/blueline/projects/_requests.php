<?php
$attributes = array('class' => '', 'id' => '_project');
echo form_open($form_action, $attributes); ?>

<input id="id" type="hidden" name="id" value="<?= $id?>"/>
<div class="form-group">
    <label for="reference">Projects ID</label>
    <input class='form-control' style='height: 35px;border: none;' name="project" title="projects"  value='<?= $project->reference ?>' readonly/>
        
    
</div>
<div class="form-group">
    <label for="reference">Projects Name</label>
    <input class='form-control' style='height: 35px;border: none;' name="project" title="projects"  value='<?= $project->name ?>' readonly/>
        
    
</div>
<div class="form-group">
    <label for="textfield"><?= $this->lang->line('application_description'); ?></label>
    <textarea class="input-block-level form-control" id="textfield" name="description" required></textarea>
</div>




<div class="modal-footer">
    <input type="submit" name="send" class="btn btn-primary" value="SEND REQUEST"/>
    <a class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('application_close'); ?></a>
</div>

<?php echo form_close(); ?>
