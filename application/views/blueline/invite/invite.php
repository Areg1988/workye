<style>
    .multipleInput-container {

        padding:1px;
        padding-bottom:0;
        cursor:text;
        font-size:15px;
        width:100%;
        border-radius: 6px
    }

    .multipleInput-container input {
        font-size:15px;
        clear:both;
        height:60px;
        border:0;
        margin-bottom:1px;
    }

    .multipleInput-container ul {
        list-style-type:none;
    }

    li.multipleInput-email {
        float:left;
        padding:6px ;
        color: #fff;
        background: #FD9160;
        margin-top: 0;
        border-radius: 6px;
        margin: 6px 2px 6px 6px;
    }

    .multipleInput-close {
        width:16px;
        height:16px;
        display:block;
        float:right;
        margin: -2px 0px 0px 8px;
        color: #fff;
        font-size: 16px;
    }
</style>

<?php
$attributes = array('class' => '', 'id' => '_project');
echo form_open($form_action, $attributes); ?>
<p>Write email employee to invite him to your projects</p>
<div class="form-group">
    <label for="phases"><?= $this->lang->line('application_email'); ?> *</label>
    <input type="text" name="email" class="form-control" id="my_input" value="" required/>
</div>
<div class="form-group">
    <label for="phases"><?= $this->lang->line('application_message'); ?> </label>
    <input type="text" name="message" class="form-control" id="message" value="" />
</div>
<div class="modal-footer">
    <input type="submit" name="send" class="btn btn-primary" value="<?= $this->lang->line('application_send'); ?>"/>
    <a class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('application_close'); ?></a>
</div>
<?php echo form_close(); ?>


<script>


    (function( $ ){

        $.fn.multipleInput = function() {

            return this.each(function() {

                // create html elements

                // list of email addresses as unordered list
                $list = $('<ul />');

                // input
                var $input = $('<input type="text" style="height: 25px" />').keyup(function(event) {

                    if(event.which == 32 || event.which == 188) {
                        // key press is space or comma
                        var val = $(this).val().slice(0, -1); // remove space/comma from value

                        // append to list of emails with remove button
                        $list.append($('<li class="multipleInput-email"><span> ' + val + '</span></li>')
                            .append($('<a href="#" class="multipleInput-close" title="Remove">x</a>')
                                .click(function(e) {
                                    $(this).parent().remove();
                                    e.preventDefault();
                                })
                            )
                        );
                        $(this).attr('placeholder', '');
                        // empty input
                        $(this).val('');
                    }

                });

                // container div
                var $container = $('<div class="multipleInput-container" />').click(function() {
                    $input.focus();
                });

                // insert elements into DOM
                $container.append($list).append($input).insertAfter($(this));

                // add onsubmit handler to parent form to copy emails into original input as csv before submitting
                var $orig = $(this);
                $(this).closest('form').submit(function(e) {

                    var emails = new Array();
                    $('.multipleInput-email span').each(function() {
                        emails.push($(this).html());
                    });
                    emails.push($input.val());

                    $orig.val(emails.join());

                });

                return $(this).hide();

            });

        };
    })( jQuery );

    $('#my_input').multipleInput();
</script>