<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
      type="text/css"/>
 <!-- Include external CSS. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 
    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_style.min.css" rel="stylesheet" type="text/css" />



<body>
<style>
    .form-group {
        background-color:white ;
    }
 
</style>

<div class="sample">
   <div id='message' style='display:none'>
   <div class="alert alert-success" role="alert">Page added successfully.</div>
   </div>
    <div id='message2' style='display:none'>
   <div class="alert alert-success" role="alert">Page updated successfully.</div>
   </div>


    <input type="hidden" id="id" value="<?= $page[0]->id ?>">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Page name" value="<?= $page[0]->name ?>">
        </div>
        <div class="form-group">
            <label for="title">title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="title" value="<?= $page[0]->title ?>">
        </div>
        <div class="form-group">
            <label for="description">Page description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Page description" value="<?= $page[0]->description ?>">
        </div>
        <div class="form-group">
            <label for="keyWords">Key words</label>
            <input type="text" class="form-control" name="keyWords" id="keyWords" placeholder="Key words" value="<?= $page[0]->words ?>">
        </div>

        <textarea class="form-control" id="mytextarea" name="content"> <?= $page[0]->body ?></textarea>
 <button type="submit" class="btn btn-default" id="submit" style="margin-top: 30px">SAVE</button>

  
</div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/js/froala_editor.pkgd.min.js"></script>
 
<script>
  
    $(function() {
        $('textarea').froalaEditor({ fullPage: true, htmlRemoveTags: [ 'base']});

        })


</script>
<?php if(empty($page)) { ?>
    <script>
        $('#submit').click(function () {

            var body = tinyMCE.activeEditor.getContent().replace(/style/g,'style1');
            var title = $('#title').val();
            var name = $('#name').val().replace(/ /g,'-');
            var description = $('#description').val();
            var keyWords = $('#keyWords').val();
            var token = csfrData['fcs_csrf_token'];
            $.ajax({
                url: "/cms/createNewPage",
                type: "POST",
                data:{body: body, fcs_csrf_token: token,title:title,name:name,description:description,keyWords:keyWords},
                success: function (result) {
                    $('#title').val(' ')
                    $('#name').val(' ')
                    $('#description').val(' ');
                    $('#keyWords').val(' ');
                    tinyMCE.activeEditor.setContent(' ');
                    $('#message').css('display', 'block')
                    setTimeout(function(){  $('#message').css('display', 'none') }, 4000);
                }
            });

        })
    </script>
<?php } else { ?>
    <script>
        $('#submit').click(function () {

            var body = tinyMCE.activeEditor.getContent().replace(/style/g,'style1');
            var title = $('#title').val()
            var name = $('#name').val()
            var description = $('#description').val();
            var keyWords = $('#keyWords').val();
            var id = $('#id').val();
            var token = csfrData['fcs_csrf_token'];
            $.ajax({
                url: "/cms/updatePageById",
                type: "POST",
                data:{body: body, fcs_csrf_token: token,title:title,name:name,description:description,keyWords:keyWords,id:id},
                success: function (result) {

                    $('#message2').css('display', 'block')
                    setTimeout(function(){  $('#message2').css('display', 'none') }, 4000);
                }
            });

        })
    </script>
<?php } ?>
