<style>
    .pagination > li {
        display: inline;
        padding: 0px !important;
        margin: 0px !important;
        border: none !important;
    }

    .modal-backdrop {
        z-index: -1 !important;
    }

    iframe {
        height: 700px !important;
    }

    .btn {
        display: inline-block;
        padding: 6px 12px !important;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }

    .btn-primary {
        color: #fff !important;
        background: #428bca !important;
        border-color: #357ebd !important;
        box-shadow: none !important;
    }

    .btn-danger {
        color: #fff !important;
        background: #d9534f !important;
        border-color: #d9534f !important;
        box-shadow: none !important;
    }
</style>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


<script language="JavaScript" src="https://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"
        type="text/javascript"></script>
<script language="JavaScript"
        src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js"
        type="text/javascript"></script>


<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<body>

<div class="row">
<div style="    margin: 15px;">
    <a href="/create-new-page" class="btn btn-primary btn-lg " role="button">Create new page</a>
</div>

    <div class="col-md-12">


        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Title</th>
                <th>Key words</th>

                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Title</th>
                <th>Key words</th>

                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </tfoot>

            <tbody>
            <?php foreach ($pages as $page) { ?>
                <tr>
                    <td><a href="/pages/<?= $page->name ?>" target="_blank" style="text-transform:lowercase"><?= $page->name ?></a></a></td>
                    <td  style="text-transform:lowercase"><?= $page->description ?></td>
                    <td  style="text-transform:lowercase"><?= $page->title ?></td>
                    <td  style="text-transform:lowercase"><?= $page->words ?></td>


                    <td>
                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                            <a href="/update-page/<?= $page->id ?>" class="btn btn-primary btn-xs"
                                   ><span class="glyphicon glyphicon-pencil"></span></a>
                        </p>
                    </td>
                    <td>
                        <p data-placement="top" data-toggle="tooltip" title="Delete">
                            <a href="/deletePage/<?= $page->id ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                        </p>
                    </td>
                </tr>
            <?php } ?>
    
            </tbody>
        </table>


    </div>
</div>


</body>
<!-- Initialize the editor. -->
<script>
    $(document).ready(function () {
        $('#datatable').dataTable();

        $("[data-toggle=tooltip]").tooltip();

    });

</script>