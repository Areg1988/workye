<?php
/**
 * @file        Application View
 * @author      Luxsys <support@freelancecockpit.com>
 * @copyright   By Luxsys (http://www.freelancecockpit.com)
 * @version     3.x.x
 */

$act_uri = $this->uri->segment(1, 0);
$lastsec = $this->uri->total_segments();
$act_uri_submenu = $this->uri->segment($lastsec);
if (!$act_uri) {
    $act_uri = 'cdashboard';
}
if (is_numeric($act_uri_submenu)) {
    $lastsec = $lastsec - 1;
    $act_uri_submenu = $this->uri->segment($lastsec);
}
$message_icon = false;

if ($this->user or $this->client) {
    $name = $this->uri->segment('1');

    if ($name == 'homePage' or $name == 'jobs' or $name == 'pricings' or $name == 'how-it-works' or $name == 'get-hired') { ?>
        <!doctype html>
        <!--[if IE 7 ]>
        <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
        <!--[if IE 8 ]>
        <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
        <!--[if IE 9 ]>
        <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
        <!--[if (gt IE 9)|!(IE)]><!-->
        <html lang="en-gb" class="no-js"> <!--<![endif]-->

        <head>

            <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
            <script>(function (d) {
                    var config = {
                            kitId: 'gnf7xqu',
                            scriptTimeout: 3000,
                            async: true
                        },
                        h = d.documentElement, t = setTimeout(function () {
                            h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                        }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                        s = d.getElementsByTagName("script")[0], a;
                    h.className += " wf-loading";
                    tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
                    tk.async = true;
                    tk.onload = tk.onreadystatechange = function () {
                        a = this.readyState;
                        if (f || a && a != "complete" && a != "loaded") return;
                        f = true;
                        clearTimeout(t);
                        try {
                            Typekit.load(config)
                        } catch (e) {
                        }
                    };
                    s.parentNode.insertBefore(tk, s)
                })(document);</script>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="keywords" content=""/>
            <meta name="description" content=""/>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
            <title>workye - A modern way to hire virtual employees online</title>
            <meta name="description" content="
We'll Find You The Perfect employees For Your Projects! It's Easy & Affordable. Secure Payments. Increased Productivity. 90% of Customers Rehire. Trusted by 4M+ Businesses. Services: Designers, Web, Mobile, Marketers, Writers, Developers, Coders.">
            <!-- Favicon -->
            <link rel="shortcut icon" href="images/favicon.ico">

            <!-- this styles only adds some repairs on idevices  -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <!-- Google fonts - witch you want to use - (rest you can just remove) -->
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic'
                  rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
                  rel='stylesheet'
                  type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
                  rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet'
                  type='text/css'>

            <!--[if lt IE 9]>
            <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <!-- Head CSS and JS -->
            <script src="<?= base_url() ?>assets/blueline/js/plugins/jquery-2.2.4.min.js?ver=<?= $core_settings->version; ?>"></script>

            <link rel="stylesheet" href="<?= base_url() ?>assets/blueline/css/app.css?ver=<?= $core_settings->version; ?>"/>

            <!-- ######### CSS STYLES ######### -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="stylesheet" href="/assets/blueline/pages/css/reset.css" type="text/css"/>
            <link rel="stylesheet" href="/assets/blueline/pages/css/style.css" type="text/css"/>

            <!-- simple line icons -->
            <link rel="stylesheet" type="text/css" href="/assets/blueline/pages/css/simpleline-icons/simple-line-icons.css"
                  media="screen"/>

            <!-- et linefont icons -->
            <link rel="stylesheet" href="/assets/blueline/pages/css/et-linefont/etlinefont.css">

            <!-- animations -->
            <link href="/assets/blueline/pages/js/animations/css/animations.min.css" rel="stylesheet" type="text/css"
                  media="all"/>

            <!-- responsive devices styles -->
            <link rel="stylesheet" media="screen" href="/assets/blueline/pages/css/responsive-leyouts.css" type="text/css"/>

            <!-- shortcodes -->
            <link rel="stylesheet" media="screen" href="/assets/blueline/pages/css/shortcodes.css" type="text/css"/>


            <!-- mega menu -->

            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
                  integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
                  crossorigin="anonymous">
            <link href="/assets/blueline/pages/js/mainmenu/demo.css" rel="stylesheet">
            <link href="/assets/blueline/pages/js/mainmenu/menu.css" rel="stylesheet">

            <!-- MasterSlider -->
            <link rel="stylesheet" href="/assets/blueline/pages/js/masterslider/style/masterslider.css"/>
            <link rel="stylesheet" href="/assets/blueline/pages/js/masterslider/skins/default/style.css"/>

            <!-- cubeportfolio -->
            <link rel="stylesheet" type="text/css" href="/assets/blueline/pages/js/cubeportfolio/cubeportfolio.min.css">

            <!-- owl carousel -->
            <link href="/assets/blueline/pages/js/carouselowl/owl.transitions.css" rel="stylesheet">
            <link href="/assets/blueline/pages/js/carouselowl/owl.carousel.css" rel="stylesheet">

            <!-- tabs 2 -->
            <link href="/assets/blueline/pages/js/tabs2/tabacc.css" rel="stylesheet"/>
            <link href="/assets/blueline/pages/js/tabs2/detached.css" rel="stylesheet"/>

            <style>

                .mainnavbar {
                    background: #fff;
                    -webkit-box-shadow: none!important;
                    box-shadow: none!important;
                }

                .notification-center__header, .notification-center__header a {
                    -webkit-box-shadow: 0 -2px 0 0 #eee inset;
                    box-shadow: inset 0 -2px 0 0 #eee;
                }
                .table-head, #main .action-bar, #message .header, .form-header, .notification-center__header a.active {
                    box-shadow:none!important;
                }
                .user_online__indicator {
                    display: inline-block;
                    height: 15px;
                    width: 15px;
                  border: none!important;
                    border-radius: 50%;
                    vertical-align: text-top;
                    border-width: 2px;
                    border-style: solid;
                    margin: 1px -4px 0 -2px;
                }
                .topbar-userpic {
                    margin-right: -10px;
                    margin-top: 12px;
                }
                .content-area {
                    margin-left: 0!important;
                    padding: 0 15px;
                }
                .navbar-brand {
                    padding: 0;
                    margin: 10px 0;
                }
                .topbar__name {
                    position: relative;
                    top: -14px;
                }
                .navbar-header {
                    height: 50px;
                }
            </style>

        </head>
        <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" style="height: 50px">
                        <!-- Logo -->
                        <div class="navbar-header" style="width: 140px;padding: 0px 16px 0 16px;">
                            <a href='/'>
                                <div class="navbar-brand" style='width: 140px;padding: 0 32px 0 0;'>
                                    <img id='logoImg' src='/assets/blueline/images/workye.com.svg' width='100%'>
                                </div>
                            </a>
                        </div>
                        <div class="content-area" onclick="">
                            <div class="row mainnavbar">
                                <div class="topbar__left noselect">
                                    <i class="icon dripicons-menu topbar__icon fc-dropdown--trigger hidden"></i>
                                    <div class="fc-dropdown shortcut-menu grid">
                                        <div class="grid__col-6 shortcut--item"><i
                                                    class="ion-ios-paper-outline shortcut--icon"></i> <?= $this->lang->line('application_create_invoice'); ?>
                                        </div>
                                        <div class="grid__col-6 shortcut--item"><i
                                                    class="ion-ios-lightbulb shortcut--icon"></i> <?= $this->lang->line('application_create_project'); ?>
                                        </div>
                                        <div class="grid__col-6 shortcut--item"><i
                                                    class="ion-ios-pricetags shortcut--icon"></i> <?= $this->lang->line('application_create_ticket'); ?>
                                        </div>
                                        <div class="grid__col-6 shortcut--item"><i
                                                    class="ion-ios-email shortcut--icon">

                                            </i> <?= $this->lang->line('application_write_messages'); ?>
                                        </div>
                                    </div>
                                    <i class="icon dripicons-bell topbar__icon fc-dropdown--trigger" data-placement="bottom"
                                       title="<?= $this->lang->line('application_alerts'); ?>"><?php if ($notification_count > 0) {
                                            ?><span class="topbar__icon_alert"></span><?php
                                        } ?></i>
                                    <div class="fc-dropdown notification-center">
                                        <div class="notification-center__header">
                                            <a href="#" class="active"><?= $this->lang->line('application_alerts'); ?>
                                                (<?= $notification_count; ?>)</a>
                                            <!-- <a href="#"><?= $this->lang->line('application_announcements'); ?></a> -->
                                        </div>
                                        <ul class="notificationlist">
                                            <?php
                                            foreach ($notification_list as $value): ?>
                                                <li class="">
                                                    <p class="truncate"><?= $value; ?></p>
                                                </li>
                                            <?php endforeach; ?>
                                            <?php if ($notification_count == 0) {
                                                ?>
                                                <li><p class="truncate"><?= $this->lang->line('application_no_events_yet'); ?></p></li>
                                                <?php
                                            } ?>
                                        </ul>
                                    </div>
                                    <?php if (isset($projects_icon)) {?>
                                        <i class="icon dripicons-stopwatch topbar__icon fc-dropdown--trigger" data-placement="bottom"
                                           title="<?= $this->lang->line('application_running_timers'); ?>">
                                            <?php if ($task_notifications) { ?>
                                                <span class="topbar__icon_alert"></span>
                                                <?php } ?>
                                        </i>
                                        <div class="fc-dropdown notification-center shortcut-menu">
                                            <div class="notification-center__header">
                                                <a href="#" class="active"><?= $this->lang->line('application_running_timers'); ?></a>
                                            </div>
                                            <ul class="notificationlist task__notifications details">
                                                <?php foreach ($task_notifications as $value) {
                                                    $task_count = 1; ?>
                                                    <li>
                                                        <span><?= $value->project->name; ?></span>
                                                        <a href="<?= base_url(); ?>projects/view/<?= $value->project_id; ?>/<?= $value->id; ?>"><?= $value->name; ?></a>
                                                        <?php $timertime = (time() - $value->tracking) + $value->time_spent; ?>
                                                        <span id="notification_timer<?= $value->id; ?>"
                                                              class="pull-right badge timer__badge resume"></span>
                                                        <script>$(document).ready(function () {
                                                                startTimer("resume", "<?=$timertime; ?>", "#notification_timer<?=$value->id; ?>");
                                                            });</script>
                                                    </li>
                                                    <?php
                                                } ?>
                                                <?php if (!isset($task_count)) {
                                                    ?>
                                                    <li>
                                                        <p class="truncate"><?= $this->lang->line('application_no_timers_running'); ?></p>
                                                    </li>
                                                    <?php
                                                } ?>
                                            </ul>
                                        </div>
                                        <?php
                                    } ?>
                                    <?php if (true) {?>
                                        <span class="hidden-xs">
                                            <a href="<?= site_url('messages'); ?>" title="<?= $this->lang->line('application_messages'); ?>">
                                                <i class="icon dripicons-inbox topbar__icon">
                                                <?php if ($messages_new[0]->amount != '0') {?>
                                                    <span class="topbar__icon_alert"></span>
                                                <?php } ?>
                                                </i>
                                            </a>
                                            <a href="/requests">
                                                <i class="icon glyphicon glyphicon-hourglass"></i>
                                            </a>

                                        </span>
                                        <?php
                                    } ?>
                                    <!-- <i class="ion-ios-search-strong topbar__icon shortcut-menu--trigger"></i> -->
                                </div>
                                <div class="topbar noselect">
                                    <?php $userimage = $this->user->userpic; ?>
                                    <img class="img-circle topbar-userpic" src="<?= $userimage; ?>" height="40px">
                                    <span class="user_online__indicator user_online__indicator--online"></span>
                                    <span class="topbar__name fc-dropdown--trigger">
                                        <span class="hidden-xs" style="text-transform:capitalize;font-weight: 700;">
                                            <?php echo character_limiter($this->user->firstname . ' ' . $this->user->lastname, 25); ?>
                                        </span>
                                        <i class="icon dripicons-chevron-down topbar__drop"></i>
                                    </span>
                                    <div class="fc-dropdown profile-dropdown">
                                        <ul>
                                            <li>
                                                <a href="<?= site_url('agent'); ?>" data-toggle="mainmodal">
                                                    <span class="icon-wrapper">
                                                        <i class="icon dripicons-gear"></i>
                                                    </span>
                                                    <?= $this->lang->line('application_profile'); ?>
                                                </a>
                                            </li>
                                            <li class="fc-dropdown__submenu--trigger">
                                                 <span class="icon-wrapper">
                                                    <i class="icon dripicons-chevron-left"></i>
                                                 </span>
                                                <?= $current_language; ?>
                                                <ul class="fc-dropdown__submenu">
                                                    <span class="fc-dropdown__title"><?= $this->lang->line('application_languages'); ?></span>
                                                    <?php foreach ($installed_languages as $entry) { ?>
                                                        <li>
                                                            <a href="<?= base_url() ?>agent/language/<?= $entry; ?>">
                                                                <img src="<?= base_url() ?>assets/blueline/img/<?= $entry; ?>.png"
                                                                     class="language-img b-lazy"> <?= ucwords($entry); ?>
                                                            </a>
                                                        </li>
                                                        <?php } ?>
                                                </ul>

                                            </li>
                                            <li class="profile-dropdown__logout">
                                                <a href="<?= site_url('logout'); ?>"
                                                   title="<?= $this->lang->line('application_logout'); ?>">
                                                    <?= $this->lang->line('application_logout'); ?>
                                                    <i class="icon dripicons-power pull-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Notify -->
                        <?php if ($this->session->flashdata('message')) {
                            $exp = explode(':', $this->session->flashdata('message')) ?>
                            <div class="notify <?= $exp[0] ?>"><?= $exp[1] ?></div>
                            <?php
                        } ?>
                        <div class="ajax-notify"></div>

                    </div>
                </div>
            </div>
        </header>
        <!-- Notify -->
        <?php if ($this->session->flashdata('message')) {
            $exp = explode(':', $this->session->flashdata('message')) ?>
            <div class="notify <?= $exp[0] ?>"><?= $exp[1] ?></div>
            <?php
        } ?>
        <div class="ajax-notify"></div>
        <?=
        $yield;
        ?>
        </body>

        <!-- ######### JS FILES ######### -->
        <!-- get jQuery used for the theme -->
        <script type="text/javascript" src="/assets/blueline/pages/js/universal/jquery.js"></script>
        <script src="/assets/blueline/pages/js/style-switcher/styleselector.js"></script>
        <script src="/assets/blueline/pages/js/animations/js/animations.min.js" type="text/javascript"></script>
        <script src="/assets/blueline/pages/js/mainmenu/bootstrap.min.js"></script>
        <script src="/assets/blueline/pages/js/mainmenu/customeUI.js"></script>
        <script src="/assets/blueline/pages/js/masterslider/masterslider.min.js"></script>
        <script type="text/javascript">
            (function ($) {
                "use strict";
                var slider = new MasterSlider();
                // adds Arrows navigation control to the slider.
                slider.control('arrows');
                slider.control('bullets');

                slider.setup('masterslider', {
                    width: 1400,    // slider standard width
                    height: 680,   // slider standard height
                    space: 0,
                    speed: 45,
                    layout: 'fullwidth',
                    loop: true,
                    preload: 0,
                    autoplay: true,
                    view: "parallaxMask"
                });
            })(jQuery);
        </script>
        <script src="/assets/blueline/pages/js/scrolltotop/totop.js" type="text/javascript"></script>
        <script type="text/javascript" src="/assets/blueline/pages/js/mainmenu/sticky.js"></script>
        <script type="text/javascript" src="/assets/blueline/pages/js/mainmenu/modernizr.custom.75180.js"></script>
        <script type="text/javascript"
                src="/assets/blueline/pages/js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
        <script type="text/javascript" src="/assets/blueline/pages/js/cubeportfolio/main.js"></script>
        <script src="/assets/blueline/pages/js/tabs2/index.js"></script>
        <script>
            $('.accordion, .tabs').TabsAccordion({
                hashWatch: true,
                pauseMedia: true,
                responsiveSwitch: 'tablist',
                saveState: sessionStorage,
            });
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll >= 1) {
                    //clearHeader, not clearheader - caps H
                    $(".fancy-menu-wrap").addClass("fancy-menu-wrap-scrolled");
                } else {
                    if (scroll <= 0) {
                        $(".fancy-menu-wrap").removeClass("fancy-menu-wrap-scrolled");
                    }
                }
            });
        </script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/assets/blueline/pages/js/aninum/jquery.animateNumber.min.js"></script>
        <script src="/assets/blueline/pages/js/carouselowl/owl.carousel.js"></script>
        <script type="text/javascript" src="/assets/blueline/pages/js/universal/custom.js"></script>
        <script type="text/javascript" src="/assets/blueline/pages/js/script.js"></script>
        <script type="text/javascript"
                src="<?= base_url() ?>/assets/blueline/js/app.js?ver=<?= $core_settings->version; ?>"></script>
        </html>
    <?php } else { ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <meta name="robots" content="none"/>
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <title><?= $core_settings->company; ?></title>

    <script src="<?= base_url() ?>assets/blueline/js/plugins/jquery-2.2.4.min.js?ver=<?= $core_settings->version; ?>"></script>


    <?php
    require_once '_partials/fonts.php';
    require_once '_partials/js_vars.php';
    ?>

    <link rel="stylesheet" href="<?= base_url() ?>assets/blueline/css/app.css?ver=<?= $core_settings->version; ?>"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/blueline/css/user.css?ver=<?= $core_settings->version; ?>"/>
    <?= get_theme_colors($core_settings); ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>']
            = '<?php echo $this->security->get_csrf_hash(); ?>';

    </script>
</head>
<style>
    #hideMenu {
        cursor: pointer;
    }


    .topbar__left {
        display: inline-block;
        margin: 0 15px;
        font-size: 18px;
        float: left;
    }




    .box-shadow {
        -webkit-box-shadow: none;
        box-shadow: none;

    }

    .box-shadow {
        padding: 24px;
    }

    .notification-badge2 {

        background: #ed5564;
        padding: 4px 8px 4px 5px;
        color: #fff;
        /* font-weight: 600; */
        font-size: 11px;
        /* float: right; */
        /* margin-right: -6px; */
        /* margin-top: -2px; */
        border-radius: 216px;

    }

    @media only screen and (max-width: 768px) {
        #hideMenu {
            display: none;
        }
    }




</style>
<body>
<div id="mainwrapper">

    <div class="side">
        <div class="sidebar-bg"></div>
        <div class="sidebar">
            <div class="navbar-header" style="width: 140px;padding: 0px 16px 0 16px;">
                <a href='/'>
                    <div class="navbar-brand" style='width: 140px;padding: 0 32px 0 0;'>
                        <img id='logoImg' src='/assets/blueline/images/workye.com.svg' width='100%'>
                    </div>
                </a>
            </div>

            <ul class="nav nav-sidebar">
                <?php foreach ($menu as $key => $value) {
                    ?>
                    <?php
                    if (strtolower($value->link) == 'cmessages') {
                        $message_icon = true;
                    } ?>
                    <li id="<?= strtolower($value->name); ?>" class="<?php if ($act_uri == strtolower($value->link)) {
                        echo 'active';
                    } ?>"><a href="<?= site_url($value->link); ?>"><span class="menu-icon"><i
                                        class="fa <?= $value->icon; ?>"></i>
                            </span>
                            <span class="nav-text"><?php echo $this->lang->line('application_' . $value->link); ?></span>
                            <?php if (strtolower($value->link) == 'cmessages' && $messages_new[0]->amount != '0') {
                                ?><span class="notification-badge"><?= $messages_new[0]->amount; ?></span><?php
                            } ?>
                            <?php if (strtolower($value->link) == 'quotations' && $quotations_new[0]->amount != '0') {
                                ?><span class="notification-badge"><?= $quotations_new[0]->amount; ?></span><?php
                            } ?>
                            <?php if (strtolower($value->link) == 'cestimates' && $estimates_new[0]->amount != '0') {
                                ?><span class="notification-badge"><?= $estimates_new[0]->amount; ?></span><?php
                            } ?>

                        </a>
                    </li>
                    <?php
                } ?>
            </ul>


        </div>
    </div>

    <div class="content-area">
        <div class="row mainnavbar">

            <div class="topbar__left noselect">
                <i id='hideMenu' class='glyphicon glyphicon-menu-hamburger' style='margin-right:20px'></i>
                <a href="#" class="menu-trigger"><i class="ion-navicon visible-xs"></i></a>
                <?php if ($message_icon) {
                    ?>
                    <span class="hidden-xs">
                  <a href="<?= site_url('cmessages'); ?>" title="<?= $this->lang->line('application_messages'); ?>">
                     <i class="ion-archive topbar__icon">
                          <?php if ($messages_new[0]->amount != '0') {
                              ?> <span class="topbar__icon_alert"></span><?php
                          } ?>
                     </i>



                  </a>

              </span>
                    <span class="hidden-xs">
                         <a href="/crequests">
                         <i class="icon glyphicon glyphicon-hourglass topbar__icon"></i>
                        </a>
                         </span>
                    <span class="hidden-xs">
                        <a href="/">
                         <i class="icon glyphicon glyphicon-home topbar__icon"></i>
                        </a>
                         </span>
                    <span class="hidden-xs">
                        <a href="/pricings">
                         <i class="icon glyphicon glyphicon-book topbar__icon"></i>
                        </a>
                         </span>
                    <?php
                } ?>
            </div>
            <div class="topbar noselect">
                <img class="img-circle topbar-userpic" src="<?= $this->client->userpic; ?>" height="40px">
                <span class="user_online__indicator user_online__indicator--online"></span>
                <span class="topbar__name fc-dropdown--trigger" style="text-transform:capitalize;font-weight: 700;">
          <?php echo character_limiter($this->client->firstname . ' ' . $this->client->lastname, 25); ?> <i
                            class="ion-chevron-down" style="padding-left: 2px;"></i>
      </span>
                <div class="fc-dropdown profile-dropdown">
                    <ul>
                        <li>
                            <a href="<?= site_url('agent'); ?>" data-toggle="mainmodal">
                                <span class="icon-wrapper"><i
                                            class="ion-gear-a"></i></span> <?= $this->lang->line('application_profile'); ?>
                            </a>
                        </li>

                        <li class="fc-dropdown__submenu--trigger">
                            <span class="icon-wrapper"><i
                                        class="ion-ios-arrow-back"></i></span> <?= $current_language; ?>
                            <ul class="fc-dropdown__submenu">
                                <span class="fc-dropdown__title"><?= $this->lang->line('application_languages'); ?></span>
                                <?php foreach ($installed_languages as $entry) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?>agent/language/<?= $entry; ?>">
                                            <img src="<?= base_url() ?>assets/blueline/img/<?= $entry; ?>.png"
                                                 class="language-img"> <?= ucwords($entry); ?>
                                        </a>
                                    </li>

                                    <?php
                                } ?>
                            </ul>

                        </li>
                        <li class="profile-dropdown__logout">
                            <a href="<?= site_url('logout'); ?>"
                               title="<?= $this->lang->line('application_logout'); ?>">
                                <?= $this->lang->line('application_logout'); ?> <i class="ion-power pull-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>


        <?= $yield ?>


    </div>
    <!-- Notify -->
    <?php if ($this->session->flashdata('message')) {
        $exp = explode(':', $this->session->flashdata('message')) ?>
        <div class="notify <?= $exp[0] ?>"><?= $exp[1] ?></div>
        <?php
    } ?>


    <!-- Modal -->
    <div class="modal fade" id="mainModal" tabindex="-1" role="dialog" data-backdrop="static"
         aria-labelledby="mainModalLabel" aria-hidden="true"></div>


    <!-- Js Files -->

    <script type="text/javascript"
            src="<?= base_url() ?>assets/blueline/js/app.js?ver=<?= $core_settings->version; ?>"></script>
    <script>
        flatdatepicker(false, langshort);
    </script>


</div> <!-- Mainwrapper end -->
<script>
    $('#hideMenu').on('click', function () {
        console.log($('.side').hasClass('hideMenu'));
        if ($('.side').hasClass('hideMenu')) {
            $('.nav-text').css('display', 'block')
            $('.navbar-header').css('height', 'auto')
            $('.sidebar').css('width', '200px')
            $('.sidebar-bg').css('width', '200px')
            $('.content-area').css('margin-left', '200px')
            $('.navbar-brand').css('display', 'block')
            $('.side').attr('class', 'side ')
            $('.menu-sub').css('display', 'block')
            $('#logoImg').attr('src', 'assets/blueline/images/workye.com.svg');
            $('.navbar-brand').attr('style', 'width: 140px; padding: 0px 16px; height: 65px;')
            $('.navbar-header').attr('style', 'width: 140px;padding: 0 32px 0 0;')
            $('.main').css('margin-left', '0')
            $('#hideMenu').css('margin-left', '0')

        } else {

            $('.nav-text').css('display', 'none')
            $('#logoImg').attr('src', 'assets/blueline/img/workye-logo-w-black.svg');
            $('.navbar-brand').css('width', '60px')
            $('.main').css('margin-left', '10px')

            $('#hideMenu').css('margin-left', '5px')
            $('.navbar-header').css('height', '65px')
            $('.sidebar').css('width', '60px')
            $('.sidebar-bg').css('width', '60px')
            $('.side').css('width', '60px')
            $('.side').attr('class', 'side hideMenu')
            $('.content-area').css('margin-left', '50px')
            $('.nav-sidebar li a').css('height', '50px')
            $('.menu-sub').css('display', 'none')
            $('.navbar-header').attr('style', 'width:60px')
            $('.navbar-brand').attr('style', 'width: 60px; padding: 0px 32px 0px 0px;')
        }
    })
</script>
</body>
</html>
<?php } ?>
<?php } else {



?>
    <!doctype html>
<!--[if IE 7 ]>
<html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>

    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <script>(function (d) {
            var config = {
                    kitId: 'gnf7xqu',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement, t = setTimeout(function () {
                    h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                s = d.getElementsByTagName("script")[0], a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function () {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {
                }
            };
            s.parentNode.insertBefore(tk, s)
        })(document);</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    <title>workye - A modern way to hire virtual employees online</title>
    <meta name="description" content="
We'll Find You The Perfect employees For Your Projects! It's Easy & Affordable. Secure Payments. Increased Productivity. 90% of Customers Rehire. Trusted by 4M+ Businesses. Services: Designers, Web, Mobile, Marketers, Writers, Developers, Coders.">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
          rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>


    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- ######### CSS STYLES ######### -->
    <link rel="stylesheet" href="/assets/blueline/pages/css//bootsrap.css">
    <link rel="stylesheet" href="/assets/blueline/pages/css/reset.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/blueline/pages/css/style.css" type="text/css"/>

    <!-- simple line icons -->
    <link rel="stylesheet" type="text/css" href="/assets/blueline/pages/css/simpleline-icons/simple-line-icons.css"
          media="screen"/>

    <!-- et linefont icons -->
    <link rel="stylesheet" href="/assets/blueline/pages/css/et-linefont/etlinefont.css">

    <!-- animations -->
    <link href="/assets/blueline/pages/js/animations/css/animations.min.css" rel="stylesheet" type="text/css"
          media="all"/>

    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="/assets/blueline/pages/css/responsive-leyouts.css" type="text/css"/>

    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="/assets/blueline/pages/css/shortcodes.css" type="text/css"/>


    <!-- mega menu -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
          crossorigin="anonymous">
    <link href="/assets/blueline/pages/js/mainmenu/demo.css" rel="stylesheet">
    <link href="/assets/blueline/pages/js/mainmenu/menu.css" rel="stylesheet">

    <!-- MasterSlider -->
    <link rel="stylesheet" href="/assets/blueline/pages/js/masterslider/style/masterslider.css"/>
    <link rel="stylesheet" href="/assets/blueline/pages/js/masterslider/skins/default/style.css"/>

    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="/assets/blueline/pages/js/cubeportfolio/cubeportfolio.min.css">

    <!-- owl carousel -->
    <link href="/assets/blueline/pages/js/carouselowl/owl.transitions.css" rel="stylesheet">


    <!-- tabs 2 -->
    <link href="/assets/blueline/pages/js/tabs2/tabacc.css" rel="stylesheet"/>
    <link href="/assets/blueline/pages/js/tabs2/detached.css" rel="stylesheet"/>

</head>


<?=
$yield;
?>


<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<script type="text/javascript" src="/assets/blueline/pages/js/universal/jquery.js"></script>
<script src="/assets/blueline/pages/js/style-switcher/styleselector.js"></script>
<script src="/assets/blueline/pages/js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="/assets/blueline/pages/js/mainmenu/bootstrap.min.js"></script>
<script src="/assets/blueline/pages/js/mainmenu/customeUI.js"></script>


<script src="/assets/blueline/pages/js/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/blueline/pages/js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="/assets/blueline/pages/js/mainmenu/modernizr.custom.75180.js"></script>
<script type="text/javascript" src="/assets/blueline/pages/js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="/assets/blueline/pages/js/cubeportfolio/main.js"></script>
<script src="/assets/blueline/pages/js/tabs2/index.js"></script>
<script>
    $('.accordion, .tabs').TabsAccordion({
        hashWatch: true,
        pauseMedia: true,
        responsiveSwitch: 'tablist',
        saveState: sessionStorage,
    });
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 1) {
            //clearHeader, not clearheader - caps H
            $(".fancy-menu-wrap").addClass("fancy-menu-wrap-scrolled");
        } else {
            if (scroll <= 0) {
                $(".fancy-menu-wrap").removeClass("fancy-menu-wrap-scrolled");
            }
        }
    });
</script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/assets/blueline/pages/js/aninum/jquery.animateNumber.min.js"></script>
<script type="text/javascript" src="/assets/blueline/pages/js/script.js"></script>

</html>

<?php } ?>
