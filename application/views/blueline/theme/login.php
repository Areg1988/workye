<?php 
/**
 * @file        Login View
 * @author      Luxsys <support@freelancecockpit.com>
 * @copyright   By Luxsys (http://www.freelancecockpit.com)
 * @version     3.2.0
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <META Http-Equiv="Cache-Control" Content="no-cache">
    <META Http-Equiv="Pragma" Content="no-cache">
    <META Http-Equiv="Expires" Content="0">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="18000">
    
    <title><?=$core_settings->company;?></title>
    
    <link href="<?=base_url()?>assets/blueline/css/bootstrap.min.css?ver=<?=$core_settings->version;?>" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/blueline/css/plugins/animate.css?ver=<?=$core_settings->version;?>" />
    <link rel="stylesheet" href="<?=base_url()?>assets/blueline/css/plugins/nprogress.css" />
    <link href="<?=base_url()?>assets/blueline/css/blueline.css?ver=<?=$core_settings->version;?>" rel="stylesheet">
    <link href="<?=base_url()?>assets/blueline/css/user.css?ver=<?=$core_settings->version;?>" rel="stylesheet" /> 
    <?=get_theme_colors($core_settings);?>
    <?php require_once '_partials/fonts.php'; ?>

     <link rel="SHORTCUT ICON" href="<?=base_url()?>assets/blueline/img/favicon.ico"/>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

  </head>

  <body class="login" style="background-image:url('<?=base_url()?>assets/blueline/images/backgrounds/<?=$core_settings->login_background;?>')">
    <div class="container-fluid">
      <div class="row" style="margin-bottom:0px">
        <?=$yield?>
      </div>
    </div>
     <!-- Notify -->
    <?php if ($this->session->flashdata('message')) {
    $exp = explode(':', $this->session->flashdata('message'))?>
        <div class="notify <?=$exp[0]?>"><?=$exp[1]?></div>
    <?php
} ?>
    <script src="<?=base_url()?>assets/blueline/js/plugins/jquery-2.2.4.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/blueline/js/plugins/velocity.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/blueline/js/plugins/velocity.ui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/blueline/js/plugins/validator.min.js"></script>
   

  </body>
</html>
