<style>
    [class*=flicon-] {
        display: inline-block;
        width: 24px;
        height: 24px;
        fill: transparent;
        stroke: #4D525B;
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-width: 1.2px
    }
    @media only screen and (max-width: 375px) {
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 10px 6px 10px;
            border-top: 1px solid #eee;
        }
        .table-div {
            padding: 0 10px;
        }
    }
</style>
<div class="col-sm-12  col-md-12 main">
    <div class="row">

        <div class="box-shadow" >
            <a href="<?= base_url() ?>categories/create" class="btn btn-primary"
               data-toggle="mainmodal"
               style='margin-bottom:10px;margin-top: 20px'> <?= $this->lang->line('application_create_category'); ?></a>
            <div class="table-head"><?= $this->lang->line('application_subscriptions'); ?></div>

            <div class="table-div" >
                <table class="data table" id="subscriptions" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
                    <thead>
                    <th class="hidden-xs hidden-md sorting">#</th>
                    <th class="hidden-xs hidden-md sorting">Name</th>

                    <th class="hidden-xs hidden-md sorting">Description</th>
                    <th class="hidden-xs hidden-md sorting">Icon</th>
                    <th class="hidden-xs hidden-md sorting">Edite</th>
                    <th class="hidden-xs hidden-md sorting">Delete</th>

                    </thead>
                    <?php foreach ($categories as $value): ?>

                        <tr>
                            <td>
                                <?= $value->id ?>
                            </td>
                            <td>
                                <a href="categories/editSubCategory/<?= $value->id ?>"> <?= $value->name ?></a>
                            </td>
                            <td>
                                <?= $value->description ?>
                            </td>

                            <td>
                                <?= $value->icon ?>
                            </td>

                            <td>
                                <a href="categories/edite_categories/<?= $value->id ?>"
                                   class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td>
                                <a href="categories/deleteCategory/<?= $value->id ?>"
                                   class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>

</div>

<script>

    $("[id='job categories']").attr('class', 'active')

</script>