<?php
$attributes = ['class' => '', 'style' => 'background-color: white;padding: 15px;'];
echo form_open($form_action, $attributes);

?>
    <input class="form-control" name="id" type="hidden" value="<?= $categories->id ?>" />
<div class="form-group">
    <label for="terms">name</label>
    <input class="form-control" name="name" type="text" value="<?= $categories->name ?>" />
</div>
<div class="form-group">
    <label for="terms">Description</label>
    <input class="form-control" name="description" type="text" value="<?= $categories->description ?>" />
</div>
<div class="form-group">
    <label for="terms">Icon</label>
    <textarea class="form-control" name="icon" ><?= $categories->icon ?></textarea>
</div>

  <input type="submit" name="send" id="submit" class="btn btn-primary" value="<?=$this->lang->line('application_save');?>"/>
<?php echo form_close(); ?>


