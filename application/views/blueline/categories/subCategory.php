
<div class="box-shadow">
    <a href="<?= base_url() ?>categories/createSubCategory" class="btn btn-primary"
       data-toggle="mainmodal"
       style='margin-bottom:10px'><?= $this->lang->line('application_create_category'); ?></a>
    <div class="table-head"><?= $this->lang->line('application_subscriptions'); ?></div>

    <div class="table-div">
        <table class="data table" id="subscriptions" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
            <thead>
            <th class="hidden-xs" width="70px">#</th>
            <th>Name</th>

            <th class="hidden-xs hidden-md">Category_id</th>

            <th class="hidden-xs hidden-md">Delete</th>

            </thead>
            <?php foreach ($subCategories as $value): ?>

                <tr>
                    <td>
                        <?= $value->id ?>
                    </td>
                    <td>
                        <a href="<?= base_url() ?>categories/subCategories/<?= $value->id ?>"> <?= $value->name ?></a>
                    </td>


                    <td>
                        <?= $value->category ?>
                    </td>

                    <td>
                        <a href="<?= base_url() ?>categories/deleteSubCategories/<?= $value->id ?>"
                           class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>

            <?php endforeach; ?>
        </table>
    </div>
</div>

