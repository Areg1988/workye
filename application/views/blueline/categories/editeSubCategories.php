<style>
    #categories{
        height: 37px!important;
    }
</style>
<?php
$attributes = ['class' => '', 'style' => 'background-color: white;padding: 15px;'];
echo form_open($form_action, $attributes);

?>
<input class="form-control" name="id" type="hidden" value="<?= $subCategories['id'] ?>" />
<div class="form-group">
    <label for="terms">name</label>
    <input class="form-control" name="name" type="text" value="<?= $subCategories['name'] ?>" />
</div>
<div class="form-group">
    <label for="terms">Main category</label>
    <select  class="form-control" name="categories" id="categories">
        <option value="<?= $subCategories['category_id'] ?>"></option>
        <?php foreach ($categories as $value) {?>
        <option value="<?= $value->id ?>"><?= $value->name ?></option>
        <?php }?>
    </select>
<!--    <input class="form-control" name="categoryId" type="text" value="--><?//= $subCategories->category_id ?><!--" />-->
</div>


<input type="submit" name="send" id="submit" class="btn btn-primary" value="<?=$this->lang->line('application_save');?>"/>
<?php echo form_close(); ?>


