<style>
.label-danger {
    background-color: white!important;
    color:#d9534f!important;
    border:1px solid #d9534f!important;
}</style>
<div class="col-sm-12  col-md-12 ">
    <div class="row">
        <div class="box-shadow">
            <div class="table-div">
                <div class="table-head">Requests</div>
                <table class="data table" id="projects" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th >Project ID</th>
                        <th>Project Name</th>
                        <th >Employee Name</th>
                        <!--<th>Value</th>-->
                        <th class="hidden-xs ">Description</th>
                        <th class="hidden-xs ">Status</th>
                        <th class="hidden-xs ">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach($requests as $request) { ?>
                     <tr>
                         
                        <th>
                        <?php if($request['type'] and $request['status'] == 'pending') {?>
                            <a href='crequests/view/<?= $request['id']?>'  data-toggle="mainmodal">
                            <?= $request['reference'] ?>
                            </a>
                        <?php } else {?>
                         <?= $request['reference'] ?>
                        <?php }?>
                        </th>
                        <th><?= $request['project'] ?></th>
                        <th><?= $request['fullName'] ?></th>
                        <!--<th><?= $request['sum'] ?><i class='glyphicon glyphicon-euro' style='font-size: 10px;'></i></th>-->
                        <th class="hidden-xs "><?= $request['description'] ?></th>
                        <th class="hidden-xs ">
                        <?php if($request['status'] == 'Approved') { ?>
                            <span class="label label-success"><?= $request['status'] ?></span>
                        <?php }elseif($request['status'] == 'pending') {?>
                            <span class="label label-info"><?= $request['status'] ?></span>
                            <?php }elseif($request['status'] == 'Refused') {?>
                                <span class="label label-danger"><?= $request['status'] ?></span>
                            <?php } ?>
                        </th>
                        <th class="hidden-xs ">
                        <a href="/crequests/delete/<?= $request['id'] ?>"><i class='glyphicon glyphicon-trash'></i></a>
                        </th>
                    </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    sessionStorage.setItem("valuePlan", 'as');
    sessionStorage.setItem("countPlan", 'asd');
</script>