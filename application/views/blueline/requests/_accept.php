<div class="col-sm-12  col-md-12 ">

    <div class="row" style="background: white">
        <div class="box-shadow">
            <?php
            $attributes = array('class' => '', 'id' => '_project');
            echo form_open($form_action, $attributes); ?>

            <input id="id" type="hidden" name="id" value="<?= $request['id'] ?>"/>
            <div class="form-group">
                <label for="reference">Projects ID</label>
                <input class='form-control' style='height: 35px;border: none;' name="project" title="projects"
                       value='<?= $request['reference'] ?>' readonly/>


            </div>
            <div class="form-group">
                <label for="reference">Projects Name</label>
                <input class='form-control' style='height: 35px;border: none;' name="project" title="projects"
                       value='<?= $request['project'] ?>' readonly/>
            </div>
            <div class="form-group">
                <label for="textfield"><?= $this->lang->line('application_description'); ?></label>
                <textarea class="input-block-level form-control" id="textfield" name="description"
                          readonly><?= $request['description'] ?></textarea>
            </div>

            <div class="form-group">
                <label for="last4">Last 4 number</label>
                <input class='form-control' style='height: 35px;border: none;' name="last4" title="last4"
                       value='**** **** **** <?= $employes->last ?>' readonly/>
            </div>
            <div class="form-group">
                <label for="type">Card type</label>
                <?php if ($employes->brand == 'MasterCard') { ?>
                    <img src="/assets/blueline/images/icons/master.jpg" width="50px" height="30px"
                         style="display: block;margin-left: 10px;margin-top: 5px;" alt="">
                <?php } elseif ($employes->brand == 'visa') { ?>
                <?php } ?>
            </div>
            <div class="form-group" id="value" >
                <label for="value">Value</label>
                <input class='form-control' style='height: 35px;border: none;' name="value" title="value"
                       value=''/>
            </div>
            <input type="hidden" name="cardId" value="<?= $employes->brand ?>">
            <div class="form-group">
                <label for="status">Stutus</label>
                <select class="form-control" id="status" name="status" required style='height:35px;border:none'>
                    <option value='Approved'>Approved</option>
                    <option value='Refused'>Refused</option>
                </select>
            </div>

            <div class="form-group message" style='display:none'>
                <label for="message">Message</label>
                <textarea class="input-block-level form-control" id="message" name="message"></textarea>
            </div>

            <div class="modal-footer">
                <input type="submit" name="send" class="btn btn-primary" value="SEND REQUEST"/>
                <a class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('application_close'); ?></a>
            </div>
        </div>

    </div>
</div>
<?php echo form_close(); ?>
<script>
    $('#status').on('change', function () {
        if ($('#status').val() == 'Refused') {
            $('.message').attr('style', 'display:block');
            $('#message').attr('required', 'required')
            $('#value').attr('style', 'display:none');
        }
        if ($('#status').val() == 'Approved') {
            $('.message').attr('style', 'display:none')
            $('#message').removeAttr('required', 'required')
            $('#value').attr('style', 'display:block');
        }
    })

</script>