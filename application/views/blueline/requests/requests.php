<style>
.label-danger {
    background-color: white!important;
    color:#d9534f!important;
    border:1px solid #d9534f!important;
}
th {
    padding: 20px;
}
</style>
<div class="col-sm-12  col-md-12 ">
    <div class="row">
        <div class="box-shadow">
            
<!--            <div class="row" style="    display: inline;">-->
<!--       <a href="/requests/create" class="btn btn-primary" data-toggle="mainmodal">New Requests</a>-->
<!--      -->
<!--        </div>-->
    
                
            <div class="table-div">
            
                <div class="table-head">Requests</div>
                <table class="data table" id="projects" rel="<?= base_url() ?>" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Project Name</th>
                        <th class="hidden-xs ">Employee</th>
                        <!--<th>Sum</th>-->
                        <th class="hidden-xs ">Description</th>
                        <th>Status</th>
                        <th class="hidden-xs ">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach($requests as $request) { ?>
                     <tr>
                         <?php if($this->user->admin and $request['status'] == 'Approved') {?>
                        <th>
                            <a  href='/requests/update/<?= $request['id'] ?>'>
                            <?= $request['company'] ?>
                            </a>
                        </th>
                         <?php } else {?>
                        <th>
                            <?= $request['company'] ?>
                        </th>
                         <?php } ?>
                        <th><?= $request['project'] ?></th>
                        <th class="hidden-xs "><?= $request['fullName'] ?></th>
                        <!--<th><?= $request['sum'] ?><i class='glyphicon glyphicon-euro' style='font-size: 10px;'></i></th>-->
                        <th class="hidden-xs "><?= $request['description'] ?></th>
                        <th>
                        <?php if($request['status'] == 'Approved') { ?>
                            <span class="label label-success"><?= $request['status'] ?></span>
                        <?php }elseif($request['status'] == 'Paid') {?>
                            <span class="label label-success"><?= $request['status'] ?></span>
                        <?php }elseif($request['status'] == 'pending') {?>
                            <span class="label label-info"><?= $request['status'] ?></span>
                        <?php }elseif($request['status'] == 'Refused') {?>
                                <span class="label label-danger"><?= $request['status'] ?></span>
                        <?php } ?>
                        </th>
                        <th class="hidden-xs ">
                        <a href="/requests/delete/<?= $request['id'] ?>"><i class='glyphicon glyphicon-trash'></i></a>
                        </th>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    $("#requests").attr('class', 'active')

</script>