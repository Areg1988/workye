<?php
$attributes = array('class' => '', 'id' => '_project');
echo form_open($form_action, $attributes);
if (isset($project)) { ?>
    <input id="id" type="hidden" name="id" value="<?php echo $project->id; ?>"/>
<?php } ?>

<div class="form-group">
    <label for="reference">Projects *</label>
    <select class='form-control' style='height: 35px;border: none;' name="project" title="projects">
        <?php foreach($projects as $project) { ?>
            <option  value="<?= $project->id ?>"><?= $project->reference ?> <?= $project->name ?></option>
        <?php } ?>
    </select>
</div>





<div class="form-group">
    <label for="phases">Value *</label>
    <input type="text" name="phases" class="form-control" id="phases" value="" required/>
</div>

<div class="form-group">
    <label for="textfield"><?= $this->lang->line('application_description'); ?></label>
    <textarea class="input-block-level form-control" id="textfield" name="description" required></textarea>
</div>




<div class="modal-footer">
    <input type="submit" name="send" class="btn btn-primary" value="SEND REQUEST"/>
    <a class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('application_close'); ?></a>
</div>

<?php echo form_close(); ?>
