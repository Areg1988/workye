

<div class="col-xs-12 col-sm-12" style="padding: 0">
    <a href="/pricings" class="btn btn-primary">Update Now</a>
    <?php if($data) {?>
    <a  id='cancelPlan' class="btn btn-danger">Cancel Plan</a>
    <div class="box-shadow">
        <div class="table-head" style="margin-top: 20px; ">Pricing PLan</div>
        <table class="table noclick" id="items" rel="<?=base_url()?>" style="background: white" cellspacing="0" cellpadding="0">
            <thead>

            <th>Pricing Plan</th>
            <th>Employee count</th>
            <th class="hidden-xs ">Category</th>
            <th class="hidden-xs ">Sub Category</th>
            <th>Next Payment</th>
            <th>Payment type</th>


            </thead>
            <tr>
                <td  align="left"><?= $data['title'] ?></td>
                <td  align="left"><?= $data['count'] ?> Employes</td>
                <td align="left" class="hidden-xs "><?= $data['category'] ?></td>
                <td  align="left" style="text-transform: capitalize" class="hidden-xs "><?= $data['subCategory'] ?></td>
                <td  align="left"><?= $data['nextPaymant'] ?></td>
                <td   align="left" style="text-transform: capitalize" ><?= $data['type'] ?></td>

            </tr>
        </table>


    </div>
<div id="paymant" style="display: none">
    <?php
    $attributes = array('class' => '', 'id' => 'payment-form');
    echo form_open('/cpricing/pay', $attributes);
    ?>
    <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="<?= $data['publishable_key'] ?>"
            data-image="/assets/blueline/img/workye-logo-w-black.svg"
            data-name="Workey.com"
            data-panel-label="Add a new credit card"
            data-label="Add a new credit card"
            data-allow-remember-me=false
            data-email="<?php echo $this->client->email; ?>">
        data-locale="auto">
    </script>
    <?php echo form_close(); ?>
</div>
</div>
<script>

    $('#cancelPlan').on('click', function () {
        var answer = confirm('if you cancel your plan, all of your projects will be delated. Are you sure?')
        if(answer == true) {
            var token = csfrData['fcs_csrf_token'];
            console.log(token)
            $.ajax({
                url: "/cpricing/cancel",
                type: "POST",
                data: {fcs_csrf_token: token},
                success: function (data) {
                    var res = JSON.parse(data)
                   if(res.data == 'true') {
                        window.location.href = '/cpricing/cancelPlan'
                   } else if(res.data == 'false'){
                       $('#paymant').attr('style', 'display:block')
                       $('.stripe-button-el').click()
                   }
                }
            });


        } else {

        }
    })
</script>
<?php } ?>