<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<div class="col-xs-12 col sm-12">
    <div class="row">
        <div class="box-shadow" style="background: white;    padding: 15px;">
            <?php
            $attributes = array('class' => '', 'id' => 'payment-form');
            echo form_open('/cpricing/stripepay', $attributes);
            ?>
            <input type="hidden" name="subscriptionId" value="<?= $data['subscriptionId'] ?>">
            <input type="hidden" name="pricingId" id="pricingId" value="">
            <div class="form-group">
                <label for="title">Pricing Plan</label>
                <input id="title" name="title" type="text" class="form-control" value="" required/>
            </div>
            <div class="form-group">
                <label for="pricing">Employee count</label>
                <input id="pricing" name="pricing" type="text" class="form-control" value="" required/>
            </div>

            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="form-control" style="height: 35px;border: none" required>
                    <option value="">Please choose category</option>
                    <?php foreach ($data['category'] as $value): ?>
                        <option value="<?= $value->id ?>"><?= $value->name ?></option>
                    <?php endforeach; ?>
                </select>

            </div>
            <div class="form-group">
                <label for="subCategory">Sub Category</label>
                <select name="subCategory" id="subCategory" class="form-control" style="height: 35px;border: none"
                        required>
                    <option value="">Please choose first category</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nextPaymant">Next Payment</label>
                <input id="nextPaymant" type="text" name="nextPaymant" class="form-control"
                       value="<?php if ($data['type'] == 'annually') {
                           echo date('Y-m-d', strtotime('+1 year'));
                       } elseif ($data['type'] == 'monthly') {
                           echo date('Y-m-d', strtotime('+1 month'));
                       } else {
                           
                       }
                       ?>
                " readonly/>
            </div>
            <div class="form-group">
                <label for="type">Payment type</label>
                <select id="type" type="text" name="type" class="form-control" style="height: 35px;border: none"
                        required>
                    <option value="">Please choose Paymant type</option>
                    <option value="monthly" <?php if($data['type'] == 'monthly') echo 'selected'; ?>>Monthly</option>
                    <option value="annually" <?php if($data['type'] == 'annually') echo 'selected'; ?>>Annually</option>

                </select>

            </div>


            <div id="payment-errors" class="payment-errors"></div>
            <?php
            if (isset($errors) && !empty($errors) && is_array($errors)) {
                echo '<div class="alert alert-danger"><h4>Error!</h4>The following error(s) occurred:<ul>';
                foreach ($errors as $e) {
                    echo "<li>$e</li>";
                }
                echo '</ul></div>';
            } ?>

            <div class="payment-help" style="padding: 20px 20px">You can pay with Mastercard,
                Visa, American Express, JCB, Discover, Diners Club.
            </div>

            <div class="form-group">
                <label>CARD NUMBER</label>
                <input type="text" size="20" autocomplete="off" class="form-control card-number input-medium"
                       placeholder="Enter the number without spaces or hyphens.">
            </div>


            <div class="row">
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label>MONTH (MM)</label>
                        <input type="text" class="form-control card-expiry-month"
                               placeholder="Month">
                    </div>
                </div>
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label>YEAR (YYYY)</label>
                        <input type="text" size="4" class="form-control card-expiry-year"
                               placeholder="Year">
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>CVC</label>
                        <input type="text" size="4" autocomplete="off" class="form-control card-cvc input-mini"
                               placeholder="CVC code">
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="valueSum">PAYMENT *</label>
                <input id="valueSum" type="text" name="sum" class="required form-control number money-format"
                       value="" readonly required/>
            </div>
            <button type="submit" class="btn btn-primary"
                    id="submitBtn">Send
            </button>
            <?php echo form_close(); ?>


            <script type="text/javascript">// <![CDATA[
                Stripe.setPublishableKey('<?php echo $data['public_key']; ?>');
                // ]]></script>


            <script type="text/javascript" src="<?= base_url() ?>assets/blueline/js/plugins/buy.js"></script>




        </div>
    </div>
    <script>
        var countPlan = sessionStorage.getItem("countPlan");
        var valuePlan = sessionStorage.getItem("valuePlan");
        var planeName = sessionStorage.getItem("planeName");
        var planeId = sessionStorage.getItem("id");

        console.log($('#type').val())

        $('#type').on('change', function () {
            if($('#type').val() == 'annually') {
                valuePlan = valuePlan * 12;
            }else {
                valuePlan = sessionStorage.getItem("valuePlan")
            }
            $('#valueSum').val(valuePlan)
        })

        if (planeId) {
            $('#pricingId').val(planeId)
        } else {
            $('#pricingId').val('1')

        }
        $('#title').val(planeName)
        $('#pricing').val(countPlan)
        $('#valueSum').val(valuePlan)

        $('#category').on('change', function () {
            var category = $('#category').val()
            var token = csfrData['fcs_csrf_token'];
            $.ajax({
                url: "/home/getCategory/",
                type: "POST",
                data: {fcs_csrf_token: token, category: category},
                success: function (data) {
                    $('#subCategory').empty()
                    var subCategory = JSON.parse(data)
                    var data2 = subCategory.data
                    for (var i = 0; i < data2.length; i++) {
                        $('#subCategory').append('<option value="' + data2[i]['id'] + '">' + data2[i]['name'] + '</option>')
                    }

                }
            });

        })

    </script>