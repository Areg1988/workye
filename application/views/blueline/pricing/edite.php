<div id='message' style='display:none'>
    <div class="alert alert-success" role="alert">Page added successfully.</div>
</div>
<div id='message2' style='display:none'>
    <div class="alert alert-success" role="alert">Page updated successfully.</div>
</div>

<?php
$attributes = ['class' => '', 'style' => 'background-color: white;padding: 15px;'];
echo form_open($form_action, $attributes);

?>
    <input class="form-control" name="id" type="hidden" value="<?= $pricing->id ?>" />
<div class="form-group">
    <label for="terms">name</label>
    <input class="form-control" name="name" type="text" value="<?= $pricing->name ?>" />
</div>
<div class="form-group">
    <label for="terms">starter</label>
    <input class="form-control" name="starter" type="text" value="<?= $pricing->starter ?>" />
</div>
<div class="form-group">
    <label for="terms">standard</label>
    <input class="form-control" name="standard" type="text" value="<?= $pricing->standard ?>" />
</div>
<div class="form-group">
    <label for="terms">professional</label>
    <input class="form-control" name="professional" type="text" value="<?= $pricing->professional ?>" />
</div>
<div class="form-group">
    <label for="terms">enterprise</label>
    <input class="form-control" name="enterprise" type="text" value="<?= $pricing->enterprise ?>" />
</div>

  <input type="submit" name="send" id="submit" class="btn btn-primary" value="<?=$this->lang->line('application_save');?>"/>
<?php echo form_close(); ?>


<?php

if($pricingPlans) { ?>
    <script>
        $('#submit').click(function () {

            $('#message').css('display', 'block')
            setTimeout(function(){  $('#message').css('display', 'none') }, 4000);

        })
    </script>
<?php } else { ?>
    <script>
        $('#submit').click(function () {

            $('#message2').css('display', 'block')
            setTimeout(function(){  $('#message2').css('display', 'none') }, 4000);

        })
    </script>
<?php } ?>
