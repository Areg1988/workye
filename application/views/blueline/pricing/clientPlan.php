<?php
$attributes = array('class' => '', 'id' => '_item');
echo form_open($form_action, $attributes);
?>

<?php if(isset($items)){ ?>
    <input id="id" type="hidden" name="id" value="<?=$items->id;?>" />
<?php } ?>
    <div class="form-group">
        <label for="title">Pricing Plan</label>
        <input id="title" name="title" type="text" class="form-control"  value="<?=  $data['title'] ?>"  required/>
    </div>
    <div class="form-group">
        <label for="pricing">Employee count</label>
        <input id="pricing" name="pricing" type="text" class="form-control"  value="<?=  $data['pricing'] ?>"  required/>
    </div>
    <div class="form-group">
        <label for="type">Employee</label>
        <input id="type" type="text" name="type" class=" form-control"  value="<?= $data['subCategory']?>"  readonly/>
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <input id="category" type="text" name="category" class="form-control number"  value="<?= $data['category']?>"  readonly/>
    </div>
 <div class="form-group">
        <label for="subCategory">Sub Category</label>
        <input id="subCategory" type="text" name="subCategory" class="form-control"  value="<?= $data['subCategory']?>"  readonly/>
    </div>
    <div class="form-group">
        <label for="nextPaymant">Next Payment</label>
        <input id="nextPaymant" type="text" name="nextPaymant" class="form-control"  value="<?= $data['nextPaymant']?>"  readonly/>
    </div><div class="form-group">
        <label for="type">Payment type</label>
        <input id="type" type="text" name="type" class=" form-control"  value="<?= $data['type']?>"  readonly/>
    </div>


    <div class="modal-footer">
        <a href="/pricings"  class="btn btn-primary" >Upgrade</a>
        <a class="btn" data-dismiss="modal"><?=$this->lang->line('application_close');?></a>
    </div>
<?php echo form_close(); ?>