<body class="bg-cover compact">
<main class="site_wrapper">

    <div class="clearfix"></div>

    <?php
    if ($this->user or $this->client) {

    } else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
    }

    ?>

    <div class="clearfix"></div>

    <!-- Slider
    ======================================= -->
    <section class="carousel-section">
        <div class="container-fluid">
            <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                        <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item item-1 active fade">
                            <h1>Teams and teamwork <br> together at last</h1>
                            <p>
                                Video conferencing, screen share, and file sharing all in one
                                place,<br> designed to fit the way you work.
                            </p>
                            <p>
                                <a href="#" class="btn-mine mine1">Try Meetings free</a>
                                <a href="#" class="btn-mine mine2">See Pricing</a>
                            </p>
                            <!--<img src="/assets/blueline/pages/images/home_1_wide.png" alt="home_1_wide.png">-->
                        </div>
                        <div id="item-2" class="item item-2 fade">
                            <div class="container">
                                <div class="col-lg-4">
                                    <h1>Meetrings <br> just got way better.</h1>
                                    <p>
                                        Get ready for easier, more intuitive meetings with the new Webex
                                        <br> Meetings desktop app. <a href="#">Learn more</a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn-mine mine1">Try Meetings free</a>
                                        <a href="#" class="btn-mine mine2">See Pricing</a>
                                    </p>
                                </div>
                                <div class="col-lg-8">
                                    <img src="assets/blueline/pages/images/home_hero2.jpg" alt="home_hero2.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                                class="fas fa-angle-left"></span></a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                                class="fas fa-angle-right"></span></a>
                </div>
            </div>
        </div>
    </section>
    <!-- end of masterslider -->
    <div class="clearfix"></div>
    <section class="featured_section6 featured_section88">
        <div class="container">
            <div class="row">
                <h3 class="text-centered second-column" style="margin-top: 40px;margin-bottom: 0;padding: 0">With you every step of the way.</h3>
                <p class="col-md-6 col-md-offset-3 text-centered" style="color: black;">From
                    online meetings, to whiteboarding, to file sharing with the whole team,
                    work progresses with Webex. It’s how millions do their best teamwork.</p>
            </div>
            <div class="one_fourth animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="100">
                <i class="fa fa-desktop"></i>
                <h4>Pricing Tables</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="200">
                <i class="fa fa-line-chart"></i>
                <h4>New Sections</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="300">
                <i class="fa fa-picture-o"></i>
                <h4>More Websites</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth last animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">
                <i class="fa fa-magic"></i>
                <h4>Build your Own</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="clearfix"></div>

            <div class="one_fourth btm animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="500">
                <i class="fa fa-diamond"></i>
                <h4>Flexibility</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth btm animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="600">
                <i class="fa fa-youtube-play"></i>
                <h4>Video Sections</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth btm animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="700">
                <i class="fa fa-crop"></i>
                <h4>PSD Files</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

            <div class="one_fourth btm last animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="800">
                <i class="fa fa-comments-o"></i>
                <h4>Cross Check</h4>
                <p>Many desktop publish packages web page editors now use.</p>
            </div><!-- end section -->

        </div>
    </section>
   <!-- end featured section 1 -->


    <section class="featured_section2">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-1" style="padding: 20px">
                    <div>
                        <h3>Cisco Webex Meetings</h3>
                        <h1>Online meetings made simple.</h1>
                        <p class="third-section-text">Webex Meetings brings people all over the globe together. It’s like
                            being there in person, even when you can’t be. Join from any device, get HD quality audio and
                            video, or share your screen.
                            <br>
                            <a class="learn-more" href="">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6" style="padding: 20px">
                    <div class="">
                        <img class="response-img section-response-img"
                             style="max-width: 513px; max-height:321px;margin-top: 57px;"
                             src="https://www.webex.com//content/dam/wbx/global/images/home_6.png" alt=""></div>
                </div>
            </div>
        </div>
    </section><!-- end featured section 2 -->


    <section class="featured_section3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1" style="padding: 20px">
                    <div class="">
                        <img class="response-img section-response-img"
                             style="max-width: 513px; max-height:321px;margin-top: 57px;"
                             src="https://www.webex.com/content/dam/wbx/global/images/home_7.png" alt=""></div>
                </div>
                <div class="col-md-4" style="padding: 20px">
                    <div>
                        <h3>Cisco Webex Teams</h3>
                        <h1>Continuous collaboration in one place.</h1>
                        <p class="third-section-text">Webex Teams brings your work beyond the meeting. One secure app for
                            video conferencing, group messaging, file sharing and whiteboarding. Support your team from
                            start to finish.
                            <br>
                            <a class="learn-more" href="#">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="featured_section4">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-1" style="padding: 20px">
                    <div>
                        <h3>Cisco Webex Calling</h3>
                        <h1>Conference calling to connect with anyone.</h1>
                        <p class="third-section-text">Call anyone from Cisco IP phones or directly from a Webex Teams space. Webex Calling is a cloud-based phone service that’s perfect for small to mid-size organizations. Get the benefits of traditional phone systems without the complicated deployment.
                            <a class="learn-more" href="#">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6" style="padding: 20px">
                    <div>
                        <img class="response-img section-response-img"
                             style="max-width:530px;"
                             src="https://www.webex.com/content/dam/wbx/global/images/home_8.png" alt=""></div>
                </div>
            </div>
        </div>
    </section>


<section class="conferencing-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <h3>Cisco Webex Devices</h3>
                <h1>Conferencing devices
                    to connect
                    and create.
                </h1>
            </div>
            <div class="col-md-6">
                <p>Build the best workplace for team collaboration. Award-winning Webex Devices connect your work to the meeting room and beyond. It’s live teamwork, whether you’re in the room or not.
                    <br> <a href="#" class="learn-more">Learn more</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 hidden-xs col-lg-offset-1">
                <img src="/assets/blueline/pages/images/imageshome/home_9.png" alt="home9">
            </div>
        </div>
    </div>
</section>

<section class="work-with-others-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Webex works well <br>
                    with others.
                </h1>
                <ul class="work-with-ul">
                    <li>
                        <a href="#"></a>
                    </li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
                <p>
                    Keep working the way you do. Webex Integrations
                    connect the tools you already use to ensure work moves
                    smoothly. Or, create custom integrations to fit right into
                    your existing workflow.
                </p>
                <a href="#" class="learn-more">Learn more</a>
            </div>
        </div>
    </div>
</section>


<section class="security-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1 text-center" style="padding: 20px">
                <img class="response-img section-response-img" style="max-width: 418px;margin-top:50px;" src="/assets/blueline/pages/images/imageshome/home_11.png" alt="home11">
            </div>
            <div class="col-md-4" style="padding: 20px">
                <div>
                    <h1>
                        With us, security <br> always comes <br> first.
                    </h1>
                    <p class="third-section-text">
                        Everything you share, say, and type is protected by end-to-end encryption. Authorized administrators can manage and enforce security policies. And thanks to Cisco’s high security standards, Webex products are some of the most secure collaboration tools available.
                        <br>
                        <a class="learn-more" href="#">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="dont-just-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <h1>Don’t just take <br>it from us.</h1>
            </div>
            <div class="col-md-6 text-center">
                <h3>93% of Fortune 100 companies <br>
                    use Cisco Webex products for collaboration</h3>
            </div>
        </div>
    </div>
</section>

<section class="test-run-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Take Webex for a test run.</h1>
                <p>
                    <a href="#" class="btn-mine mine1">Get started</a>
                    <a href="#" class="learn-more">View Plans & Pricing</a>
                </p>
            </div>
        </div>
    </div>
</section>

    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
    ?>


</main>


