<link rel="stylesheet" href="assets/blueline/css/poges/prices.css">
<body class="bg-cover compact">

<main class="site_wrapper">


    <div class="clearfix"></div>

    <?php
    if($this->user or $this->client) {

    }else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
    }

    ?>


    <div class="clearfix"></div>

    <!-- Slider
    ======================================= -->

    <section class="pricing-plans-section">
        <div class="container" style='margin-top:50px'>
            <h1 class="heading-main1 text-center">Pricing plans</h1>
            <h3 class="heading-main3 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quo quos tempore tenetur.</h3>
            <div class="jsx-1856235351 pricing-section-container">
                <div class="jsx-3308074835 pricing-users-count-container">

                    <?php foreach ($pricingPlans as $value) {

                        if ($value->id == 1) { ?>
                            <a class="jsx-1595149912 user-selection-option selected selected-price"
                               title="<?= $value->id ?>">
                                <?= $value->name ?>
                            </a>
                            <?php
                        } else {
                            ?>

                            <a class="jsx-1595149912 user-selection-option" title="<?= $value->id ?>">
                                <?= $value->name ?>
                            </a>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="jsx-1856235351 pricing-plans" style="margin: 50px auto;">
                    <div class="jsx-442581490 plan-container plan-container-red">
                        <div class="jsx-442581490 plan-header plan-header-red">Starter</div>
                        <div class="jsx-442581490 plan-price plan-price-red">
                            <div class="jsx-442581490 plan-top-text"></div>
                            <div class="jsx-1065529218 plan-price-text">
                                    <span class="jsx-1065529218 plan-price-currency-text">
                                       €
                                    </span>
                                <span class="jsx-1065529218 plan-price-number" id="starterW">
                                       <?= $firstPlans->starter ?>
                                    </span>
                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span>
                            </div>
                        </div>
                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                        <div class="jsx-442581490 plan-feature-component-wrapper">
                            <div class="jsx-117003128 plan-features-container">
                                <div class="jsx-1757491152 features-header-text">
                                    <div class="jsx-1757491152 features-header-text-content">
                                        <h3>2 Hours per day</h3>

                                        Starter includes:
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="jsx-117003128 plan-features-list" style="margin-top: -2px;">
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                      fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Management
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Unlimited Project Tasks
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timers

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timesheets

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Milestones
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Support Tickets
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on Projects

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Multi File Upload

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Activities Commenting

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Projects & tasks Notes

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Employees Management

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Private Messaging


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email Notifications


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Automatic Matching

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Dedicated Team Leader

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">24/7 email support


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="jsx-442581490 plan-container plan-container-yellow selected">
                        <div class="jsx-442581490 plan-header plan-header-yellow">Standard</div>
                        <div class="jsx-442581490 plan-price plan-price-yellow">
                            <div class="jsx-442581490 plan-top-text">Most Popular</div>
                            <div class="jsx-1065529218 plan-price-text"><span
                                        class="jsx-1065529218 plan-price-currency-text">€
                                    <!-- --> </span>
                                <span class="jsx-1065529218 plan-price-number" id="standardW">
                                          <?= $firstPlans->standard ?>
                                    </span>
                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span></div>
                        </div>
                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                        <div class="jsx-442581490 plan-feature-component-wrapper">
                            <div class="jsx-117003128 plan-features-container">
                                <div class="jsx-1757491152 features-header-text">
                                    <div class="jsx-1757491152 features-header-text-content">
                                        <h3>3 Hours per day</h3>
                                        Standard includes all basic <br> plan features,
                                    </div>
                                    <div class="jsx-1757491152 features-header-text-content-second-line">
                                        <div
                                                class="jsx-1757491152 features-second-line-plus features-second-line-plus-yellow">
                                            in addition to:
                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-117003128 plan-features-list">
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                      fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Management
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Unlimited Project Tasks
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timers

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timesheets

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Milestones
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Support Tickets
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on Projects

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Multi File Upload

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Activities Commenting

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Projects & tasks Notes

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Employees Management

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Private Messaging


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email Notifications


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Automatic Matching

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Dedicated Team Leader

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Receive Employee’s CV and
                                            Portfolio


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email interview


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">24/7 email/phone Support


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="jsx-442581490 plan-container plan-container-green">
                        <div class="jsx-442581490 plan-header plan-header-green">Professional</div>
                        <div class="jsx-442581490 plan-price plan-price-green">
                            <div class="jsx-442581490 plan-top-text"></div>
                            <div class="jsx-1065529218 plan-price-text"><span
                                        class="jsx-1065529218 plan-price-currency-text"> €
                                    <!-- --> </span>
                                <span class="jsx-1065529218 plan-price-number" id="professionalW">
                                    <?= $firstPlans->professional ?>
                                    </span>
                                <span
                                        class="jsx-1065529218 plan-price-per-month-text"> /Mo </span></div>
                        </div>
                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                        <div class="jsx-442581490 plan-feature-component-wrapper">
                            <div class="jsx-117003128 plan-features-container">
                                <div class="jsx-1757491152 features-header-text">
                                    <div class="jsx-1757491152 features-header-text-content">
                                        <h3>4 Hours per day
                                        </h3>

                                        Professional includes all basic plan features,

                                    </div>
                                    <div class="jsx-1757491152 features-header-text-content-second-line">

                                        <div
                                                class="jsx-1757491152 features-second-line-plus features-second-line-plus-green">
                                            in addition to:
                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-117003128 plan-features-list">
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                      fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Management
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Unlimited Project Tasks
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timers

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timesheets

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Milestones
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Support Tickets
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on Projects

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Multi File Upload

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Activities Commenting

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Projects & tasks Notes

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Employees Management

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Private Messaging


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email Notifications


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Automatic Matching

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Dedicated Team Leader

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Receive Employee’s CV and
                                            Portfolio


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email interview


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Phone Interview


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Interview employee live


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">24/7 Priority Support with <30
                                            min.
                                            response time


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="jsx-442581490 plan-container plan-container-purple">
                        <div class="jsx-442581490 plan-header plan-header-purple">Enterprise</div>
                        <div class="jsx-442581490 plan-price plan-price-purple">
                            <div class="jsx-442581490 plan-top-text"></div>
                            <div class="jsx-1065529218 plan-price-text"><span
                                        class="jsx-1065529218 plan-price-currency-text"> €
                                    <!-- --> </span>
                                <span class="jsx-1065529218 plan-price-number" id="enterpriseW">
                                      <?= $firstPlans->enterprise ?>
                                    </span>
                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span></div>
                        </div>
                        <div class="jsx-442581490 plan-billing-text">Or billed annually
                        </div>
                        <div class="jsx-442581490 plan-feature-component-wrapper">
                            <div class="jsx-117003128 plan-features-container">
                                <div class="jsx-1757491152 features-header-text">
                                    <div class="jsx-1757491152 features-header-text-content">
                                        <h3>6 Hours per day
                                        </h3>
                                        Enterprise includes all basic <br> plan features,
                                    </div>
                                    <div class="jsx-1757491152 features-header-text-content-second-line">
                                        <div
                                                class="jsx-1757491152 features-second-line-plus features-second-line-plus-purple">
                                            in addition to:
                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-117003128 plan-features-list">
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                      fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Management
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Unlimited Project Tasks
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timers

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Tasks Timesheets

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Project Milestones
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">
                                            Support Tickets
                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on Projects

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Multi File Upload

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Activities Commenting

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Projects & tasks Notes

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Employees Management

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Private Messaging


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email Notifications


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Automatic Matching

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Dedicated Team Leader

                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Receive Employee’s CV and
                                            Portfolio


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Email interview


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Phone Interview


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">Interview employee live


                                        </div>
                                    </div>
                                    <div class="jsx-3843086273 plan-feature-wrapper">
                                        <div class="jsx-3843086273 pricing-checkmark">
                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                        fill="#40C870" fill-rule="evenodd"></path>
                                            </svg>
                                        </div>
                                        <div class="jsx-3843086273 plan-feature">24/7 Priority Support with <10
                                            min.
                                            response time
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="jsx-1856235351 mobile-pricing-section-container">
                <div class="jsx-1856235351 pricing-selections-container">
                    <div class="jsx-311422583 pricing-users-count-container">
                        <div class="Select has-value Select--single">
                            <select class="form-control" id="selectPricingPlan">

                                <?php foreach ($pricingPlans as $value) { ?>

                                    <option value="<?= $value->id ?>"><?= $value->name ?></option>

                                <?php } ?>

                            </select>
                        </div>
                    </div>

                </div>
                <div class="jsx-1856235351 mobile-pricing-plans">
                    <div class="slider ">
                        <div class="slider-frame ">
                            <div class=" single-item">
                                <div class="slider-slide">
                                    <div class="jsx-442581490 plan-container plan-container-red isCurrentSlide">
                                        <div class="jsx-442581490 plan-header plan-header-red">Starter</div>
                                        <div class="jsx-442581490 plan-price plan-price-red">
                                            <div class="jsx-442581490 plan-top-text"></div>
                                            <div class="jsx-1065529218 plan-price-text"><span
                                                        class="jsx-1065529218 plan-price-currency-text"> €
                                                       </span>
                                                <span class="jsx-1065529218 plan-price-number" id="starter">
                                                        <!-- -->499<!-- -->
                                                    </span>
                                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span>
                                            </div>
                                        </div>
                                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                                        <div class="jsx-442581490 plan-feature-component-wrapper">
                                            <div class="jsx-117003128 plan-features-container">
                                                <div class="jsx-1757491152 features-header-text">
                                                    <div class="jsx-1757491152 features-header-text-content">
                                                        <h3>2 Hours per day</h3>
                                                        Starter includes:
                                                    </div>
                                                </div>
                                                <div class="jsx-117003128 plan-features-list">


                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                      fill="#40C870" fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Management
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Unlimited Project Tasks
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timers

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timesheets

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Milestones
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Support Tickets
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on
                                                            Projects

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Multi File
                                                            Upload

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Activities
                                                            Commenting

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Projects &
                                                            tasks
                                                            Notes

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Employees
                                                            Management

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Private
                                                            Messaging


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email
                                                            Notifications


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Automatic
                                                            Matching

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Dedicated Team
                                                            Leader

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">24/7 email
                                                            support


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-slide"
                                >
                                    <div class="jsx-442581490 plan-container plan-container-yellow selected">
                                        <div class="jsx-442581490 plan-header plan-header-yellow">standard</div>
                                        <div class="jsx-442581490 plan-price plan-price-yellow">
                                            <div class="jsx-442581490 plan-top-text">Most Popular</div>
                                            <div class="jsx-1065529218 plan-price-text"><span
                                                        class="jsx-1065529218 plan-price-currency-text"> <!-- -->€
                                                    <!-- --> </span>
                                                <span class="jsx-1065529218 plan-price-number" id="standard">
                                                      799
                                                    </span>
                                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span>
                                            </div>
                                        </div>
                                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                                        <div class="jsx-442581490 plan-feature-component-wrapper">
                                            <div class="jsx-117003128 plan-features-container">
                                                <div class="jsx-1757491152 features-header-text">
                                                    <div class="jsx-1757491152 features-header-text-content">
                                                        <h3>3 Hours per day</h3>
                                                        Standard includes all basic plan features,
                                                    </div>
                                                    <div class="jsx-1757491152 features-header-text-content-second-line">

                                                        <div class="jsx-1757491152 features-second-line-plus features-second-line-plus-yellow">
                                                            in addition to:
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jsx-117003128 plan-features-list">
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                      fill="#40C870" fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Management
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Unlimited Project Tasks
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timers

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timesheets

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Milestones
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Support Tickets
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on
                                                            Projects

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Multi File
                                                            Upload

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Activities
                                                            Commenting

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Projects &
                                                            tasks
                                                            Notes

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Employees
                                                            Management

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Private
                                                            Messaging


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email
                                                            Notifications


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Automatic
                                                            Matching

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Dedicated Team
                                                            Leader

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Receive
                                                            Employee’s
                                                            CV and Portfolio


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email interview


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">24/7
                                                            email/phone
                                                            Support


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-slide">
                                    <div class="jsx-442581490 plan-container plan-container-green">
                                        <div class="jsx-442581490 plan-header plan-header-green">Professional
                                        </div>
                                        <div class="jsx-442581490 plan-price plan-price-green">
                                            <div class="jsx-442581490 plan-top-text"></div>
                                            <div class="jsx-1065529218 plan-price-text"><span
                                                        class="jsx-1065529218 plan-price-currency-text">€
                                                    <!-- --> </span>
                                                <span class="jsx-1065529218 plan-price-number"
                                                      id="professional">
                                                     999
                                                    </span>
                                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span>
                                            </div>
                                        </div>
                                        <div class="jsx-442581490 plan-billing-text">Or billed annually</div>
                                        <div class="jsx-442581490 plan-feature-component-wrapper">
                                            <div class="jsx-117003128 plan-features-container">
                                                <div class="jsx-1757491152 features-header-text">
                                                    <div class="jsx-1757491152 features-header-text-content">
                                                        <h3>4 Hours per day</h3>
                                                        Professional includes all basic plan features,
                                                    </div>
                                                    <div class="jsx-1757491152 features-header-text-content-second-line">
                                                        <div class="jsx-1757491152 features-second-line-plus features-second-line-plus-green">
                                                            in addition to:
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jsx-117003128 plan-features-list">
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                      fill="#40C870" fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Management
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Unlimited Project Tasks
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timers

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timesheets

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Milestones
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Support Tickets
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on
                                                            Projects

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Multi File
                                                            Upload

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Activities
                                                            Commenting

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Projects &
                                                            tasks
                                                            Notes

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Employees
                                                            Management

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Private
                                                            Messaging


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email
                                                            Notifications


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Automatic
                                                            Matching

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Dedicated Team
                                                            Leader

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Receive
                                                            Employee’s
                                                            CV and Portfolio


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email interview


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Phone Interview


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Interview
                                                            employee
                                                            live


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            24/7 Priority Support with <30 min. response time
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-slide">
                                    <div class="jsx-442581490 plan-container plan-container-purple">
                                        <div class="jsx-442581490 plan-header plan-header-purple">enterprise
                                        </div>
                                        <div class="jsx-442581490 plan-price plan-price-purple">
                                            <div class="jsx-442581490 plan-top-text"></div>
                                            <div class="jsx-1065529218 plan-price-text"><span
                                                        class="jsx-1065529218 plan-price-currency-text">€
                                                    <!-- --> </span>
                                                <span class="jsx-1065529218 plan-price-number" id="enterprise">
                                                        1999
                                                    </span>
                                                <span class="jsx-1065529218 plan-price-per-month-text"> /Mo </span>
                                            </div>
                                        </div>
                                        <div class="jsx-442581490 plan-billing-text">Or billed annually
                                        </div>
                                        <div class="jsx-442581490 plan-feature-component-wrapper">
                                            <div class="jsx-117003128 plan-features-container">
                                                <div class="jsx-1757491152 features-header-text">
                                                    <div class="jsx-1757491152 features-header-text-content">
                                                        <h3>6 Hours per day</h3>
                                                        Enterprise includes all basic plan features,
                                                    </div>
                                                    <div class="jsx-1757491152 features-header-text-content-second-line">
                                                        <div class="jsx-1757491152 features-second-line-plus features-second-line-plus-purple">
                                                            in addition to:
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jsx-117003128 plan-features-list">
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                      fill="#40C870" fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Management
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Unlimited Project Tasks
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timers

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Tasks Timesheets

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Project Milestones
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            Support Tickets
                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Project Timer

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Gantt Charts on
                                                            Projects

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Multi File
                                                            Upload

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Activities
                                                            Commenting

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Projects &
                                                            tasks
                                                            Notes

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Employees
                                                            Management

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Private
                                                            Messaging


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email
                                                            Notifications


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Automatic
                                                            Matching

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Dedicated Team
                                                            Leader

                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Receive
                                                            Employee’s
                                                            CV and Portfolio


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Email interview


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Phone Interview


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">Interview
                                                            employee
                                                            live


                                                        </div>
                                                    </div>
                                                    <div class="jsx-3066633563 plan-feature-wrapper">
                                                        <div class="jsx-3843086273 pricing-checkmark">
                                                            <svg width="16" height="16" viewBox="0 0 16 16"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                        d="M7.547 15.964a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm-1.085-3.953l4.797-4.798A.999.999 0 1 0 9.845 5.8L6.472 9.172 4.759 7.435A.999.999 0 1 0 3.335 8.84l3.127 3.172z"
                                                                        fill="#40C870"
                                                                        fill-rule="evenodd"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="jsx-3843086273 plan-feature">
                                                            24/7 Priority Support with <10 min. response time
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="jsx-1856235351 mobile-signup-button-wrapper">
                    <button tabindex="-1" class="ladda-button signup-button brand-pink xl" data-style="zoom-in"
                            data-size="xl" type="button"><span class="ladda-label">Sign Up</span></button>
                </div>
            </div>
            <div class="jsx-2709407614 grey-background"></div>
        </div>
    </section>
    <!-- end of masterslider -->
    <div class="clearfix margin_top10"></div>

    <section class="faqs-section">
        <div class="container">
            <h2 class="heading-main2 text-center">
                Frequently asked questions
            </h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #1
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #2
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #3
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading1">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #1
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading2">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #2
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        <i class="more-less fas fa-angle-down"></i>
                                        Collapsible Group Item #3
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials-mine-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-main2 text-center">Testimonials</h2>
                </div>
            </div>
            <div class="row mt80">
                <div class="col-md-8 col-md-offset-2">
                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item">
                                <div class="profile-circle" style="background-color: rgba(0,0,0,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque  ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                            <div class="active item">
                                <div class="profile-circle" style="background-color: rgba(145,169,216,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque  ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>

<script type="text/javascript" src="/assets/blueline/pages/js/universal/jquery.js"></script>
<script src="assets/blueline/js/pricing.js"></script>