<!DOCTYPE html>

<html xmlns:og="http://opengraphprotocol.org/schema/" lang="en-US" itemscope itemtype="http://schema.org/WebPage"
      class="region-nav-hero">
<head>


    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <script>(function (d) {
            var config = {
                    kitId: 'gnf7xqu',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement, t = setTimeout(function () {
                    h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                s = d.getElementsByTagName("script")[0], a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function () {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {
                }
            };
            s.parentNode.insertBefore(tk, s)
        })(document);</script>
    <style type="text/css">
        /* Forces background color to be black on /domain-name-search */
        .collection-tour-domains.has--iframe-open [class*=Frame-overlay-] {
            background: black;
        }

        /* Forces background color to be black on /domain-name-search */
        .collection-tour-domains.has--iframe-open [class*=Signup-overlay-] {
            background: black;
        }
    </style>
    <style>
        #content {
            max-width: 90%;
            margin: 0 auto;
        }
    </style>

    <!-- End of Squarespace Headers -->





    <script type="text/javascript" src="assets/blueline/js/jquery.min.js"></script>


    <link rel="stylesheet" href="assets/blueline/css/poges/brandAssests.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/header.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/footer.css">

</head>

<body class="collection-page">


<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
?>




<section class="generic-hero has-dark-background">
    <div class="hero-panel">
        <img class="hero-image" src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/598b5bed893fc0b893562f4a/1502305268248/final_asset-press-and-media-banner.jpg"/>
        <span class="hero-title">Press & Media</span>
        <div class="hero-description"><p>Press &amp; Media</p></div>
    </div>
</section>


<nav class="tab-nav">


    <a class="tab-subnav " href="/press-coverage/" data-name="Coverage">Coverage</a>


    <a class="tab-subnav " href="/press-releases/" data-name="Releases">Releases</a>


    <a class="tab-subnav " href="/press-images/" data-name="Images">Images</a>


    <a class="tab-subnav is-active" href="/logo-guidelines/" data-name="Logo Guidelines">Logo Guidelines</a>


    <a class="tab-subnav " href="/brand-guidelines/" data-name="Brand Guidelines">Brand Guidelines</a>


    <a class="tab-subnav " href="/press-spelling/" data-name="Spelling">Spelling</a>


    <a class="tab-subnav " href="/press-contact/" data-name="Contact">Contact</a>


</nav>

<nav class="select-nav is-hidden">
    <select class="select-subnav">


        <option class="translate" name="Press Coverage" data-href="/press-coverage/">
            Coverage
        </option>


        <option class="translate" name="Press Releases" data-href="/press-releases/">
            Releases
        </option>


        <option class="translate" name="Press Images" data-href="/press-images/">
            Images
        </option>


        <option class="translate" name="Logo Guidelines" data-href="/logo-guidelines/" data-visited="/logo-guidelines/"
                selected>
            Logo Guidelines
        </option>


        <option class="translate" name="Brand Guidelines" data-href="/brand-guidelines/">
            Brand Guidelines
        </option>


        <option class="translate" name="Press Spelling" data-href="/press-spelling/">
            Spelling
        </option>


        <option class="translate" name="Press Contact" data-href="/press-contact/">
            Contact
        </option>


    </select>
</nav>


<main id="content" class="content page-logo-guidelines" role="main" data-content-field="main-content"
      data-url-id="collection-5963dda0d2b8573c43938d24">
    <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1530028374526"
         id="page-5963dda0d2b8573c43938d24">
        <div class="row sqs-row">
            <div class="col sqs-col-12 span-12">
                <div class="sqs-block html-block sqs-block-html" data-block-type="2"
                     id="block-yui_3_17_2_5_1499797635935_14177">
                    <div class="sqs-block-content"><h1 class="text-align-center">Logo Guidelines</h1>
                        <p class="text-align-center">The Squarespace brand includes the words, phrases, symbols, designs
                            and other distinctive brand features associated with Squarespace and our services (“Brand
                            Assets”).</p></div>
                </div>
                <div class="sqs-block horizontalrule-block sqs-block-horizontalrule" data-block-type="47"
                     id="block-yui_3_17_2_8_1499802234577_27150">
                    <div class="sqs-block-content">
                        <hr/>
                    </div>
                </div>
                <div class="sqs-block html-block sqs-block-html" data-block-type="2"
                     id="block-yui_3_17_2_8_1499802234577_27211">
                    <div class="sqs-block-content"><h3>Requirements</h3>
                        <p>Before grabbing a Squarespace logo, please be sure to follow our basic rules:</p>
                        <ol>
                            <li><p>Comply with our <a href="https://www.squarespace.com/brand-guidelines">Brand
                                        Guidelines</a>.</p></li>
                            <li><p>Don’t alter the shape, proportion, color or orientation of the logos. Keep ‘em black
                                    and white, and only as they appear below. (We know you have some great design ideas
                                    — put them to work with a new <a href="https://www.squarespace.com/">Squarespace
                                        site</a>!)</p></li>
                            <li><p>Provide at least as much padding around the logo as what we’ve displayed below. This
                                    helps our logo appear clean and uncluttered.</p></li>
                        </ol>
                        <p>Finally, if you have any questions about logo usage, you can reach us <a
                                href="https://www.squarespace.com/press-contact">here</a>.</p></div>
                </div>
                <div class="sqs-block horizontalrule-block sqs-block-horizontalrule" data-block-type="47"
                     id="block-yui_3_17_2_8_1499802234577_28574">
                    <div class="sqs-block-content">
                        <hr/>
                    </div>
                </div>
                <div class="row sqs-row">
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block html-block sqs-block-html" data-block-type="2"
                             id="block-2eff326044f708bee430">
                            <div class="sqs-block-content"><h3>Squarespace Logos</h3></div>
                        </div>
                    </div>
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block button-block sqs-block-button" data-block-type="53"
                             id="block-yui_3_17_2_2_1500387673944_17686">
                            <div class="sqs-block-content">
                                <div class="sqs-block-button-container--right" data-alignment="right"
                                     data-button-size="small">
                                    <a href="/s/squarespace-logos.zip"
                                       class="sqs-block-button-element--small sqs-block-button-element">Download All
                                        Logos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row sqs-row">
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-b5dbaaed3cd3c09fd242">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:36.279998779296875%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;Black Horizontal&lt;/p&gt;">
                                 
                                            <img class="thumb-image" alt=" Black Horizontal "
                                                 src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2e2e6f2e13a9e79e716/1499717024220/"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2e2e6f2e13a9e79e716/1499717024220/"
                                                 data-image-dimensions="2500x907" data-image-focal-point="0.5,0.5"
                                                  data-image-id="587fa2e2e6f2e13a9e79e716"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>Black Horizontal</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-79273f95326e112dbda4">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:36.279998779296875%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;White Horizontal&lt;/p&gt;">
                        
                                            <img class="thumb-image" alt=" White Horizontal "
                                                 src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2d36b8f5b0b1f586285/1499717024224/squarespace-logo-horizontal-white.jpg"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2d36b8f5b0b1f586285/1499717024224/squarespace-logo-horizontal-white.jpg"
                                                 data-image-dimensions="2500x907" data-image-focal-point="0.5,0.5"
                                                  data-image-id="587fa2d36b8f5b0b1f586285"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>White Horizontal</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row sqs-row">
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-26195dc8a4d2129af10a">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:82.95999908447266%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;Black Stacked&lt;/p&gt;">
                                          
                                            <img class="thumb-image" alt=" Black Stacked "
                                                 src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa31bd2b857d46b448465/1499717024227/squarespace-logo-stacked-black.jpg"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa31bd2b857d46b448465/1499717024227/squarespace-logo-stacked-black.jpg"
                                                 data-image-dimensions="2500x2074" data-image-focal-point="0.5,0.5"
                                                  data-image-id="587fa31bd2b857d46b448465"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>Black Stacked</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-fbde93049f34eefbf3ec">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:82.95999908447266%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;White Stacked&lt;/p&gt;">
                                           
                                            <img class="thumb-image" alt=" White Stacked "
                                                src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa340b8a79bac6fdebd24/1499717024230/squarespace-logo-stacked-white.jpg"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa340b8a79bac6fdebd24/1499717024230/squarespace-logo-stacked-white.jpg"
                                                 data-image-dimensions="2500x2074" data-image-focal-point="0.5,0.5"
                                                 data-image-id="587fa340b8a79bac6fdebd24"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>White Stacked</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row sqs-row">
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-c2c52f5b433806b19f59">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:92.76000213623047%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;Black Symbol&lt;/p&gt;">
                                          
                                            <img class="thumb-image" alt=" Black Symbol "
                                                src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2f4725e255a02f090cb/1499717024232/squarespace-logo-symbol-black.jpg"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2f4725e255a02f090cb/1499717024232/squarespace-logo-symbol-black.jpg"
                                                 data-image-dimensions="2500x2319" data-image-focal-point="0.5,0.5"
                                                data-image-id="587fa2f4725e255a02f090cb"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>Black Symbol</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col sqs-col-6 span-6">
                        <div class="sqs-block image-block sqs-block-image" data-block-type="5"
                             id="block-5be659d6669e9b3e9ec9">
                            <div class="sqs-block-content">


                                <div class="image-block-outer-wrapper layout-caption-below design-layout-inline   ">

                                    <div class="intrinsic" style="max-width:2500.0px;">

                                        <div style="padding-bottom:92.76000213623047%;"
                                             class="image-block-wrapper   has-aspect-ratio"
                                             data-description="&lt;p&gt;White Symbol&lt;/p&gt;">
                                          
                                            <img class="thumb-image" alt=" White Symbol "
                                                 src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2fd440243130a3dc983/1499717024235/squarespace-logo-symbol-white.jpg"
                                                 data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa2fd440243130a3dc983/1499717024235/squarespace-logo-symbol-white.jpg"
                                                 data-image-dimensions="2500x2319" data-image-focal-point="0.5,0.5"
                                                 data-load="false" data-image-id="587fa2fd440243130a3dc983"
                                                 data-type="image"/>
                                        </div>


                                        <div class="image-caption-wrapper">
                                            <div class="image-caption"><p>White Symbol</p></div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="sqs-block spacer-block sqs-block-spacer" data-aspect-ratio="5.900621118012422"
                     data-block-type="21" id="block-a07f28f53b04535fef90">
                    <div class="sqs-block-content">&nbsp;</div>
                </div>
                <div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-916ab97e6932ae28b504">
                    <div class="sqs-block-content"><h3>Required Clear Space</h3></div>
                </div>
                <div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-8331e2a49e8bc87b96fe">
                    <div class="sqs-block-content">


                        <div class="image-block-outer-wrapper layout-caption-hidden design-layout-inline   ">

                            <div class="intrinsic" style="max-width:2500.0px;">

                                <div style="padding-bottom:36.31999969482422%;"
                                     class="image-block-wrapper   has-aspect-ratio" data-description="">
                                   
                                    <img class="thumb-image"
                                         alt="squarespace-logo-horizontal-black-clear-space-diagram.jpg"
                                         src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa38f1b631bd312ce6baf/1500392853655/squarespace-logo-horizontal-black-clear-space-diagram.jpg"
                                         data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa38f1b631bd312ce6baf/1500392853655/squarespace-logo-horizontal-black-clear-space-diagram.jpg"
                                         data-image-dimensions="2500x908" data-image-focal-point="0.5,0.5"
                                         data-load="false" data-image-id="587fa38f1b631bd312ce6baf" data-type="image"/>
                                </div>


                            </div>

                        </div>


                    </div>
                </div>
                <div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-dd876d1090fd3ec3ef42">
                    <div class="sqs-block-content">


                        <div class="image-block-outer-wrapper layout-caption-hidden design-layout-inline   ">

                            <div class="intrinsic" style="max-width:2500.0px;">

                                <div style="padding-bottom:83.0%;" class="image-block-wrapper   has-aspect-ratio"
                                     data-description="">
                                   
                                    <img class="thumb-image"
                                         alt="squarespace-logo-stacked-black-clear-space-diagram.jpg"
                                         src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa3abd1758ebcc389821d/1500392860548/squarespace-logo-stacked-black-clear-space-diagram.jpg"
                                         data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa3abd1758ebcc389821d/1500392860548/squarespace-logo-stacked-black-clear-space-diagram.jpg"
                                         data-image-dimensions="2500x2075" data-image-focal-point="0.5,0.5"
                                         data-image-id="587fa3abd1758ebcc389821d" data-type="image"/>
                                </div>


                            </div>

                        </div>


                    </div>
                </div>
                <div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-27430831c626d2c6e931">
                    <div class="sqs-block-content">


                        <div class="image-block-outer-wrapper layout-caption-hidden design-layout-inline   ">

                            <div class="intrinsic" style="max-width:2500.0px;">

                                <div style="padding-bottom:93.0%;" class="image-block-wrapper   has-aspect-ratio"
                                     data-description="">
                                   
                                    <img class="thumb-image" alt="squarespace-logo-symbol-black-clear-space-diagram.jpg"
                                         src="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa3b4db29d66d9a2638da/1500392869859/squarespace-logo-symbol-black-clear-space-diagram.jpg"
                                         data-image="https://static1.squarespace.com/static/5134cbefe4b0c6fb04df8065/t/587fa3b4db29d66d9a2638da/1500392869859/squarespace-logo-symbol-black-clear-space-diagram.jpg"
                                         data-image-dimensions="2500x2325" data-image-focal-point="0.5,0.5"
                                          data-image-id="587fa3b4db29d66d9a2638da" data-type="image"/>
                                </div>


                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<hr/>

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none">
    <symbol id="twitter-icon" viewBox="0 0 64 64">
        <path
            d="M48,22.1c-1.2,0.5-2.4,0.9-3.8,1c1.4-0.8,2.4-2.1,2.9-3.6c-1.3,0.8-2.7,1.3-4.2,1.6 C41.7,19.8,40,19,38.2,19c-3.6,0-6.6,2.9-6.6,6.6c0,0.5,0.1,1,0.2,1.5c-5.5-0.3-10.3-2.9-13.5-6.9c-0.6,1-0.9,2.1-0.9,3.3 c0,2.3,1.2,4.3,2.9,5.5c-1.1,0-2.1-0.3-3-0.8c0,0,0,0.1,0,0.1c0,3.2,2.3,5.8,5.3,6.4c-0.6,0.1-1.1,0.2-1.7,0.2c-0.4,0-0.8,0-1.2-0.1 c0.8,2.6,3.3,4.5,6.1,4.6c-2.2,1.8-5.1,2.8-8.2,2.8c-0.5,0-1.1,0-1.6-0.1c2.9,1.9,6.4,2.9,10.1,2.9c12.1,0,18.7-10,18.7-18.7 c0-0.3,0-0.6,0-0.8C46,24.5,47.1,23.4,48,22.1z"/>
    </symbol>
    <symbol id="twitter-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M44.7,25.5c0,0.3,0,0.6,0,0.8C44.7,35,38.1,45,26.1,45c-3.7,0-7.2-1.1-10.1-2.9 c0.5,0.1,1,0.1,1.6,0.1c3.1,0,5.9-1,8.2-2.8c-2.9-0.1-5.3-2-6.1-4.6c0.4,0.1,0.8,0.1,1.2,0.1c0.6,0,1.2-0.1,1.7-0.2 c-3-0.6-5.3-3.3-5.3-6.4c0,0,0-0.1,0-0.1c0.9,0.5,1.9,0.8,3,0.8c-1.8-1.2-2.9-3.2-2.9-5.5c0-1.2,0.3-2.3,0.9-3.3 c3.2,4,8.1,6.6,13.5,6.9c-0.1-0.5-0.2-1-0.2-1.5c0-3.6,2.9-6.6,6.6-6.6c1.9,0,3.6,0.8,4.8,2.1c1.5-0.3,2.9-0.8,4.2-1.6 c-0.5,1.5-1.5,2.8-2.9,3.6c1.3-0.2,2.6-0.5,3.8-1C47.1,23.4,46,24.5,44.7,25.5z"/>
    </symbol>
    <symbol id="facebook-icon" viewBox="0 0 64 64">
        <path
            d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z"/>
    </symbol>
    <symbol id="facebook-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z"/>
    </symbol>
    <symbol id="instagram-icon" viewBox="0 0 64 64">
        <path
            d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z"/>
    </symbol>
    <symbol id="instagram-mask" viewBox="0 0 64 64">
        <path
            d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z"/>
    </symbol>
    <symbol id="pinterest-icon" viewBox="0 0 64 64">
        <path
            d="M32,16c-8.8,0-16,7.2-16,16c0,6.6,3.9,12.2,9.6,14.7c0-1.1,0-2.5,0.3-3.7 c0.3-1.3,2.1-8.7,2.1-8.7s-0.5-1-0.5-2.5c0-2.4,1.4-4.1,3.1-4.1c1.5,0,2.2,1.1,2.2,2.4c0,1.5-0.9,3.7-1.4,5.7 c-0.4,1.7,0.9,3.1,2.5,3.1c3,0,5.1-3.9,5.1-8.5c0-3.5-2.4-6.1-6.7-6.1c-4.9,0-7.9,3.6-7.9,7.7c0,1.4,0.4,2.4,1.1,3.1 c0.3,0.3,0.3,0.5,0.2,0.9c-0.1,0.3-0.3,1-0.3,1.3c-0.1,0.4-0.4,0.6-0.8,0.4c-2.2-0.9-3.3-3.4-3.3-6.1c0-4.5,3.8-10,11.4-10 c6.1,0,10.1,4.4,10.1,9.2c0,6.3-3.5,11-8.6,11c-1.7,0-3.4-0.9-3.9-2c0,0-0.9,3.7-1.1,4.4c-0.3,1.2-1,2.5-1.6,3.4 c1.4,0.4,3,0.7,4.5,0.7c8.8,0,16-7.2,16-16C48,23.2,40.8,16,32,16z"/>
    </symbol>
    <symbol id="pinterest-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M32,48c-1.6,0-3.1-0.2-4.5-0.7c0.6-1,1.3-2.2,1.6-3.4c0.2-0.7,1.1-4.4,1.1-4.4 c0.6,1.1,2.2,2,3.9,2c5.1,0,8.6-4.7,8.6-11c0-4.7-4-9.2-10.1-9.2c-7.6,0-11.4,5.5-11.4,10c0,2.8,1,5.2,3.3,6.1 c0.4,0.1,0.7,0,0.8-0.4c0.1-0.3,0.2-1,0.3-1.3c0.1-0.4,0.1-0.5-0.2-0.9c-0.6-0.8-1.1-1.7-1.1-3.1c0-4,3-7.7,7.9-7.7 c4.3,0,6.7,2.6,6.7,6.1c0,4.6-2,8.5-5.1,8.5c-1.7,0-2.9-1.4-2.5-3.1c0.5-2,1.4-4.2,1.4-5.7c0-1.3-0.7-2.4-2.2-2.4 c-1.7,0-3.1,1.8-3.1,4.1c0,1.5,0.5,2.5,0.5,2.5s-1.8,7.4-2.1,8.7c-0.3,1.2-0.3,2.6-0.3,3.7C19.9,44.2,16,38.6,16,32 c0-8.8,7.2-16,16-16c8.8,0,16,7.2,16,16C48,40.8,40.8,48,32,48z"/>
    </symbol>
</svg>



<div class="www-domains domains-overlay">
    <div class="domains-overlay-content">

        <div class="loading-screen">
            <div class="loader">
                <svg viewBox="0 0 32 32">
                    <defs>
                        <style>.cls-1, .cls-2 {
                                fill: #fff;
                                isolation: isolate
                            }

                            .cls-1 {
                                opacity: .13
                            }

                            .cls-2 {
                                opacity: .89
                            }</style>
                    </defs>
                    <path class="cls-1"
                          d="M16 0a16 16 0 1 0 16 16A16 16 0 0 0 16 0m0 4A12 12 0 1 1 4 16 12 12 0 0 1 16 4"/>
                    <path class="cls-2" d="M16 0a16 16 0 0 1 16 16h-4A12 12 0 0 0 16 4V0z"/>
                </svg>
            </div>
        </div>

        <div class="scroll-up">
      <span class="scroll-up-button">
        <span>Back to your results</span>
        <span class="caret"><svg x="0px" y="0px" viewBox="0 0 10 6" width="10px" height="6px">
  <polyline points="9.2,5.1 5,0.8 0.8,5.1 "/>
</svg>
</span>
      </span>
            <div class="exit">
                <div class="www-x light">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                        <line x1="1.9" y1="1.9" x2="23.1" y2="23.1"/>
                        <line x1="23.1" y1="1.9" x2="1.9" y2="23.1"/>
                    </svg>

                </div>
            </div>
        </div>

        <div id="domain-name-search">
            <div class="scrollable-content">


                <header id="domains-header" class="has-dark-background">
  <span class="header-logo">
    <a class="is-borderless" href="/">
      <svg class="squarespace-mark is-fill notranslate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 24"
           width="30px" height="24px">
  <title>Squarespace</title>
  <path
      d="M24.93671,11.30019c-0.38681-0.38681-1.01281-0.38681-1.39961,0l-9.23691,9.23691c-1.31214,1.31214-3.44709,1.31214-4.75855,0.00068c-0.38681-0.38681-1.01281-0.38681-1.39961,0c-0.38681,0.38681-0.38681,1.01281,0,1.39961c2.0837,2.0837,5.47407,2.08302,7.55778-0.00068l9.2369-9.2369C25.32352,12.313,25.32352,11.687,24.93671,11.30019zM28.01613,8.22077c-2.0837-2.0837-5.47408-2.08302-7.55778,0.00068l-9.2369,9.2369c-0.38681,0.38681-0.38681,1.01281,0,1.39961c0.38681,0.38681,1.01281,0.38681,1.39961,0l9.2369-9.2369c1.31214-1.31214,3.4471-1.31214,4.75855-0.00068c1.31214,1.31214,1.31214,3.4471,0,4.75923l-7.43066,7.43066c-0.38681,0.38681-0.38681,1.01281,0,1.39961c0.38681,0.38681,1.01281,0.38681,1.39961,0l7.43066-7.43066C30.09984,13.69553,30.09984,10.30447,28.01613,8.22077zM21.85797,2.06261c-2.0837-2.0837-5.47408-2.08302-7.55778,0.00068l-9.2369,9.2369c-0.38681,0.38681-0.38681,1.01281,0,1.39961s1.01281,0.38681,1.39961,0l9.2369-9.2369c1.31214-1.31214,3.44709-1.31214,4.75855-0.00068c0.38681,0.38681,1.01281,0.38681,1.39961,0C22.24478,3.07541,22.24478,2.44941,21.85797,2.06261zM18.77855,5.14203c-0.38681-0.38681-1.01281-0.38681-1.39961,0l-9.2369,9.2369c-1.31214,1.31214-3.4471,1.31214-4.75855,0.00068c-1.31214-1.31214-1.31214-3.4471,0-4.75923l7.43066-7.43066c0.38681-0.38681,0.38681-1.01281,0-1.39961s-1.01281-0.38681-1.39961,0L1.98387,8.22077c-2.0837,2.0837-2.0837,5.47476,0,7.55846s5.47408,2.08302,7.55778-0.00068l9.2369-9.2369C19.16535,6.15484,19.16535,5.52884,18.77855,5.14203z"/>
</svg>

      <svg class="squarespace-full is-fill notranslate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168 24"
           width="168px" height="24px">
  <title>Squarespace</title>
  <path
      d="M79.5626,5.53063l-5.24026,11.58048h1.33081l1.36326-3.03909h6.25848l1.34703,3.03909h1.39632L80.77798,5.53063H79.5626zM77.52551,12.88971l2.62856-5.84795l2.61171,5.84795H77.52551zM41.8326,10.73783c-2.52935-0.54218-3.12082-1.14987-3.12082-2.23424V8.47052c0-1.03445,0.95272-1.85615,2.44762-1.85615c1.18232,0,2.25046,0.37809,3.31798,1.24845l0.75556-1.00201c-1.1661-0.93587-2.36526-1.41254-4.0411-1.41254c-2.18433,0-3.77781,1.33081-3.77781,3.13767v0.03244c0,1.88921,1.21539,2.74335,3.86016,3.3186c2.41455,0.50911,2.9898,1.11681,2.9898,2.18433v0.03307c0,1.13365-1.01885,1.95472-2.56304,1.95472c-1.59285,0-2.74273-0.54218-3.94189-1.62654l-0.80485,0.95334c1.37947,1.23161,2.87437,1.8393,4.69745,1.8393c2.28353,0,3.9263-1.2809,3.9263-3.25247v-0.03244C45.57796,12.23273,44.39502,11.29623,41.8326,10.73783zM59.77015,11.36174V11.3293c0-3.17073-2.34904-5.91409-5.84795-5.91409s-5.88039,2.77642-5.88039,5.94653v0.03307c0,3.17011,2.34841,5.91346,5.84733,5.91346c1.41317,0,2.64477-0.45983,3.61434-1.21539l1.52734,1.36325l0.88721-0.96894l-1.51112-1.28152C59.26104,14.15438,59.77015,12.79113,59.77015,11.36174zM57.45418,14.40083L55.3023,12.4461l-0.87099,0.96894l2.15188,1.83992c-0.72249,0.54218-1.64277,0.85414-2.661,0.85414c-2.62793,0-4.53399-2.13566-4.53399-4.74737V11.3293c0-2.61233,1.87299-4.71492,4.50092-4.71492c2.62855,0,4.53399,2.13566,4.53399,4.74737v0.03307C58.42312,12.54468,58.07809,13.57913,57.45418,14.40083zM71.26765,12.29824c0,2.51313-1.34704,3.79465-3.46585,3.79465c-2.18494,0-3.51514-1.39632-3.51514-3.87701V5.61237h-1.29774v6.68587c0,3.25247,1.93788,4.99381,4.77981,4.99381c2.875,0,4.79665-1.74135,4.79665-5.09239V5.61237h-1.29774V12.29824zM150.96526,16.10911c-2.52997,0-4.45163-2.08637-4.45163-4.74737V11.3293c0-2.64477,1.90544-4.71492,4.45163-4.71492c1.56042,0,2.64478,0.65698,3.61372,1.57726l0.88721-0.95272c-1.14987-1.08436-2.39833-1.8237-4.4847-1.8237c-3.3835,0-5.8149,2.69406-5.8149,5.94653v0.03307c0,3.30176,2.44762,5.91346,5.76559,5.91346c2.08638,0,3.40034-0.80485,4.5995-2.02024l-0.85414-0.83792C153.57697,15.50141,152.5419,16.10911,150.96526,16.10911zM137.64406,5.53063l-5.24025,11.58048h1.3308l1.36327-3.03909h6.25848l1.34703,3.03909h1.39632l-5.24025-11.58048H137.64406zM135.60698,12.88971l2.62856-5.84795l2.61171,5.84795H135.60698zM159.83484,15.92817v-4.02425h6.2747v-1.18294h-6.2747V6.79531h7.01404V5.61237h-8.31178v11.49875h8.39352v-1.18294H159.83484zM102.27933,11.90392h6.27471v-1.18294h-6.27471V6.79531h7.01405V5.61237h-8.31179v11.49875h8.39352v-1.18294h-7.09579V11.90392zM97.54881,9.06199V9.02955c0-0.90343-0.3288-1.7089-0.88721-2.26731c-0.7225-0.72249-1.85614-1.14987-3.26868-1.14987h-4.94453v11.49875h1.29774v-4.46785h3.28554l3.3342,4.46785h1.59348l-3.53136-4.69808C96.23484,12.08486,97.54881,10.96743,97.54881,9.06199zM89.74613,11.47654V6.81153h3.5482c1.85615,0,2.94051,0.85414,2.94051,2.26731v0.03244c0,1.47868-1.23223,2.36526-2.95673,2.36526H89.74613zM116.6849,10.73783c-2.52998-0.54218-3.12144-1.14987-3.12144-2.23424V8.47052c0-1.03445,0.95271-1.85615,2.44762-1.85615c1.18294,0,2.25046,0.37809,3.31798,1.24845l0.75618-1.00201c-1.16672-0.93587-2.36588-1.41254-4.04109-1.41254c-2.18495,0-3.77843,1.33081-3.77843,3.13767v0.03244c0,1.88921,1.21601,2.74335,3.86016,3.3186c2.41517,0.50911,2.9898,1.11681,2.9898,2.18433v0.03307c0,1.13365-1.01823,1.95472-2.56242,1.95472c-1.59348,0-2.74335-0.54218-3.94251-1.62654l-0.80485,0.95334c1.3801,1.23161,2.87499,1.8393,4.69807,1.8393c2.28353,0,3.92567-1.2809,3.92567-3.25247v-0.03244C120.42963,12.23273,119.24731,11.29623,116.6849,10.73783zM127.9528,5.61237h-4.30376v11.49875h1.29774V13.0538h2.79202c2.44761,0,4.51777-1.28152,4.51777-3.76158V9.25915C132.25656,6.99247,130.54828,5.61237,127.9528,5.61237zM130.94197,9.3415c0,1.51112-1.24846,2.52935-3.15389,2.52935h-2.84131V6.81153h2.90745c1.85614,0,3.08775,0.85414,3.08775,2.49691V9.3415zM18.77845,6.54165c0.38683-0.38683,0.38683-1.01281,0-1.39964c-0.38679-0.38679-1.01281-0.38679-1.3996,0l-9.2369,9.2369c-1.31217,1.31217-3.44709,1.31217-4.75856,0.0007c-1.31213-1.31213-1.31213-3.44709,0-4.75922l7.43067-7.43067c0.38679-0.38683,0.38679-1.01281,0-1.39964c-0.38683-0.38679-1.01281-0.38679-1.39964,0L1.98376,8.22076c-2.08368,2.08372-2.08368,5.47477,0,7.55845c2.08372,2.08372,5.47411,2.08302,7.55779-0.00066L18.77845,6.54165zM5.06321,12.6998c0.38679,0.38679,1.01281,0.38679,1.3996,0l9.2369-9.2369c1.31213-1.31213,3.44709-1.31213,4.75856-0.0007c0.38679,0.38683,1.01281,0.38683,1.3996,0c0.38683-0.38679,0.38683-1.01277,0-1.3996c-2.08368-2.08372-5.47407-2.08302-7.55775,0.0007l-9.2369,9.2369C4.67638,11.68699,4.67638,12.31298,5.06321,12.6998zM24.93661,11.30021c-0.38679-0.38683-1.01281-0.38683-1.3996,0l-9.2369,9.2369c-1.31213,1.31213-3.44709,1.31213-4.75856,0.00066c-0.38679-0.38679-1.01281-0.38679-1.3996,0c-0.38683,0.38683-0.38683,1.01281,0,1.3996c2.08368,2.08372,5.47407,2.08302,7.55775-0.00066l9.2369-9.2369C25.32343,12.31298,25.32343,11.68699,24.93661,11.30021zM20.45826,8.22146l-9.2369,9.2369c-0.38683,0.38679-0.38683,1.01281,0,1.3996c0.38679,0.38683,1.01281,0.38683,1.3996,0l9.2369-9.2369c1.31217-1.31213,3.44709-1.31213,4.75856-0.00066c1.31213,1.31213,1.31213,3.44709,0,4.75922l-7.43067,7.43067c-0.38679,0.38679-0.38679,1.01281,0,1.3996c0.38683,0.38683,1.01281,0.38683,1.39964,0l7.43067-7.43067c2.08368-2.08368,2.08368-5.47473,0-7.55845C25.93233,6.13708,22.54194,6.13774,20.45826,8.22146z"></path>
</svg>

    </a>
  </span>

                    <div class="header-buttons">
                        <div class="www-x light">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                                <line x1="1.9" y1="1.9" x2="23.1" y2="23.1"/>
                                <line x1="23.1" y1="1.9" x2="1.9" y2="23.1"/>
                            </svg>

                        </div>
                    </div>
                </header>


                <div class="search-app-wrapper no-transitions">
                    <div class="search-app has-dark-background">
                    </div>
                </div>


                <aside class="domains-transfer-aside">
                    <div class="transfer-icon">
                        <svg x="0px" y="0px" width="30px" height="24px" viewBox="0 0 30 24">
                            <polyline points="0,8.8 27.7,8.8 22.1,1 "/>
                            <polyline points="29.9,15.2 2.2,15.2 7.8,23 "/>
                        </svg>

                    </div>
                    <div class="title">
                        <h2>Already have a domain?</h2>
                        <h3>Transfer your domain to Squarespace in just a few easy steps.</h3>
                    </div>
                </aside>


                <section id="domains-marketing-wrapper" class="has-dark-background"></section>
            </div>
        </div>


        <div class="scroll-down">
            <div class="scroll-down-copy">
                <h3>All-In-One Domains Pricing</h3>
                <h4>Including free SSL, free WHOIS privacy, and a spam-free parking page</h4>
            </div>
            <div class="scroll-down-actions www-layout">
        <span class="transfer-icon"><svg x="0px" y="0px" width="30px" height="24px" viewBox="0 0 30 24">
  <polyline points="0,8.8 27.7,8.8 22.1,1 "/>
  <polyline points="29.9,15.2 2.2,15.2 7.8,23 "/>
</svg>
</span>
                <span class="transfer-domain">Transfer your domain</span>
                <span class="scroll-down-button">Learn more <span class="extended-text">about Domains</span></span>
            </div>
        </div>
    </div>
</div>


<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>


</body>
<script src="assets/blueline/js/menu.js"></script>
</html>

