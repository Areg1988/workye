<!DOCTYPE html>

<html >
<head>


    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <script>(function (d) {
            var config = {
                    kitId: 'gnf7xqu',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement, t = setTimeout(function () {
                    h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                s = d.getElementsByTagName("script")[0], a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function () {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {
                }
            };
            s.parentNode.insertBefore(tk, s)
        })(document);</script>

    <script type="text/javascript" src="assets/blueline/js/jquery.min.js"></script>
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <link rel="stylesheet" href="assets/blueline/css/poges/terms.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/header.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/footer.css">


</head>

<body class="collection-page">

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
?>

<section class="generic-hero has-dark-background">
    <div class="hero-panel">
        <img class="hero-image" data-use-bg-image="true"/>
        <span class="hero-title">The Fine Print</span>
        <div class="hero-description"><p>The Fine Print</p>


        </div>
    </div>
</section>


<nav class="tab-nav">


    <a class="tab-subnav is-active" href="/terms-of-service/" data-name="Terms of Service">Terms of Service</a>


    <a class="tab-subnav " href="/privacy/" data-name="Privacy Policy">Privacy Policy</a>


    <a class="tab-subnav " href="/acceptable-use-policy/" data-name="Acceptable Use Policy">Acceptable Use Policy</a>


    <a class="tab-subnav " href="/cookie-policy/" data-name="Cookie Policy">Cookie Policy</a>


    <a class="tab-subnav " href="/copyright-policy/" data-name="Copyright Policy">Copyright Policy</a>


    <a class="tab-subnav " href="/image-terms/" data-name="Image Terms">Image Terms</a>


    <a class="tab-subnav " href="/dpa/" data-name="Data Processing Addendum">Data Processing Addendum</a>


    <a class="tab-subnav " href="/developer-terms/" data-name="Developer Terms">Developer Terms</a>


    <a class="tab-subnav " href="/imprint/" data-name="Imprint">Imprint</a>


</nav>

<nav class="select-nav is-hidden">
    <select class="select-subnav">


        <option class="translate" name="Terms of Service" data-href="/terms-of-service/"
                data-visited="/terms-of-service/" selected>
            Terms of Service
        </option>


        <option class="translate" name="Privacy Policy" data-href="/privacy/">
            Privacy Policy
        </option>


        <option class="translate" name="Acceptable Use Policy" data-href="/acceptable-use-policy/">
            Acceptable Use Policy
        </option>


        <option class="translate" name="Cookie Policy" data-href="/cookie-policy/">
            Cookie Policy
        </option>


        <option class="translate" name="Copyright Policy" data-href="/copyright-policy/">
            Copyright Policy
        </option>


        <option class="translate" name="Image Terms" data-href="/image-terms/">
            Image Terms
        </option>


        <option class="translate" name="Data Processing Addendum" data-href="/dpa/">
            Data Processing Addendum
        </option>


        <option class="translate" name="Developer Terms" data-href="/developer-terms/">
            Developer Terms
        </option>


        <option class="translate" name="Imprint" data-href="/imprint/">
            Imprint
        </option>


    </select>
</nav>


<main id="content" class="content page-terms-of-service" role="main" data-content-field="main-content"
      data-url-id="collection-5ad0cd4d8a922de8df830d7e">
    <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1537824469607"
         id="page-5ad0cd4d8a922de8df830d7e">
        <div class="row sqs-row">
            <div class="col sqs-col-12 span-12">
                <div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-4c9db08b1cd1b97fbd5e">
                    <div class="sqs-block-content"><h1 style="text-align: center;">Terms of
                            Service</h1>
                        <p style="white-space: pre-wrap;">&nbsp;</p>
                        <p style="white-space: pre-wrap;">&nbsp;</p>
                        <p style="white-space: pre-wrap;">&nbsp;</p>
                        <p ><strong>Effective Date: May 14, 2018</strong></p>
                        <blockquote><p ">This page explains our terms of service, which
                                contain important information about your legal rights. When you use Squarespace, you’re
                                agreeing to these terms. To help make them easier to understand, we’ve also included
                                annotations in these gray boxes. The annotations aren't part of the official terms and
                                have no legal effect, but are intended to help you follow the text.</p></blockquote>
                        <p>Hello and welcome to Squarespace’s Terms of Service!</p>
                        <p >These Terms of Service ("Terms") cover your use of and access
                            to the sites, templates, products, applications, tools and features (collectively, the
                            "Services") provided by Squarespace (as defined below), including without limitation during
                            free trials, on the website and associated domains of <a href="https://www.squarespace.com">www.squarespace.com</a>
                            and on Squarespace web and mobile applications.</p>
                        <p >By using or accessing the Services, you're agreeing to these
                            Terms, our <a href="/copyright-policy">Copyright Policy</a>, our <a
                                href="/acceptable-use-policy">Acceptable Use Policy</a> and our <a href="/dpa">Data
                                Processing Addendum</a> (collectively, this “Agreement”). If you're using the Services
                            for an organization, you're agreeing to this Agreement on behalf of that organization, and
                            you represent and warrant that you can do so. References to “you”, "your” and similar terms
                            are construed accordingly in this Agreement. If you don’t agree to all the terms in this
                            Agreement, you may not use or access the Services.</p>
                        <p >If you are a resident of or have your principal place of
                            business in the United States of America or any of its territories or possessions (the
                            “US”), you are agreeing to this Agreement with Squarespace, Inc. and are a “US User”.
                            Otherwise, you are agreeing to this Agreement with Squarespace Ireland Limited (“Squarespace
                            Ireland”) and are a “Non-US User”. References to “Squarespace”, “us”, “we” and “our” mean
                            Squarespace, Inc. if you are a US User or Squarespace Ireland if you are a Non-US User.
                            &nbsp;&nbsp;If your place of residence or principal place of business changes, the
                            Squarespace entity you contract with will be determined by your new residence or principal
                            place of business, as specified above, from the date it changes.</p>
                        <p >Please read this Agreement carefully! It includes important
                            information about your legal rights, and covers areas such as automatic subscription
                            renewals, warranty disclaimers, limitations of liability, the resolution of disputes by
                            arbitration and a class action waiver. Please note if you are an EU Consumer (as defined
                            below), some of these provisions may not apply to you and you may be entitled to specific
                            rights under the mandatory laws of the country in which you live.</p>
                        <p >We’ve tried to make this Agreement fair and straightforward,
                            but feel free to <a href="https://support.squarespace.com/hc/en-us/requests/new">contact
                                us</a> if you have any questions or suggestions.</p>
                        <p ><strong>1.&nbsp;&nbsp; &nbsp;Creating An Account</strong></p>
                        <blockquote><p >Make sure your account information is accurate and
                                that you keep your account safe. You’re responsible for your account and any activity on
                                it. Also, you need to be at least 16 years old to use Squarespace.</p></blockquote>
                        <p style="margin-left: 40px; "><strong>1.1. Signing Up.</strong> To use
                            many of the Services, you must first create an account (“Account”). You agree to provide us
                            with accurate, complete and updated information for your Account. We may need to use this
                            information to contact you.</p>
                        <p style="margin-left: 40px; "><strong>1.2. Staying Safe.</strong> Please
                            safeguard your Account and make sure others don't have access to your Account or password.
                            You're solely responsible for any activity on your Account and for maintaining the
                            confidentiality and security of your password. We’re not liable for any acts or omissions by
                            you in connection with your Account. You must immediately notify us if you know or have any
                            reason to suspect that your Account or password have been stolen, misappropriated or
                            otherwise compromised or in case of any actual or suspected unauthorized use of your
                            Account.</p>
                        <p style="margin-left: 40px; ;"><strong>1.3. Sixteen And Older.</strong>
                            The Services are not intended for and may not be used by children under the age of 16. By
                            using the Services, you represent that you're at least 16. If you’re under the age of 18,
                            depending on where you live, you may need to have your parent or guardian’s consent to this
                            Agreement and they may need to enter into this Agreement on your behalf.</p>
                        <p style=";"><strong>2.&nbsp;&nbsp; &nbsp;Your Content</strong></p>
                        <blockquote><p style=";">When you upload content to Squarespace, you still
                                own it. You do, however, give us permission to use it in ways necessary to provide,
                                improve, promote and protect our services. For example, when you upload a photo, you
                                give us the right to save it and display it on your site at your direction. We also may
                                promote or feature your site, but you can opt out if you don’t want us to do that.</p>
                        </blockquote>
                        <p style="margin-left: 40px; ;"><strong>2.1. Your User Content Stays
                                Yours.</strong> Users of the Services (whether you or others) may provide us with
                            content, including without limitation text, photos, images, audio, video, code and any other
                            materials (“User Content"). Your User Content stays yours, except for the limited rights
                            that enable us to provide, improve, promote and protect the Services as described in this
                            Agreement.</p>
                        <p style="margin-left: 40px; ;"><strong>2.2. Your License To Us.</strong>&nbsp;When
                            you provide User Content via the Services, you grant Squarespace (including our third party
                            hosting providers acting on our behalf) a non-exclusive, worldwide, perpetual, irrevocable,
                            royalty-free, sublicensable, transferable right and license to use, host, store, reproduce,
                            modify, create derivative works of (such as those resulting from translations, adaptations
                            or other changes we make so that User Content works better with the Services), communicate,
                            publish, publicly display, publicly perform and distribute User Content for the limited
                            purposes of allowing us to provide, improve, promote and protect the Services. This Section
                            does not affect any rights you may have under applicable data protection laws.</p>
                        <p style="margin-left: 40px; ;"><strong>2.3. Featuring Your Site.</strong>
                            We may choose to feature sites you use the Services to create or publish (“Your Sites”) or
                            names, trademarks, service marks or logos included on Your Sites. You grant us a perpetual,
                            worldwide, royalty-free, non-exclusive right and license to use any version of Your Sites,
                            or any portion of Your Sites, including without limitation names, trademarks, service marks
                            or logos on Your Sites, for the limited purpose of Squarespace marketing and promotional
                            activities. For example, we may feature Your Sites on our Templates page, on the Customers
                            sections of our sites or on our social media accounts. You waive any claims against us
                            relating to any moral rights, artists’ rights or any other similar rights worldwide that you
                            may have in or to Your Sites or names, trademarks, service marks or logos on Your Sites and
                            any right of inspection or approval of any such use. You can opt out of being featured
                            through your Account. This Section does not affect any rights you may have under applicable
                            data protection laws.</p>
                        <p style=";"><strong>3.&nbsp;&nbsp; &nbsp;Your Responsibilities</strong>
                        </p>
                        <blockquote><p style=";">You’re responsible for the content you publish on
                                Squarespace, and you vouch to us that it’s all okay to use. We ask that you follow our
                                rules and don’t do anything illegal with the services. Also keep in mind that what you
                                upload may be publicly viewable.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>3.1. Only Use Content You’re
                                Allowed To Use.</strong> You represent and warrant that you own all rights to your User
                            Content or otherwise have (and will continue to have) all rights and permissions to legally
                            use, share, display, transfer and license your User Content via the Services and in the
                            manner required by this Agreement. If we use your User Content in the ways described in this
                            Agreement, you represent and warrant that such use will not infringe or violate the rights
                            of any third party, including without limitation any copyrights, trademarks, privacy rights,
                            publicity rights, contract rights, trade secrets or any other intellectual property or
                            proprietary rights. Also, content on the Services may be protected by others' intellectual
                            property, trade secret or other rights. Please don't copy, upload, download or share content
                            unless you have the right to do so.</p>
                        <p style="margin-left: 40px; ;"><strong>3.2. Follow The Law.</strong> You
                            represent and warrant that your use of the Services is not contrary to law, including
                            without limitation applicable export or import controls and regulations and sanctions.</p>
                        <p style="margin-left: 40px; ;"><strong>3.3. Share Responsibly. </strong>The
                            Services let you share User Content including without limitation on social media and the
                            open web, so please think carefully about your User Content. We’re not responsible for what
                            you share via the Services.</p>
                        <p style="margin-left: 40px; ;"><strong>3.4. Comply With Our Acceptable Use
                                Policy. </strong>&nbsp;You must comply with our <a href="/acceptable-use-policy">Acceptable
                                Use Policy</a>. &nbsp;You represent and warrant that all your User Content complies with
                            our <a href="/acceptable-use-policy">Acceptable Use Policy</a>.</p>
                        <p style="margin-left: 40px; ;"><strong>3.5. Your Sites And Your End Users
                                Are Your Responsibility.</strong> Your Sites may have their own visitors, customers and
                            users (“End Users”). You understand and agree that <strong>Your Sites and your End Users are
                                your responsibility</strong>, and you’re solely responsible for compliance with any laws
                            or regulations related to Your Sites and/or your End Users. <strong>We’re not liable for,
                                and won’t provide you with any legal advice regarding, Your Sites or your End
                                Users.</strong> This does not limit or affect any liability we may have to you
                            separately for any breach of the other provisions of this Agreement.</p>
                        <p style=";"><strong>4.&nbsp;&nbsp; &nbsp;Third Party Services And Sites,
                                User Content And Squarespace Specialists</strong></p>
                        <blockquote><p style=";">If you use or connect another service on or to
                                Squarespace, follow a link to another site or work with someone you find on Squarespace
                                (such as a specialist or Circle member), what happens is between you and them. We’re not
                                responsible for it or what either of you do. There’s also a lot of content on
                                Squarespace uploaded by our users (like you). We’re not responsible for that either.</p>
                        </blockquote>
                        <p style="margin-left: 40px; ;"><strong>4.1. Third Party Services.</strong>
                            The Services are integrated with various third party services and applications
                            (collectively, “Third Party Services”) that may make available to you their content and
                            products. Examples of Third Party Services include our domain registrar, social media
                            platforms, Squarespace Specialists (as defined below), eCommerce Payment Processors (as
                            defined below), stock images and email service subscriptions for sale via the Services and
                            other integration partners and service providers. These Third Party Services may have their
                            own terms and policies, and your use of them will be governed by those terms and policies.
                            <strong>We don't control Third Party Services, and we’re not liable for Third Party Services
                                or for any transaction you may enter into with them, or for what they do. Your security
                                when using Third Party Services is your responsibility.</strong> You also agree that we
                            may, at any time and in our sole discretion, and without any notice to you, suspend, disable
                            access to or remove any Third Party Services. We’re not liable to you for any such
                            suspension, disabling or removal, including without limitation for any loss of profits,
                            revenue, data, goodwill or other intangible losses you may experience as a result (except
                            where prohibited by applicable law).</p>
                        <p style="margin-left: 40px; ;"><strong>4.2. Third Party Sites.</strong>
                            The Services may contain links to third party sites. When you access third party sites, you
                            do so at your own risk. <strong>We don’t control and aren’t liable for those sites and what
                                those third parties do.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>4.3. User Content</strong>. The
                            Services or sites created using the Services may contain User Content: (a) that is offensive
                            or objectionable; (b) that contains errors; (c) that violates intellectual property, trade
                            secret, privacy, publicity or other rights or the good name of you or third parties; (d)
                            that is harmful to your or others’ computers or networks; (e) that is unlawful or illegal;
                            or (f) the downloading, copying or use of which is subject to additional terms and policies
                            of third parties or is protected by intellectual property, trade secret, privacy or other
                            laws. By operating the Services, we don’t represent or imply that we endorse your or other
                            users’ User Content, or that we believe such User Content to be accurate, useful, lawful or
                            non-harmful. We’re not a publisher of, and we’re not liable for, any User Content uploaded,
                            posted, published or otherwise made available via the Services by you or other users. You're
                            responsible for taking precautions to protect yourself, and your computer or network, from
                            User Content accessed via the Services.</p>
                        <p style="margin-left: 40px; ;"><strong>4.4. Squarespace
                                Specialists.</strong> Certain parts of the Services may provide directories of, and
                            information about, independent third party Squarespace users ("Squarespace Specialists") who
                            can help you use the Services. Squarespace does not employ, is not affiliated with and does
                            not endorse Squarespace Specialists. Squarespace Specialists are a Third Party Service, as
                            defined in Section 4.1.</p>
                        <p style="margin-left: 40px; ;"><strong>4.5. Squarespace Logo</strong>.
                            Squarespace Logo includes icons obtained from The Noun Project, Inc. (“Noun Project”). Noun
                            Project is a Third Party Service, as defined in Section 4.1, and your use of its icons is
                            subject to Noun Project’s <a
                                href="https://thenounproject.com/legal/#!terms-of-use">terms</a>.</p>
                        <p style="margin-left: 40px; ;"><strong>4.6. Developer Terms.</strong>
                            Squarespace may provide you with functionality to connect to, integrate or share information
                            with a Third Party Service through Developer Tools (as defined in the <a
                                href="/developer-terms">Developer Terms</a>). By accessing or using, or providing a
                            Third Party Service with access to or use of, the Developer Tools, you agree to the <a
                                href="/developer-terms">Developer Term</a><a href="#">s</a>. Any access to or use of
                            such Third Party Services is at your own risk and is your responsibility, and is governed by
                            the terms of Third Party Services in Section 4.1.</p>
                        <p style=";"><strong>5.&nbsp;&nbsp; &nbsp;Our Intellectual
                                Property</strong></p>
                        <blockquote><p style=";">Squarespace is protected by various intellectual
                                property laws. This section summarizes what we own and how we share.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>5.1. Squarespace Owns
                                Squarespace.</strong> The Services are protected by copyright, trademark and other US
                            and foreign laws. This Agreement doesn't grant you any right, title or interest in the
                            Services, others’ User Content, our trademarks, logos or other brand features or
                            intellectual property or trade secrets or others’ content in the Services. You agree not to
                            change, translate or otherwise create derivative works of the Services or others’ User
                            Content.</p>
                        <p style="margin-left: 40px; ;"><strong>5.2. We Can Use Your Feedback For
                                Free.</strong> We welcome your feedback, ideas or suggestions (“Feedback”), but you
                            agree that we may use your Feedback without any restriction or obligation to you, even after
                            this Agreement is terminated. This Section does not limit or affect any rights you may have
                            under applicable data protection laws.</p>
                        <p style="margin-left: 40px; ;"><strong>5.3. Our Demo Content.</strong> We
                            may provide templates or other products featuring demo content, including without limitation
                            text, photos, images, graphics, audio and video (“Demo Content”), to provide you with ideas
                            or inspiration. Unless we tell you otherwise, Demo Content (or any portion of it) may not
                            remain on Your Site or be distributed, publicly displayed, publicly performed or otherwise
                            published.</p>
                        <p style="margin-left: 40px; ;"><strong>5.4. Our Betas Are Still In
                                Beta</strong>. We may release products and features that we’re still testing and
                            evaluating. Those Services will be marked as beta, preview or early access (or a similar
                            phrasing), and may not be as reliable as our other Services.</p>
                        <p style=";"><strong>6.&nbsp;&nbsp; &nbsp;Our Rights</strong></p>
                        <blockquote><p style=";">To operate effectively and protect the security
                                and integrity of Squarespace, we need to maintain control over what happens on our
                                services.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>6.1. Important Things We Can
                                Do.</strong> <strong>We reserve these rights, which we may exercise at any time and in
                                our sole discretion, and without liability or notice to you </strong>(except where
                            prohibited by applicable law): (a) we may change parts or all of the Services and their
                            functionality; (b) we may suspend or discontinue parts or all of the Services; (c) we may
                            terminate, suspend, restrict or disable your access to or use of parts or all of the
                            Services; (d) we may terminate, suspend, restrict or disable access to your Account or
                            parts, some or all of Your Sites; and (e) we may change our eligibility criteria to use the
                            Services (and if such eligibility criteria changes are prohibited by law where you live, we
                            may revoke your right to use the Services in that jurisdiction).</p>
                        <p style="margin-left: 40px; ;"><strong>6.2. How We Handle Ownership
                                Disputes.</strong> Sometimes, ownership of an Account or site is disputed between
                            parties, such as a business and its employee, or a web designer and a client. We try not to
                            get involved in these disputes. <strong>However, we reserve the right, at any time and in
                                our sole discretion, and without notice to you, to determine rightful Account or site
                                ownership and to transfer an Account or site to such owner. Our decision in that respect
                                is final.</strong> If we feel that we can’t reasonably determine the rightful owner, we
                            reserve the right to suspend an Account or site until the disputing parties reach a
                            resolution. We also may request documentation, such as a government-issued photo ID, credit
                            card invoice or business license, to help determine the rightful owner.</p>
                        <p style="margin-left: 40px; ;"><strong>6.3. HTTPS Encryption.</strong> We
                            may offer HTTPS encryption for Your Sites. By registering a custom domain via the Services,
                            or pointing a custom domain to the Services, you authorize us to create and maintain a
                            certificate for the limited purpose of providing HTTPS for Your Sites.</p>
                        <p style=";"><strong>7.&nbsp;&nbsp; &nbsp;Privacy</strong></p>
                        <p style=";">Our <a href="/privacy">Privacy Policy</a> explains how we
                            collect, use and share your personal information for our own purposes. Be sure to read it
                            carefully, but note it is not part of this Agreement and can change. It is really important
                            that you comply with data protection laws when using the services, such as when you collect
                            others’ personal information or use cookies or similar technologies (including those we drop
                            for you at your request, such as for web analytics). Our <a href="/dpa">Data Processing
                                Addendum</a> explains how we handle, on your instructions, others’ personal information
                            you collect using the services or any of your User Content which contains others’ personal
                            information. Be sure to read that carefully also.</p>
                        <p style="margin-left: 40px; ;"><strong>7.1. Privacy Policy.</strong> By
                            using the Services, you confirm that you have read and understood our <a href="/privacy">Privacy
                                Policy</a>. However, it is not a contractual document and does not form part of this
                            Agreement and we may change it from time to time. &nbsp;</p>
                        <p style="margin-left: 40px; ;"><strong>7.2. Data Processing
                                Addendum.</strong> Our <a href="/dpa">Data Processing Addendum</a> forms part of this
                            Agreement.</p>
                        <p style="margin-left: 40px; ;"><strong>7.3. You Must Comply With Data
                                Protection, Security And Privacy Laws. You agree and warrant that you are solely
                                responsible when using Your Sites or the Services for complying with applicable data
                                protection, security and privacy laws and regulations (including, where applicable, the
                                EU General Data Protection Regulation and the EU e-Privacy Directive/Regulation),
                                including any notice and consent requirements</strong>. This includes without limitation
                            the collection and processing by you of any personal data, when you use Your Sites and the
                            Services to send marketing and other electronic communications to individuals and when using
                            cookies and similar technologies on Your Sites (including, in particular, those which we
                            place for you at your request as part of the Services, such as to undertake analytics for
                            you).</p>
                        <p style="margin-left: 80px; ;"><strong>7.3.1. Privacy Policies</strong>.
                            <strong>If applicable law requires, you must provide and make available to your End Users on
                                Your Sites a legally compliant privacy policy.</strong></p>
                        <p style="margin-left: 80px; ;"><strong>7.3.2. Cookies And Similar
                                Technologies. If applicable law requires, you must provide and make available to your
                                End Users on Your Sites a legally compliant cookie policy. </strong>You must capture
                            valid consent, both for you and us, for any cookies or similar technologies used on or
                            through Your Site (including those we drop on your request or with your permission) where
                            required, including, where applicable, by the EU e-Privacy Directive/Regulation and under
                            national laws implementing the same. Please see our <a href="/cookie-policy">Cookie
                                Policy</a> for more information about use of cookies and similar technologies. &nbsp;
                        </p>
                        <p style="margin-left: 40px; ;"><strong>7.4. Protect And Improve The
                                Services.</strong> You agree that we may protect and improve our Services through
                            analysis of your use of the Services, your End Users’ use of Your Sites and/or analysis of
                            your and your End Users’ personal information in anonymized, pseudonymized, de-personalized
                            and/or aggregated form. If applicable law requires, you should explain this in your privacy
                            policy. See our <a href="/privacy">Privacy Policy</a> for more information about how and
                            what we do in this regard.</p>
                        <p style=";"><strong>8.&nbsp;&nbsp; &nbsp;Copyright</strong></p>
                        <blockquote><p style=";">We comply with copyright law, and respond to
                                complaints about copyright infringement in accordance with our Copyright Policy.</p>
                        </blockquote>
                        <p style=";">We respect the intellectual property of others and ask that
                            you do too. We respond to notices of alleged copyright infringement if they comply with the
                            law, and such notices should be reported via the process described in our <a
                                href="/copyright-policy">Copyright Policy</a>, which is incorporated by reference into
                            this Agreement. We reserve the right to delete or disable content alleged to be infringing,
                            and to terminate Accounts of repeat infringers without any refunds.</p>
                        <p style=";"><strong>9.&nbsp;&nbsp; &nbsp;Paid Services And Fees</strong>
                        </p>
                        <blockquote><p style=";">Certain Services are paid services. This section
                                explains how we handle payments for those paid services. For certain paid services, such
                                as domain registrations and site subscriptions, we’ll automatically bill you in regular
                                intervals (such as monthly or annually) unless you disable auto-renewal or cancel your
                                subscription. You can do that anytime.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>9.1. Fees.</strong> You can access
                            certain portions of the Services by submitting a fee payment (such additional services,
                            “Paid Services”). Paid Services will remain in effect until cancelled or terminated in
                            accordance with this Agreement. We’ll tell you about fees for Paid Services before charging
                            you. You may cancel Paid Services at any time via the Services. If you don't pay for Paid
                            Services on time, we reserve the right to suspend or cancel your access to the Paid
                            Services. Transaction fees and additional fees may also apply to certain portions of the
                            Services, and we’ll tell you about those fees before charging you. Our fees will appear on
                            an invoice that we provide via the Services, within your eCommerce Payment Processor
                            account(s) and/or on a mobile app store invoice, unless otherwise indicated. Please note
                            that different Paid Services have different fees and payment schedules, and canceling one
                            Paid Service may not cancel all your Paid Services.</p>
                        <p style="margin-left: 40px; ;"><strong>9.2. Taxes.</strong> All fees are
                            exclusive of applicable national, provincial, state, local or other taxes (“Taxes”), unless
                            explicitly stated otherwise. You're responsible for all applicable Taxes, and we'll charge
                            Taxes in addition to the fees for the Services when required to do so. If you're exempt from
                            Taxes, you must provide us with valid tax exemption documentation. We reserve the right to
                            determine if the documentation provided is valid. Tax exemption will, provided we’re
                            satisfied it’s valid and applicable, only apply from and after the date we receive such
                            documentation. If Squarespace has a legal obligation to pay or collect indirect Taxes (such
                            as value-added tax or goods and services tax) on the sale to you under the laws of your
                            country (where you are established, have a permanent address or usually reside), you shall
                            be liable for payment of any such indirect Taxes. Where Squarespace does not have a legal
                            obligation to pay or collect indirect Taxes on a sale of Paid Services to you, you may be
                            required to self-assess those Taxes under the applicable laws of your country (where you are
                            established, have a permanent address or usually reside).</p>
                        <p style="margin-left: 40px; ;"><strong>9.3. Automatic Subscription
                                Renewals.</strong> To ensure uninterrupted service, we'll automatically bill you for
                            certain Paid Services from the date you submit your initial payment and on each renewal
                            period afterwards until cancellation. Your renewal period will be equal in time to the
                            renewal period of your current subscription. For example, if you're on a monthly
                            subscription plan, each billable renewal period will be for one (1) month. We’ll
                            automatically charge you the applicable amount using the payment method you have on file
                            with us. We’ll let you know in advance if you’re purchasing a Paid Service that includes
                            auto-renewal payments. You can disable auto-renewal at any time <a
                                href="https://support.squarespace.com/hc/en-us/articles/205810508-Cancelling-service#toc-option-2---disable-auto-renew">via
                                the Services</a>.</p>
                        <p style="margin-left: 40px; ;"><strong>9.4. Refunds.</strong> While you
                            may cancel any Paid Services at any time, you won't be issued a refund except in our sole
                            discretion, or if legally required. We offer a free trial so you can try out your website
                            subscription. Please note applicable statutory rights of cancellation may not result in a
                            refund, as we do not charge for this trial period.</p>
                        <p style="margin-left: 40px; ;"><strong>9.5. Fee Changes.</strong> We may
                            change our fees at any time. We’ll provide you with advance notice of these fee changes via
                            the Services. New fees will not apply retroactively. If you don't agree with the fee
                            changes, you have the right to reject the change by canceling the applicable Paid Service
                            before your next payment date.</p>
                        <p style="margin-left: 40px; ;"><strong>9.6. Chargebacks.</strong> If you
                            contact your bank or credit card company to decline, chargeback or otherwise reverse the
                            charge of any payable fees to us (“Chargeback”), we may automatically terminate your
                            Account. If you have questions about a payment made to us, we encourage you to contact <a
                                href="https://support.squarespace.com/hc/en-us/requests/new">Customer Care</a> before
                            filing a Chargeback. We reserve our right to dispute any Chargeback.</p>
                        <p style="margin-left: 40px; ;"><strong>9.7. Our Payment
                                Processor.</strong> We use a third party payment processor (the “Payment Processor”) to
                            bill you through a payment account linked to your Account. The processing of payments will
                            be subject to the terms, conditions and privacy policies of the Payment Processor, in
                            addition to this Agreement. <strong>Our current Payment Processor is Stripe, and your
                                payments are processed by Stripe in accordance with Stripe’s terms of service and
                                privacy policy.</strong> You agree to pay us, through the Payment Processor, all charges
                            at the prices then in effect for any purchase in accordance with the applicable payment
                            terms. You agree to make payment using the payment method you provide with your Account. We
                            reserve the right to correct, or to instruct our Payment Processor to correct, any errors or
                            mistakes, even if payment has already been requested or received.</p>
                        <p style="margin-left: 40px; ;"><strong>9.8. Fees For Third Party
                                Services. </strong>Third Party Services purchased via the Services may be subject to
                            different refund policies that those Third Party Services determine, and they may be
                            non-refundable. The purchase terms and conditions for such Third Party Services may be
                            displayed during the purchase process, such as through a link to the purchase terms and
                            conditions. It's your responsibility to verify your ability to purchase, cancel or obtain a
                            refund for a Third Party Service. Unless otherwise stated in this Agreement, we don’t offer
                            refunds for purchases of Third Party Services.</p>
                        <p style=";"><strong>10.&nbsp;&nbsp; &nbsp;Your eCommerce On
                                Squarespace</strong></p>
                        <blockquote><p style=";">We offer tools to help you conduct eCommerce
                                activities on Squarespace, such as selling your products or collecting donations. How
                                you conduct your eCommerce activities is your responsibility, and we’re not liable for
                                it. Also, be sure to follow our eCommerce rules, or we may terminate your account.
                                Finally, when you use a third party to process payments for your eCommerce activities,
                                remember that your relationship is with them, not us.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>10.1. eCommerce
                                Responsibilities.</strong> The Services include features that enable you to provide or
                            sell products and services to, or otherwise collect payments from, your End Users (such
                            activities, “Your eCommerce”). <strong>We’re not a party to, and we aren’t liable for, Your
                                eCommerce. You're solely responsible for Your eCommerce, and compliance with any laws or
                                regulations related to it, including without limitation the following: &nbsp;</strong>
                        </p>
                        <p style="margin-left: 80px; ;"><strong>10.1.1. Taxes.</strong> You're
                            solely responsible for: (a) all Taxes and fees associated with Your eCommerce, including
                            without limitation any Taxes related to the purchase or sale of products or services in
                            connection with Your eCommerce; (b) collecting, reporting and remitting required Taxes to
                            relevant government authorities; (c) informing your End Users of required Taxes, and
                            providing them with invoices as required by applicable law; (d) monitoring distance sales
                            thresholds in the EU and other indirect Taxes (such as value-added tax or goods and services
                            tax) and registration thresholds in the countries where you have customers or where you ship
                            goods to or provide services to; and (e) registering for indirect Taxes in countries where
                            you are required to register. You also agree that any tax estimates, reporting or related
                            materials that we may provide via the Services are for illustration purposes only, and you
                            may not rely on them to comply with your tax obligations. We do not give tax advice, and
                            nothing we say should be interpreted as such.</p>
                        <p style="margin-left: 80px; ;"><strong>10.1.2. Fulfillment And
                                Delivery.</strong> You're solely responsible for fulfilling and delivering your products
                            and services to your End Users. &nbsp;</p>
                        <p style="margin-left: 80px; ;"><strong>10.1.3. Claims And
                                Warranties.</strong> You're solely responsible for any claims or warranties you make in
                            connection with Your eCommerce and any claims made by End Users against you.</p>
                        <p style="margin-left: 80px; ;"><strong>10.1.4. Customer Service.</strong>
                            You're solely responsible for handling any comments or complaints related to Your eCommerce,
                            including without limitation any issues related to payments, promotions, refunds or
                            chargebacks. You agree to provide accurate and complete contact information on Your Sites so
                            that your End Users can submit comments or complaints to you.</p>
                        <p style="margin-left: 80px; ;"><strong>10.1.5. Site Terms, Policies And
                                Legal Compliance.</strong> You agree to post and make clearly available on Your Sites a
                            privacy and cookie policy, and any other terms or policies that may be required by
                            applicable law, and you warrant that Your Sites and Your eCommerce and your conduct will
                            comply with all applicable laws and regulations. You agree that we won’t provide any legal
                            advice regarding such terms, policies or compliance.</p>
                        <p style="margin-left: 80px; ;"><strong>10.1.6. Consumer, eCommerce And
                                Other Laws.</strong> You are also responsible for complying with any consumer, eCommerce
                            and related laws.</p>
                        <p style="margin-left: 40px; ;"><strong>10.2. eCommerce
                                Restrictions.</strong> You may not offer or sell any products or services which, in our
                            sole discretion,: (a) we consider hazardous, counterfeit, stolen, fraudulent, abusive or
                            adverse to our interests or reputation; (b) are prohibited for sale, distribution or use; or
                            (c) otherwise fail to comply with any applicable laws or regulations, including without
                            limitation with respect to intellectual property, trade secrets, privacy or publicity
                            rights, consumer protection, product safety or trade regulations or export controls,
                            regulations or sanctions.</p>
                        <p style="margin-left: 40px; ;"><strong>10.3. eCommerce Suspensions. While
                                we’d prefer not to, we may, at any time and in our sole discretion, and without any
                                notice to you, suspend, restrict or disable access to or remove your Account, Your Sites
                                or Your eCommerce, without any liability to you or to any End Users, including without
                                limitation for any loss of profits, revenue, data, goodwill or other intangible losses
                                (except where prohibited by applicable law).</strong> For example, we may suspend Your
                            eCommerce if you’re violating this Agreement.</p>
                        <p style="margin-left: 40px; ;"><strong>10.4. eCommerce Payment
                                Processors.</strong> To accept payments from your End Users in connection with Your
                            eCommerce, you may integrate Your Sites with third party payment processors (“eCommerce
                            Payment Processors”). <strong>Your relationship with such eCommerce Payment Processors is
                                governed by those eCommerce Payment Processors’ terms and policies. We don’t control and
                                aren’t liable for any eCommerce Payment Processors, or for any transaction you may enter
                                into with or through any eCommerce Payment Processors.</strong> eCommerce Payment
                            Processors are a Third Party Service, as defined in Section 4.1. While we will try to
                            provide advance notice, you agree that <strong>we may, at any time and in our sole
                                discretion, and without any notice to you, suspend, restrict or disable access to or
                                remove from the Services, any eCommerce Payment Processors, without any liability to you
                                or to any End Users, including without limitation for any loss of profits, revenue,
                                data, goodwill or other intangible losses (except where prohibited by applicable
                                law).</strong> Your eCommerce Payment Processors may provide invoices for any
                            transaction fees associated with Your eCommerce transactions.</p>
                        <p style=";"><strong>11.&nbsp;&nbsp; &nbsp;Domains</strong></p>
                        <blockquote><p style=";">This section explains how we provide our domain
                                services. Your domain registrations are also subject to agreements with third parties,
                                including ICANN and our third party registrar partners.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>11.1. Reseller Services.</strong>
                            We work with third party registrars to provide you with domain services. When you register a
                            domain name, or renew or transfer an existing domain name, via the Services, you are bound
                            by the relevant registrar’s terms and conditions. Registrars are a Third Party Service, as
                            defined in Section 4.1. <strong>Currently, the registrar for the Services is Tucows Inc.,
                                and all registrations and renewals via the Services are subject to the </strong><a
                                href="https://www.opensrs.com/docs/contracts/exhibita.htm?"><strong>Tucows Terms and
                                    Conditions</strong></a><strong> (the “Tucows Terms”), under which we’re the
                                “Reseller”. Your breach of the Tucows Terms is a breach of this Agreement.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>11.2. ICANN.</strong> Your use of
                            our domain services is subject to the policies, including without limitation the dispute
                            resolution policies, of the Internet Corporation for Assigned Names and Numbers (“ICANN”).
                            Your rights and responsibilities as a domain name registrant under ICANN’s 2009 Registrar
                            Accreditation Agreement are summarized <a
                                href="https://www.icann.org/resources/pages/responsibilities-2014-03-14-en">here</a>.
                            You can learn more about domain name registration in general <a
                                href="https://www.icann.org/resources/pages/educational-2012-02-25-en">here</a>. Country
                            code top level domain names may not be subject to ICANN policies. In such cases, the
                            applicable policies are set forth in the Tucows Terms.</p>
                        <p style="margin-left: 40px; ;"><strong>11.3. Transfers, Renewals And
                                Refunds.</strong> <strong>You may not be able to transfer a domain name for the first
                                sixty (60) days following registration.</strong> For renewals, we or our registrar will
                            try to provide you notice thirty (30) days before, five (5) days before and three (3) days
                            after your scheduled domain renewal date. However, you agree that renewing your domain is
                            solely your responsibility. <strong>If you cancel a domain name purchase within the first
                                five (5) days following your purchase, if the Tucows Terms permit, we may provide a full
                                refund. However, we don't offer refunds for domain renewals or transfers.</strong></p>
                        <p style=";"><strong>12.&nbsp;&nbsp; &nbsp;Term And Termination</strong>
                        </p>
                        <blockquote><p style=";">Either of us can end this agreement at any
                                time.</p></blockquote>
                        <p style=";">This Agreement will remain in effect until terminated by
                            either you or us. You may terminate this Agreement at any time via the Services. <strong>We
                                reserve the right to change, suspend or discontinue, or terminate, restrict or disable
                                your use of or access to, parts or all of the Services or their functionality at any
                                time at our sole discretion and without notice.</strong> For example, we may suspend or
                            terminate your use of part or all of the Services if you're violating these Terms or our <a
                                href="/acceptable-use-policy">Acceptable Use Policy</a>. We will endeavor to provide you
                            reasonable notice upon suspending or terminating part or all of the Services. All sections
                            of this Agreement that by their nature should survive termination shall survive termination,
                            including without limitation Your Content, Our Intellectual Property, Warranty Disclaimers,
                            Limitation of Liability, Indemnification, Dispute Resolution and Additional Terms.</p>
                        <p style=";"><strong>13.&nbsp;&nbsp; &nbsp;Warranty Disclaimers.</strong>
                        </p>
                        <blockquote><p style=";">We work hard to make Squarespace great, but the
                                services are provided as is, without warranties.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>13.1. Disclaimers. To the fullest
                                extent permitted by applicable law, Squarespace makes no warranties, either express or
                                implied, about the Services. The Services are provided “as is” and “as available”.
                                Squarespace also disclaims any warranties of merchantability, fitness for a particular
                                purpose and non-infringement. No advice or information, whether oral or written,
                                obtained by you from Squarespace, shall create any warranty. Squarespace makes no
                                warranty or representation that the Services will: (a) be timely, uninterrupted or
                                error-free; (b) meet your requirements or expectations; or (c) be free from viruses or
                                other harmful components. However, Squarespace will provide the Services with reasonable
                                care.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>13.2. Exceptions.</strong> Under
                            certain circumstances, some jurisdictions don't permit the disclaimers in Section 13.1, so
                            they may not apply to you. However, the disclaimers apply to the maximum extent permitted by
                            applicable law. You may have other statutory rights and nothing in this Agreement affects
                            your statutory rights or rights under mandatory laws. The duration of statutorily required
                            warranties, if any, shall be limited to the maximum extent permitted by applicable law.</p>
                        <p style=";"><strong>14.&nbsp;&nbsp; &nbsp;Limitation Of Liability</strong>
                        </p>
                        <blockquote><p style=";">If something bad happens as a result of your using
                                Squarespace, our liability is capped.</p></blockquote>
                        <p style=";"><strong>Unless you are an EU Consumer, you acknowledge and
                                agree that to the fullest extent permitted by applicable law, in no event will
                                Squarespace and its affiliates and its and their directors, officers, employees and
                                agents be liable with respect to any claims arising out of or related to the Services or
                                this Agreement for: (a) any indirect, special, incidental, exemplary, punitive or
                                consequential damages; (b) any loss of profits, revenue, data, goodwill or other
                                intangible losses; (c) any damages related to your access to, use of or inability to
                                access or use parts, some or all of &nbsp;your Account, Your Sites or parts or all of
                                the Services, including without limitation interruption of use or cessation or
                                modification of any aspect of the Services; (d) any damages related to unavailability,
                                degradation, loss, corruption, theft, unauthorized access or, unauthorized alteration
                                of, any content, information or data, including without limitation User Content and Your
                                eCommerce data; (e) any User Content or other conduct or content of any user or third
                                party using the Services, including without limitation defamatory, offensive or unlawful
                                conduct or content; or (f) any Third Party Services or third party sites accessed via
                                the Services. If you are an EU Consumer, we shall, despite any other provision in this
                                Agreement, provide the Services with reasonable care but will not be liable for any
                                losses which were not a reasonably foreseeable consequence of our breach of this
                                Agreement (except in relation to death or personal injury resulting from our negligence
                                or fraud). These limitations apply to any theory of liability, whether based on
                                warranty, contract, tort, negligence, strict liability or any other legal theory,
                                whether or not Squarespace has been informed of the possibility of such damage, and even
                                if a remedy set forth in this Agreement is found to have failed its essential purpose.
                                To the fullest extent permitted by applicable law (whether or not you are an EU
                                Consumer), in no event shall the aggregate liability of Squarespace for all claims
                                arising out of or related to the Services and this Agreement exceed the greater of
                                twenty dollars ($20) or the amounts paid by you to Squarespace in the twelve (12) months
                                immediately preceding the event that gave rise to such claim.</strong> If you are an EU
                            Consumer, Squarespace is liable under statutory provisions for intent and gross negligence
                            by us, our legal representatives, directors or other vicarious agents. An “EU Consumer”
                            means a natural person acting for purposes outside their trade, business, craft or
                            profession (as opposed to a User for business or commercial purposes) habitually residing in
                            the European Economic Area.</p>
                        <p style=;"><strong>15.&nbsp;&nbsp; &nbsp;Indemnification</strong></p>
                        <blockquote><p style=";">If you do something that gets us sued, you’ll
                                cover us.</p></blockquote>
                        <p style=";"><strong>To the fullest extent permitted by law, you agree to
                                indemnify and hold harmless Squarespace and its affiliates and its and their directors,
                                officers, employees and agents from and against all damages, losses, liabilities, costs,
                                claims, demands, fines, awards and expenses of any kind (including without limitation
                                reasonable attorneys' fees and costs) arising out of or related to: (a) your breach of
                                this Agreement; (b) your User Content, Your Sites and Your eCommerce; (c) any claims by,
                                on behalf of or against your End Users; (d) your violation of any law or regulation or
                                the rights or good name of any third party; and (e) any claims from tax authorities in
                                any country in relation to Your eCommerce operations, including without limitation your
                                sales to individual consumers (including distance sales) and other operations for which
                                Squarespace may be held jointly and severally liable. Your indemnification obligations
                                under this Section shall not apply to the extent directly caused by our breach of this
                                Agreement or, where you are an EU Consumer, to the extent that the consequences were not
                                reasonably foreseeable.</strong></p>
                        <p style=";"><strong>16.&nbsp;&nbsp; &nbsp;Dispute Resolution</strong></p>
                        <blockquote><p style=";">This Section 16 may not apply to you. If it does,
                                before filing a claim against Squarespace, you agree to try to work it out informally
                                with us first. Also, all formal disputes must be resolved through arbitration following
                                the rules described below, unless you opt out of arbitration following the procedure
                                described below. Finally, claims can only be brought individually, and not as part of a
                                class action.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>16.1. Applicability.</strong> This
                            Section 16 shall only apply to: (a) US Users; (b) Non-US Users who are not EU Consumers; or
                            (c) EU Consumers who bring any claim against Squarespace in the US (to the extent not in
                            conflict with Section 17.2).</p>
                        <p style="margin-left: 40px; ;"><strong>16.2. Informal Resolution.</strong>
                            Before filing a claim against Squarespace, you agree to try to resolve the dispute by first
                            emailing <a href="mailto:legal@squarespace.com">legal@squarespace.com</a> with a description
                            of your claim. We'll try to resolve the dispute informally by following up via email, phone
                            or other methods. If we can’t resolve the dispute within thirty (30) days of our receipt of
                            your first email, you or Squarespace may then bring a formal proceeding.</p>
                        <p style="margin-left: 40px; ;"><strong>16.3. Arbitration Agreement. Unless
                                you opt-out during the Opt-Out Period in accordance with Section 16.4, you and
                                Squarespace agree to resolve any claims, disputes and matters arising out of or in
                                connection with this Agreement (including without limitation its existence, formation,
                                operation and termination) and/or the Services (including without limitation
                                non-contractual disputes and matters) through final and binding arbitration and you and
                                Squarespace expressly waive the right to formal court proceedings (including without
                                limitation trial by jury), except as set forth below. Discovery and rights to appeal in
                                arbitration are generally more limited than in a lawsuit, and other rights that you and
                                we would have in court may not be available in arbitration. There is no judge or jury in
                                arbitration, only an independent third party that will act as the arbitrator, and court
                                review of an arbitration award is limited.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>16.4. Arbitration Opt-Out. You can
                                decline this agreement to arbitrate by emailing us at </strong><a
                                href="mailto:arbitration-opt-out@squarespace.com"><strong>arbitration-opt-out@squarespace.com</strong></a><strong>
                                within thirty (30) days of the date that you first agree to this Agreement (“Opt-Out
                                Period”). Your email must be sent from the email address you use for your Account, and
                                must include your full name, residential address and a clear statement that you want to
                                opt out of arbitration. If you opt out of arbitration pursuant to this Section 16.4,
                                then Sections 16.3, 16.5, 16.6 and 16.7 of these Terms do not apply to you. This opt-out
                                doesn’t affect any other sections of the Terms, including without limitation Sections
                                16.9 (Time for Filing), 16.10 (No Class Actions) and 17.2 (Controlling Law; Judicial
                                Forum for Disputes). If you have any questions about this process, please
                                contact </strong><a
                                href="mailto:legal@squarespace.com"><strong>legal@squarespace.com</strong></a><strong>.
                                &nbsp;</strong>&nbsp;</p>
                        <p style="margin-left: 40px;"><strong>16.5. Arbitration Time For
                                Filing. </strong>Any arbitration must be commenced by filing a demand for arbitration
                            within one (1) year after the date the party asserting the claim first knows or reasonably
                            should know of the act, omission or default giving rise to the claim. If applicable law
                            prohibits a one (1) year limitation period for asserting claims, any claim must be asserted
                            within the shortest time period permitted by applicable law.</p>
                        <p style="margin-left: 40px;"><strong>16.6. Arbitration
                                Procedures. </strong>JAMS, Inc. (“JAMS”) will administer the arbitration in accordance
                            with the <a href="https://www.jamsadr.com/rules-streamlined-arbitration/">JAMS Streamlined
                                Arbitration Rules &amp; Procedures</a> (“JAMS Rules”) in effect at the time of the
                            dispute.</p>
                        <p style="margin-left: 80px;"><strong>16.6.1. US Users.</strong> If you
                            are a US User, you and Squarespace agree that this Agreement affects interstate commerce, so
                            the US Federal Arbitration Act and federal arbitration law apply and govern the
                            interpretation and enforcement of these provisions (despite Section 17.2 below). Any
                            arbitration hearings will take place at a location to be agreed upon in New York, New York,
                            in English, and shall be settled by one (1) commercial arbitrator with substantial
                            experience in resolving intellectual property and commercial contract disputes, who shall be
                            selected from the appropriate list of JAMS arbitrators in accordance with the JAMS Rules.
                            The arbitrator must follow this Agreement and can award the same damages and relief as a
                            court (including without limitation reasonable attorneys' fees and costs), except that the
                            arbitrator may not award declaratory or injunctive relief benefiting anyone but the parties
                            to the arbitration. Judgment upon the award rendered by such arbitrator may be entered in
                            any court of competent jurisdiction.</p>
                        <p style="margin-left: 80px; ;"><strong>16.6.2. Non-US Users</strong>. If
                            you are a Non-US User, you and Squarespace agree that any arbitration hearings will take
                            place at a location to be agreed upon in Dublin, Ireland, in English, and shall be settled
                            by one (1) commercial arbitrator with substantial experience in resolving intellectual
                            property and commercial contract disputes, who shall be selected in accordance with the JAMS
                            Rules. The arbitrator must follow this Agreement and can award the same damages and relief
                            as a court (including without limitation reasonable attorneys' fees and costs), except that
                            the arbitrator may not award declaratory or injunctive relief benefiting anyone but the
                            parties to the arbitration. Judgment upon the award rendered by such arbitrator may be
                            entered in any court of competent jurisdiction.</p>
                        <p style="margin-left: 80px; ;"><strong>16.6.3. EU Consumers.</strong> If
                            you are an EU Consumer who brings a claim against Squarespace in the US, you and Squarespace
                            agree that any arbitration hearings will take place at a location to be agreed upon in New
                            York, New York, in English, and shall be settled by one (1) commercial arbitrator with
                            substantial experience in resolving intellectual property and commercial contract disputes,
                            who shall be selected from the appropriate list of JAMS arbitrators in accordance with the
                            JAMS Rules. The arbitrator must follow this Agreement and can award the same damages and
                            relief as a court (including without limitation reasonable attorneys' fees and costs),
                            except that the arbitrator may not award declaratory or injunctive relief benefiting anyone
                            but the parties to the arbitration. Judgment upon the award rendered by such arbitrator may
                            be entered in any court of competent jurisdiction.</p>
                        <p style="margin-left: 40px;"><strong>16.7. Arbitration Fees.</strong>
                            The JAMS Rules will govern payment of all arbitration fees. We won’t seek our attorneys’
                            fees and costs in arbitration unless the arbitrator determines that your claim is frivolous.
                        </p>
                        <p style="margin-left: 40px;"><strong>16.8. Exceptions To Arbitration
                                Agreement.</strong> Either you or Squarespace may bring a lawsuit solely for injunctive
                            relief to stop unauthorized use or abuse of the Services, or intellectual property
                            infringement or misappropriation (for example, trademark, trade secret, copyright or patent
                            rights) without first engaging in arbitration or the informal dispute resolution process
                            described above.</p>
                        <p style="margin-left: 80px;"><strong>16.8.1. US Users. If you are a US
                                User, either you or Squarespace may assert claims, if they qualify, in small claims
                                court in New York, New York or any US county where you live or work.</strong></p>
                        <p style="margin-left: 80px;"><strong>16.8.2. Non-US Users. If you are a
                                Non-US User, either you or Squarespace may assert claims, if they qualify, in small
                                claims court in Dublin, Ireland or any county in the Ireland where you live or
                                work.</strong></p>
                        <p style="margin-left: 80px;"><strong>16.8.3. EU Consumers. If you are an
                                EU Consumer who brings a claim against Squarespace in the US, such claims must be
                                asserted, if they qualify, in small claims court in New York, New York.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>16.9. Time For Filing.</strong> Any
                            claim not subject to arbitration must be commenced within one (1) year after the date the
                            party asserting the claim first knows or reasonably should know of the act, omission or
                            default giving rise to the claim. If applicable law prohibits a one (1) year limitation
                            period for asserting claims, any claim must be asserted within the shortest time period
                            permitted by applicable law.</p>
                        <p style="margin-left: 40px; ;"><strong>16.10. NO CLASS ACTIONS. You may
                                only resolve disputes with us on an individual basis, and may not bring a claim as a
                                plaintiff or a class member in a class, consolidated or representative action. Class
                                actions, class arbitrations, private attorney general actions and consolidation with
                                other arbitrations aren't allowed.</strong></p>
                        <p style=";"><strong>17.&nbsp;&nbsp; &nbsp;Additional Terms</strong></p>
                        <blockquote><p style=";">This section includes some additional important
                                terms. For instance, this Agreement is the whole agreement between us regarding your use
                                of Squarespace. Depending on where you reside or have your place of business, this
                                Agreement is governed by either US or Irish law. If we ever change it in a way that
                                meaningfully reduces your rights, we’ll give you notice and an opportunity to cancel.
                                Also, if you’re reading this in a language other than English, note that the English
                                language version controls.</p></blockquote>
                        <p style="margin-left: 40px; ;"><strong>17.1. Entire Agreement.</strong>
                            This Agreement constitutes the entire agreement between you and Squarespace regarding the
                            subject matter of this Agreement, and supersedes and replaces any other prior or
                            contemporaneous agreements, or terms and conditions applicable to the subject matter of this
                            Agreement. You agree that you have not relied upon, and have no remedies in respect of, any
                            term, condition, statement, warranty or representation except those expressly set out in
                            this Agreement. You also may be subject to additional terms, policies or agreements that may
                            apply when you use other services, including Third Party Services. This Agreement creates no
                            third party beneficiary rights.</p>
                        <p style="margin-left: 40px; ;"><strong>17.2. Controlling Law; Judicial
                                Forum For Disputes.</strong></p>
                        <p style="margin-left: 80px;"><strong>17.2.1. US Users.</strong> If you
                            are a US User, this Agreement (including its existence, formation, operation and
                            termination) and the Services as well as all disputes and matters arising out of or in
                            connection with this Agreement and the Services (including non-contractual disputes and
                            matters) shall be governed in all respects by the laws of the State of New York, without
                            regard to its conflict of law provisions. If Section 16 is found not to apply to you or your
                            claim, or if you opt out of arbitration pursuant to Section 16.4, you and Squarespace agree
                            that any judicial proceeding (other than small claims actions) arising out of or in
                            connection with this Agreement (including its existence, formation, operation and
                            termination) and/or the Services (including non-contractual disputes and matters) must be
                            brought exclusively in the federal or state courts of New York, New York and you and
                            Squarespace consent to venue and personal jurisdiction in such courts.</p>
                        <p style="margin-left: 80px; ;"><strong>17.2.2. Non-US Users</strong>. If
                            you are a Non-US User, this Agreement (including its existence, formation, operation and
                            termination) and the Services as well as all disputes and matters arising out of or in
                            connection with this Agreement and the Services (including non-contractual disputes and
                            matters) shall be governed in all respects by the laws of Ireland, without regard to its
                            conflict of law provisions. If you are an EU Consumer, this Section does not limit or affect
                            any rights you may have under any mandatory laws of the country where you habitually live.
                            If Section 16 is found not to apply to you or your claim, or if you opt out of arbitration
                            pursuant to Section 16.4, you and Squarespace agree that, except where Section 17.2.3
                            applies, any judicial proceeding (other than small claims actions) arising out of or in
                            connection with this Agreement (including its existence, formation, operation and
                            termination) and/or the Services (including non-contractual disputes and matters) must be
                            brought exclusively in the courts of Ireland and you and Squarespace consent to venue and
                            personal jurisdiction in such courts.</p>
                        <p style="margin-left: 80px; ;"><strong>17.2.3. EU Consumers.</strong> If
                            you are an EU Consumer, as long as Section 16 does not apply to you or your claim, you and
                            Squarespace agree that any judicial proceeding arising out of or in connection with this
                            Agreement (including its existence, formation, operation and termination) and/or the
                            Services (including non-contractual disputes and matters) may only be brought in a court
                            located in Ireland or a court with jurisdiction in your place of habitual residence. If you
                            are an EU Consumer and Squarespace wishes to enforce any of its rights against you as a
                            consumer, we may do so only in the courts of the jurisdiction in which you habitually
                            reside.</p>
                        <p style="margin-left: 40px; ;"><strong>17.3. EU Online Dispute
                                Resolution.</strong> If you are an EU Consumer, you can access the European Commission’s
                            online dispute resolution platform <a href="http://ec.europa.eu/consumers/odr">here</a>.
                            Please note that Squarespace Ireland is not committed nor obliged to use an alternative
                            dispute resolution entity to resolve disputes with you.</p>
                        <p style="margin-left: 40px;"><strong>17.4. Waiver, Severability And
                                Assignment.</strong> Our failure to enforce any provision of this Agreement is not a
                            waiver of our right to do so later. If any provision of this Agreement is found
                            unenforceable, the remaining provisions will remain in full effect and an enforceable term
                            will be substituted reflecting our intent as closely as possible. You may not delegate,
                            transfer or assign this Agreement or any of your rights or obligations hereunder without our
                            prior written consent, and any such attempt will be of no effect. We may delegate, transfer
                            or assign this Agreement or some or all of our rights and obligations hereunder, in our sole
                            discretion, to any of our affiliates or subsidiaries or to any purchaser of any of our
                            business or assets associated with the Services, with thirty (30) days prior written notice.
                            If you are an EU Consumer, we will ensure that the delegation, transfer or assignment does
                            not adversely affect your rights under this Agreement.</p>
                        <p style="margin-left: 40px; ;"><strong>17.5. Modifications. We may modify
                                this Agreement from time to time, and will always post the most current version on our
                                site. If a modification meaningfully reduces your rights, we’ll notify you (by, for
                                example, sending you an email or displaying a prominent notice within the Services). The
                                notice will designate a reasonable period after which the new terms will take effect.
                                Modifications will never apply retroactively. By continuing to use or access the
                                Services after any modifications come into effect, you agree to be bound by the modified
                                Agreement and price changes. If you disagree with our changes, then you should stop
                                using the Services and cancel all Paid Services.</strong></p>
                        <p style="margin-left: 40px; ;"><strong>17.6. Events Beyond Our
                                Control.</strong> We are not in breach of this Agreement or liable to you if there is
                            any total or partial failure of performance of the Services resulting from any act,
                            circumstance, event or matter beyond our reasonable control. This may include where such
                            results from any act of God, fire, act of government or state or regulation, war, civil
                            commotion, terrorism, insurrection, inability to communicate with third parties for whatever
                            reason, failure of any computer dealing or necessary system, failure or delay in
                            transmission of communications, failure of any internet service provider, strike, industrial
                            action or lock-out or any other reason beyond our reasonable control.</p>
                        <p style="margin-left: 40px; ;"><strong>17.7. Translation.</strong> This
                            Agreement was originally written in English. We may translate this Agreement into other
                            languages. In the event of a conflict between a translated version and the English version,
                            the English version will control except where prohibited by applicable law.</p>
                        <p >&nbsp;</p></div>
                </div>
            </div>
        </div>
    </div>
</main>

<hr/>



<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none">
    <symbol id="twitter-icon" viewBox="0 0 64 64">
        <path
            d="M48,22.1c-1.2,0.5-2.4,0.9-3.8,1c1.4-0.8,2.4-2.1,2.9-3.6c-1.3,0.8-2.7,1.3-4.2,1.6 C41.7,19.8,40,19,38.2,19c-3.6,0-6.6,2.9-6.6,6.6c0,0.5,0.1,1,0.2,1.5c-5.5-0.3-10.3-2.9-13.5-6.9c-0.6,1-0.9,2.1-0.9,3.3 c0,2.3,1.2,4.3,2.9,5.5c-1.1,0-2.1-0.3-3-0.8c0,0,0,0.1,0,0.1c0,3.2,2.3,5.8,5.3,6.4c-0.6,0.1-1.1,0.2-1.7,0.2c-0.4,0-0.8,0-1.2-0.1 c0.8,2.6,3.3,4.5,6.1,4.6c-2.2,1.8-5.1,2.8-8.2,2.8c-0.5,0-1.1,0-1.6-0.1c2.9,1.9,6.4,2.9,10.1,2.9c12.1,0,18.7-10,18.7-18.7 c0-0.3,0-0.6,0-0.8C46,24.5,47.1,23.4,48,22.1z"/>
    </symbol>
    <symbol id="twitter-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M44.7,25.5c0,0.3,0,0.6,0,0.8C44.7,35,38.1,45,26.1,45c-3.7,0-7.2-1.1-10.1-2.9 c0.5,0.1,1,0.1,1.6,0.1c3.1,0,5.9-1,8.2-2.8c-2.9-0.1-5.3-2-6.1-4.6c0.4,0.1,0.8,0.1,1.2,0.1c0.6,0,1.2-0.1,1.7-0.2 c-3-0.6-5.3-3.3-5.3-6.4c0,0,0-0.1,0-0.1c0.9,0.5,1.9,0.8,3,0.8c-1.8-1.2-2.9-3.2-2.9-5.5c0-1.2,0.3-2.3,0.9-3.3 c3.2,4,8.1,6.6,13.5,6.9c-0.1-0.5-0.2-1-0.2-1.5c0-3.6,2.9-6.6,6.6-6.6c1.9,0,3.6,0.8,4.8,2.1c1.5-0.3,2.9-0.8,4.2-1.6 c-0.5,1.5-1.5,2.8-2.9,3.6c1.3-0.2,2.6-0.5,3.8-1C47.1,23.4,46,24.5,44.7,25.5z"/>
    </symbol>
    <symbol id="facebook-icon" viewBox="0 0 64 64">
        <path
            d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z"/>
    </symbol>
    <symbol id="facebook-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z"/>
    </symbol>
    <symbol id="instagram-icon" viewBox="0 0 64 64">
        <path
            d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z"/>
    </symbol>
    <symbol id="instagram-mask" viewBox="0 0 64 64">
        <path
            d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z"/>
    </symbol>
    <symbol id="pinterest-icon" viewBox="0 0 64 64">
        <path
            d="M32,16c-8.8,0-16,7.2-16,16c0,6.6,3.9,12.2,9.6,14.7c0-1.1,0-2.5,0.3-3.7 c0.3-1.3,2.1-8.7,2.1-8.7s-0.5-1-0.5-2.5c0-2.4,1.4-4.1,3.1-4.1c1.5,0,2.2,1.1,2.2,2.4c0,1.5-0.9,3.7-1.4,5.7 c-0.4,1.7,0.9,3.1,2.5,3.1c3,0,5.1-3.9,5.1-8.5c0-3.5-2.4-6.1-6.7-6.1c-4.9,0-7.9,3.6-7.9,7.7c0,1.4,0.4,2.4,1.1,3.1 c0.3,0.3,0.3,0.5,0.2,0.9c-0.1,0.3-0.3,1-0.3,1.3c-0.1,0.4-0.4,0.6-0.8,0.4c-2.2-0.9-3.3-3.4-3.3-6.1c0-4.5,3.8-10,11.4-10 c6.1,0,10.1,4.4,10.1,9.2c0,6.3-3.5,11-8.6,11c-1.7,0-3.4-0.9-3.9-2c0,0-0.9,3.7-1.1,4.4c-0.3,1.2-1,2.5-1.6,3.4 c1.4,0.4,3,0.7,4.5,0.7c8.8,0,16-7.2,16-16C48,23.2,40.8,16,32,16z"/>
    </symbol>
    <symbol id="pinterest-mask" viewBox="0 0 64 64">
        <path
            d="M0,0v64h64V0H0z M32,48c-1.6,0-3.1-0.2-4.5-0.7c0.6-1,1.3-2.2,1.6-3.4c0.2-0.7,1.1-4.4,1.1-4.4 c0.6,1.1,2.2,2,3.9,2c5.1,0,8.6-4.7,8.6-11c0-4.7-4-9.2-10.1-9.2c-7.6,0-11.4,5.5-11.4,10c0,2.8,1,5.2,3.3,6.1 c0.4,0.1,0.7,0,0.8-0.4c0.1-0.3,0.2-1,0.3-1.3c0.1-0.4,0.1-0.5-0.2-0.9c-0.6-0.8-1.1-1.7-1.1-3.1c0-4,3-7.7,7.9-7.7 c4.3,0,6.7,2.6,6.7,6.1c0,4.6-2,8.5-5.1,8.5c-1.7,0-2.9-1.4-2.5-3.1c0.5-2,1.4-4.2,1.4-5.7c0-1.3-0.7-2.4-2.2-2.4 c-1.7,0-3.1,1.8-3.1,4.1c0,1.5,0.5,2.5,0.5,2.5s-1.8,7.4-2.1,8.7c-0.3,1.2-0.3,2.6-0.3,3.7C19.9,44.2,16,38.6,16,32 c0-8.8,7.2-16,16-16c8.8,0,16,7.2,16,16C48,40.8,40.8,48,32,48z"/>
    </symbol>
</svg>
<noscript id="squarespace-context" data-websiteid="5134cbefe4b0c6fb04df8065"></noscript>

<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-BCCP"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>


<div class="www-domains domains-overlay">
    <div class="domains-overlay-content">

        <div class="loading-screen">
            <div class="loader">
                <svg viewBox="0 0 32 32">
                    <defs>
                        <style>.cls-1, .cls-2 {
                                fill: #fff;
                                isolation: isolate
                            }

                            .cls-1 {
                                opacity: .13
                            }

                            .cls-2 {
                                opacity: .89
                            }</style>
                    </defs>
                    <path class="cls-1"
                          d="M16 0a16 16 0 1 0 16 16A16 16 0 0 0 16 0m0 4A12 12 0 1 1 4 16 12 12 0 0 1 16 4"/>
                    <path class="cls-2" d="M16 0a16 16 0 0 1 16 16h-4A12 12 0 0 0 16 4V0z"/>
                </svg>
            </div>
        </div>

        <div class="scroll-up">
      <span class="scroll-up-button">
        <span>Back to your results</span>
        <span class="caret"><svg x="0px" y="0px" viewBox="0 0 10 6" width="10px" height="6px">
  <polyline points="9.2,5.1 5,0.8 0.8,5.1 "/>
</svg>
</span>
      </span>
            <div class="exit">
                <div class="www-x light">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                        <line x1="1.9" y1="1.9" x2="23.1" y2="23.1"/>
                        <line x1="23.1" y1="1.9" x2="1.9" y2="23.1"/>
                    </svg>

                </div>
            </div>
        </div>

        <div id="domain-name-search">
            <div class="scrollable-content">


                <header id="domains-header" class="has-dark-background">
  <span class="header-logo">
    <a class="is-borderless" href="/">
      <svg class="squarespace-mark is-fill notranslate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 24"
           width="30px" height="24px">
  <title>Squarespace</title>
  <path
      d="M24.93671,11.30019c-0.38681-0.38681-1.01281-0.38681-1.39961,0l-9.23691,9.23691c-1.31214,1.31214-3.44709,1.31214-4.75855,0.00068c-0.38681-0.38681-1.01281-0.38681-1.39961,0c-0.38681,0.38681-0.38681,1.01281,0,1.39961c2.0837,2.0837,5.47407,2.08302,7.55778-0.00068l9.2369-9.2369C25.32352,12.313,25.32352,11.687,24.93671,11.30019zM28.01613,8.22077c-2.0837-2.0837-5.47408-2.08302-7.55778,0.00068l-9.2369,9.2369c-0.38681,0.38681-0.38681,1.01281,0,1.39961c0.38681,0.38681,1.01281,0.38681,1.39961,0l9.2369-9.2369c1.31214-1.31214,3.4471-1.31214,4.75855-0.00068c1.31214,1.31214,1.31214,3.4471,0,4.75923l-7.43066,7.43066c-0.38681,0.38681-0.38681,1.01281,0,1.39961c0.38681,0.38681,1.01281,0.38681,1.39961,0l7.43066-7.43066C30.09984,13.69553,30.09984,10.30447,28.01613,8.22077zM21.85797,2.06261c-2.0837-2.0837-5.47408-2.08302-7.55778,0.00068l-9.2369,9.2369c-0.38681,0.38681-0.38681,1.01281,0,1.39961s1.01281,0.38681,1.39961,0l9.2369-9.2369c1.31214-1.31214,3.44709-1.31214,4.75855-0.00068c0.38681,0.38681,1.01281,0.38681,1.39961,0C22.24478,3.07541,22.24478,2.44941,21.85797,2.06261zM18.77855,5.14203c-0.38681-0.38681-1.01281-0.38681-1.39961,0l-9.2369,9.2369c-1.31214,1.31214-3.4471,1.31214-4.75855,0.00068c-1.31214-1.31214-1.31214-3.4471,0-4.75923l7.43066-7.43066c0.38681-0.38681,0.38681-1.01281,0-1.39961s-1.01281-0.38681-1.39961,0L1.98387,8.22077c-2.0837,2.0837-2.0837,5.47476,0,7.55846s5.47408,2.08302,7.55778-0.00068l9.2369-9.2369C19.16535,6.15484,19.16535,5.52884,18.77855,5.14203z"/>
</svg>

      <svg class="squarespace-full is-fill notranslate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168 24"
           width="168px" height="24px">
  <title>Squarespace</title>
  <path
      d="M79.5626,5.53063l-5.24026,11.58048h1.33081l1.36326-3.03909h6.25848l1.34703,3.03909h1.39632L80.77798,5.53063H79.5626zM77.52551,12.88971l2.62856-5.84795l2.61171,5.84795H77.52551zM41.8326,10.73783c-2.52935-0.54218-3.12082-1.14987-3.12082-2.23424V8.47052c0-1.03445,0.95272-1.85615,2.44762-1.85615c1.18232,0,2.25046,0.37809,3.31798,1.24845l0.75556-1.00201c-1.1661-0.93587-2.36526-1.41254-4.0411-1.41254c-2.18433,0-3.77781,1.33081-3.77781,3.13767v0.03244c0,1.88921,1.21539,2.74335,3.86016,3.3186c2.41455,0.50911,2.9898,1.11681,2.9898,2.18433v0.03307c0,1.13365-1.01885,1.95472-2.56304,1.95472c-1.59285,0-2.74273-0.54218-3.94189-1.62654l-0.80485,0.95334c1.37947,1.23161,2.87437,1.8393,4.69745,1.8393c2.28353,0,3.9263-1.2809,3.9263-3.25247v-0.03244C45.57796,12.23273,44.39502,11.29623,41.8326,10.73783zM59.77015,11.36174V11.3293c0-3.17073-2.34904-5.91409-5.84795-5.91409s-5.88039,2.77642-5.88039,5.94653v0.03307c0,3.17011,2.34841,5.91346,5.84733,5.91346c1.41317,0,2.64477-0.45983,3.61434-1.21539l1.52734,1.36325l0.88721-0.96894l-1.51112-1.28152C59.26104,14.15438,59.77015,12.79113,59.77015,11.36174zM57.45418,14.40083L55.3023,12.4461l-0.87099,0.96894l2.15188,1.83992c-0.72249,0.54218-1.64277,0.85414-2.661,0.85414c-2.62793,0-4.53399-2.13566-4.53399-4.74737V11.3293c0-2.61233,1.87299-4.71492,4.50092-4.71492c2.62855,0,4.53399,2.13566,4.53399,4.74737v0.03307C58.42312,12.54468,58.07809,13.57913,57.45418,14.40083zM71.26765,12.29824c0,2.51313-1.34704,3.79465-3.46585,3.79465c-2.18494,0-3.51514-1.39632-3.51514-3.87701V5.61237h-1.29774v6.68587c0,3.25247,1.93788,4.99381,4.77981,4.99381c2.875,0,4.79665-1.74135,4.79665-5.09239V5.61237h-1.29774V12.29824zM150.96526,16.10911c-2.52997,0-4.45163-2.08637-4.45163-4.74737V11.3293c0-2.64477,1.90544-4.71492,4.45163-4.71492c1.56042,0,2.64478,0.65698,3.61372,1.57726l0.88721-0.95272c-1.14987-1.08436-2.39833-1.8237-4.4847-1.8237c-3.3835,0-5.8149,2.69406-5.8149,5.94653v0.03307c0,3.30176,2.44762,5.91346,5.76559,5.91346c2.08638,0,3.40034-0.80485,4.5995-2.02024l-0.85414-0.83792C153.57697,15.50141,152.5419,16.10911,150.96526,16.10911zM137.64406,5.53063l-5.24025,11.58048h1.3308l1.36327-3.03909h6.25848l1.34703,3.03909h1.39632l-5.24025-11.58048H137.64406zM135.60698,12.88971l2.62856-5.84795l2.61171,5.84795H135.60698zM159.83484,15.92817v-4.02425h6.2747v-1.18294h-6.2747V6.79531h7.01404V5.61237h-8.31178v11.49875h8.39352v-1.18294H159.83484zM102.27933,11.90392h6.27471v-1.18294h-6.27471V6.79531h7.01405V5.61237h-8.31179v11.49875h8.39352v-1.18294h-7.09579V11.90392zM97.54881,9.06199V9.02955c0-0.90343-0.3288-1.7089-0.88721-2.26731c-0.7225-0.72249-1.85614-1.14987-3.26868-1.14987h-4.94453v11.49875h1.29774v-4.46785h3.28554l3.3342,4.46785h1.59348l-3.53136-4.69808C96.23484,12.08486,97.54881,10.96743,97.54881,9.06199zM89.74613,11.47654V6.81153h3.5482c1.85615,0,2.94051,0.85414,2.94051,2.26731v0.03244c0,1.47868-1.23223,2.36526-2.95673,2.36526H89.74613zM116.6849,10.73783c-2.52998-0.54218-3.12144-1.14987-3.12144-2.23424V8.47052c0-1.03445,0.95271-1.85615,2.44762-1.85615c1.18294,0,2.25046,0.37809,3.31798,1.24845l0.75618-1.00201c-1.16672-0.93587-2.36588-1.41254-4.04109-1.41254c-2.18495,0-3.77843,1.33081-3.77843,3.13767v0.03244c0,1.88921,1.21601,2.74335,3.86016,3.3186c2.41517,0.50911,2.9898,1.11681,2.9898,2.18433v0.03307c0,1.13365-1.01823,1.95472-2.56242,1.95472c-1.59348,0-2.74335-0.54218-3.94251-1.62654l-0.80485,0.95334c1.3801,1.23161,2.87499,1.8393,4.69807,1.8393c2.28353,0,3.92567-1.2809,3.92567-3.25247v-0.03244C120.42963,12.23273,119.24731,11.29623,116.6849,10.73783zM127.9528,5.61237h-4.30376v11.49875h1.29774V13.0538h2.79202c2.44761,0,4.51777-1.28152,4.51777-3.76158V9.25915C132.25656,6.99247,130.54828,5.61237,127.9528,5.61237zM130.94197,9.3415c0,1.51112-1.24846,2.52935-3.15389,2.52935h-2.84131V6.81153h2.90745c1.85614,0,3.08775,0.85414,3.08775,2.49691V9.3415zM18.77845,6.54165c0.38683-0.38683,0.38683-1.01281,0-1.39964c-0.38679-0.38679-1.01281-0.38679-1.3996,0l-9.2369,9.2369c-1.31217,1.31217-3.44709,1.31217-4.75856,0.0007c-1.31213-1.31213-1.31213-3.44709,0-4.75922l7.43067-7.43067c0.38679-0.38683,0.38679-1.01281,0-1.39964c-0.38683-0.38679-1.01281-0.38679-1.39964,0L1.98376,8.22076c-2.08368,2.08372-2.08368,5.47477,0,7.55845c2.08372,2.08372,5.47411,2.08302,7.55779-0.00066L18.77845,6.54165zM5.06321,12.6998c0.38679,0.38679,1.01281,0.38679,1.3996,0l9.2369-9.2369c1.31213-1.31213,3.44709-1.31213,4.75856-0.0007c0.38679,0.38683,1.01281,0.38683,1.3996,0c0.38683-0.38679,0.38683-1.01277,0-1.3996c-2.08368-2.08372-5.47407-2.08302-7.55775,0.0007l-9.2369,9.2369C4.67638,11.68699,4.67638,12.31298,5.06321,12.6998zM24.93661,11.30021c-0.38679-0.38683-1.01281-0.38683-1.3996,0l-9.2369,9.2369c-1.31213,1.31213-3.44709,1.31213-4.75856,0.00066c-0.38679-0.38679-1.01281-0.38679-1.3996,0c-0.38683,0.38683-0.38683,1.01281,0,1.3996c2.08368,2.08372,5.47407,2.08302,7.55775-0.00066l9.2369-9.2369C25.32343,12.31298,25.32343,11.68699,24.93661,11.30021zM20.45826,8.22146l-9.2369,9.2369c-0.38683,0.38679-0.38683,1.01281,0,1.3996c0.38679,0.38683,1.01281,0.38683,1.3996,0l9.2369-9.2369c1.31217-1.31213,3.44709-1.31213,4.75856-0.00066c1.31213,1.31213,1.31213,3.44709,0,4.75922l-7.43067,7.43067c-0.38679,0.38679-0.38679,1.01281,0,1.3996c0.38683,0.38683,1.01281,0.38683,1.39964,0l7.43067-7.43067c2.08368-2.08368,2.08368-5.47473,0-7.55845C25.93233,6.13708,22.54194,6.13774,20.45826,8.22146z"></path>
</svg>

    </a>
  </span>

                    <div class="header-buttons">
                        <div class="www-x light">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                                <line x1="1.9" y1="1.9" x2="23.1" y2="23.1"/>
                                <line x1="23.1" y1="1.9" x2="1.9" y2="23.1"/>
                            </svg>

                        </div>
                    </div>
                </header>


                <div class="search-app-wrapper no-transitions">
                    <div class="search-app has-dark-background">
                    </div>
                </div>


                <aside class="domains-transfer-aside">
                    <div class="transfer-icon">
                        <svg x="0px" y="0px" width="30px" height="24px" viewBox="0 0 30 24">
                            <polyline points="0,8.8 27.7,8.8 22.1,1 "/>
                            <polyline points="29.9,15.2 2.2,15.2 7.8,23 "/>
                        </svg>

                    </div>
                    <div class="title">
                        <h2>Already have a domain?</h2>
                        <h3>Transfer your domain to Squarespace in just a few easy steps.</h3>
                    </div>
                </aside>


                <section id="domains-marketing-wrapper" class="has-dark-background"></section>
            </div>
        </div>


        <div class="scroll-down">
            <div class="scroll-down-copy">
                <h3>All-In-One Domains Pricing</h3>
                <h4>Including free SSL, free WHOIS privacy, and a spam-free parking page</h4>
            </div>
            <div class="scroll-down-actions www-layout">
        <span class="transfer-icon"><svg x="0px" y="0px" width="30px" height="24px" viewBox="0 0 30 24">
  <polyline points="0,8.8 27.7,8.8 22.1,1 "/>
  <polyline points="29.9,15.2 2.2,15.2 7.8,23 "/>
</svg>
</span>
                <span class="transfer-domain">Transfer your domain</span>
                <span class="scroll-down-button">Learn more <span class="extended-text">about Domains</span></span>
            </div>
        </div>
    </div>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>



</body>
<script src="assets/blueline/js/menu.js"></script>
</html>

