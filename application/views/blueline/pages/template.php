<!doctype html>
<html xmlns:og="http://opengraphprotocol.org/schema/" lang="en-US" itemscope itemtype="http://schema.org/WebPage"
      class="region-about">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?=  $data->description ?>">

    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <script>(function (d) {
            var config = {
                    kitId: 'gnf7xqu',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement, t = setTimeout(function () {
                    h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
                }, config.scriptTimeout), tk = d.createElement("script"), f = false,
                s = d.getElementsByTagName("script")[0], a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function () {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {
                }
            };
            s.parentNode.insertBefore(tk, s)
        })(document);</script>
    <style type="text/css">
        /* Forces background color to be black on /domain-name-search */
        .collection-tour-domains.has--iframe-open [class*=Frame-overlay-] {
            background: black;
        }

        /* Forces background color to be black on /domain-name-search */
        .collection-tour-domains.has--iframe-open [class*=Signup-overlay-] {
            background: black;
        }
    </style>
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/blueline/css/poges/<?= $data->body;  ?>">
    <link rel="stylesheet" href="assets/blueline/css/poges/header.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/footer.css">
    <title><?=  $data->title ?></title>
</head>
<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
?>
<?= $data->body;  ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>

</body>
</html>


