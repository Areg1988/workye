<body class="bg-cover compact">
<main class="site_wrapper">
    <div class="clearfix"></div>

    <?php
    if($this->user or $this->client) {

    }else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
    }
    ?>

    <div class="clear-fix"></div>

    <section class="the-way-you-work-section">
        <div class="container">
            <div class="row">
                <h3 class="heading-main3">Cisco Webex Devices</h3>
                <h1 class="heading-main1">Designed for the <br> way you work.</h1>
                <p class="paragraf-main">
                    Webex Devices help your team communicate clearly and create together in real time. It's teamwork without interruption, just inspiration.
                </p>
                <a href="#" class="btn-mine bg-whiter">Contact Sales</a>
            </div>
        </div>
    </section>

    <section class="meet-devices-section">
        <div class="container">
            <div class="row">
                <h2 class="heading-main2">Meet Webex Devices.</h2>
                <h3 class="heading-main3">
                    Get the most out of Cisco Webex Meetings and Cisco Webex Teams with tools designed for better teamwork.
                </h3>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <img src="/assets/blueline/pages/images/imageshome/devices_2.png" alt="devices_2.png">
                    </div>
                    <h3 class="heading-main3 bd">Cisco Webex Board</h3>
                    <p class="paragraf-main">All-in-one smart whiteboard and video screen.</p>
                </div>
                <div class="col-md-4">
                    <div>
                        <img src="/assets/blueline/pages/images/imageshome/devices_3.png" alt="devices_3.png">
                    </div>
                    <h3 class="heading-main3 bd">Cisco Webex Room Devices</h3>
                    <p class="paragraf-main">Complete video systems for rooms of all sizes.</p>
                </div>
                <div class="col-md-4">
                    <div>
                        <img src="/assets/blueline/pages/images/imageshome/devices_4.png" alt="devices_4.png">
                    </div>
                    <h3 class="heading-main3 bd">Cisco Webex Desk Devices</h3>
                    <p class="paragraf-main">Compact video systems designed for desktops.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="wherever-you-are-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h2 class="heading-main2">Create as a team. Wherever <br> you are.</h2>
                        <p class="third-section-text">Sketch your idea on the Cisco Webex Board's touchscreen and let team members mark up your drawing from anywhere. The all-in-one device shares presentations and video calls in stunning 4K HD.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal5">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 text-center">
                    <img src="/assets/blueline/pages/images/imageshome/clock.jpg" alt="clock.jpg">
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal5" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="heading-main2">Create as a team. <br> Wherever you are.</h2>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Cut down on the clutter.</h3>
                                    <p class="paragraf-main">
                                        Simplify the meeting room. Webex Board replaces your video screens, whiteboards, phones, cameras, and microphones.
                                    </p>
                                </div>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">JSketch, edit, or co-edit in real-time.</h3>
                                    <p class="paragraf-main">
                                        Sketch directly on the digital whiteboard or use the pen. You can edit drawings shared by anyone in the space—even co-editing with remote participants.
                                    </p>
                                </div>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Stored right where it was created.</h3>
                                    <p class="paragraf-main">
                                        Everything you share and create is kept in the same space where you communicate with your team. That way, you have all of the information in one place.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <img src="/assets/blueline/pages/images/blue-modal5.png" alt="modal">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="everything-works-together-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="/assets/blueline/pages/images/imageshome/devices_6.jpg" alt="devices.jpg">
                </div>
                <div class="col-md-6">
                    <div>
                        <h2 class="heading-main2">Everything <br> works together.</h2>
                        <p class="third-section-text">Webex Devices work together with Webex Meetings and Webex Teams to give you the best possible meeting and teamwork experiences. Everything’s compatible. Everything just works.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal6">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal6" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal6.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Everything works together.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">You are the remote.</h3>
                                    <p class="paragraf-main">
                                        Control your video system from any device by using the Webex Teams app.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Auto-save is always on.</h3>
                                    <p class="paragraf-main">
                                        Everything you draw on the Webex Board is saved to your Webex Teams space automatically.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Already cloud ready.</h3>
                                    <p class="paragraf-main">
                                        All video systems connect to the cloud. Whether your technology is on-site or transitioning to the cloud, everything is compatible and connected.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="ready-to-start-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h2 class="heading-main2">Ready to start? <br>Your device is <br> already on. <br></h2>
                        <p class="third-section-text">Your room device or Webex Board is ready when you are—video systems wake up automatically when you enter the room. The Cisco Touch 10 can even dim the lights and close the curtains for you.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal7">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 text-center">
                    <img src="/assets/blueline/pages/images/imageshome/devices_7.jpg" alt="devices.jpg" class="mt80">
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal7" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal7.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Ready to start? Your device is already on.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Join by walking in.</h3>
                                    <p class="paragraf-main">
                                        Automatic wake-up technology means your video system turns on when you walk into the room. No more fumbling with remotes or buttons to get started.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Stays on the speaker.</h3>
                                    <p class="paragraf-main">
                                        Cameras automatically focus on the person speaking using a wide-angle lens and speaker-tracking technology.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Go cable-free.</h3>
                                    <p class="paragraf-main">
                                        Wireless sharing lets you present easily by sharing your screen in an instant. Searching for an HDMI cable is now a thing of the past.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="be-there-now-section">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <img src="/assets/blueline/pages/images/imageshome/devices_8.jpg" alt="devices.jpg">
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <div>
                        <h2 class="heading-main2">Be there now.</h2>
                        <p class="third-section-text">High-definition video and crystal-clear audio bring everyone in the meeting together in one room. Remote participants can share their ideas as easily and effectively as those at the office.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal8">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal8" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal8.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Be there now.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">High definition. No exceptions.</h3>
                                    <p class="paragraf-main">
                                        All room devices and desk devices feature HD video so everyone sees as clearly as if they were in the same room.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Tomorrow's technology inside.</h3>
                                    <p class="paragraf-main">
                                        Cisco devices use future-facing technology to deliver high-quality video and audio—like the Webex Board's 4K camera and 12-microphone array.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">No room is too big.</h3>
                                    <p class="paragraf-main">
                                        The Room 70 can support very large rooms by using multiple cameras and stereo sound.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="explore-webex-devices-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-main2">Explore Webex Devices</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_9.jpg" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex Board
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_10.png" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex Room 55
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_11.png" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex Room 70
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-6 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_13.png" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex Room Kit
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_14.png" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex Room Kit Plus
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 mt32">
                    <div class="row">
                        <a target="_blank" href="#">
                            <div class="col-md-12">
                                <img src="/assets/blueline/pages/images/imageshome/devices_12.png" alt="devices">
                            </div>
                            <div class="col-md-12 title-mine">
                                Cisco Webex DX 80
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="get-in-touch-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="heading-main1">Get in touch with us.</h1>
                    <a href="#" class="btn-mine bg-whiter">Contact sales</a>
                </div>
            </div>
        </div>
    </section>


    <section class="pininfarina-uses-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h2 class="heading-main2">Pininfarina uses <br> Webex to stay <br> ahead of the <br> curve.</h2>
                        <p class="paragraf-main">
                            Italian company <a href="#">Pininfarina</a> designs spaces and transportation systems as well as cars, using Webex Boards to share sketches and keep an eye on the big picture.
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 text-center">
                    <img src="/assets/blueline/pages/images/imageshome/devices_15.png" alt="devices">
                </div>
            </div>
        </div>
    </section>

    <section class="explore-more-section">
        <div class="container">
            <div class="row">
                <h2 class="heading-main2">Explore more of Webex.</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex Meetings</h4>
                            <p>Video meetings made simple</p>
                            <img src="/assets/blueline/pages/images/imageshome/devices_16.png" alt="devices">
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex Teams</h4>
                            <p>Continuous teamwork in one place</p>
                            <img src="/assets/blueline/pages/images/imageshome/devices_17.png" alt="devices">
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex calling</h4>
                            <p>Calling without complexity</p>
                            <img src="/assets/blueline/pages/images/imageshome/teams_22.png" alt="teams22">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php
   include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>
</body>
