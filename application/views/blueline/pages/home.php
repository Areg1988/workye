<!DOCTYPE html>
<html id="main-html">
<link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
<script>(function (d) {
        var config = {
                kitId: 'gnf7xqu',
                scriptTimeout: 3000,
                async: true
            },
            h = d.documentElement, t = setTimeout(function () {
                h.className = h.className.replace(/wf-loading/g, "") + " wf-inactive";
            }, config.scriptTimeout), tk = d.createElement("script"), f = false,
            s = d.getElementsByTagName("script")[0], a;
        h.className += " wf-loading";
        tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
        tk.async = true;
        tk.onload = tk.onreadystatechange = function () {
            a = this.readyState;
            if (f || a && a != "complete" && a != "loaded") return;
            f = true;
            clearTimeout(t);
            try {
                Typekit.load(config)
            } catch (e) {
            }
        };
        s.parentNode.insertBefore(tk, s)
    })(document);</script>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="SHORTCUT ICON" href="<?= base_url() ?>assets/blueline/img/favicon.ico"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="https://use.typekit.net/usz0ztd.css" as="font"/>
    <title>workye.com</title>
    <meta name="description" content="
We'll Find You The Perfect employees For Your Projects! It's Easy & Affordable. Secure Payments. Increased Productivity. 90% of Customers Rehire. Trusted by 4M+ Businesses. Services: Designers, Web, Mobile, Marketers, Writers, Developers, Coders.">
    <script type="text/javascript" src="assets/blueline/js/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/blueline/css/poges/getHired.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/footer.css">
    <link rel="stylesheet" href="assets/blueline/css/poges/header.css">
</head>

<body>
<div class="jsx-3980414583 homepage-logo" style='width: 540px;margin: 200px auto;'>
    <img src='/assets/blueline/images/workye.com.svg' width='50%'>
    <h1 style='    font-size: 58px;'>Under Construction
    </h1>
</div>

</body>
</html>