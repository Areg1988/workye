
<body class="bg-cover compact">

<main class="site_wrapper">
    <div class="clearfix"></div>

    <?php
    if($this->user or $this->client) {

    }else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
    }
    ?>
    <div class="clearfix"></div>

    <section class="hiw-header-section">
        <div class="container">
            <div class="row">
                <h3 class="heading-main3">Cisco Webex Meetings</h3>
                <h1 class="heading-main1">Simply the Best <br> Video Conferencing <br> & Online Meetings.</h1>
                <p class="paragraf-main">
                    Webex Devices help your team communicate clearly and create together in real time. It's teamwork without interruption, just inspiration.
                </p>
                <p>
                    <a href="#" class="btn-mine bg-whiter">Try Meetings free</a>
                    <a href="#" class="btn-mine mine2">See pricing</a>
                </p>
            </div>
        </div>
    </section>

    <section class="everything-works-together-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="/assets/blueline/pages/images/imageshome/meetings_2.jpg" alt="meetings.jpg">
                </div>
                <div class="col-md-6">
                    <div>
                        <h2 class="heading-main2">Meet face to <br> face. Wherever <br> you are.</h2>
                        <p class="third-section-text">                        <p class="third-section-text">Webex Devices work together with Webex Meetings and Webex Teams to give you the best possible meeting and teamwork experiences. Everything’s compatible. Everything just works.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal1">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="heading-main2">Meet face to face. <br> Wherever you are.</h2>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Unmatched video quality.</h3>
                                    <p class="paragraf-main">
                                        HD video with customizable layouts make it easier to actively participate in the meeting—even when you’re remote.
                                    </p>
                                </div>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Join from any device.</h3>
                                    <p class="paragraf-main">
                                        Host or join a meeting from any device: mobile, tablet, laptop, or video device with one consistent experience.
                                    </p>
                                </div>
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Flexible device compatibility.</h3>
                                    <p class="paragraf-main">
                                        Easily join from a video device and connect using any standards-based video conferencing system—Cisco or otherwise.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <img src="/assets/blueline/pages/images/blue-modal1.png" alt="modal">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="ready-to-start-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h2 class="heading-main2">Get on the <br> same page.</h2>
                        <p class="third-section-text">Seeing is believing. Share your screen so everyone can view your document, spreadsheet, or application. You can even have the host record the meeting so others can get up to speed later.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal2">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 text-center">
                    <img src="/assets/blueline/pages/images/imageshome/meetings_3.png" alt="meetings.png">
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal2.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Get on the same page.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Present anything.</h3>
                                    <p class="paragraf-main">
                                        Share your whole screen or just one document or application. Even natively screen share from your mobile device.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Record a meeting.</h3>
                                    <p class="paragraf-main">
                                        It’s easy to record a meeting to share with folks who couldn’t join. Once the meeting is over, you’ll receive an email with an mp4 file of the meeting.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Broadcast a meeting.</h3>
                                    <p class="paragraf-main">
                                        With the Facebook Live integration, you can easily share a meeting with your fans and followers.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
            </div>
        </div>
    <!-- modal ends here / -->

    <section class="be-there-now-section">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <img src="/assets/blueline/pages/images/imageshome/meetings_4.jpg" alt="meetings.jpg">
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <div>
                        <h2 class="heading-main2">Invite anyone <br> or everyone.</h2>
                        <p class="third-section-text">Schedule and join meetings from where you work. Everyone you invite can join your online meeting, no matter how they’re connecting, even guests. Big crowd? Not a problem. You can meet with over 40,000 people.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal3">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal3" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal3.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Invite anyone or everyone.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Works with your existing calendar.</h3>
                                    <p class="paragraf-main">
                                        With calendar integrations, you can schedule in a snap. Meetings are automatically added to your calendar. Supports Outlook, Office 365, Google and more.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Integrates well with other tools.</h3>
                                    <p class="paragraf-main">
                                        Launch meetings from Salesforce, Microsoft Teams, Slack, and popular learning management systems like Canvas and Moodle.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Wow your audience.</h3>
                                    <p class="paragraf-main">
                                        Get everyone into the meeting. Whether you need meetings for 2 people or 40,000, Webex has solutions for you.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="ready-to-start-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h2 class="heading-main2">Click or tap <br> to join.</h2>
                        <p class="third-section-text">Easily start or join using Productivity Tools. Now you can join a video meeting with no dialing in. You can also host or join without a download by using the Webex app.
                            <br>
                            <a type="button" class="learn-more with-plus" href="#" data-toggle="modal" data-target="#modal4">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 text-center">
                    <img src="/assets/blueline/pages/images/imageshome/meetings_5.png" alt="meetings.png">
                </div>
            </div>
        </div>
    </section>
    <!-- modal for the section ^ -->
    <div class="modal" id="modal4" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close-blue-button" data-dismiss="modal"></button>
                </div>
                <div class="bg">
                    <div class="circle"></div>
                </div>
                <div class="modal-body read-more-modal-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="/assets/blueline/pages/images/blue-modal4.png" alt="modal" class="max-width846">
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="heading-main2 text-center">Click or tap to join.</h2>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Everything you need in one place.</h3>
                                    <p class="paragraf-main">
                                        Webex Productivity Tools lets you quickly schedule, start and join meetings right from your toolbar.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Answer to join.</h3>
                                    <p class="paragraf-main">
                                        Have the meeting call you using the Call Me option. Join just by answering the phone.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mt40">
                                    <h3 class="heading-main3 fwb">Skip the download.</h3>
                                    <p class="paragraf-main">
                                        Joining a meeting is a breeze using a browser. Hop on in seconds—with nothing to download. Click or tap a button and you’re in.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <!-- modal ends here / -->

    <section class="host-events-section">
        <div class="container mx-732">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-main2 text-center">Host events. Train your team. <br> Support your customers.</h2>
                </div>
            </div>
        </div>
        <div class="container mx-732">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="h4-main">Webinars</h4>
                    <p class="paragraf-main">
                        With Cisco Webex Events, host interactive webinars with HD video and broadcast-quality audio for audiences up to 3,000.
                    </p>
                    <a href="#" class="learn-more">Learn more</a>
                </div>
                <div class="col-md-6">
                    <h4 class="h4-main">Webcasting</h4>
                    <p class="paragraf-main">
                        Host large-scale virtual events for more than 40,000 participants with expertise from Cisco Webex Webcasting to ensure it’s a success.
                    </p>
                    <a href="#" class="learn-more">Learn more</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h4 class="h4-main">Online learning</h4>
                    <p class="paragraf-main">
                        Lead live or on-demand trainings with Cisco Webex Training. Host breakout sessions, testing and automated grading with over 1,000 participants.
                    </p>
                    <a href="#" class="learn-more">Learn more</a>
                </div>
                <div class="col-md-6">
                    <h4 class="h4-main">Remote support</h4>
                    <p class="paragraf-main">
                        Cisco Webex Support provides efficient customer service with remote desktop control and live chat.
                    </p>
                    <a href="#" class="learn-more">Learn more</a>
                </div>
            </div>
        </div>
    </section>


    <section class="explore-more-section">
        <div class="container">
            <div class="row">
                <h2 class="heading-main2">Explore more of Webex.</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex Teams</h4>
                            <p>Continuous teamwork in one place</p>
                            <img src="/assets/blueline/pages/images/imageshome/meetings_7.png" alt="meetings">
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex Calling</h4>
                            <p>Calling without complexity</p>
                            <img src="/assets/blueline/pages/images/imageshome/teams_22.png" alt="teams22" style="max-width: 197px;">
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div>
                        <a href="#">
                            <h4>Webex Devices</h4>
                            <p>Video devices to connect and create</p>
                            <img src="/assets/blueline/pages/images/imageshome/meetings_8.png" alt="meetings_8" style="margin-top: 158px;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>







<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/footer.php');
?>
</body>
