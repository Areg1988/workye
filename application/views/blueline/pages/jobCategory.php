<link rel="stylesheet" href="assets/blueline/css/poges/jobCategory.css">
<body class="bg-cover compact">

<div class="site_wrapper">


    <div class="clearfix"></div>

    <?php
    if($this->user or $this->client) {

    }else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/application/views/blueline/includes/header.php');
    }

    ?>


    <div class="clearfix"></div>

    <!-- Slider
    ======================================= -->

    <!-- masterslider -->
         <div class='container'>
         <div id="major-category-links" class="Grid">

<?php foreach ($categories as $categorie) { ?>
<div class="Grid-col Grid-col--tablet-6 Grid-col--desktopSmall-4">
    <div class="PageJob-majorCategory">
        <span class="PageJob-majorCategory-illustration">
            <?= $categorie->icon ?>

        </span>

        <div class="PageJob-majorCategory-content">
            <button type="button" class="PageJob-majorCategory-button"
                    id="category-<?= $categorie->id ?>">
                <h2 class="PageJob-majorCategory-title">
                    <?= $categorie->name ?>
                    <?= $count->category_id  ?>
                    <?php
                             
                            foreach ($count as $key) {
                            if($key->category_id == $categorie->name) {
                                    echo "(".$key->sum.")";
                                }
                            //     if (intval($key['categoryId']) == intval($categorie->id)) {
                            //         $sum += intval($key['count']);
                            //     }

                            // }
                            // if($count[$sum]['category_id'] == $categorie->id) {
                            //     echo "(".$count[$sum]['sum'].")";
                            //     $sum++;
                            // }
                            }   
                            ?>
                           
                </h2>
                <div class="PageJob-majorCategory-description">
                    <?= $categorie->description ?>
                </div>
            </button>
        </div>
    </div>
</div>

        <section style="display: none" class="PageJob-category category-<?= $categorie->id ?>">
        <hr style='    border-color: #00d647;margin: 0 0 30px 0;'>
            <ul class="PageJob-browse-list Grid">

                <?php foreach ($subCategory as $value) {
                    if ($categorie->id == $value->category_id) {
                        ?>

                        <li class="Grid-col Grid-col--6 Grid-col--tablet-4 Grid-col--desktopSmall-3"
                            data-job-category-item=".NET jobs">
                            <a class="PageJob-category-link"
                               title="<?= $value->id ?>"
                              >
                                <?= $value->name; ?>
                            </a>

                        </li>

                    <?php } ?>
                <?php }
                ?>
            </ul>


        </section>


<?php } ?>
</div>
         </div>
    <!-- end of masterslider -->


    <div class="clearfix margin_top10"></div>


    <div class="featured_section1">
        <div class="container">

            <div class="one_third animate" data-anim-type="fadeInLeft">

                <h3>Awesome Beautiful Featured
                    Power Pack Themes</h3>

                <p class="aliright">Lorem Ipsum as their default model text
                    andasearchfor lorem ipsum will uncover many
                    web sites versions have over the years</p>

                <a href="#" class="button one">Get Started Now!</a>

            </div><!-- end section -->

            <div class="two_third last">

                <div class="one_third animate" data-anim-type="fadeIn" data-anim-delay="300">

                    <i class="fa fa-bullhorn"></i>
                    <h4>30 Diffrent Websites</h4>
                    <p class="smtfont">Discover a undoubtable sectionsefined and manymore websites always.</p>

                </div><!-- end section -->

                <div class="one_third animate" data-anim-type="fadeIn" data-anim-delay="400">

                    <i class="fa fa-lightbulb-o"></i>
                    <h4>Tons of Features</h4>
                    <p class="smtfont">Discover a undoubtable sectionsefined and manymore websites always.</p>

                </div><!-- end section -->

                <div class="one_third last animate" data-anim-type="fadeIn" data-anim-delay="500">

                    <i class="fa fa-star-o"></i>
                    <h4>FREE Updates</h4>
                    <p class="smtfont">Discover a undoubtable sectionsefined and manymore websites always.</p>

                </div><!-- end section -->

            </div>

        </div>
    </div><!-- end featured section 1 -->


    <div class="clearfix divider_line margin_top10 margin_bottom10"></div>


    <div class="featured_section2">
        <div class="container">

            <div class="one_fourth_less animate" data-anim-type="fadeIn" data-anim-delay="300">

                <img src="http://placehold.it/275x220" alt="" class="rimg"/> <h4>Responsive</h4>
                <p>Many desktop package web page editors in use the model search many web sites.</p>
                <a href="#" class="button two">Read More</a>

            </div><!-- end section -->

            <div class="one_fourth_less animate" data-anim-type="fadeIn" data-anim-delay="400">

                <img src="http://placehold.it/275x220" alt="" class="rimg"/> <h4>Modern Design</h4>
                <p>Many desktop package web page editors in use the model search many web sites.</p>
                <a href="#" class="button two">Read More</a>

            </div><!-- end section -->

            <div class="one_fourth_less active animate" data-anim-type="fadeIn" data-anim-delay="500">

                <img src="http://placehold.it/275x220" alt="" class="rimg"/> <h4>Diffrent Themes</h4>
                <p>Many desktop package web page editors in use the model search many web sites.</p>
                <a href="#" class="button two">Read More</a>

            </div><!-- end section -->

            <div class="one_fourth_less last animate" data-anim-type="fadeIn" data-anim-delay="600">

                <img src="http://placehold.it/275x220" alt="" class="rimg"/> <h4>Easy to Use</h4>
                <p>Many desktop package web page editors in use the model search many web sites.</p>
                <a href="#" class="button two">Read More</a>

            </div><!-- end section -->

        </div>
    </div><!-- end featured section 2 -->


    <div class="clearfix margin_top12"></div>


    <div class="featured_section3">
        <div class="container">

            <div class="onecol_forty">
                <img src="/assets/blueline/pages/images/site-img5.png" alt="" class="one animate"
                     data-anim-type="fadeInLeft" data-anim-delay="200"/>
                <img src="/assets/blueline/pages/images/site-img5-2.png" alt="" class="two animate"
                     data-anim-type="fadeInRight" data-anim-delay="200"/>
            </div>

            <div class="onecol_sixty last animate" data-anim-type="fadeIn" data-anim-delay="500">
    	<span><em>Choose from Different Layouts and Templates</em>
		<b>Start Creating a Whole Great</b>
		<strong>New Layout.</strong></span>
                <p class="bigtfont">There are many variations of passages of Lorem Ipsum available, but the majority
                    have suffered alteration in some form, by injected humour randomised words enerators onthe Internet
                    tend to repea predefined chunks as necessary making words combine with a handful of model sentence
                    structures, to generate Lorem Ipsum which looks reasonable generated Lorem Ipsum is therefore
                    always.</p>
                <br/><br/><br/>
                <a href="#" class="button three">Get Started Now!</a>

            </div>

        </div>
    </div><!-- end featured section 3 -->


    <div class="clearfix margin_top12"></div>


    <div class="featured_section4">
        <div class="container">

            <div class="tabs detached hide-title cross-fade animate" data-anim-type="fadeIn" data-anim-delay="100">

                <section>
                    <h1><span aria-hidden="true" class="icon-paper-plane"></span></h1>

                    <h3 class="color1">Hope you Enjoy it.</h3>

                    <p class="less1">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in
                        a piece of classical Latin literature from of the more obscure Latin words, consectetur, from a
                        Lorem Ipsum passage, and going through the cites of the word in classical literature discovered
                        the undoubtable source. Lorem Ipsum comes from sections.</p>


                </section><!-- end section -->

                <section id="nested-instance">
                    <h1><span aria-hidden="true" class="icon-badge"></span></h1>

                    <h3 class="color1">30 Diffrent Demos</h3>

                    <p class="less1">Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still in their
                        infancy. Various versions have evolved over the years.</p>


                </section><!-- end section -->

                <section>
                    <h1><span aria-hidden="true" class="icon-umbrella"></span></h1>

                    <h3 class="color1">Onepage. Left &amp Right Menus</h3>

                    <p class="less1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type and scrambled it to make a type specimen book. It has survived not only
                        five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                        passages, and more recently with desktop publishing software like Aldus PageMaker including
                        versions of Lorem Ipsum.</p>

                </section><!-- end section -->

                <section>
                    <h1><span aria-hidden="true" class="icon-cup"></span></h1>

                    <h3 class="color1">Tons of Features</h3>

                    <p class="less1">There are many variations of passages of Lorem Ipsum available, but the majority
                        have suffered alteration in some form, by injected humour, or randomised words which don't look
                        even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure
                        there isn't anything embarrassing hidden in the middle of text.</p>

                </section><!-- end section -->

            </div>

        </div>
    </div><!-- end featured section 4 -->

    <div class="clearfix"></div>


    <div class="featured_section5">
        <div class="container">

            <div class="counters5 animate" data-anim-type="fadeIn" data-anim-delay="300">

                <div class="one_fourth"><h4>Clients</h4> <span id="target21">0</span>
                    <div class="hsmline"></div>
                </div><!-- end section -->

                <div class="one_fourth"><h4>Projects</h4> <span id="target22">0</span>
                    <div class="hsmline"></div>
                </div><!-- end section -->

                <div class="one_fourth"><h4>Awards</h4> <span id="target23">0</span>
                    <div class="hsmline"></div>
                </div><!-- end section -->

                <div class="one_fourth last"><h4>Likes</h4> <span id="target24">0</span>
                    <div class="hsmline"></div>
                </div><!-- end section -->

            </div>

        </div>
    </div><!-- end featured section 5 -->


    <div class="clearfix"></div>


    <div class="featured_section6">
        <div class="container">

            <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="100">

                <i class="fa fa-desktop"></i>
                <h4>Pricing Tables</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="200">

                <i class="fa fa-line-chart"></i>
                <h4>New Sections</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="300">

                <i class="fa fa-picture-o"></i>
                <h4>More Websites</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth last animate" data-anim-type="fadeInUp" data-anim-delay="400">

                <i class="fa fa-magic"></i>
                <h4>Build your Own</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="clearfix"></div>

            <div class="one_fourth btm animate" data-anim-type="fadeInUp" data-anim-delay="500">

                <i class="fa fa-diamond"></i>
                <h4>Flexibility</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth btm animate" data-anim-type="fadeInUp" data-anim-delay="600">

                <i class="fa fa-youtube-play"></i>
                <h4>Video Sections</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth btm animate" data-anim-type="fadeInUp" data-anim-delay="700">

                <i class="fa fa-crop"></i>
                <h4>PSD Files</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="one_fourth btm last animate" data-anim-type="fadeInUp" data-anim-delay="800">

                <i class="fa fa-comments-o"></i>
                <h4>Cross Check</h4>
                <p>Many desktop publish packages web page editors now use.</p>

            </div><!-- end section -->

            <div class="cdarrow animate" data-anim-type="fadeInDown" data-anim-delay="500"></div>

        </div>
    </div><!-- end featured section 6 -->

    <div class="fltiphone animate" data-anim-type="fadeInRight" data-anim-delay="500"></div>


    <div class="clearfix margin_top12"></div>


    <div class="featured_section7">
        <div class="container">

            <h2 class="title21">Meet our Team <em>Our industry experts help scale grow and succeed.</em></h2>

            <div class="one_half left animate" data-anim-type="fadeInLeft" data-anim-delay="100">

                <img src="http://placehold.it/245x300" alt=""/>

                <br/>
                <h4>John Casalena</h4>
                <p class="smtfont">Founder &amp; CEO</p>
                <div class="hsmline2"></div>
                <br/><br/>
                <p>Lorem Ipsum as their default model texta anvarious versions have over the years</p>
                <br/>

                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>

                <br/><br/>
                <a href="#" class="button four">Read More</a>

            </div><!-- end section -->

            <div class="one_half right animate" data-anim-type="fadeInRight" data-anim-delay="100">

                <img src="http://placehold.it/245x300" alt=""/>

                <br/>
                <h4>Andrew Buttar</h4>
                <p class="smtfont">Vice President</p>
                <div class="hsmline2 two"></div>
                <br/><br/>
                <p>Lorem Ipsum as their default model texta anvarious versions have over the years</p>
                <br/>

                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>

                <br/><br/>
                <a href="#" class="button four">Read More</a>

            </div><!-- end section -->

            <div class="one_half left animate" data-anim-type="fadeInLeft" data-anim-delay="100">

                <img src="http://placehold.it/245x300" alt=""/>

                <br/>
                <h4>Alan Frenkel</h4>
                <p class="smtfont">Creative Officer</p>
                <div class="hsmline2"></div>
                <br/><br/>
                <p>Lorem Ipsum as their default model texta anvarious versions have over the years</p>
                <br/>

                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>

                <br/><br/>
                <a href="#" class="button four">Read More</a>

            </div><!-- end section -->

            <div class="one_half right animate" data-anim-type="fadeInRight" data-anim-delay="100">

                <img src="http://placehold.it/245x300" alt=""/>

                <br/>
                <h4>Keely Ancrile</h4>
                <p class="smtfont">Lead Support</p>
                <div class="hsmline2 two"></div>
                <br/><br/>
                <p>Lorem Ipsum as their default model texta anvarious versions have over the years</p>
                <br/>

                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>

                <br/><br/>
                <a href="#" class="button four">Read More</a>

            </div><!-- end section -->

        </div>
    </div><!-- end featured section 7 -->


    <div class="clearfix margin_top12"></div>


    <div class="featured_section8 animate" data-anim-type="fadeIn" data-anim-delay="200">

        <div class="puhtext">

            <b>Everything you need to build an</b>
            <strong>Awesome Website! on TF.</strong>

            <p>All the Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the
                first true generator on the you need to be sure there isn't anything embarrassing hidden in the middle
                alteration in some form Internet.</p>

            <div class="hsmline3 margin_top4 animate" data-anim-type="fadeInRight" data-anim-delay="500"></div>

        </div>

    </div><!-- end featured section 8 -->


    <div class="clearfix"></div>


    <div class="featured_section9">
        <div class="container">

            <div class="left animate" data-anim-type="fadeIn" data-anim-delay="300">

                <i class="fa fa-lightbulb-o"></i>
                <h2>Multiple Designed</h2>
                <h1>Powerful Themes</h1>

                <a href="#"><i class="fa fa-caret-right"></i> Read More</a>

            </div><!-- end section -->

            <div class="center animate" data-anim-type="fadeIn" data-anim-delay="400">

                <i class="fa fa-eye"></i>
                <h2>Smooth &amp; Reliable</h2>
                <h1>Parallax Effects</h1>

                <a href="#"><i class="fa fa-caret-right"></i> Read More</a>

            </div><!-- end section -->

            <div class="right animate" data-anim-type="fadeIn" data-anim-delay="500">

                <i class="fa fa-mobile"></i>
                <h2>Looks Great on</h2>
                <h1>Mobile Devices</h1>

                <a href="#"><i class="fa fa-caret-right"></i> Read More</a>

            </div><!-- end section -->

        </div>
    </div><!-- end featured section 9 -->


    <div class="clearfix"></div>


    <div class="featured_section10">
        <div class="container">

            <h2 class="title21">Best Packages <em>Readable content of a page when looking at its layout.</em></h2>

            <div class="one_fourth animate" data-anim-type="fadeIn" data-anim-delay="300">
                <div class="pricingtable9">

                    <h3>STANDARD</h3>
                    <strong>$4</strong>
                    <b>/month</b>

                    <br/><br/>
                    <a href="#" class="button five">BUY NOW!</a>

                    <span>

                <i class="fa fa-check"></i> Curabitur fringilla tincidun<br/>
                <i class="fa fa-check"></i> Sed lect pharetra oncus<br/>
                <i class="fa fa-check"></i> Integer erat facilisis porta<br/>
                <i class="fa fa-check"></i> Maecenas vestibum

            </span>

                </div>
            </div><!-- end section -->

            <div class="one_fourth animate" data-anim-type="fadeIn" data-anim-delay="400">
                <div class="pricingtable9">

                    <h3>PERSONAL</h3>
                    <strong>$8</strong>
                    <b>/month</b>

                    <br/><br/>
                    <a href="#" class="button five">BUY NOW!</a>

                    <span>

                <i class="fa fa-check"></i> Curabitur fringilla tincidun<br/>
                <i class="fa fa-check"></i> Sed lect pharetra oncus<br/>
                <i class="fa fa-check"></i> Integer erat facilisis porta<br/>
                <i class="fa fa-check"></i> Maecenas vestibum

            </span>

                </div>
            </div><!-- end section -->

            <div class="one_fourth animate" data-anim-type="fadeIn" data-anim-delay="500">
                <div class="pricingtable9">

                    <h3>BUSINESS</h3>
                    <strong>$27</strong>
                    <b>/month</b>

                    <br/><br/>
                    <a href="#" class="button five">BUY NOW!</a>

                    <span>

                <i class="fa fa-check"></i> Curabitur fringilla tincidun<br/>
                <i class="fa fa-check"></i> Sed lect pharetra oncus<br/>
                <i class="fa fa-check"></i> Integer erat facilisis porta<br/>
                <i class="fa fa-check"></i> Maecenas vestibum

            </span>

                </div>
            </div><!-- end section -->

            <div class="one_fourth last animate" data-anim-type="fadeIn" data-anim-delay="600">
                <div class="pricingtable9">

                    <h3>UNLIMITED</h3>
                    <strong>$45</strong>
                    <b>/month</b>

                    <br/><br/>
                    <a href="#" class="button five">BUY NOW!</a>

                    <span>

                <i class="fa fa-check"></i> Curabitur fringilla tincidun<br/>
                <i class="fa fa-check"></i> Sed lect pharetra oncus<br/>
                <i class="fa fa-check"></i> Integer erat facilisis porta<br/>
                <i class="fa fa-check"></i> Maecenas vestibum

            </span>

                </div>
            </div><!-- end section -->

        </div>
    </div><!-- end featured section 10 -->


    <div class="clearfix margin_top12"></div>


    <div class="featured_section11 animate" data-anim-type="fadeIn" data-anim-delay="200">
        <div class="container">

            <h2 class="title21">Latest from Blog <em>Readable content of a page when looking at its layout.</em></h2>

            <div class="one_third">

                <img src="http://placehold.it/359x220" alt="" class="rimg"/>

                <span class="bdate"><a href="#">Jan <strong>27</strong> 2015</a></span>

                <h4>Contrary popular simply piece</h4>
                <p>Default model and search as for ipsum will many web sites still in their infancy versions have the
                    over the years.</p>

                <a href="#" class="button six">Read More</a>

            </div><!-- end section -->

            <div class="one_third">

                <img src="http://placehold.it/359x220" alt="" class="rimg"/>

                <span class="bdate"><a href="#">Jan <strong>28</strong> 2015</a></span>

                <h4>Publish packages web page</h4>
                <p>Default model and search as for ipsum will many web sites still in their infancy versions have the
                    over the years.</p>

                <a href="#" class="button six">Read More</a>

            </div><!-- end section -->

            <div class="one_third last">

                <img src="http://placehold.it/359x220" alt="" class="rimg"/>

                <span class="bdate"><a href="#">Jan <strong>29</strong> 2015</a></span>

                <h4>Have evolved over the years</h4>
                <p>Default model and search as for ipsum will many web sites still in their infancy versions have the
                    over the years.</p>

                <a href="#" class="button six">Read More</a>

            </div><!-- end section -->

        </div>
    </div><!-- end featured section 11 -->


    <div class="clearfix margin_top12"></div>


    <div class="featured_section12 animate" data-anim-type="fadeIn" data-anim-delay="200">
        <div class="container">

            <h2 class="title21">What our People Say</h2>

            <div id="owl-demo12" class="owl-carousel">

                <div class="peosays">

                    <div class="one_half">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Michile Johnson</strong> manager - websitename.com

                        </div>
                    </div><!-- end section -->

                    <div class="one_half last">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Katie Abbigail</strong> team - websitename.com

                        </div>
                    </div><!-- end section -->

                </div><!-- end a slide section -->

                <div class="peosays">

                    <div class="one_half">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Devon Joaquin</strong> manager - websitename.com

                        </div>
                    </div><!-- end section -->

                    <div class="one_half last">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Darby Kaitlynn</strong> team - websitename.com

                        </div>
                    </div><!-- end section -->

                </div><!-- end a slide section -->

                <div class="peosays">

                    <div class="one_half">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Chaim Hayden</strong> manager - websitename.com

                        </div>
                    </div><!-- end section -->

                    <div class="one_half last">
                        <div class="box">" Many desktop publishing packages and web page editors now use Lorem Ipsum as
                            their default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Various versions have evolved over the years.
                        </div>

                        <div class="who">

                            <img src="http://placehold.it/44x44" alt=""/>
                            <strong>Brooke Marisa</strong> team - websitename.com

                        </div>
                    </div><!-- end section -->

                </div><!-- end a slide section -->

            </div>

        </div>
    </div><!-- end featured section 12 -->


    <div class="clearfix"></div>


    <div class="client_logos animate" data-anim-type="fadeIn" data-anim-delay="100">
        <div class="container">

            <img src="http://placehold.it/180x50" alt=""/>
            <img src="http://placehold.it/180x50" alt=""/>
            <img src="http://placehold.it/180x50" alt=""/>
            <img src="http://placehold.it/180x50" alt=""/>
            <img src="http://placehold.it/180x50" alt=""/>
            <img src="http://placehold.it/180x50" alt=""/>

        </div>
    </div><!-- end client logos -->


    <div class="clearfix"></div>


    <footer class="footer7">
        <div class="container">

            <div class="one_third">

                <h4 class="roboto caps">About Linstar</h4>

                <p>Letraset sheets containing loremsum passages andmore recently with desktop publish likes as softwares
                    like pagep including versions.</p>

                <br>

                <ul>
                    <li><i class="fa fa-map-marker fa-lg"></i> 2901 Marmora Road, Glassgow,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seattle,
                        WA 98122-1090
                    </li>
                    <li><i class="fa fa-phone"></i> 1 -234 -456 -7890</li>
                    <li><i class="fa fa-envelope"></i> <a href="mailto:info@yourdomain.com">info@yourdomain.com</a></li>
                </ul>

            </div><!-- end section -->

            <div class="one_third">

                <h4 class="roboto caps">Best Features</h4>

                <div class="one_half">
                    <ul>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Home Variations</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Awsome Sliders</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Tons of Features</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Different Pages</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Single &amp; Portfolios</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Recent Blogs News</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Layered PSD Files</a></li>
                    </ul>
                </div>

                <div class="one_half last">
                    <ul>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Home Variations</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Awsome Sliders</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Tons of Features</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Different Pages</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Single &amp; Portfolios</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Recent Blogs News</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i> Layered PSD Files</a></li>
                    </ul>
                </div>

            </div><!-- end section -->

            <div class="one_third last">

                <h4 class="roboto caps">Keep in Tough</h4>

                <p>Sign up for our emails and get Exclusive offers! The latest news! Inspiration &amp; styling tips!</p>

                <br>

                <div class="newsletter3">
                    <form method="get" action="index.html">
                        <input class="enter_email_input" name="samplees" id="samplees" value="Enter your email..."
                               onfocus="if(this.value == 'Enter your email...') {this.value = '';}"
                               onblur="if (this.value == '') {this.value = 'Enter your email...';}" type="text">
                        <input name="" value="Sign up" class="input_submit" type="submit">
                    </form>
                </div>

                <div class="clearfix margin_top2"></div>

                <ul class="footer_social_links two">
                    <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i
                                    class="fa fa-facebook"></i></a></li>
                    <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i
                                    class="fa fa-twitter"></i></a></li>
                    <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i
                                    class="fa fa-google-plus"></i></a></li>
                    <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i
                                    class="fa fa-linkedin"></i></a></li>
                    <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i
                                    class="fa fa-rss"></i></a></li>
                </ul>


            </div><!-- end section -->

        </div><!-- end footer -->

    </footer>

    <div class="copyright_info5">
        <div class="container">

            <p>Copyright © 2015 LinStar.com. All rights reserved.</p>  <span><a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a></span>

        </div>
    </div>

    <a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


</div>
<script>
    $('.PageJob-majorCategory-button').on('click',function(e) {
        categoryId = $(this).attr('id');
        if($('.'+categoryId).attr('style') == 'display:block;width:100%') {
            $(this).find('h2').attr('style', 'color:black')
            $('.'+categoryId).attr('style', 'display:none;text-decoration: underline;')
        }else {
            $(this).find('h2').attr('style', 'color:#00d647;text-decoration: underline;')    
            $('.'+categoryId).attr('style', 'display:block;width:100%')
        }
        
    })
        $('.PageJob-category-link').on('click', function(e) {
            sessionStorage.setItem("sub_category", $(this).attr('title'));
            $('.modal-signup-container').attr('style', 'display:flex')
        })
        $('.close-button').on('click', function () {
        $('.modal-signup-container').attr('style', 'display:none')
    })
    $('#getstarted').on('click', function() {
      
            var emailClient = $('#emailClient').val() 
            if(emailClient.length > 2 ) {
                window.location.href = 'http://www.workye.com/pricings?email='+emailClient
            }
        })
</script>