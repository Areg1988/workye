$(document).ready(function () {
    $('#hamburger-menu').on('click', function () {
        if ($('#hamburger-menu').hasClass('open')) {
            $('#hamburger-menu').attr('class', 'jsx-1699065342 hamburger-menu')
            $('#side-menu-mobile').attr('class', 'jsx-1410744371 side-menu-mobile')
        } else {
            $('#hamburger-menu').attr('class', 'jsx-1699065342 hamburger-menu  open ')
            $('#side-menu-mobile').attr('class', 'jsx-1410744371 side-menu-mobile  open ')
        }
    })
})

