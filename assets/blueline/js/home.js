
    $(window).resize(function (e) {
        console.log(window.innerWidth)
        if (window.innerWidth < 768) {
            $('.slick-track').slick('unslick');
            $('.switching-words-component').slick('unslick');
            $('.slick-track').slick('unslick');
            $('.slick-list').children().attr('class', 'items-without-carousel');
            $('.items-without-carousel').children().attr('class', ' board-carousel-item-component board-carousel-item');


        }
    });
  $('.slick-track').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,

        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,

        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.switching-words-component').slick({

        arrows: false,
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000
    });


    $(".testing1").css("opacity", '0.2');
    $(".quote-component").css("opacity", '0');
    $(".slick-track").on("afterChange", function (event) {
        $(".testing1").css("opacity", '0.2');
        $(".slick-center").css("opacity", '1');
        $(".slick-center").find('.board-carousel-item').find('.quote-component').css("opacity", '1');

    });
    $(".slick-track").on("beforeChange", function (event) {
        $(".quote-component").css("opacity", '0');

    });

