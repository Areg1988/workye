$(document).ready(function () {
    var url = window.location.href

    var token = csfrData['fcs_csrf_token'];
    var pathname = new URL(url).pathname;
    var urlPathname = pathname.split('/');
    var clientId = urlPathname[2];
    var countPlan = sessionStorage.getItem("countPlan");
    var valuePlan = sessionStorage.getItem("valuePlan");
    $("#tokenDiv").val(token)
    $('#valueSum').val(valuePlan)
    $('#clientId').val(clientId)
    $('#subscription_id').val(countPlan)
    var myVar = setInterval(function () {
        check()
    }, 3000);

    function check() {
        $.ajax({
            url: "/welcome/checkActivaton",
            type: "POST",
            data: {id: clientId, fcs_csrf_token: token},
            success: function (data) {
                var activite = JSON.parse(data)
                if (activite.data == '1') {
                } else {
                    clearInterval(myVar);
                    $('#continue').removeAttr('disabled')
                    $('#resendEmail').css('display', 'none')
                    $('#activated').css('display', 'block')
                }
            }
        });
    }
})

function category() {
var category = $('#category').val()
    var token = csfrData['fcs_csrf_token'];
    $.ajax({
        url: "/welcome/getCategory/",
        type: "POST",
        data: {fcs_csrf_token: token, category: category},
        success: function (data) {
            $('#subCategory').empty()
            var subCategory = JSON.parse(data)
            var data2 = subCategory.data
            console.log(data2);
            for (var i = 0; i < data2.length; i++) {
                $('#subCategory').append('<option value="' + data2[i]['id'] + '">' + data2[i]['name'] + '</option>')
            }

        }
    });
}
$('#continue').on('click', function () {
    $('#stepOne').css('display', 'none')
    $('#stepTwo').css('display', 'Block')
    var subCategory = sessionStorage.getItem("sub_category");
    var valuePlan = sessionStorage.getItem("valuePlan");
    var countPlan = sessionStorage.getItem("countPlan");
    var token = csfrData['fcs_csrf_token'];
    var data3;
    if(subCategory) {
        $.ajax({
            url: "/welcome/getCategoryId/",
            type: "POST",
            data: {fcs_csrf_token: token, category: subCategory},
            success: function (data) {
                var data2 = JSON.parse(data)
                data3 = data2.data
                $("#category option").each(function(){
                    if($(this).val() == data3['category_id']) {
                        $(this).attr('selected' ,'selected')
                        category()

                    }
                });

            }
        });
    }


    
  
           
    $('#employes').html(countPlan)
    $('#employesValue').html(valuePlan)
})


 function continuePrice2() {
    var url = window.location.href
    var pathname = new URL(url).pathname;
    var urlPathname = pathname.split('/');
    var clientId = urlPathname[2];
    var payType = sessionStorage.getItem("payType");
    if (payType == 'free') {
        $('#continuePrice2').removeAttr('data-target')
        $('#continuePrice2').removeAttr('data-toggle')
        window.location.href = '/welcome/activationFree?id='+clientId;
    }
    $('#payCategory').val($('#category').val())
    $('#paySubCategory').val($('#subCategory').val())
     $('#categoryDiv').attr('style', 'display:none')
     $('#paymantDiv').removeAttr('style')
}

$('#category').on('change', function () {
    var category = $('#category').val()
    var token = csfrData['fcs_csrf_token'];
    $.ajax({
        url: "/welcome/getCategory/",
        type: "POST",
        data: {fcs_csrf_token: token, category: category},
        success: function (data) {
            $('#subCategory').empty()
            var subCategory = JSON.parse(data)
            var data2 = subCategory.data
            for (var i = 0; i < data2.length; i++) {
                $('#subCategory').append('<option value="' + data2[i]['id'] + '">' + data2[i]['name'] + '</option>')
            }
           
        }
    });

})
$(document).ready(function () {

    $("#payment-form").submit(function (event) {
        $('#submitBtn').attr('disabled', 'disabled');
        return false;
    });
    $("#payment-form").change(function () {
        $('#submitBtn').removeAttr("disabled");
    });

});

$('#annually').on('click', function () {
    sessionStorage.setItem("payType", 'annually');
    $('#montlyaccount').attr( 'style', 'color: #d43f3a;font-weight: bolder')
    $('#annualyArrow').removeAttr('style')
    $('#categoryDiv').removeAttr('style')
    $('#monthlyArrow').attr( 'style', 'display:none')
    $('#freeArrow').attr( 'style', 'display:none')
    $('#freeAccount').attr( 'style', 'color:rgb(0, 214, 71);font-weight: bolder')
    $('#annualyArrow').attr( 'style', 'color:rebeccapurple;')
    $('#annaulyAccount').attr( 'style', 'color:white;font-weight: bolder')
    $('#annually').attr('style', 'padding: 20px 20px;background-color:rebeccapurple; ;border-radius: 10PX;color:white')
    $('#free').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    $('#monthly').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    $('#continuePrice').removeAttr('disabled')
    var valuePlan = sessionStorage.getItem("valuePlan");
    $('#valueSum').val(Number(valuePlan)* 12 )
    $('#paymantType').val('annually')
    continuePrice()
})


$('#monthly').on('click', function () {
    sessionStorage.setItem("payType", 'monthly');
    $('#freeAccount').attr( 'style', 'color: rgb(0, 214, 71);font-weight: bolder')
    $('#annualyArrow').attr( 'style', 'display:none')
    $('#freeArrow').attr( 'style', 'display:none')
    $('#montlyaccount').attr( 'style', 'color: white;font-weight: bolder')
    $('#annaulyAccount').attr( 'style', 'color: rebeccapurple;font-weight: bolder')
    $('#monthly').attr('style', 'padding: 20px 20px;background-color:#d43f3a;border-radius: 10PX;color:white')
    $('#monthlyArrow').attr('style', 'color:#d43f3a;')
    $('#free').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    $('#annually').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    var valuePlan = sessionStorage.getItem("valuePlan");
    $('#valueSum').val(Number(valuePlan))
    $('#paymantType').val('monthly')
    $('#continuePrice').removeAttr('disabled')

    $('#categoryDiv').removeAttr('style')
    continuePrice()
})



$('#free').on('click', function () {
    $('#annualyArrow').attr( 'style', 'display:none')
    sessionStorage.setItem("payType", 'free');
    $('#monthlyArrow').attr( 'style', 'display:none')

    $('#freeAccount').attr( 'style', 'color:white;font-weight: bolder')
    $('#montlyaccount').attr( 'style', 'color:#d43f3a;font-weight: bolder')
    $('#annaulyAccount').attr( 'style', 'color:rebeccapurple;font-weight: bolder')
    $('#free').attr('style', 'padding: 20px 20px;background-color:rgb(0, 214, 71);;border-radius: 10PX;color:white')
    $('#annually').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    $('#monthly').attr('style', 'padding: 20px 20px;background-color: #f8f8f8!important;    border-radius: 10PX;')
    $('#paymantType').val('monthly')
    $('#continuePrice').removeAttr('disabled')
    $('#freeArrow').attr('style', 'color: rgb(0, 214, 71);!important')
    $('#categoryDiv').removeAttr('style')
    continuePrice()
})


function continuePrice() {
    $('#payCategory').val($('#category').val())
    $('#paySubCategory').val($('#subCategory').val())
    var subCategory = sessionStorage.getItem("sub_category");
    $("#subCategory option").each(function(){
        if($(this).val() === subCategory) {
            $(this).attr('selected' ,'selected')
        }
    });
}


